#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCheckBox>
#include <QMainWindow>
#include <QProgressBar>
#include <atomic>
#include <RemoteSetts.h>
#include <QLabel>
#include <QFuture>
#include <QMediaPlayer>
#include <QHBoxLayout>
#include <QSettings>
#include "soundread.h"
#include "udpchannel.h"
#include "json.hpp"
#include "statews.h"
#include "filesoundsource.h"
#include "ago09uart.h"
#include "wordwrapcheck.h"
#include "agobase.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

using namespace nlohmann;

class Scheduller;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onStatusLabel(QString text);
    void onState(AgoBase::State state);
    void onProgress(qint64 total, qint64 val, bool visible);
    void enableCtrls(bool enable);
    void onActivity(qint64 outs);
    void createOutChecks(int outs_cnt, QStringList names);
    void onClips(QStringList clips);
    void onBKSActive(bool active);
    void on_tangenta(bool state);
    void onVolume(int volume);

    void on_btStart_clicked();

    void on_btStop_clicked();

    void on_level(uint16_t lvl);

    void on_actionStart_triggered();

    void on_actionStop_triggered();

    void on_actionSetts_triggered();

    void on_slVolume_valueChanged(int value);

    void on_actionRemoteSetts_triggered();

    void on_btPlay_clicked();

    void on_cbClips_currentTextChanged(const QString &arg1);

    [[ noreturn ]] void on_btClose_clicked();

    void on_actionRecord_triggered();

    void on_actionUpload_triggered();

    void on_actionPlay_triggered();

    void on_actionFile_triggered();

    void onError(QString title, QString msg);
    void onCheckOut(uint64_t outs);
    void onActiveTargets(uint32_t outs, bool active);

private:
    Ui::MainWindow *ui;
    AgoBase* ago = nullptr;
//    QVector<WordWrapCheck*> dstCheck;
//    QProgressBar* volGauge;
    std::atomic_bool file_is_play;
    QLabel* statusLabel;
    QCheckBox* cbBKSOn;
    QCheckBox* cbTangenta;
    QProgressBar* playPorgress;
    QStringList names;
    QMutex volMutex;

//    void getNames();
    uint32_t getOuts();
    void showCursor();
    void playFile(bool comTranslate);
    void initAgo();
    void getClips();
    void createSoundReader();
};

extern QSettings setts;
#endif // MAINWINDOW_H
