#include "mypcm.h"
#include "QString"
#include "QFile"
#include "QDir"
#include "QProcess"
#include "QFileInfo"
#include "QDebug"
#include <QMutex>

MyPCM::MyPCM(QString fileName, int frameDuration, int mono) : QObject(), file_name(fileName), frame_duration(frameDuration), mono(mono)
{
    error = false;
    error_str = "";
    openFile(fileName);
}

MyPCM::~MyPCM()
{
    if(file)
    {
        if(file->isOpen())
            file->close();
        delete file;
    }
}

void MyPCM::openFile(QString fName)
{
    file = new QFile(fName);

    if(!file->exists())
    {
        qDebug() << "Файл не найден " << fName << " \nВозможно исходный файл не является аудиофайлом";
        setError("Файл не найден "+fName+" \nВозможно исходный файл не является аудиофайлом");
        return;
    }

    if(file->open(QIODevice::ReadOnly))
    {
        isRIFF = false;
        audioFormat = channels = sample_rate = 0;

        readChunk(file, 0);

        frame_size = byte_rate*frame_duration/1000;

        qDebug() << "Header " << frame_size;
    }
    else
    {
        qDebug() << "Не возможно открыть файл " << fName;
        setError("Не возможно открыть файл "+fName);
    }
}

bool MyPCM::needResample()
{
    return !isRIFF || format != 'EVAW' || audioFormat != 1 || !checkSampleRate() || (!mono && channels > 1);
}

QMutex ffmpeg_mutex;

//void MyPCM::resample(EqList eq_list)
//{
//    QFileInfo info(file_name);
//    QString tmpFile = QDir::tempPath()+"/"+info.baseName()+".wav";

//    QString sEqualizer = "";
//    bool use_eq = false;
//    if(eq_list.count())
//    {
//        sEqualizer = "-af \"";
//        foreach (EqPoint eq_pnt, eq_list) {
//            if(eq_pnt.gain)
//            {
//                sEqualizer += QString("equalizer=f=%1:g=%2:w=1.5:width_type=q,").arg(eq_pnt.freq).arg(eq_pnt.gain);
//                use_eq = true;
//            }
//        }
//        sEqualizer = use_eq ? sEqualizer.left(sEqualizer.length()-1)+"\"" : "";
//    }

//    QProcess process;
//    QString cmd = (mono ?
//                QString("ffmpeg.exe -i \"%1\" -f wav -acodec pcm_s16le -ar 48k -ac 1 %2 -y \"%3\"") :
//                QString("ffmpeg.exe -i \"%1\" -f wav -acodec pcm_s16le -ar 48k %2 -y \"%3\"")).arg(file_name).arg(sEqualizer).arg(tmpFile);
//    process.start(cmd);
//    process.waitForFinished();
//    QString out(process.readAllStandardOutput());
//    out += "\r\n"+QString(process.readAllStandardError());

//    if(file)
//        file->close();

//    openFile(tmpFile);

//    if(out.contains("Please reduce gain"))
//    {
//        setError(tr("Снизьте уровни эквалайзера"), false);
//    }
//}

void MyPCM::deleteTemp()
{
    QFileInfo info(file_name);
    QString tmpFile = QDir::tempPath()+"/"+info.fileName();
    QFile file(tmpFile);
    if(file.exists())
        file.remove();
}

void MyPCM::copyTemp(QString newFileName)
{
    file->close();
    file->copy(newFileName);
}

bool MyPCM::readChunk(QFile* file, int level)
{

    qint32 chunkId;
    qint32 chunkSize;

    file->read((char*)&chunkId, sizeof(chunkId));
    file->read((char*)&chunkSize, sizeof(chunkSize));

    switch(chunkId)
    {
    case 'FFIR':
    {
        isRIFF = true;
        file->read((char*)&format, sizeof(format));

        while(readChunk(file, level+1));
        return false;
    }

    case ' tmf':
    {
        file->read((char*)&audioFormat, sizeof(audioFormat));
//        if(audioFormat != 1)
//        {
//            setError("Формат файла не поддерживается ("+QString::number(audioFormat)+"), ожидается PCM");
//            return false;
//        }
        file->read((char*)&channels, sizeof(channels));
        file->read((char*)&sample_rate, sizeof(sample_rate));
//        if(!checkSampleRate(sample_rate))
//        {
//            setError("Не поддерживаемый samplerate "+QString::number(sample_rate));
//        }
        file->read((char*)&byte_rate, sizeof(byte_rate));
        file->read((char*)&blockAlign, sizeof(blockAlign));
        file->read((char*)&bitsPerSample, sizeof(bitsPerSample));

        for(int i=0;i<(chunkSize-16); i++)
        {
            char c;
            file->read(&c, 1);
        }
        return true;;
    }

    case 'atad':
    {
        file_size = chunkSize;
        file_pos = 0;
        return false;
    }

    default:
        if(level == 0)
        {
            setError("Формат файла не поддерживается");
            qDebug() << "Формат файла не поддерживается";
            return false;
        }
        else
        {
            for(int i=0;i<chunkSize; i++)
            {
                char c;
                file->read(&c, 1);
            }
            return true;
        }
    }
}

bool MyPCM::checkSampleRate()
{
    int supportSR[] = {8000, 12000, 16000, 11025, 22050, 24000, 32000, 48000};//, 44100
    for(int i=0; i<8; i++)
    {
        if(supportSR[i] == sample_rate)
            return true;
    }
    return false;
}

void MyPCM::setError(QString err, bool fatal)
{
    if(fatal)
        error = true;
    if(error_str != "")
        error_str += ", ";
    error_str += err;
}

int MyPCM::read(char* buf, int sz)//, int sr, int ch
{
    int cnt = (file_size-file_pos) >= sz ? sz : (file_size-file_pos);

    int rd = file->read(buf, cnt);
    if(rd < frame_size)
    {
        memset(&buf[rd], 0, frame_size-rd);
    }
    file_pos += rd;

    return rd;
}

bool MyPCM::eof()
{
    return file_pos >= file_size;//
}
