#include "keyboard.h"
#include "ui_keyboard.h"

#include <QToolButton>
#include <QDesktopWidget>
#include <QGuiApplication>
#include <QScreen>
#include <QInputDialog>
#include "qsettings.h"

extern QSettings setts;

const int rows = 5;
row_t keys_e[] = {
    {
        14,"`1234567890-=\b"
    },
    {
        14,"\tqwertyuiop[]\\"
    },
    {
        13,"\vasdfghjkl;'\n"
    },
    {
        12,"\azxcvbnm,.?\a"
    },
    {
        5,"\u0003\u0001 \u0002\f"
    },
};
row_t keys_e_shift[] = {
    {
        14,"~!@#$%^&*()_+\b"
    },
    {
        14,"\tQWERTYUIOP{}|"
    },
    {
        13,"\vASDFGHJKL:\"\n"
    },
    {
        12,"\aZXCVBNM,.?\a"
    },
    {
        5,"\u0003\u0001 \u0002\f"
    },
};
row_t keys_r[] = {
    {
        14,"`1234567890-=\b"
    },
    {
        14,"\tйцукенгшщзхъ\\"
    },
    {
        13,"\vфывапролджэ\n"
    },
    {
        12,"\aячсмитьбю.\a"
    },
    {
        5,"\u0003\u0001 \u0002\f"
    },
};
row_t keys_r_shift[] = {
    {
        14,"~!@#$%^&*()_+\b"
    },
    {
        14,"\tЙЦУКЕНГШЩЗХЪ/"
    },
    {
        13,"\vФЫВАПРОЛДЖЭ\n"
    },
    {
        12,"\aЯЧСМИТЬБЮ,\a"
    },
    {
        5,"\u0003\u0001 \u0002\f"
    },
};

row_t keys_num[] = {
    {
        3, "789"
    },
    {
        3, "456"
    },
    {
        3, "123"
    },
    {
        3, "-0."
    },
    {
        3, "\b\x03\f"
    }
};


Keyboard::Keyboard(QString title, QString text, QWidget *parent, kb_type_t type) :
    QDialog(parent), ui(new Ui::Keyboard), type(type)
{
    ui->setupUi(this);

    resultText = text;
    setWindowTitle(title);
    if(type==KB_PASS){
        passText = text;
        text.fill(QChar('*'), text.length());
    }
    ui->text->setPlainText(text);
    ui->text->moveCursor(QTextCursor::MoveOperation::End);

    build();

}

Keyboard::~Keyboard()
{
    delete ui;
}

void Keyboard::showEvent(QShowEvent *event)
{
    (void)event;
    if(QGuiApplication::screens().count()){
        QScreen* screen = QGuiApplication::screens().at(0);
        QRect rect = screen->geometry();
        int x = (rect.width()-width())/2;
        int y = (rect.height()-height()-40);
        move(x,y);
    }
}

bool Keyboard::run(QString &value, QString title, QWidget *parent, kb_type_t type)
{
    if(!setts.value("keyboard").toBool()){
        value = QInputDialog::getText(parent, title, title);
        return !value.isEmpty();
    }

    Keyboard kb(title, value, parent, type);
    if(kb.exec()){
        value = kb.getText();
        return true;
    }else{
        return false;
    }
}

QString Keyboard::getText()
{
    return resultText;
}

void Keyboard::clear(){
    while(ui->verticalLayout->count()>1){
        QHBoxLayout* layout = dynamic_cast<QHBoxLayout*>(ui->verticalLayout->itemAt(ui->verticalLayout->count()-1)->layout());
        if(layout){
            while(layout->count()){
                QWidget* btn = layout->itemAt(layout->count()-1)->widget();
                btn->deleteLater();
                layout->removeWidget(btn);
            }
            layout->deleteLater();
            ui->verticalLayout->removeItem(layout);
        }
    }
}

void Keyboard::build(){
    clear();

    row_t* keys = (shift || caps) ? reinterpret_cast<row_t*>(&keys_e_shift) : reinterpret_cast<row_t*>(&keys_e);
    if(rus){
        keys = (shift || caps) ? reinterpret_cast<row_t*>(&keys_r_shift) : reinterpret_cast<row_t*>(&keys_r);
    }
    if(type == KB_NUMERIC){
        keys = reinterpret_cast<row_t*>(&keys_num);
    }

    for(int i=0; i<rows; i++){
        QHBoxLayout* rowLayout = new QHBoxLayout(this);
        ui->verticalLayout->addLayout(rowLayout);

        for(int j=0; j<keys[i].cnt; j++){
            QToolButton* btn = new QToolButton(this);
            QString txt = QString(keys[i].keys[j]);
            QString ico = "";
            QString style = "";

            switch(keys[i].keys[j].toLatin1()){
            case '\b':
                txt = "Backspace";
                ico = ":/icon/backspace.png";
                style="min-width: 100px;";
                break;
            case '\t':
                txt = "Tab";
                ico = ":/icon/tab.png";
                style="min-width: 100px;";
                break;
            case '\v':
                txt = "CapsLock";
                ico = ":/icon/capslock.png";
                style="min-width: 100px;";
                btn->setCheckable(true);
                btn->setChecked(caps);
                break;
            case '\n':
                txt = "Enter";
                ico = ":/icon/enter.png";
                style="min-width: 100px;";
                break;
            case '\a':
                txt = "Shift";
                ico = ":/icon/shift.png";
                style = "min-width: 160px;";
                btn->setCheckable(true);
                btn->setChecked(shift);
                break;
            case ' ':
                txt = "                          ";
                break;
            case '\x01':
                txt = "En";
                break;
            case '\x02':
                txt = "Ru";
                break;
            case '\f':
                txt = "Ok";
                ico = ":/icon/ok.png";
                break;
            case '\x03':
                txt = "Отмена";
                ico = ":/icon/cancel.png";
                break;
            case '&':
                txt = "&&";
                break;
            }

            btn->setText(txt);
            if(!ico.isEmpty()){
                btn->setIcon(QIcon(ico));
                btn->setIconSize(QSize(64,64));
            }
            if(!style.isEmpty()){
                btn->setStyleSheet(style);
            }

            connect(btn, &QAbstractButton::clicked, [this,btn](bool checked){
                (void)checked;

                QString txt = btn->text();
                if(txt == "Backspace"){
                    ui->text->textCursor().deletePreviousChar();
                    int col = ui->text->textCursor().columnNumber();
                    if(!passText.isEmpty() && col < passText.length()){
                        passText = passText.remove(col,1);
                    }
//                    QString text = ui->text->toPlainText();
//                    auto textCursor = ui->text->textCursor();
//                    int col = textCursor.columnNumber();
//                    if(col > 0){
//                        text.remove(col-1, 1);
//                        ui->text->setPlainText(text);
//                        ui->text->moveCursor(QTextCursor::MoveOperation::End);
//                    }
                }else if(txt == "Tab"){
                    addText("\t");
                }else if(txt == "CapsLock"){
                    caps = btn->isChecked();
                    build();
//                    btn->setChecked(caps);
                }else if(txt == "Shift"){
                    shift = !shift;
                    build();
                }else if(txt == "Enter"){
                    addText("\r\n");
                }else if(txt == "                          "){
                    addText(" ");
                }else if(txt == "En"){
                    rus = false;
                    build();
                }else if(txt == "Ru"){
                    rus = true;
                    build();
                }else if(txt == "Ok"){
                    resultText = type!=KB_PASS ? ui->text->toPlainText() : passText;
                    done(QDialog::Accepted);
                }else if(txt == "Отмена"){
                    done(QDialog::Rejected);
                }else{
                    addText(txt);
                    if(shift){
                        shift = false;
                        build();
                    }
                }
            });

            rowLayout->addWidget(btn);
        }
    }
}

void Keyboard::buildNum(){
    clear();


}

void Keyboard::addText(QString txt){
    if(type==KB_PASS){
        int col = ui->text->textCursor().columnNumber();
        passText = passText.insert(col, txt);
        txt.fill(QChar('*'), txt.length());
    }
    ui->text->textCursor().insertText(txt);
//    QString text = ui->text->toPlainText();
//    text += txt;
//    ui->text->setPlainText(text);
//    ui->text->moveCursor(QTextCursor::MoveOperation::End);
}

void Keyboard::checkShift(bool check){
    shift = check;
    for(int i=0; i<ui->verticalLayout->count(); i++){
        QHBoxLayout* layout = dynamic_cast<QHBoxLayout*>(ui->verticalLayout->itemAt(i)->layout());
        if(layout){
            for(int j=0; j<layout->count(); j++){
                QToolButton* btn = dynamic_cast<QToolButton*>(layout->itemAt(j)->widget());
                if(btn){
                    if(btn->text() == "Shift"){
                        btn->setChecked(check);
                    }
                    if(btn->text().length() == 1){
                        if(check)
                            btn->setText(btn->text().toUpper());
                        else
                            btn->setText(btn->text().toLower());
                    }
                }
            }
        }
    }
}
