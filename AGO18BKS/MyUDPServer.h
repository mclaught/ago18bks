/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyUDPServer.h
 * Author: user
 *
 * Created on 26 ноября 2019 г., 13:51
 */

#ifndef MYUDPSERVER_H
#define MYUDPSERVER_H

#include <thread>
#include <netinet/in.h>
#include "proto.h"
#include <chrono>
#include <mutex>
#include <time.h>
#include <sys/time.h>
#include <set>

#define PIDFILE "/run/ago18bks"
#define MAX_PACKET_SIZE (4096)
#define LISTEN_SOCKS    2

using namespace std;

class MyUDPServer {
public:
    MyUDPServer();
    MyUDPServer(const MyUDPServer&);
    virtual ~MyUDPServer();
    
    bool isStoped = false;
    set<eth_cmd_t> start_requests;
    uint32_t start_req_tm = 0;
    uint32_t start_tm = 0;

    void start();
    void stop();
    void run();
    void sendCommand(uint8_t* data, int len, uint8_t type, uint8_t cmd, int repeats=1, uint8_t addr = 0xFF);
    void sendBuffer(packet_command_t* packet, int len);
    
    static void dumpPacket(pack_hdr_t *pack);
private:
    uint16_t ports[LISTEN_SOCKS];
    string iface;
    int sock[LISTEN_SOCKS];
    mutex sock_mutex[LISTEN_SOCKS];
    string broadcast;
    std::thread main_thread;
    bool active = true;
    uint8_t buf[MAX_PACKET_SIZE];
    
    string waitForIface(string if_name);
    void parse_packet(pack_hdr_t* pack, sockaddr_in from_addr);
    void error(string mes, bool _exit=false);
    void sendStartReq(uint8_t* data, int len, pack_type_t type, eth_cmd_t cmd);
    void sendStartRequests();
    
    friend void main_thread_func(MyUDPServer* __this);
};

uint32_t GetTickCount();
uint64_t GetTickCount_us();

extern MyUDPServer server;

#endif /* MYUDPSERVER_H */


