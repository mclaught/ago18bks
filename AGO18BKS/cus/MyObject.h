/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyObject.h
 * Author: user
 *
 * Created on 19 сентября 2018 г., 19:49
 */

#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <string>
#include <map>
#include <vector>
#include <functional>
#include "messages.h"

using namespace std;

class MyObject {
public:
    typedef function<void (void*)> mes_handler;
    
    MyObject();
    MyObject(const MyObject& orig);
    virtual ~MyObject();

    void join(Message mes, mes_handler handler);
    void sendMessage(Message mes, void* params);
private:
    typedef vector<mes_handler> connect_set;

    map<Message, connect_set> links;

};

#endif /* MYOBJECT_H */

