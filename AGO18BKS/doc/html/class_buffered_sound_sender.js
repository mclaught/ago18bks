var class_buffered_sound_sender =
[
    [ "BufferedSoundSender", "class_buffered_sound_sender.html#a0f132fb1cac1bb35a4831706b9b539a6", null ],
    [ "BufferedSoundSender", "class_buffered_sound_sender.html#aa30e921aac90f9c487cbd2681699694f", null ],
    [ "~BufferedSoundSender", "class_buffered_sound_sender.html#a615032d24fafb04163c3afa24e2eb4f4", null ],
    [ "addOpusPacket", "class_buffered_sound_sender.html#afd8f647fc44c289e31693dd74341fbc9", null ],
    [ "fifo_cnt", "class_buffered_sound_sender.html#a64d38584eae6a17a739f9efa1006f1f0", null ],
    [ "iniOpus", "class_buffered_sound_sender.html#a181a32fc17c034dc5cd3bd761d65e23c", null ],
    [ "onStart", "class_buffered_sound_sender.html#a1311363307ce48f779dd4237d6873607", null ],
    [ "onStop", "class_buffered_sound_sender.html#a717815dccf684085624a248a81ce40fa", null ],
    [ "sendSoundPacket", "class_buffered_sound_sender.html#a0287d3bb24cf8a8fa68c79b5e5fd7c7e", null ],
    [ "sendStartStop", "class_buffered_sound_sender.html#a5fc114ac8c6a2bed9fe0fe419fec66eb", null ],
    [ "start", "class_buffered_sound_sender.html#a2ea7731fde8d1d3d80a8586225e73efb", null ],
    [ "stop", "class_buffered_sound_sender.html#a3da9787e56b40aaa4db14d95306c5550", null ],
    [ "fifo", "class_buffered_sound_sender.html#ae440de57149c790912e77bb71963678f", null ],
    [ "fifo_in", "class_buffered_sound_sender.html#a8b323ad8c11abe218d770448378301d1", null ],
    [ "fifo_mutex", "class_buffered_sound_sender.html#ad5fce2ec2c88fa3c13fe20ab09d4d9c0", null ],
    [ "fifo_out", "class_buffered_sound_sender.html#a16a208cd5e1a7fdd024516ffaf1b8960", null ],
    [ "in_samples", "class_buffered_sound_sender.html#af61370de06e34ab40de91109394c985c", null ],
    [ "last_stat_tm", "class_buffered_sound_sender.html#a8c4f6b5d596d98da9e2aa8225a21e760", null ],
    [ "out_samples", "class_buffered_sound_sender.html#ac8e9d1616292bf6f60d2b6e97682dcc1", null ]
];