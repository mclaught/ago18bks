#ifndef SCHEDULLER_H
#define SCHEDULLER_H

#include <QDateTime>
#include <QDir>
#include <QMap>
#include <QMutex>
#include <QObject>
#include <QSharedPointer>
#include <QTimer>
#include <QVector>
#include "json.hpp"
#include "fileinfo.h"

using namespace nlohmann;

class Scheduller : public QObject
{
    Q_OBJECT
public:
    QVector<PFileInfo> items;
    bool hasBadFiles = false;

    explicit Scheduller(QObject *parent = nullptr);

    void load();
    void save();
    PFileInfo findSignalled(qint64 time);
    void addItem(PFileInfo item);
    void removeItem(int index);
    PFileInfo findFile(QString path);

    void start();
    void stop();

    void update_tasks();

    static qint64 timeFromStr(QString str);
    static QString timeToStr(qint64 time);
    static qint64 currentTime();
    bool removeFile(int index);
private:
    QDir audioDir;
    QDir infoDir;
    QTimer timer;
    QVector<PFileInfo> active_tasks;
    QMutex tasksMutex;

signals:
    void periodic(qint64 time);
    void signaled(PFileInfo item);
    void badFile(QString name);
private slots:
    void onTimer();
};

extern Scheduller scheduller;

#endif // SCHEDULLER_H
