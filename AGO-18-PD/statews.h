#ifndef STATEWS_H
#define STATEWS_H

#include "remotesetts.h"

#include <QObject>
#include <QTimer>
#include <QtWebSockets/QWebSocket>

class StateWS : public QObject
{
    Q_OBJECT
public:
    explicit StateWS(QString ip, int port, QObject *parent = nullptr);

    void close();
    void playClip(QString name, int source, uint32_t outs);
    void stopClip(int source);
    void getSetts();
    void getClips();
private:
//    RemoteSetts *rSt;
    QString _url;
    QWebSocket* ws = nullptr;

private slots:
    void onTextMessageReceived(QString message);
    void onConnected();
    void onDisconnected();
    void onError(QAbstractSocket::SocketError error);

signals:
    void onActivity(uint64_t outs);
    void connected();
    void disconnected();
    void onNames(int outs_cnt, QStringList names);
    void onClips(QStringList clips);
};

#endif // STATEWS_H
