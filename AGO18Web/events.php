<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?1">
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        include "navbar.php";
        ?>
        
        <div class="container">
            <br>
            <h1><img src="img/log.svg" width="64" height="64"><span>&nbsp;&nbsp;&nbsp;</span>Журнал событий</h1>
            <br>
            
            <div class="accordion shadow" id="events">
            </div>
        </div>
        
        <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/alert.js"></script>
        <script type="text/javascript" src="js/alertify.js"></script>
        <script type="text/javascript" src="js/constants.js"></script>
        <script type="text/javascript" src="js/rest.js"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script type="text/javascript" src="js/MyView.js"></script>
        <script type="text/javascript" src="js/StateTable.js"></script>
        <script type="text/javascript" src="js/EventsView.js"></script>
        <script type="text/javascript" src="js/MainPage.js"></script>
        <script>
            
            
            $.get("get_events.php", {cmd: "files"}, function(answer){
                console.log(answer);
                
                var array = JSON.parse(answer);
                
                let html = '';
                
                for(let i=0; i<array.length; i++){
                    let file = array[i];
                    console.info("file", file);
                    
                    html += `<div class="card">`;
                    
                    html += `<div class="card-header" id="heading-${file}">`;
                        html += `<h5 class="mb-0">`;
                        html += `<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-${file}" aria-expanded="false" aria-controls="collapse-${file}">`;
                        html += file;
                        html += '</button>';
                        html += '</h5>';
                        html += '</div>';
                    html += '</div>';
                    
                    html += `<div id="collapse-${file}" file="${file}" class="collapse" aria-labelledby="heading-${file}" data-parent="#events">`;
                    html += `<div class="card-body" id="content-${file}">`;
                    html += `</div>`;
                    html += `</div>`;
                    
                    html += '</div>';
                }
                
                $('#events').html(html);
            
                $('.collapse').on('show.bs.collapse', function(){
                    let file = $(this).attr('file');
                    console.info('collapse', file);
                    
                    $.get('get_events.php', {cmd: 'log', file: file}, function(answer){
                        let lines = answer.split('\n');
                        let items = [];
                        let html = '';
                        
                        for(let i=0; i<lines.length; i++){
                            let line = lines[i];
                            let parts = line.split('\t');
                            
                            if(parts.length >= 4){
                                console.info("parts", parts);
                                let time = parseInt(parts[0])
                                let sTime = (new Date(time)).toLocaleTimeString();//`${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
                                console.info("time", sTime);
                                
                                let type = parseInt(parts[1]);
                                
                                let message = parts[3];
                                
                                let item = {
                                    time: time,
                                    sTime: sTime,
                                    type: type,
                                    message: message
                                };
                                items.push(item);
                            }
                        }
                        
                        items.sort(function(a,b){
                            if(a.time < b.time)
                                return 1;
                            else if(a.time > b.time)
                                return -1;
                            else
                                return 0;
                        });
                        
                        for(let i=0; i<items.length; i++){
                            let item = items[i];
                            let color = 'text-dark';
                            let txt_color = 'text-white';
                            switch(item.type){
                                case 0:
                                    color = 'bg-light';
                                    txt_color = 'text-dark';
                                    break;
                                case 1:
                                    color = 'bg-warning';
                                    txt_color = 'text-dark';
                                    break;
                                case 2:
                                    color = 'bg-danger';
                                    txt_color = 'text-white';
                                    break;
                            }

                            html += `<div class="row">`;
                            html += `<span class="col-2 ${color} ${txt_color}">${item.sTime}</span>`;
                            html += `<span class="col-10 ${color} ${txt_color}">${item.message}</span>`;
                            html += `</div>`;
                        }
                        
                        $('#content-'+file).html(html);
                    });
                });
            });
        </script>
    </body>
</html>