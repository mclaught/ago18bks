/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BaseWriter.h
 * Author: user
 *
 * Created on 27 ноября 2019 г., 2:04
 */

#ifndef BASEWRITER_H
#define BASEWRITER_H

#include <stdint.h>
#include <string>

using namespace std;

class BaseWriter {
public:
    BaseWriter();
    BaseWriter(const BaseWriter& orig);
    virtual ~BaseWriter();

//    virtual void open(string filename, string descr){};
    virtual void write(int16_t* pcm, int samples) = 0;
private:

};

#endif /* BASEWRITER_H */

