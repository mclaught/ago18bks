#include "log.h"
#include "QDateTime"
#include "QStandardPaths"

Log _log;

Log::Log(QObject *parent)
    : QObject{parent}
{
    auto paths = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
    QString path = paths.length() ? paths[0] : "";
    if(!path.isEmpty())
        path = path+"/";

    file = new QFile(path+"PD_log.txt");
    file->open(QIODevice::OpenModeFlag::WriteOnly);// | QIODevice::OpenModeFlag::Append
}

void Log::log(QString str)
{
    if(file && file->isOpen()){
        auto dt = QDateTime::currentDateTime();

        file->write((dt.toString("dd:MM:yyyy hh:mm:ss.zzz")+"\t\t"+str+"\r\n").toLocal8Bit());
        file->flush();
    }
}
