/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SoundSender.h
 * Author: user
 *
 * Created on 6 декабря 2019 г., 0:52
 */

#ifndef SOUNDSENDER_H
#define SOUNDSENDER_H

#include "BaseSoundSender.h"
#include <thread>
#include <opus/opus.h>
#include <mutex>
#include <memory>
#include <set>
#include <atomic>
#include "proto.h"

#define FIFO_FRAMES   1000
#define FIFO_SIZE     (FIFO_FRAMES*AUDIO_SAMPLES)

using namespace std;

class BufferedSoundSender : public BaseSoundSender {
public:
    BufferedSoundSender();
    BufferedSoundSender(const BufferedSoundSender& orig);
    virtual ~BufferedSoundSender();
    
    virtual void start();
    virtual void stop();
    virtual void sendSoundPacket(int cnt);
    
protected:
//    recursive_mutex fifo_mutex;
    int in_samples = 0;
    int out_samples = 0;
    
    opus_int16 fifo[FIFO_SIZE];
    int fifo_in = 0;
    int fifo_out = 0;
    atomic_int fifo_cnt = 0;
    uint32_t last_stat_tm = 0;
    uint8_t last_pack_n = 0;
    
    int get_fifo_cnt();
    
    virtual void onStart()override;
    virtual void onStop()override;
    
    void addOpusPacket(const unsigned char* data, opus_int32 buf_len);
    void iniOpus();
    void sendStartStop(bool start);
    
private:
    OpusDecoder *opus = nullptr;
    bool first = true;
};

#endif /* SOUNDSENDER_H */

