var messages_8h =
[
    [ "SessionsPrms", "struct_sessions_prms.html", "struct_sessions_prms" ],
    [ "TragetPrms", "struct_traget_prms.html", "struct_traget_prms" ],
    [ "Message", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efa", [
      [ "MSG_SERVER_CONNECTED", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efaa85cd779d8874cb5be612388f947efe5a", null ],
      [ "MSG_SERVER_DISCONNECTED", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efaa088d8867488b04eb8db07fcb39709ab6", null ],
      [ "MSG_START_SESSION", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efaa60a3d5524e2250491e7f84c7a3f4625c", null ],
      [ "MSG_FINISH_SESSION", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efaab95f5cc906bb76bed0008cd2c6966ecd", null ],
      [ "MSG_SEARCH_SERVER", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efaae9ff8f3bd297a183ce32b34f1b5203cd", null ],
      [ "MSG_SERVER_FOUND", "messages_8h.html#a4f09127c805cc1f5ee20e67db7b45efaa86b053261806e165dfc0b57700dcdac9", null ]
    ] ]
];