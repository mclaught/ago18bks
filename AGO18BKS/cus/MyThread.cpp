/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.cpp
 * Author: user
 * 
 * Created on 16 января 2017 г., 9:56
 */

#include <sys/syslog.h>

#include "MyThread.h"

MyThread::MyThread() : terminated(true)
{
}

MyThread::MyThread(const MyThread& orig)
{
}

MyThread::~MyThread()
{
    terminate();
}

void MyThread::start()
{
    terminated = false;
    _thrd = thread(_thread_func, this);
}

void MyThread::terminate()
{
    terminated = true;
    if(_thrd.joinable())
        _thrd.join();
}

void MyThread::_thread_func(MyThread* _this)
{
    _this->exec();
}
