include(version.h)

QT       += core gui multimedia network websockets concurrent serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

VERSION = 23.08.07.03
DEFINES += "VERSION=$${VERSION}"

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ago09uart.cpp \
    agobase.cpp \
    basesoundsource.cpp \
    clipsdialog.cpp \
    drivesdialog.cpp \
    fileinfo.cpp \
    filesdialog.cpp \
    filesmodel.cpp \
    filesoundsource.cpp \
    keyboard.cpp \
    log.cpp \
    main.cpp \
    mainwindow.cpp \
    mypcm.cpp \
    remotesetts.cpp \
    scheditemdialog.cpp \
    scheduller.cpp \
    settingsdialog.cpp \
    soundread.cpp \
    statews.cpp \
    targetscheckwidget.cpp \
    targetsdialog.cpp \
    targetseditor.cpp \
    timeeditor.cpp \
    udpchannel.cpp \
    wavsaver.cpp \
    wordwrapcheck.cpp

HEADERS += \
    ago09uart.h \
    agobase.h \
    basesoundsource.h \
    clipsdialog.h \
    drivesdialog.h \
    fileinfo.h \
    filesdialog.h \
    filesmodel.h \
    filesoundsource.h \
    json.hpp \
    keyboard.h \
    log.h \
    mainwindow.h \
    mypcm.h \
    remotesetts.h \
    scheditemdialog.h \
    scheduller.h \
    settingsdialog.h \
    soundread.h \
    statews.h \
    targetscheckwidget.h \
    targetsdialog.h \
    targetseditor.h \
    timeeditor.h \
    udpchannel.h \
    version.h \
    wavsaver.h \
    wordwrapcheck.h

FORMS += \
    clipsdialog.ui \
    drivesdialog.ui \
    filesdialog.ui \
    keyboard.ui \
    mainwindow.ui \
    scheditemdialog.ui \
    settingsdialog.ui \
    targetscheckwidget.ui \
    targetsdialog.ui \
    targetseditor.ui \
    timeeditor.ui \
    wordwrapcheck.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc

#RC_FILE = resources.rc

include(shared.pri)

DISTFILES += \
    resources.rc \
    style_ligth \
    targets.txt

RC_ICONS = icon/mainicon.ico
