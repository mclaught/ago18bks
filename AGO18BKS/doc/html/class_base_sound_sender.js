var class_base_sound_sender =
[
    [ "BaseSoundSender", "class_base_sound_sender.html#a00830721b758753d89fc3686d5452afa", null ],
    [ "BaseSoundSender", "class_base_sound_sender.html#aabc4916f5355415186e0ef38ea439f10", null ],
    [ "~BaseSoundSender", "class_base_sound_sender.html#a788e44e2aaf1e6e8f2db141152dec256", null ],
    [ "cycle", "class_base_sound_sender.html#a751a9d34724fc21bd42178efb5ddea09", null ],
    [ "isBusy", "class_base_sound_sender.html#a5dfec7deb0e01c0ec40cb3ac37bd7933", null ],
    [ "onStart", "class_base_sound_sender.html#a837a9177447f9c03bd9882ae0d649bec", null ],
    [ "onStop", "class_base_sound_sender.html#a3e2740188f5022a8c9d0ab9486ea6c7f", null ],
    [ "run", "class_base_sound_sender.html#ae58cbf1a102e20babe217cfba0615bb6", null ],
    [ "selectMasterBoard", "class_base_sound_sender.html#ac32c535c008fd8708a8fa1951a43387d", null ],
    [ "sendSoundPacket", "class_base_sound_sender.html#a0207c2b226d67fd7ef5763ea0962be94", null ],
    [ "start", "class_base_sound_sender.html#abe058862b3788383c5ecb36c18955e79", null ],
    [ "stop", "class_base_sound_sender.html#a0a3960ae4f1b3ac9c63f6219c09d7b44", null ],
    [ "unselectMasterBoard", "class_base_sound_sender.html#a363b20544fad433ac577a59cd2c44057", null ],
    [ "ss_thread_func", "class_base_sound_sender.html#aab7e5cb90f5f6308703e0a62df61d430", null ],
    [ "active", "class_base_sound_sender.html#af42cbe7d9676bee5b272384dca9d22ad", null ],
    [ "master_brd_addr", "class_base_sound_sender.html#a385e1f795dfb12c26db60a0c9b8339d9", null ],
    [ "master_brd_chs", "class_base_sound_sender.html#a6ecd658c56875be259e8d583eee4dc39", null ],
    [ "pd_start", "class_base_sound_sender.html#acacea9c0bdc368408632b4c43275eded", null ],
    [ "thrd", "class_base_sound_sender.html#a2f987a7e52f28a6bd70d278d6f396b2f", null ]
];