class SoundParams extends MyListView{
//    snd_params;
    
    init(){
        super.init();
        this.snd_params = {};
        this.update();
    }
    
    getItemsCount(){//override
        return 2;
    }

    build(index){
        console.dir(this.snd_params);
        
        if(index == 0){
            var ins_div = $('<div id="ins" class="col-lg-4 col-md-12 accordion">');
            if(this.snd_params != null && this.snd_params.ins!=null){
                var insView = new OutsParams(ins_div, this.snd_params.ins, "ins");
                insView.init();
            }
            return ins_div;
        }else{
            var outs_div = $('<div id="outs" class="col-lg-8 col-md-12 accordion">');
            if(this.snd_params != null && this.snd_params.outs!=null){
                var outsView = new OutsParams(outs_div, this.snd_params.outs, "outs");
                outsView.init();
            }
            return outs_div;
        }
    }

    async update(){
        var _this = this;
        
        var obj = await (new Rest()).get_snd_params();
        _this.snd_params = obj.snd_params;

        _this.setState();
    }
}

class InOutParams extends MyListView{
    constructor(conatiner){
        super(conatiner);
    }
    
    inactive(name){
        return $("<button>", {
            class: "btn btn-outline-secondary col-12"
        }).html(name+" не доступен");
    }
}

//class InsParams extends InOutParams{
////    data;
//    
//    constructor(conatiner, data){
//        super(conatiner);
//        this.data = data;
//    }
//    
//    getItemsCount(){
//        var cnt = this.data!=null ? this.data.length : 0;
//        console.log("cnt="+cnt);
//        return cnt;
//    }
//
//    build(index){
//        var _this = this;
//        
//        console.log("In "+index);
//        console.dir(this.data[index]);
//        
//        if(this.data[index] == null){
//            return this.inactive("Вход "+(index+1));
//        }
//        
//        var capabs = this.data[index].capabs;
//        var cfg = this.data[index].cfg;
//        if(capabs==null || cfg==null){
//            return this.inactive("Вход "+(index+1));
//        }
//        
//        var volMin = capabs.inp_capb.min_dB;
//        var volMax = capabs.inp_capb.max_dB;
//        var volStep = capabs.inp_capb.step_dB;
//        var volume = $("<div>", {class: "row"}).append(
//            $("<div>", {class: "input-group mb-3 col"}).append(
//                $("<div>", {class: "input-group-prepend"}).append(
//                    $("<span>", {class: "input-group-text"}).html("Громкость")
//                ),
//                $("<input>", {
//                    class: "ml-3 mr-3 col",
//                    type: "range", 
//                    min: volMin, 
//                    max: volMax, 
//                    step: volStep,
//                    value: cfg.lvl
//                }).on("change", function(value){
//                    $(this).next().find("span").html(this.value+"dB");
//                    cfg.lvl = parseFloat(this.value);
//                    _this.onChange(index, cfg);
//                }),
//                $("<div>", {class: "input-group-append"}).append(
//                    $("<span>", {class: "input-group-text"}).html(cfg.lvl+"dB")
//                ),
//            )
//        );
//
//        let view = $(`
//            <div class="card">
//                <div class="card-header bg-info">
//                    <a class="btn col white-text" data-toggle="collapse" data-target="#inCollapse`+index+`" href="#inCollapse`+index+`" role="button" aria-expanded="false" aria-controls="inCollapse`+index+`">
//                      <div class="row">
//                        <img src="img/input.svg" width="24" height="24">
//                        <h7>Вход `+(index+1)+`</h7>
//                        <span class="col"></span>
//                        <span class="badge badge-light mr-3"> Громкость: `+cfg.lvl+`</span>
//                      </div>  
//                    </a>
//                </div>
//        
//                <div class="collapse mb-5" id="inCollapse`+index+`" data-parent="#ins">
//                  <div class="card-body">
//
//                    <div class="volume"></div>
//                  </div>
//                </div>
//
//                <div class="border p-2">
//                    <h5>Эквалайзер</h5>
//                    <div class="bypass"></div>
//                    <fieldset class="eq" `+((cfg.eq.bypass!=null && cfg.eq.bypass)  ? "disabled" : "")+`></fieldset>
//                </div>
//            </div>
//        `);
//        
//        view.find(".volume").append(volume);
//        
//        return view;
//    }
//    
//    onChange(index, cfg){
//        console.log("Changed in"+index);
//        var json = JSON.stringify(cfg);
//        console.log(json);
//        
//        var query = {
//            command: 'set_snd_params',
//            channel_type: "ins",
//            channel: index,
//            snd_params: cfg
//        };
//        var strQuery = JSON.stringify(query);
//        $.get('command.php', {query: strQuery}, function(answer){
//            console.log(answer);
//
//        });
//        
//    }
//}

class OutsParams extends InOutParams{
//    data;
    
    constructor(conatiner, data, type){
        super(conatiner);
        this.data = data;
        this.type = type;
    }
    
    getItemsCount(){
        var cnt = this.data!=null ? this.data.length : 0;
        console.log("cnt="+cnt);
        return cnt;
    }
    
    build(index){
        var _this = this;

        console.dir(settings);
        var typeStr = (this.type == "ins") ? "Вход" : "Выход";
        let ch_setts = (this.type == "ins") ? settings.ins : settings.outs; 
        
//        console.log("Out "+index);
//        console.dir(this.data[index]);
        
        let ch_name = ch_setts!==null && index<ch_setts.length ? ch_setts[index].name : null;
        if(ch_name === null || ch_name === "")
            ch_name = typeStr+" "+(index+1);
        console.log(ch_name);
        
        if(this.data[index] == null){
            return this.inactive(ch_name);
        }
        
        var capabs = this.data[index].capabs;
        var cfg = this.data[index].cfg;
        if(capabs==null || cfg==null){
            return this.inactive(ch_name);
        }
        
        var chckd = (cfg.eq!=null && cfg.eq.bypass!=null && cfg.eq.bypass) ? "checked" : "";
        var bypass = $("<div class='form-group form-check'>").append(
            $("<input type='checkbox' class='form-check-input' "+chckd+">").on("change", function(){
                console.log(this.checked);
                cfg.eq.bypass = this.checked ? 1 : 0;
                _this.onChange(index, cfg);
                if(this.checked)
                    $(this).parent().parent().next().attr("disabled", "");
                else
                    $(this).parent().parent().next().removeAttr("disabled");
            }),
            $('<label class="form-check-label" for="exampleCheck1">Байпасс</label>')
        );
        
        var cpb = (this.type == "ins") ? capabs.inp_capb : capabs.outp_capb;
        var volMin = cpb.min_dB;
        var volMax = cpb.max_dB;
        var volStep = cpb.step_dB;
        var volume = $("<div>", {class: "row"}).append(
            $("<div>", {class: "input-group mb-3 col"}).append(
                $("<div>", {class: "input-group-prepend"}).append(
                    $("<span>", {class: "input-group-text sm-text"}).html("Громкость")
                ),
                $("<input>", {
                    class: "ml-3 mr-3 col",
                    type: "range", 
                    min: volMin, 
                    max: volMax, 
                    step: volStep,
                    value: cfg.lvl
                }).on("change", function(value){
                    $(this).next().find("span").html(this.value+" дБ");
                    $(this).parents(".card").find("span.vol-badge").html("Громкость: "+this.value);
                    cfg.lvl = parseFloat(this.value);
                    _this.onChange(index, cfg);
                }),
                $("<div>", {class: "input-group-append"}).append(
                    $("<span>", {class: "input-group-text sm-text"}).html(Math.round(cfg.lvl*100)/100+"дБ")
                ),
            )
        );
        
        if(this.type == "outs"){
            var fcut = cfg.fcut_indx < capabs.hpf_capb.number ? capabs.hpf_capb.fcut[cfg.fcut_indx] : "-";
            var fcuts = $("<select>", {
                class: "form-control"
            }).on("change", function(){
                console.log("fcut = "+this.value);
                cfg.fcut_indx = parseInt(this.value);
                $(this).parents(".card").find("span.flt-badge").html(`Фильтр: ${capabs.hpf_capb.fcut[cfg.fcut_indx]}Гц`);
                _this.onChange(index, cfg);
            });
            for(let i=0; i<capabs.hpf_capb.number; i++){
                let option = $("<option>", {
                    value: i
                }).html(capabs.hpf_capb.fcut[i]);
                if(capabs.hpf_capb.fcut[i] === fcut){
                    option.attr("selected", 1);
                }
                fcuts.append(option);
            }

            var voltage = cfg.amplf_volt < capabs.pa_capb.out_lvl_amount ? capabs.pa_capb.out_lvl[cfg.amplf_volt] : "-";
            var voltages = $("<select>", {
                class: "form-control"
            }).on("change", function(){
                console.log("amplf_volt = "+this.value);
                cfg.amplf_volt = parseInt(this.value);
                $(this).parents(".card").find("span.volt-badge").html(`Напряжение: ${capabs.pa_capb.out_lvl[cfg.amplf_volt]}В`);
                _this.onChange(index, cfg);
            });
            for(let i=0; i<capabs.pa_capb.out_lvl_amount; i++){
                let option = $("<option>", {
                    value: i
                }).html(capabs.pa_capb.out_lvl[i]);
                if(capabs.pa_capb.out_lvl[i] == voltage){
                    option.attr("selected", 1);
                }
                voltages.append(option);
            }
        }
        
        let eq_capb = capabs.eq_capb;
        let eqDiv = $("<div>");
        let eq_cnt = (cfg.eq!=null && cfg.eq.ec!=null) ? cfg.eq.ec.length : 0;
        for(let i=0; i<eq_cnt; i++){
            console.log("Add eq section"+i);
            var eqSection = $("<div>", {class: "row"})
                .append(
                    $("<div>", {class: "input-group mb-3 col-3"}).append(
                        $("<div>", {class: "input-group-prepend"}).append(
                            $("<span>", {class: "input-group-text sm-text"}).html("Частота")
                        ),
                        $("<input>", {
                            class: "form-control",
                            type: "number",
                            min: eq_capb.fmin_hz,
                            max: eq_capb.fmax_hz,
                            step: 1,
                            placeholder: "Частота",
                            title: "Частота",
                            value: cfg.eq.ec[i].freq
                        }).on("change", function(){
                            cfg.eq.ec[i].freq = parseFloat(this.value);
                            _this.onChange(index,cfg);
                        }),
                        $("<div>", {class: "input-group-append"}).append(
                            $("<span>", {class: "input-group-text sm-text"}).html("Гц")
                        ),
                    ),
                    
                    $("<div>", {class: "input-group mb-3 col-2"}).append(
                        $("<div>", {class: "input-group-prepend"}).append(
                            $("<span>", {class: "input-group-text sm-text"}).html("Band")
                        ),
                        $("<input>", {
                            class: "form-control",
                            type: "number",
                            min: eq_capb.min_band,
                            max: eq_capb.max_band,
                            step: 0.1,
                            placeholder: "Band",
                            title: "Band",
                            value: Math.round(cfg.eq.ec[i].band * 10) / 10
                        }).on("change", function(){
                            console.log("EQ band " + i + " = " + this.value);
                            cfg.eq.ec[i].band = parseFloat(this.value);
                            _this.onChange(index, cfg);
                        }),
                    ),
                            
                    $("<div>", {class: "input-group mb-3 col"}).append(
                        $("<input>", {
                            class: "col form-control ml-3 mr-3",
                            type: "range",
                            min: eq_capb.min_gain_dB,
                            max: eq_capb.max_gain_dB,
                            step: 1,
                            value: cfg.eq.ec[i].gain
                        }).on("change", function(){
                            console.log("EQ gain " + i + " = " + this.value);
                            cfg.eq.ec[i].gain = parseFloat(this.value);
                            $(this).next().find("span").html(cfg.eq.ec[i].gain+"дБ");
                            _this.onChange(index, cfg);
                        }),
                        $("<div>", {class: "input-group-append"}).append(
                            $("<span>", {class: "input-group-text sm-text"}).html(cfg.eq.ec[i].gain+"дБ")
                        ),
                    ),        
                );
            
            eqDiv.append(eqSection);
        }
        
        if(eq_cnt > 0){
            eqDiv.append(
                $("<div>", {class: "input-group mb-3 ml-1 row"}).append(
                    $("<div>", {class: "input-group-prepend"}).append(
                        $("<span>", {class: "input-group-text sm-text"}).html("Усиление")
                    ),
                    $("<input>", {
                        class: "col form-control ml-3 mr-3",
                        type: "range",
                        min: eq_capb.min_out_gain_dB,
                        max: eq_capb.max_out_gain_dB,
                        step: 1,
                        value: cfg.eq.outGain
                    }).on("change", function(){
                        console.log("EQ out gain = " + this.value);
                        cfg.eq.outGain = parseFloat(this.value);
                        $(this).next().find("span").html(cfg.eq.outGain+"дБ");
                        _this.onChange(index, cfg);
                    }),
                    $("<div>", {class: "input-group-append"}).append(
                        $("<span>", {class: "input-group-text sm-text"}).html(cfg.eq.outGain+"дБ")
                    ),
                )
            );
        }

        var hdr = ch_name;
        var id = typeStr+"Collapse"+(index+1);
        
        //
        let view = $(`
            <div class="card shadow rounded">
                <div class="card-header bg-info">
                    <a class="btn col white-text" data-toggle="collapse" data-target="#`+id+`" href="#`+id+`" role="button" aria-expanded="false" aria-controls="`+id+`">
                      <div class="row">
                        <img src="img/output.svg" width="24" height="24" class="ml-3 mr-1">
                        <h7 class="mr-3">`+hdr+`</h7>
                        <span class="col"></span>
                        <span class="badge badge-light mr-1 vol-badge"> Громкость: ${Math.round(cfg.lvl*100)/100}дБ</span>
                        <span class="badge badge-light mr-1 volt-badge"> Напряжение: ${voltage}В</span>
                        <span class="badge badge-light mr-3 flt-badge"> Фильтр: ${fcut}Гц</span>
                      </div>  
                    </a>
                </div>
        
                <div class="collapse mb-2" id="`+id+`" data-parent="#`+this.type+`">
                  <div class="card-body">

                    <div class="volume"></div>

                    <div class='row'>
                        <div class="input-group mb-3 col-6 filter">
                            <div class="input-group-prepend">
                                <span class="input-group-text sm-text" id="basic-addon1">Фильтр ВЧ</span>
                            </div>
                        </div>

                        <div class="input-group mb-3 col-6 voltages">
                            <div class="input-group-prepend">
                                <span class="input-group-text sm-text" id="basic-addon1">Напряжение</span>
                            </div>
                        </div>
                    </div>

                    <div class="border p-2 eq-full">
                        <h5>Эквалайзер</h5>
                        <div class="bypass"></div>
                        <fieldset class="eq" `+((cfg.eq.bypass!=null && cfg.eq.bypass)  ? "disabled" : "")+`></fieldset>
                    </div>
                  </div>
                </div>
            </div>
        `);
        
        view.find(".bypass").append(bypass);
            view.find(".volume").append(volume);
        if(this.type == "outs"){
            view.find(".filter").append(fcuts);
            view.find(".filter").append("<div class='input-group-append'><span class='input-group-text'>Гц</span></div>");
            view.find(".voltages").append(voltages);
            view.find(".voltages").append("<div class='input-group-append'><span class='input-group-text'>В</span></div>");
        }else{
            view.find(".filter").remove();
            view.find(".voltages").remove();
            view.find(".flt-badge").remove();
            view.find(".volt-badge").remove();
        }
        if(cfg.eq.ec != null && cfg.eq.ec.length > 0)
            view.find(".eq").append(eqDiv);
        else
            view.find(".eq-full").remove();
        
        return view;
    }
    
    async onChange(index, cfg){
        console.log("Changed "+index);
        var json = JSON.stringify(cfg);
        console.log(json);
        
//        var query = {
//            command: 'set_snd_params',
//            channel_type: this.type,
//            channel: index,
//            snd_params: cfg
//        };
//        var strQuery = JSON.stringify(query);
//        $.get('command.php', {query: strQuery}, function(answer){
//            console.log(answer);
//
//        });
        
        console.dir(await (new Rest()).set_snd_params(this.type, index, cfg));
        
    }
}