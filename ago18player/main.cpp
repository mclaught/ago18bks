/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 11 января 2021 г., 16:21
 */

#include "mainthread.h"

#include <cstdlib>
#include <sys/time.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <iostream>
#include <syslog.h>
#include <execinfo.h>
#include <unistd.h>
#include <fstream>
#include <chrono>

#define PID_PATH "/run/ago18player"
//#define BY_TIMER
#if defined (BY_TIMER)
    #define VERSION "23.04.13-01-tmr"
#else
    #define VERSION "23.04.13-01"
#endif

using namespace std;

MainThread* thrd = nullptr;
chrono::time_point<chrono::system_clock> start_tm;
int packs_cnt = 6;
string pidname;

void start_timer(int period){

    struct itimerval tv;
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = period;  // when timer expires, reset to 100ms
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = period;   // 100 ms == 100000 us
    if(setitimer(ITIMER_REAL, &tv, NULL))
        printf("setitimer(): %s\n", strerror(errno));
}

void unlink_pid_file(){
    for(int i=0; i<10; i++){
        ifstream file(pidname);
        if(file.good())
            unlink(pidname.c_str());
        else
            break;
    }
}

static void signal_error(int sig, siginfo_t *si, void *ptr)
{
#ifndef NO_STACK_TRACE
    
    void* ErrorAddr;
    void* Trace[16];
    int x;
    int TraceSize;
    char** Messages;

    // запишем в лог что за сигнал пришел


#if __WORDSIZE == 64 // если дело имеем с 64 битной ОС
    // получим адрес инструкции которая вызвала ошибку
    cerr << "Signal: " << strsignal(sig) << ", Addr: 0x" << hex << (uint64_t) si->si_addr << endl;
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%lu\n", strsignal(sig), (uint64_t)si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_RIP];
#else
    // получим адрес инструкции которая вызвала ошибку
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%X\n", strsignal(sig), (int) si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_EIP];
#endif

    // произведем backtrace чтобы получить весь стек вызовов
    TraceSize = backtrace(Trace, 16);
    Trace[1] = ErrorAddr;

    // получим расшифровку трасировки
    Messages = backtrace_symbols(Trace, TraceSize);
    if (Messages)
    {
        printf("== Backtrace ==\n");

        // запишем в лог
        for (x = 1; x < TraceSize; x++)
        {
            printf("%s\n",Messages[x]);
        }

        printf("== End Backtrace ==\n");
        free(Messages);
    }
#endif

    closelog();
    unlink_pid_file();
    exit(0);
}

chrono::time_point<chrono::system_clock> last_tm;
void AlrmHandler(int sig)
{
    auto tm = chrono::system_clock::now();
    int comn_dur = chrono::duration_cast<chrono::microseconds>(tm - start_tm).count();
    int dtm = chrono::duration_cast<chrono::microseconds>(tm - last_tm).count();
    last_tm = tm;

    int must_sent = comn_dur/(AUDIO_FRAME_MS*1000);
    int to_send = must_sent - thrd->sent;

    if(thrd){
        thrd->sendSoundPacket(to_send);
        thrd->read_file();
    }

#ifdef DEBUG_PRINT
    printf("%d\t%d\n", dtm, to_send);
#endif
}

void TermHandler(int sig)
{
    cerr << "caught TERM signal: " << strsignal(sig) << endl;
    unlink_pid_file();
    exit(0);
}

void HupHandler(int sig)
{
    cerr << "caught HUP signal: " << strsignal(sig) << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, "caught HUP signal: %s", strsignal(sig));
}

void IntHandler(int sig)
{
    cerr << "caught INT signal: " << strsignal(sig) << endl;
    cout << strsignal(sig) << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, "caught INT signal: %s", strsignal(sig));
    unlink_pid_file();
    exit(0);
}

int ConfigureSignalHandlers()
{
    int ign_sig_cnt = 5;
    int ign_sig[] = {SIGUSR2, SIGPIPE, SIGTSTP, SIGPROF, SIGCHLD};//SIGALRM,
    int fatal_sig[] = {SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGIOT, SIGBUS, SIGFPE, SIGSEGV, SIGSTKFLT, SIGCONT, SIGPWR, SIGSYS};

    for (int i = 0; i < ign_sig_cnt; i++)
        signal(ign_sig[i], SIG_IGN);

    for (int i = 0; i < 12; i++)
    {
        struct sigaction sigact;
        sigact.sa_flags = SA_SIGINFO;
        sigact.sa_sigaction = signal_error;
        sigemptyset(&sigact.sa_mask);

        sigaction(fatal_sig[i], &sigact, 0);
    }

    struct sigaction sigstpSA;
    sigstpSA.sa_handler = AlrmHandler;
    sigemptyset(&sigstpSA.sa_mask);
    sigstpSA.sa_flags = 0;
    sigaction(SIGALRM, &sigstpSA, NULL);

    struct sigaction sigtermSA;
    sigtermSA.sa_handler = TermHandler;
    sigemptyset(&sigtermSA.sa_mask);
    sigtermSA.sa_flags = 0;
    sigaction(SIGTERM, &sigtermSA, NULL);

    struct sigaction sigusr1SA;
    sigusr1SA.sa_handler = HupHandler;
    sigemptyset(&sigusr1SA.sa_mask);
    sigusr1SA.sa_flags = 0;
    sigaction(SIGUSR1, &sigusr1SA, NULL);

    struct sigaction sighupSA;
    sighupSA.sa_handler = HupHandler;
    sigemptyset(&sighupSA.sa_mask);
    sighupSA.sa_flags = 0;
    sigaction(SIGHUP, &sighupSA, NULL);

    struct sigaction sigintSA;
    sigintSA.sa_handler = IntHandler;
    sigemptyset(&sigintSA.sa_mask);
    sigintSA.sa_flags = 0;
    sigaction(SIGINT, &sigintSA, NULL);

    return 0;
}


/*
 * 
 */
int main(int argc, char** argv) {
    ConfigureSignalHandlers();

    sched_param param;
    param.sched_priority = 22;
    if(sched_setscheduler(0, SCHED_FIFO, &param))
        printf("sched_setscheduler(): %s\n", strerror(errno));

    string args[argc];
    for(int i=0; i<argc; i++){
        args[i] = argv[i];
    }
    
    if(argc == 2 && args[1]=="-v"){
        printf("%s\n", VERSION);
        exit(0);
    }

    printf("AGO-18 player %s\n", VERSION);

    if(argc < 6){
        printf("Too few arguments\n");
        printf("Usage: ago18player <server_addr> <in> <outs> <broadcast_ip> <file_name> [<packs_per_period>]\n");
        exit(-1);
    }

    int serv_addr = stoi(args[1]);
    int in = stoi(args[2]);
    int outs = stoi(args[3]);
    string ip_addr = args[4];
    string fName = args[5];
    packs_cnt = argc >= 7 ? stoi(args[6]) : 6;
    int autorew = argc >= 8 ? stoi(args[7]) : 0;

    int pid = getpid();
    pidname = PID_PATH+to_string(in);
    ofstream pidfile(pidname);
    if(pidfile.good()){
        pidfile << pid;
        pidfile.close();
    }

    thrd = new MainThread(fName, ip_addr, in, outs, serv_addr, autorew>0);
    thrd->read_file();
//    thrd->start();

#if defined (BY_TIMER)
    start_timer(AUDIO_FRAME_MS*packs_cnt*1000);
#endif

    start_tm = chrono::system_clock::now();
    auto last_tm = start_tm;
    while(!thrd->terminated){
#if defined (BY_TIMER)        
        usleep(1000);
#else
        auto tm = chrono::system_clock::now();
        int dtm = chrono::duration_cast<chrono::microseconds>(tm - last_tm).count();
        if(dtm >= (packs_cnt*AUDIO_FRAME_MS*1000)){
            last_tm = tm;
            AlrmHandler(SIGALRM);
            usleep((packs_cnt-1)*AUDIO_FRAME_MS*1000);
        }
#endif        
    }

    unlink_pid_file();

    return 0;
}

