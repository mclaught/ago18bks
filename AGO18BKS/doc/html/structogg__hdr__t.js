var structogg__hdr__t =
[
    [ "bitstream_sn", "structogg__hdr__t.html#a4f84a92cada01532f5c6f5541ae4d2a9", null ],
    [ "checksum", "structogg__hdr__t.html#a81035ed176cd5ee5a7fa28af5aa03b58", null ],
    [ "gran_pos", "structogg__hdr__t.html#a8b1dd2e5e7547e7e1112b1b4a22b4436", null ],
    [ "hdr_type", "structogg__hdr__t.html#a048d3bc69daf9c572555c4d02805cd46", null ],
    [ "pattern", "structogg__hdr__t.html#a675e3d8953398ee208089fd35bb04215", null ],
    [ "pg_sn", "structogg__hdr__t.html#a7e20f33573143add5c078d935b4de57c", null ],
    [ "segm_cnt", "structogg__hdr__t.html#a0f1aff59653a3b5894a42bf328385a23", null ],
    [ "segments", "structogg__hdr__t.html#afd2e50ff92a2bc3d2edcc3cb38b9eddd", null ],
    [ "ver", "structogg__hdr__t.html#ab88f2681761282dc883301383f8538cf", null ]
];