/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BaseSoundSender.cpp
 * Author: user
 * 
 * Created on 16 декабря 2019 г., 13:54
 */

#include "BaseSoundSender.h"
#include "ConfigServer.h"
#include "MyUDPServer.h"
#include "cus/log.h"

#include <iostream>
#include <unistd.h>
#include <cmath>

sound_sender_pool_t sound_sender_pool;
shared_mutex sound_sender_mutex;

void ss_thread_func(BaseSoundSender* __this){
    __this->run();
}

BaseSoundSender::BaseSoundSender(bool in_tread) :_in_thread(in_tread) {
}

BaseSoundSender::BaseSoundSender(const BaseSoundSender& orig) {
}

BaseSoundSender::~BaseSoundSender() {
}

sound_sender_pool_t BaseSoundSender::getPool()
{
    shared_lock<shared_mutex> lock(sound_sender_mutex);
    return sound_sender_pool;
}

BaseSoundSender *BaseSoundSender::findSender(int key)
{
    shared_lock<shared_mutex> lock(sound_sender_mutex);
    sound_sender_pool_t::iterator fnd = sound_sender_pool.find(key);
    if(fnd != sound_sender_pool.end())
        return fnd->second;
    else
        return nullptr;
}

void BaseSoundSender::selectMasterBoard(){
    ch_mask_t outs = pd_start.outs;
//    cout << "selectMasterBoard: outs=" << outs;
    if(outs == 0){
        for(int i=0; i<pd_start.channels; i++){
            outs |= conf_server.getCTOutsInput(pd_start.in_ch[i]);
//            cout << " in_ch=" << (int)pd_start.in_ch[0] << " outs=" << (int)outs;
        }
    }
    
    
    int out_ch = -1;
    for(int i=0; i<static_cast<int>(CT_BITS); i++){
        if(outs & (1 << i)){
            out_ch = i;
            break;
        }
    }
//    cout << " out_ch=" << out_ch;
    
    if(out_ch != -1){
        master_brd_addr = CHAN_TO_BRD_ID(out_ch);
//        cout << " master_brd_addr=" << out_ch;
        for(int i=0; i<MAX_INP_CHANNELS; i++){
            uint8_t brd_ch = BRD_ID_TO_CHAN(master_brd_addr, i); 
            if(outs & (1 << brd_ch)){
                master_brd_chs[i] = brd_ch;

                unique_lock<shared_mutex> lock(sound_sender_mutex);
                sound_sender_pool[SOUND_SENDER_KEY(master_brd_addr, brd_ch)] = this;
            }else{
                master_brd_chs[i] = 0xFF;
            }
//            cout << " master_brd_chs[" << i << "]=" << (int)master_brd_chs[i];
        }
#if UDP_SEND_TYPE != THREAD_SEND && UDP_SEND_TYPE != TIMER_SEND
        server.sendCommand(master_brd_chs, 2, PACK_CFG, SET_MASTER, 2, master_brd_addr);
#endif
    }
    
//    cout << endl;
    
//    sound_sender_pool.insert(this);
}

void BaseSoundSender::unselectMasterBoard(){
//    cout << "unselect master board " << (int)master_brd_addr << endl;
#if UDP_SEND_TYPE != THREAD_SEND && UDP_SEND_TYPE != TIMER_SEND
    server.sendCommand(master_brd_chs, 2, PACK_CFG, CLEAR_MASTER, 2, master_brd_addr);
#endif
    for(int i=0; i<MAX_INP_CHANNELS; i++){
        unique_lock<shared_mutex> lock(sound_sender_mutex);

        sound_sender_pool_t::iterator fnd = sound_sender_pool.find(SOUND_SENDER_KEY(master_brd_addr, master_brd_chs[i]));
        if(fnd != sound_sender_pool.end()){
            sound_sender_pool.erase(fnd);
        }
        master_brd_chs[i] = 0xFF;
    }
//    sound_sender_pool.erase(this);
}

void BaseSoundSender::start(){
    if(_in_thread){
        if(thrd.joinable())
            thrd.detach();
        thrd = thread(ss_thread_func, this);

//        int prio_min = sched_get_priority_min(SCHED_FIFO);
//        int prio_max = sched_get_priority_max(SCHED_FIFO);
//        printf("prios = %d - %d", prio_min, prio_max);

        sched_param param;
        param.sched_priority = 22;
        int err = pthread_setschedparam(thrd.native_handle(), SCHED_FIFO, &param);//main_thread.native_handle()
        if(err != 0)
            cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;

//        printf("BaseSoundSender created. id=%d\n", thrd.native_handle());
    }
}

void BaseSoundSender::stop(){
    active = false;
    if(_in_thread){
        if(thrd.joinable())
            thrd.join();
        //thrd.detach();
    }
}

void BaseSoundSender::run(){
    onStart();
    
    while(active){
        cycle();
    }
    
    onStop();
}

void BaseSoundSender::cycle(){
    auto tm = chrono::high_resolution_clock::now();
    int dTm = chrono::duration_cast<chrono::microseconds>(tm - last_tm).count();
    if(dTm >= (AUDIO_FRAME_MS*SEND_FRAME_SERIES*1000)){
        last_tm = tm;
        timer_event();
        
        usleep(AUDIO_FRAME_MS*(SEND_FRAME_SERIES-2)*1000);
    }
        
}

void BaseSoundSender::timer_event()
{
    auto tm = chrono::high_resolution_clock::now();
    int procTm = chrono::duration_cast<chrono::microseconds>(tm - start_tm).count();
    int must_sent = static_cast<int>(round(procTm/(AUDIO_FRAME_MS*1000)));
    int n = must_sent - sent_cnt;

    sendSoundPacket(n);
    sent_cnt = must_sent;
//    if(n > SEND_FRAME_SERIES)
//        printf("n=%d\n", n);
}

void BaseSoundSender::onStart(){
    start_tm = chrono::high_resolution_clock::now();
    last_tm = start_tm;
    sent_cnt = 0;
}

void BaseSoundSender::onStop(){
}

bool BaseSoundSender::isBusy(){
    return active;
}

int BaseSoundSender::getInCh()
{
    return pd_start.in_ch[0];
}
