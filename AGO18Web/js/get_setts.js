var outsCnt = 20;
var insCnt = 32;
var settings = {};
var network = {};

async function get_settings(callback){
//    var query = {
//        command: "get_settings"
//    };
//    $.get('command.php', {query: JSON.stringify(query)}, function(answer){
    var obj = await (new Rest()).get_settings();
    settings = obj.settings;

    insCnt = settings.ins_cnt;
    outsCnt = settings.outs_cnt;

    $('#bksName').html(settings.bks.name);

    if(callback != null)
        callback(settings);
//    });
}
