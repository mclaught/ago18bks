#ifndef CLIPSWORKER_H
#define CLIPSWORKER_H

#include "cus/MyThread.h"

class ClipsWorker : public MyThread
{
public:
    ClipsWorker();

    virtual void start()override;
    virtual void terminate()override;

private:
    void exec();
};

extern ClipsWorker clips_worker;

#endif // CLIPSWORKER_H
