/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.h
 * Author: user
 *
 * Created on 6 декабря 2019 г., 0:26
 */

#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <thread>

using namespace std;

class MyThread {
public:
    MyThread();
    MyThread(const MyThread& orig);
    virtual ~MyThread();
    
    virtual void start();
    virtual void stop();
private:
    
    friend void my_thread_func(MyThread* __this);
    
protected:
    thread *thrd;
    bool active = true;
    
    virtual void run() = 0;
};

#endif /* MYTHREAD_H */

