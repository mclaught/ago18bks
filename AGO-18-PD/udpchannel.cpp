#include "soundread.h"
#include "udpchannel.h"

#include <QThread>
#include <QtConcurrent>

UDPChannel::UDPChannel(QHostAddress addr, uint16_t port, BaseSoundSource* reader, QObject *parent) : QObject(parent)
{
    serverAddr = addr;
    serverPort = port;
    _reader = reader;

    if(dynamic_cast<SoundReader*>(reader)){
        connect(&timer, &QTimer::timeout, this, &UDPChannel::onSoundReaderTimer);
    }else{
        connect(&timer, &QTimer::timeout, this, &UDPChannel::onOtherTimer);
    }

}

void UDPChannel::onSoundReaderTimer(){
    qint64 last_sent = sent;
    while(_reader->fifo_cnt() >= AUDIO_SAMPLES){
        sendSound();
    }
    qDebug() << sent - last_sent;
}

void UDPChannel::onOtherTimer(){
    qint64 elp = sendTimer.elapsed();
    qint64 mustSent = elp/AUDIO_FRAME_MS;
    qint64 toSend = mustSent-sent;
    for(qint64 i=0; i<toSend; i++){
        if(prebuf > 0)
            prebuf--;
        else{
            sendSound();
        }
    }

    qint64 pr = sendTimer.elapsed() - lastSentTm;
    lastSentTm = sendTimer.elapsed();
    qDebug() << pr << toSend;

//            sendTimer.restart();
}

void UDPChannel::readDgram(){
//    char buf[2048];
//    auto rd = sock->readDatagram(buf, 2048);

//    if(rd){
//        packet_command_t* pack = reinterpret_cast<packet_command_t*>(buf);
//        if(pack->hdr.type == PACK_CFG && pack->cmd == SND_CHANK_REQ){
//            //request chank
//            int cnt = pack->data[0];
//            if(reader->fifo_cnt() >= AUDIO_SAMPLES*cnt){
//                for(int i=0; i<cnt; i++){
//                    sendSound();
//                }
//            }
//        }
//    }
}

void UDPChannel::sendCommand(uint8_t* data, int len, uint8_t type, uint8_t cmd, int repeats, uint8_t addr){
    size_t sz = static_cast<size_t>(sizeof(pack_hdr_t)+1+len);
    uint8_t buf[2048];
    packet_command_t* p = reinterpret_cast<packet_command_t*>(buf);

    p->hdr.adr_from = src_ch;
    p->hdr.adr_to = addr;
    p->hdr.len = len+1;
    p->hdr.type = type;
    p->cmd = cmd;
    if(data)
        memcpy(p->data, data, len);

    for(int i=0; i<repeats; i++){
        p->hdr.repeate_cnt = i;

        sock->writeDatagram(reinterpret_cast<const char*>(buf), sz, serverAddr, serverPort);
    }
}


void UDPChannel::start(uint8_t src_ch, ch_mask_t recipients, int period){
    this->period = period;
    sock = new QUdpSocket(this);
    sock->bind(QHostAddress::AnyIPv4);
    connect(sock, &QUdpSocket::readyRead, this, &UDPChannel::readDgram);

    this->src_ch = src_ch;
    this->recip = recipients;

    start_stop_t data = {src_ch, recipients};

    //sock->writeDatagram(reinterpret_cast<const char*>(buf), pack_sz, serverAddr, serverPort);
    sendCommand(reinterpret_cast<uint8_t*>(&data), sizeof(start_stop_t), PACK_CFG, SET_RECIPNT_LIST, 1);

    if(period){
        prebuf = 0;
        timer.setTimerType(Qt::PreciseTimer);
        timer.start(period/1000);
        sent = 0;
        sendTimer.start();
    }


//    running = true;
//    prebuf = 10;
//    sendFurute =  QtConcurrent::run([this, period](){
//        while(running){
//            auto elps = sendTimer.nsecsElapsed();
//            if(elps >= period*1000){
//                sendTimer.restart();

//                for(int i=0; i<3; i++){
//                    if(prebuf > 0)
//                        prebuf--;
//                    else{
//                        sendSound();
//                    }
//                }

////                QThread::usleep(10000);
//            }

//        }
//    });
}

void UDPChannel::stop(uint8_t src_ch, ch_mask_t recipients){
    start_stop_t data = {src_ch, recipients};
    sendCommand(reinterpret_cast<uint8_t*>(&data), sizeof(start_stop_t), PACK_CFG, CLEAR_RECIPNT_LIST, 1);
    timer.stop();
    running = false;
}

void UDPChannel::selectMasterBoard(ch_mask_t outs){
    int out_ch = -1;
    for(int i=0; i<static_cast<int>(CT_BITS); i++){
        if(outs & (1 << i)){
            out_ch = i;
            break;
        }
    }

    if(out_ch != -1){
        master_brd_addr = static_cast<uint8_t>(CHAN_TO_BRD_ID(out_ch));
        for(int i=0; i<MAX_INP_CHANNELS; i++){
            uint8_t brd_ch = static_cast<uint8_t>(BRD_ID_TO_CHAN(master_brd_addr, i));
            if(outs & (1 << brd_ch)){
                master_brd_chs[i] = brd_ch;
            }else{
                master_brd_chs[i] = 0xFF;
            }
        }
        sendCommand(master_brd_chs, 2, PACK_CFG, SET_MASTER, 2, master_brd_addr);
    }
}


void UDPChannel::unselectMasterBoard(){
    sendCommand(master_brd_chs, 2, PACK_CFG, CLEAR_MASTER, 2, master_brd_addr);
    for(int i=0; i<MAX_INP_CHANNELS; i++){
        master_brd_chs[i] = 0xFF;
    }
}


void UDPChannel::sendSound(){
    packet_audio_t packet;

    packet.audio_hdr.sender = src_ch;
    packet.audio_hdr.recip = recip;

    if(_reader->fifo_read(packet.pcm, AUDIO_SAMPLES)){
        int pack_sz = sizeof(audio_hdr_t)+sizeof(int16_t)*AUDIO_SAMPLES;
        //sock->writeDatagram(reinterpret_cast<const char*>(&packet), pack_sz, serverAddr, serverPort);
        sendCommand(reinterpret_cast<uint8_t*>(&packet.audio_hdr), pack_sz, PACK_AUDIO, SND_STREAM);
        sent++;
    }
}
