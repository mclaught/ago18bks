class CommTable {
  List<Comm_table> _commTable;

  List<Comm_table> get commTable => _commTable;

  CommTable({
      List<Comm_table> commTable}){
    _commTable = commTable;
}

  CommTable.fromJson(dynamic json) {
    if (json["comm_table"] != null) {
      _commTable = [];
      json["comm_table"].forEach((v) {
        _commTable.add(Comm_table.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_commTable != null) {
      map["comm_table"] = _commTable.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Comm_table {
  int _alarm;
  int _in;
  bool _online;
  int _out;
  int _prio;

  int get alarm => _alarm;
  int get inn => _in;
  bool get online => _online;
  int get out => _out;
  int get prio => _prio;

  Comm_table({
      int alarm, 
      int inn,
      bool online, 
      int out, 
      int prio}){
    _alarm = alarm;
    _in = inn;
    _online = online;
    _out = out;
    _prio = prio;
}

  Comm_table.fromJson(dynamic json) {
    _alarm = json["alarm"];
    _in = json["in"];
    _online = json["online"];
    _out = json["out"];
    _prio = json["prio"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["alarm"] = _alarm;
    map["in"] = _in;
    map["online"] = _online;
    map["out"] = _out;
    map["prio"] = _prio;
    return map;
  }

}