#include "remotesetts.h"
#include "log.h"

#include <QEventLoop>
#include <QFileInfo>
#include <QHttpMultiPart>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>
#include <vector>

RemoteSetts::RemoteSetts(QString ip, QObject *parent) : QObject(parent), server_ip(ip)
{

}

QByteArray RemoteSetts::getAnswer(QUrl url)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager();
    QNetworkReply *reply = manager->get(QNetworkRequest(url));
    connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::errorOccurred),
              this, &RemoteSetts::slotError);

    QEventLoop wait;
    connect(manager, SIGNAL(finished(QNetworkReply*)), &wait, SLOT(quit()));
    QTimer::singleShot(1000, &wait, SLOT(quit()));
    connect(manager, SIGNAL(finished(QNetworkReply*)),manager, SLOT(deleteLater()));
    wait.exec();
    QByteArray answer = reply->readAll();
    reply->deleteLater();
    return answer;

}

QByteArray RemoteSetts::query(QString cmd){
    QString jsStr = "";
    try{
        json js = json::object();
        js["command"] = cmd.toStdString();
        jsStr = QString(js.dump().c_str());
    }catch(json::exception &e){
        qDebug() << e.what();
        return QByteArray();
    }
    const QString queryStr = QString("http://%1/command.php?query=%2").arg(server_ip, jsStr);
    qDebug() << queryStr;

    return getAnswer(QUrl(queryStr));
}

QByteArray RemoteSetts::query(QString cmd, json data)
{
    QString jsStr = "";
    try{
        data["command"] = cmd.toStdString();
        jsStr = QString(data.dump().c_str());
    }catch(json::exception &e){
        qDebug() << e.what();
        return QByteArray();
    }
    const QString queryStr = QString("http://%1/command.php?query=%2").arg(server_ip, jsStr);
    qDebug() << queryStr;

    return getAnswer(QUrl(queryStr));
}

json RemoteSetts::getNetSetts(){
    try{
        auto resp = query("get_network");
        json js = json::parse(resp.data(), nullptr, false);
        if(js.is_object() && js.contains("network"))
            return js["network"];
        else
            return json();
    }catch(json::exception &e){
        qDebug() << e.what();
        _log.log("RemoteSetts::getNetSetts: "+QString(e.what()));
        return json();
    }
}

int RemoteSetts::getPort(){
    try{
        if(network.is_null())
            network = getNetSetts();
        if(network.is_object() && network.contains("out_port")){
            return network["out_port"];
        }else{
            return 0;
        }
    }catch(json::exception &e){
        qDebug() << e.what();
        return 0;
    }
}

int RemoteSetts::getWSPort()
{
    try{
        if(network.is_null())
            network = getNetSetts();
        if(network.is_object() && network.contains("ws_port")){
            return network["ws_port"];
        }else{
            return 0;
        }
    }catch(json::exception &e){
        qDebug() << e.what();
        return 0;
    }
}

json RemoteSetts::getSetts(){
    try{
        auto resp = query("get_settings");
        json js = json::parse(resp.data(), nullptr, false);
        if(js.contains("settings"))
            return js["settings"];
        else
            return json();
    }catch(json::exception &e){
        qDebug() << e.what();
        _log.log("RemoteSetts::getSetts: "+QString(e.what()));
        return json();
    }
}

std::vector<std::string> RemoteSetts::getOutNames(){
    std::vector<std::string> res;

    try{
        outsCnt = 0;

        setts = getSetts();
        if(!setts.is_object())
            return res;
        if(!setts.is_object())
            return res;
        if(!setts.contains("outs"))
            return res;
        if(!setts["outs"].is_array())
            return res;

        outsCnt = setts["outs_cnt"];

        int i=0;
        for(json out : setts["outs"]){
            if(out.is_object() && out.contains("name")){
                std::string name = out["name"];
                res.push_back(name.c_str());
            }
            i++;
        }
    }catch(json::exception &e){
        qDebug() << e.what();
    }

    return res;
}

void RemoteSetts::slotReadyRead(){
    if(!reply){
        qDebug() << "No reply";
        return;
    }

    if(!reply->error()){
        auto data = reply->readAll();
        qDebug() << data;
    }else{
        qDebug() << "Error: " << reply->error();
    }
}

void RemoteSetts::slotError(QNetworkReply::NetworkError error){
    qDebug() << "Error: " << error;
}

void RemoteSetts::slotSslErrors(QList<QSslError> &errors){
    (void)errors;
}

void RemoteSetts::replyFinished(QNetworkReply* rep){
    if(!rep->error()){
        auto data = rep->readAll();
        qDebug() << data;
    }else{
        qDebug() << "Error: " << rep->error();
    }
}

QVector<QString> RemoteSetts::getClips()
{
    QVector<QString> clips;
    try{
        json data = json::object({
            {"dir", "pd"}
        });
        auto resp = query("get_clips", data);
        json js = json::parse(resp.data(), nullptr, false);
        if(!js.is_object() || !js.contains("result") || !js.contains("clips") || !js["clips"].is_array())
            return clips;

        for(auto &j_clip : js["clips"]){
            std::string s_clip = j_clip;
            QFileInfo info(QString(s_clip.c_str()));
            clips.push_back(info.fileName());
        }
    }catch(json::exception &e){
        qDebug() << e.what();
        _log.log("RemoteSetts::getClips: "+QString(e.what()));
    }
    return clips;
}

bool RemoteSetts::playClip(QString name, uint8_t source, uint64_t outs){
    try{
        json js = json::object();
        js["command"] = "play_clip";
        js["clip"] = "pd/"+name.toStdString();
        js["source"] = source;
        js["outs"] = outs;
        QString jsStr = QString(js.dump().c_str());
        const QString queryStr = QString("http://%1/command.php?query=%2").arg(server_ip, jsStr);
        qDebug() << queryStr;

        json ans = json::parse(getAnswer(QUrl(queryStr)));
        return ans.contains("result") && ans["result"] == "success";
    }catch(json::exception &e){
        qDebug() << e.what();
        _log.log("RemoteSetts::playClip: "+QString(e.what()));
        return false;
    }
}

bool RemoteSetts::stopClip(QString name, uint8_t source){
    try{
        json js = json::object();
        js["command"] = "stop_clip";
        js["clip"] = "pd/"+name.toStdString();
        js["source"] = source;
        QString jsStr = QString(js.dump().c_str());
        const QString queryStr = QString("http://%1/command.php?query=%2").arg(server_ip, jsStr);
        qDebug() << queryStr;

        json ans = json::parse(getAnswer(QUrl(queryStr)));
        return ans.contains("result") && ans["result"] == "success";
    }catch(json::exception &e){
        qDebug() << e.what();
        _log.log("RemoteSetts::stopClip: "+QString(e.what()));
        return false;
    }
}

int RemoteSetts::uploadFile(QString ip, QString fileName){
    QFile *file = nullptr;
    if(!fileName.isEmpty()){
        file = new QFile(fileName);
        if(!file->open(QIODevice::ReadOnly)){
            file->deleteLater();
            return -1;
        }
    }

    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart command;
    command.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"command\""));
    command.setBody("upload");
    multiPart->append(command);

    QHttpPart dir;
    dir.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"dir\""));
    dir.setBody("clips/pd");
    multiPart->append(dir);

    if(file){
        file->setParent(multiPart);

        QHttpPart filePart;
        filePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"upload-clip\"; filename=\""+ file->fileName() + "\""));
        filePart.setBodyDevice(file);
        multiPart->append(filePart);
    }

    QUrl url = QUrl(QString("http://%1/upload_clip.php").arg(ip));
    QNetworkReply *reply = manager->post(QNetworkRequest(url), multiPart);
    multiPart->setParent(reply);
    connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::errorOccurred),
              this, &RemoteSetts::slotError);
    connect(reply, &QNetworkReply::uploadProgress, this, &RemoteSetts::onProgress);

    QEventLoop wait;
    connect(manager, SIGNAL(finished(QNetworkReply*)), &wait, SLOT(quit()));
    QTimer::singleShot(30000, &wait, SLOT(quit()));
    connect(manager, SIGNAL(finished(QNetworkReply*)),manager, SLOT(deleteLater()));
    wait.exec();
    QByteArray answer = reply->readAll();
    reply->deleteLater();

    qDebug() << answer;

    return QString(answer).contains("success");
}

void RemoteSetts::onProgress(qint64 sent, qint64 total){
    emit uploadProgress(sent, total);
}
