/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyPlayer.cpp
 * Author: user
 * 
 * Created on 20 сентября 2018 г., 12:50
 */

#include "MyPlayer.h"
#include "../ConfigServer.h"
#include <iostream>
#include "../MyUDPServer.h"
#include <math.h>

using namespace std;

MyPlayer *player = nullptr;

MyPlayer::MyPlayer()  : BufferedSoundSender() {
}

MyPlayer::MyPlayer(int rate, int channels) : BufferedSoundSender(), sample_rate(rate), channels(channels){
}

MyPlayer::MyPlayer(const MyPlayer& orig) : BufferedSoundSender() {
}

MyPlayer::~MyPlayer() {
}

void MyPlayer::play(int16_t* pcm, int frm_sz){
    in_samples += frm_sz;
    if((GetTickCount() - last_stat_tm) > 1000){
        if(in_samples > AUDIO_FREQ && in_samples < AUDIO_FREQ*60 && out_samples){
            double k = static_cast<double>(out_samples) / static_cast<double>(in_samples);
            dst_sample_rate = round(AUDIO_FREQ*k);
        }
//        cout << "fifo_cnt=" << get_fifo_cnt()/AUDIO_SAMPLES << " SR=" << dst_sample_rate <<  endl;
        last_stat_tm = GetTickCount();
    }
    
    if(sample_rate == dst_sample_rate && channels == 1){
        addOpusPacket(reinterpret_cast<uint8_t*>(pcm), frm_sz*sizeof(int16_t));
    }else{
        int dst_sz = resample_size(frm_sz);
        int16_t dst[dst_sz];
        resample(pcm, frm_sz, dst);
        addOpusPacket(reinterpret_cast<uint8_t*>(dst), dst_sz*sizeof(int16_t));
    }
}

int MyPlayer::resample_size(int src_sz){
    return src_sz*dst_sample_rate/sample_rate;
}

void MyPlayer::resample(int16_t* src, int src_sz, int16_t* dst){
    int dst_sz = resample_size(src_sz);
    for(int i=0; i<dst_sz; i++){
        int j = i*sample_rate/dst_sample_rate;
        if(j < src_sz){
            dst[i] = src[j*channels];
        }
    }
}

int MyPlayer::freeFrames(){
    return FIFO_SIZE;// - fifo_cnt
}

void MyPlayer::start(ch_mask_t targets, int rate, int channels){
    cout << "Player started. targets=" << targets << endl;
    
    sample_rate = rate;
    this->channels = channels;
    
    {
//        const lock_guard<recursive_mutex> lock(fifo_mutex);
        fifo_in = fifo_out = 0;
    }
    
    pd_start.frame_length_ms = AUDIO_FRAME_MS;
    pd_start.channels = 1;
    pd_start.in_ch[0] = conf_server.cus_in;//BRD_ID_TO_CHAN(conf_server.server_addr, 0);
    pd_start.outs = targets;
    pd_start.bitrate = sample_rate*channels*16;
    pd_start.format = 0;
    
    iniOpus();
    
    BufferedSoundSender::start();
}

void MyPlayer::setTargets(ch_mask_t targets){
    pd_start.outs = targets;
}

MyPlayer* MyPlayer::create(int rate, int channels){
    if(player == nullptr){
//        cout << "Create player" << endl;
        player = new MyPlayer(rate, channels);
    }
    return player;
}

void MyPlayer::sendSoundPacket(int cnt){
    BufferedSoundSender::sendSoundPacket(cnt);
    
}
