var searchData=
[
  ['udp_5fsenders_434',['udp_senders',['../_u_d_p_sound_sender_8cpp.html#a201670397732a7cf49266d2281e2de95',1,'UDPSoundSender.cpp']]],
  ['udpchannel_435',['UDPChannel',['../class_u_d_p_channel.html',1,'UDPChannel'],['../class_u_d_p_channel.html#afa86bf0ccf435a895af351e4fbeacde9',1,'UDPChannel::UDPChannel()'],['../class_u_d_p_channel.html#adf5dce81bcf501b9a0720749cf4e0a0b',1,'UDPChannel::UDPChannel(string iface, string multicastAddr, string clientName)'],['../class_u_d_p_channel.html#aba2db23c59daedf93c432ce3b748b004',1,'UDPChannel::UDPChannel(const UDPChannel &amp;orig)']]],
  ['udpchannel_2ecpp_436',['UDPChannel.cpp',['../_u_d_p_channel_8cpp.html',1,'']]],
  ['udpchannel_2eh_437',['UDPChannel.h',['../_u_d_p_channel_8h.html',1,'']]],
  ['udpsoundsender_438',['UDPSoundSender',['../class_u_d_p_sound_sender.html',1,'UDPSoundSender'],['../class_u_d_p_sound_sender.html#a4d3e1ddb291f80e9c2199fbe9ac762c4',1,'UDPSoundSender::UDPSoundSender()'],['../class_u_d_p_sound_sender.html#acf9f0585f77f7c9c218fe7c65614578d',1,'UDPSoundSender::UDPSoundSender(const UDPSoundSender &amp;orig)']]],
  ['udpsoundsender_2ecpp_439',['UDPSoundSender.cpp',['../_u_d_p_sound_sender_8cpp.html',1,'']]],
  ['udpsoundsender_2eh_440',['UDPSoundSender.h',['../_u_d_p_sound_sender_8h.html',1,'']]],
  ['unselectmasterboard_441',['unselectMasterBoard',['../class_base_sound_sender.html#a363b20544fad433ac577a59cd2c44057',1,'BaseSoundSender']]],
  ['url_442',['url',['../struct_sessions_prms.html#a8f7fd86dcefa42c7df8a02841c4a764f',1,'SessionsPrms']]]
];
