function showAlert(title, text){
    $("#alert-title").html(title);
    $("#alert-body").html(text);
    $('#alert').modal({
        keyboard: true,
        backdrop: true 
    });
}

