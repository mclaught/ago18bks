/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketSoundSender.h
 * Author: user
 *
 * Created on 9 декабря 2019 г., 20:03
 */

#ifndef SOCKETSOUNDSENDER_H
#define SOCKETSOUNDSENDER_H

#include "BufferedSoundSender.h"
#include <memory>
#include <vector>

#define FIFO_CNT_AVG    500
#define FIFO_CNT_K      0.1

using namespace std;

class SocketSoundSender;

typedef shared_ptr<SocketSoundSender> PSocketSoundSender;
typedef vector<PSocketSoundSender> socket_sender_pool_t;

class SocketSoundSender : public BufferedSoundSender {
public:
    int sock;
    
    SocketSoundSender();
    SocketSoundSender(const SocketSoundSender& orig);
    virtual ~SocketSoundSender();
    
    void start(int sock);
    
    bool process();
    
    static PSocketSoundSender getFreeSender();
    
protected:
    virtual void cycle();
    
private:
    pd_stream_t pd_stream;
    uint32_t last_stm_tm = 0;
    
    void streamControl();

};

#endif /* SOCKETSOUNDSENDER_H */

