#ifndef MYPCM_H
#define MYPCM_H

#include "QFile"
#include "qtypeinfo.h"

class MyPCM : public QObject
{
    Q_OBJECT

private:
    QString file_name;
    QFile *file;
    int frame_duration;
    bool mono;

    void openFile(QString fName);
    bool readChunk(QFile* file, int level);
    bool checkSampleRate();
public:
    bool error;
    QString error_str;
    int frame_size;
    int file_size;
    int file_pos;
    bool isRIFF;
    qint32 format;
    qint16 audioFormat;
    qint32 sample_rate;
    qint32 byte_rate;
    qint16 channels;
    qint16 blockAlign;
    qint16 bitsPerSample;

    MyPCM(QString fileName, int frameDuration, int mono);
    ~MyPCM();

    void setError(QString err, bool fatal=true);
    int read(char* buf, int sz);
    bool eof();
    bool needResample();
    //void resample(EqList eq_list);
    void deleteTemp();
    void copyTemp(QString newFileName);
};

#endif // MYPCM_H
