TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        MyThread.cpp \
        main.cpp \
        mainthread.cpp \
        mypcm.cpp

HEADERS += \
    MyThread.h \
    mainthread.h \
    mypcm.h

LIBS += -lpthread -lstdc++fs
