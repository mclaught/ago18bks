#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <QDialog>

namespace Ui {
class Keyboard;
}

typedef struct{
    int cnt;
    QString keys;
}row_t;

class Keyboard : public QDialog
{
    Q_OBJECT

public:
    typedef enum {KB_TEXT, KB_PASS, KB_NUMERIC}kb_type_t;

    explicit Keyboard(QString title, QString text, QWidget *parent = nullptr, kb_type_t type=KB_TEXT);
    ~Keyboard();

    QString getText();
    static bool run(QString &value, QString title, QWidget *parent, kb_type_t type = KB_TEXT);
private:
    Ui::Keyboard *ui;
    kb_type_t type = KB_TEXT;
    bool shift = false;
    bool caps = false;
    bool rus = false;
    QString resultText;
    QString passText = "";

    void clear();
    void build();
    void buildNum();
    void checkShift(bool check);
    void addText(QString txt);

    void showEvent(QShowEvent* event)override;

};

extern row_t keys_e[];
extern row_t keys_e_shift[];
extern row_t keys_r[];
extern row_t keys_r_shift[];
#endif // KEYBOARD_H
