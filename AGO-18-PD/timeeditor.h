#ifndef TIMEEDITOR_H
#define TIMEEDITOR_H

#include <QWidget>

namespace Ui {
class TimeEditor;
}

class TimeEditor : public QWidget
{
    Q_OBJECT

public:
    explicit TimeEditor(QWidget *parent = nullptr);
    ~TimeEditor();

    void setTime(qint64 value);
    qint64 getTime();
private:
    Ui::TimeEditor *ui;
};

#endif // TIMEEDITOR_H
