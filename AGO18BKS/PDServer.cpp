/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PDServer.cpp
 * Author: user
 * 
 * Created on 5 декабря 2019 г., 15:42
 */

#include "PDServer.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <cstring>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>
#include <mutex>
#include <sstream>
#include "ConfigServer.h"
#include "MyUDPServer.h"
#include "cus/log.h"

PDServer pd_server;

void pd_thread_func(PDServer *__this);

PDServer::PDServer() {
}

PDServer::PDServer(const PDServer& orig) {
}

PDServer::~PDServer() {
}

void PDServer::start(){
    iface = conf_server.getNetParam("out_iface", "");
    port = conf_server.getNetParam("out_port", 47000);
    
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        log_error("PDServer socket: %s", strerror(errno));
        return;
    }
    
    struct ifreq ifr;
    ifr.ifr_ifru.ifru_addr.sa_family = AF_INET;
    strcpy(ifr.ifr_ifrn.ifrn_name, iface.c_str());
    if(ioctl(sock, SIOCGIFADDR, &ifr) == -1){
        log_error("PDServer ioctl: %s", strerror(errno));
        return;
    }
    cout << "Outer socket binds to " << inet_ntoa(((sockaddr_in*)&ifr.ifr_ifru.ifru_addr)->sin_addr) << ":" << port << endl;
    
    sockaddr_in addr;
    memset(reinterpret_cast<void*>(&addr), 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr = reinterpret_cast<sockaddr_in*>(&ifr.ifr_ifru.ifru_addr)->sin_addr;
    addr.sin_port = htons(port);
    
    if(bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1){
        log_error("PDServer bind: %s", strerror(errno));
        return;
    }
    
    if(listen(sock, 0) == -1){
        log_error("PDServer listen: %s", strerror(errno));
        return;
    }
    
    stringstream s; 
    s << "Cервер IP-пультов запущен " << inet_ntoa(addr.sin_addr) << ":" << port;
    conf_server.addServerEvent(ConfigServer::SERVER_EVT_PD, ConfigServer::EVT_NORMAL, 0, s.str().c_str());//
    
    thrd = new thread(pd_thread_func, this);

    sched_param param;
    param.sched_priority = 0;
    int err = pthread_setschedparam(thrd->native_handle(), SCHED_OTHER, &param);//main_thread.native_handle()
    if(err != 0)
        cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;

//    printf("PD server created. id=%d\n", thrd->native_handle());
}

void PDServer::stop(){
    active = false;
    thrd->join();
    delete thrd;
}

void PDServer::run(){
    while(active){
        fd_set rd_set;
        FD_ZERO(&rd_set);
        FD_SET(sock, &rd_set);
        
        int max_sock = sock;
        for(pair<int, PSocketSoundSender> c : clients){
            FD_SET(c.first, &rd_set);
            if(c.first > max_sock)
                max_sock = c.first;
        }
        
        timeval t = {1,0};
        
        int sel_n = select(max_sock+1, &rd_set, NULL, NULL, &t);
        if(sel_n == -1){
            cerr << red << "PDServer select():" << strerror(errno) << clrst << endl;
            continue;
        }else if(sel_n > 0){
            if(FD_ISSET(sock, &rd_set)){//server socket
                sockaddr_in cli_addr;
                memset(&cli_addr, 0, sizeof(sockaddr_in));
                cli_addr.sin_family = AF_INET;
                
                socklen_t cli_len = sizeof(sockaddr_in);
                
                int cli_sock = accept(sock, reinterpret_cast<sockaddr*>(&cli_addr), &cli_len);
                if(cli_sock == -1){
                    cerr << red << "PDServer accept():" << strerror(errno) << clrst << endl;
                    continue;
                }else{
//                    if(fcntl(cli_sock, F_SETFL, O_NONBLOCK) == -1){
//                        cerr << "PDServer::run fcntl(): " << strerror(errno) << endl;
//                        continue;
//                    }
                    PSocketSoundSender cli = SocketSoundSender::getFreeSender();//PSocketSoundSender(new SocketSoundSender(cli_sock));
                    cli->start(cli_sock);
                    clients[cli_sock] = cli;
                    
                    cout << "PD client connected " << inet_ntoa(cli_addr.sin_addr) << endl;
                }
            }
            set<int> to_clear;
            for(pair<int, PSocketSoundSender> pr : clients){//client sockets
                if(FD_ISSET(pr.second->sock, &rd_set)){
                    if(!pr.second->process()){
                        to_clear.insert(pr.second->sock);
                        close(pr.second->sock);
                    }
                }
            }
            for(int s : to_clear){
                clients[s]->stop();
                //delete clients[s];
                clients.erase(s);
            }
        }
    }
}

void pd_thread_func(PDServer *__this){
    __this->run();
}
