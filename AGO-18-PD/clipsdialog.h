#ifndef CLIPSDIALOG_H
#define CLIPSDIALOG_H

#include <QDialog>
#include <QListWidgetItem>

namespace Ui {
class ClipsDialog;
}

class ClipsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ClipsDialog(QWidget *parent = nullptr);
    ~ClipsDialog();

    QString getClip(QString title,QString dir);
private slots:
    void on_listClips_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::ClipsDialog *ui;
};

#endif // CLIPSDIALOG_H
