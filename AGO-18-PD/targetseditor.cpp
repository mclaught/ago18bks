#include "keyboard.h"
#include "targetseditor.h"
#include "ui_targetseditor.h"

#include <QFile>
#include <QPushButton>

TargetsEditor::TargetsEditor(QString path, int targets_cnt, QWidget *parent) :
    QDialog(parent), path(path), targets_cnt(targets_cnt),
    ui(new Ui::TargetsEditor)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Сохранить");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    load();
}

TargetsEditor::~TargetsEditor()
{
    delete ui;
}

void TargetsEditor::load(){
    QFile file(path);

    if(file.open(QIODevice::ReadOnly)){
        ui->targets->clear();

        QString line;
        while(!(line = QString(file.readLine())).isEmpty()){
            while(line.endsWith("\r") || line.endsWith("\n"))
                line = line.remove(line.length()-1,1);
            ui->targets->addItem(line);
        }
    }
    while(ui->targets->count() < targets_cnt){
        ui->targets->addItem(QString("%1").arg(ui->targets->count()+1));
    }
}

void TargetsEditor::save()
{
    QFile file(path);

    if(file.open(QIODevice::WriteOnly)){
        for(int i=0; i<ui->targets->count(); i++){
            file.write((ui->targets->item(i)->text()+"\r\n").toUtf8());
        }
        file.close();
    }
}

void TargetsEditor::on_targets_itemClicked(QListWidgetItem *item)
{
    Keyboard kb("Название направления", item->text(), this);
    if(kb.exec()){
        item->setText(kb.getText());
    }
}

