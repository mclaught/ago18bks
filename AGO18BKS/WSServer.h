/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WSServer.h
 * Author: user
 *
 * Created on 24 ноября 2020 г., 15:02
 */

#ifndef WSSERVER_H
#define WSSERVER_H

#include <set>
#include <shared_mutex>
#include <functional>

#include "cus/MyThread.h"
#include "MyWSClient.h"
#include "proto.h"

using namespace std;

class WSServer : public MyThread {
public:
    WSServer(int port);
    WSServer(const WSServer& orig);
    virtual ~WSServer();
    
    function<void(shared_ptr<MyWSClient> client)> onclient;
    
    void send(string type, string str);
private:
    int sock;
    map<int, shared_ptr<MyWSClient>> clients;
    shared_mutex clients_mutex;
    
    virtual void exec();
    map<int, shared_ptr<MyWSClient>> get_clients();
    void remove_client(int s);
};

#endif /* WSSERVER_H */

