/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyPlayer.h
 * Author: user
 *
 * Created on 20 сентября 2018 г., 12:50
 */

#ifndef MYPLAYER_H
#define MYPLAYER_H

#include <stdint.h>
#include "../BufferedSoundSender.h"
#include "../proto.h"

class MyPlayer : public BufferedSoundSender {
public:
    MyPlayer();
    MyPlayer(int rate, int channels);
    MyPlayer(const MyPlayer& orig);
    virtual ~MyPlayer();
    
    virtual void start(ch_mask_t targets, int rate, int channels);
    void play(int16_t* pcm, int frm_sz);
    virtual void sendSoundPacket(int cnt);
    int freeFrames();
    void setTargets(ch_mask_t targets);
    
    static MyPlayer* create(int rate, int channels);
private:
    bool alsa_ok;
    int sample_rate;
    int channels;
    int dst_sample_rate = AUDIO_FREQ;

    int resample_size(int src_sz);
    void resample(int16_t* src, int src_sz, int16_t* dst);
};

#endif /* MYPLAYER_H */

