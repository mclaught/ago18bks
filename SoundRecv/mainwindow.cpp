#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtMultimedia/QAudioFormat>

#include <QAudioDeviceInfo>
#include <QAudioOutput>
#include <QTimer>
#include <QUdpSocket>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QAudioFormat m_format;
    m_format.setSampleRate(48000);
    m_format.setChannelCount(1); //set channels to mono
    m_format.setSampleSize(16); //set sample sze to 16 bit
    m_format.setSampleType(QAudioFormat::SignedInt ); //Sample type as usigned integer sample
    m_format.setByteOrder(QAudioFormat::LittleEndian); //Byte order
    m_format.setCodec("audio/pcm"); //set codec as simple audio/pcm

    auto device = QAudioDeviceInfo::defaultOutputDevice();

    m_audioOut = new QAudioOutput(device, m_format, this);
    m_audioOut->setNotifyInterval(40);
    m_audioOut->setBufferSize(AUDIO_SAMPLES*32*sizeof(int16_t));
    m_audioDev = m_audioOut->start();

    qDebug() << m_audioOut->bufferSize();

    udp = new QUdpSocket(this);
    udp->bind(QHostAddress::Any, 46000);
    connect(udp, &QUdpSocket::readyRead, this, &MainWindow::onRead);

    QElapsedTimer* statTm = new QElapsedTimer();
    statTm->start();

    QTimer* timer = new QTimer(this);
    timer->setTimerType(Qt::TimerType::PreciseTimer);
    connect(timer, &QTimer::timeout, [this, statTm](){
        const int wrCnt = 8;

        if(stoped && buf_cnt >= AUDIO_SAMPLES*30){
            stoped = false;
        }
        if(!stoped && buf_cnt < AUDIO_SAMPLES*wrCnt){
            stoped = true;
        }
//        stoped = false;

        if(!stoped && buf_cnt>=AUDIO_SAMPLES*wrCnt && m_audioOut->bytesFree()>=AUDIO_SAMPLES*wrCnt*sizeof(int16_t)){
            m_audioDev->write(reinterpret_cast<char*>(&buf[buf_out]), AUDIO_SAMPLES*wrCnt*sizeof(int16_t));
            buf_out += AUDIO_SAMPLES*wrCnt;
            if(buf_out >= AUDIO_SAMPLES*BUF_SZ)
                buf_out = 0;
            buf_cnt -= AUDIO_SAMPLES*wrCnt;

//            int bufSz = (19200-m_audioOut->bytesFree())/AUDIO_SAMPLES/sizeof(int16_t)*AUDIO_FRAME_MS;
        }

        if(statTm->elapsed() >= 1000){
            int fifoSz = buf_cnt/AUDIO_SAMPLES*AUDIO_FRAME_MS;
            ui->labelFifo->setText(QString::number(fifoSz));

            int plSz = (m_audioOut->bufferSize()-m_audioOut->bytesFree())/AUDIO_SAMPLES/sizeof(int16_t)*AUDIO_FRAME_MS;
            ui->labelPlayerBuf->setText(QString::number(plSz));

            statTm->restart();
        }
    });
    timer->start(1);

    lost_n = 0;
    ui->labelLost->setText(QString::number(lost_n));
}

void MainWindow::onRead(){
    packet_audio_t pack;
    auto rd = udp->readDatagram(reinterpret_cast<char*>(&pack), sizeof(packet_audio_t));

    if(pack.hdr.type == 0 && pack.cmd == 9){
        qDebug() << m_audioOut->periodSize();

        if((AUDIO_SAMPLES*BUF_SZ - buf_cnt) >= AUDIO_SAMPLES){
            memcpy(&buf[buf_in], pack.pcm, AUDIO_SAMPLES*sizeof(int16_t));
            buf_in += AUDIO_SAMPLES;
            if(buf_in >= AUDIO_SAMPLES*BUF_SZ)
                buf_in = 0;
            buf_cnt += AUDIO_SAMPLES;
        }

        int lost = (int)pack.hdr.repeate_cnt - (int)last_n;
        if(lost < 0)
            lost += 256;
        if(lost){
            lost_n += lost;
            ui->labelLost->setText(QString::number(lost_n));
        }
        last_n = pack.hdr.repeate_cnt+1;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btPlay_clicked()
{
    lost_n = last_n = 0;
    ui->labelLost->setText(QString::number(lost_n));
}
