class MainPage extends Page{
    init(){
        super.init();
        this.view = $("<div>");
        this.settings = {};
        update();
    }
    
    async update(){
        this.view = await loadView("mainview.html");
        this.settings = (new Rest()).command("get_settings");
        setState();
    }
    
    build(){
        (new StateTable(this.view.find("#commtable"))).init();
        (new EventsView(this.view.find("#events"))).init();
        return this.view;
    }
}