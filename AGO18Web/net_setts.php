<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");

$dhcp = filter_input(INPUT_POST, 'dhcp');
$ip = filter_input(INPUT_POST, 'ip');
$mask = filter_input(INPUT_POST, 'mask');
$gate = filter_input(INPUT_POST, 'gate');
$limit = filter_input(INPUT_POST, 'limit');

$netText = "auto eth0\n".
        "allow-hotplug eth0\n".
        "iface eth0 inet $dhcp\n";
if($dhcp == 'static'){
    $netText .= "\taddress $ip\n".
        "\tnetmask $mask\n".
        "\tgateway $gate\n";
}
if($limit == "true"){
    $netText .= "post-up mii-tool eth0 -A 100baseTx-FD\n";
}

echo $netText;

file_put_contents("/tmp/eth0", $netText);// /etc/network/interfaces.d/eth0
echo exec("sudo /usr/bin/cp -f /tmp/eth0 /etc/network/interfaces.d");
