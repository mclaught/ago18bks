<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json");

//if(file_exists("/run/.ago18bks_busy")){
//    $js = (object)[];
//    $js->result = "error";
//    $js->error = "Busy";
//    echo json_encode($js);
//    exit(0);
//}


$sock = socket_create(AF_UNIX, SOCK_STREAM, 0);
socket_connect($sock, "/var/www/html/AGO18Web/files/agobks"); //

$xml = "";
if(key_exists('query', $_GET))
    $xml = filter_input(INPUT_GET, 'query')."\r\n\r\n\0";
else
    $xml = filter_input(INPUT_POST, 'query')."\r\n\r\n\0";
socket_send($sock, $xml, strlen($xml), 0);

$ans1 = socket_read($sock, 100000);

$sock_answer = str_replace(["\r\n", "\r", "\n"], '',  $ans1);

if(strpos($xml, "backup")){
    header('Content-Disposition: attachment; filename="backup.json"');
    header("ContentType: text/plain");
}

echo $sock_answer;

//socket_close($sock);
