/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Snd2Json.h
 * Author: user
 *
 * Created on 13 ноября 2020 г., 20:32
 */

#ifndef SND2JSON_H
#define SND2JSON_H

#include "json.hpp"
#include "proto.h"

using namespace nlohmann;

#define MAX_SHIFT   1024
//#define SND2JSON_DEBUG

class Snd2Json {
public:
    Snd2Json(uint8_t* buf, std::string name);
    Snd2Json(const Snd2Json& orig);
    virtual ~Snd2Json();
    
    json codec_capabilities_to_json(uint8_t *&p);
    json eq_capabilities_to_json(uint8_t *&p);
    json hpf_capabilities_to_json(uint8_t *&p);
    json pa_capabilities_to_json(uint8_t *&p);
    json snd_in_capab_to_json(uint8_t* &p);
    json snd_out_capab_to_json(uint8_t *&p);
    json eq_param_to_json(uint8_t* &p);
    json eq_common_to_json(uint8_t* &p);
    json snd_in_cfg_to_json(uint8_t* &p, int index);
    json snd_out_cfg_to_json(uint8_t* &p, int index);
    json snd_out_capab_init_packet_to_json(uint8_t* &p, int index);
private:
    uint8_t* _buf;

    void shift(uint8_t* &p, int sz, int dbg_n);
};

#endif /* SND2JSON_H */

