#include "statews.h"
#include "json.hpp"
#include "log.h"

#include <QFileInfo>

using namespace nlohmann;

StateWS::StateWS(QString ip, int port, QObject *parent) : QObject(parent)
{
    if(port){
        _url = QString("ws://%1:%2").arg(ip).arg(port);

        ws = new QWebSocket();
        connect(ws, &QWebSocket::connected, this, &StateWS::onConnected);
        connect(ws, &QWebSocket::disconnected, this, &StateWS::onDisconnected);
        connect(ws, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
        connect(ws, &QWebSocket::textMessageReceived, this, &StateWS::onTextMessageReceived);
        ws->open(QUrl(_url));
    }
}

void StateWS::close(){
    if(ws){
        ws->close();
        ws->deleteLater();
        ws = nullptr;
    }

}

void StateWS::onConnected(){
    qDebug() << "WS connected";
    try{
        json js = json::object({
            {"type", "pd"}
        });
        ws->sendTextMessage(js.dump().c_str());

        getSetts();
        getClips();
    }catch(json::exception &e){
        qDebug() << e.what();
    }

    emit connected();
}

void StateWS::onDisconnected()
{
    if(ws){
        ws->deleteLater();
        ws = nullptr;
    }
    emit disconnected();
}

void StateWS::onError(QAbstractSocket::SocketError error)
{
    (void)error;

    if(ws){
        ws->deleteLater();
        ws = nullptr;
    }
    emit disconnected();
}

void StateWS::onTextMessageReceived(QString message){
    qDebug() << message;
    uint64_t outs = 0;
    try{
        json js = json::parse(message.toStdString());
        if(js.contains("outs")){
            outs = js["outs"];
//            for(json in : js["comm_table"]){
//                if(in["online"]){
//                    outs |= (uint64_t)in["out"];
//                }
//            }
            emit onActivity(outs);
        }
        if(js.contains("command")){
            qDebug() << QString::fromStdString(js.dump());
            std::string cmd = js["command"];
            if(cmd == "get_settings"){
                if(js.contains("settings") && js["settings"].contains("outs") && js["settings"].contains("outs_cnt")){
                    QStringList names;
                    for(json out : js["settings"]["outs"]){
                        names.push_back(QString::fromStdString(out["name"]));
                    }
                    emit onNames(js["settings"]["outs_cnt"], names);
                }
            }else if(cmd == "get_clips"){
                if(js.contains("clips")){
                    QStringList clips;
                    for(json &j_clip : js["clips"]){
                        std::string s_clip = j_clip;
                        QFileInfo info(QString::fromStdString(s_clip));
                        clips.push_back(info.fileName());
                    }
                    emit onClips(clips);
                }
            }
        }
    }catch(json::exception &e){
        qDebug() << e.what();
        _log.log("StateWS::onTextMessageReceived: "+QString(e.what()));
    }
}

void StateWS::playClip(QString name, int source, uint32_t outs){
    try{
        json js = json::object({
                                   {"command", "play_clip"},
                                   {"clip", "pd/"+name.toStdString()},
                                   {"source", source},
                                   {"outs", outs}
                               });
        std::string msg = js.dump();
        ws->sendTextMessage(QString::fromStdString(msg));
    }catch(json::exception &e){
        qDebug() << e.what();
    }
}

void StateWS::stopClip(int source){
    try{
        json js = json::object({
                                   {"command", "stop_clip"},
                                   {"source", source}
                               });
        std::string msg = js.dump();
        ws->sendTextMessage(QString::fromStdString(msg));
    }catch(json::exception &e){
        qDebug() << e.what();
    }
}

void StateWS::getSetts(){
    try{
        json js = json::object({
                                   {"command", "get_settings"}
                               });
        std::string msg = js.dump();
        ws->sendTextMessage(QString::fromStdString(msg));
    }catch(json::exception &e){
        qDebug() << e.what();
    }
}

void StateWS::getClips(){
    try{
        json js = json::object({
                                   {"command", "get_clips"},
                                   {"dir", "pd"}
                               });
        std::string msg = js.dump();
        ws->sendTextMessage(QString::fromStdString(msg));
    }catch(json::exception &e){
        qDebug() << e.what();
    }
}
