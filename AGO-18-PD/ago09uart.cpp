#include "ago09uart.h"
#include <QSerialPortInfo>
#include <QDebug>
#include <QDateTime>
#include <QThread>
#include <QtConcurrent/QtConcurrent>
#include "log.h"
#include <QSettings>



extern QSettings setts;

Ago09Uart::Ago09Uart(QObject *parent) : QObject(parent)
{

    _log.log(QString("UART bytes   \t%1").arg(uart_bytes));
//    timer = new QTimer(this);
//    connect(timer, &QTimer::timeout, [this](){
//        auto tm = QDateTime::currentMSecsSinceEpoch();
//        if((tm - last_send_tm) >= 2000){
//            setTargets(targets);
//            last_send_tm = tm;
//        }
//        if(port){
//            bool bks_state = isBKSReady();
//            if(last_bks_state != bks_state){
//                last_bks_state = bks_state;
//                emit bksReady(bks_state);
//            }

//            bool tangenta_state = isTangentaPressed();
//            if(tangenta_state != last_tangenta_state){
//                last_tangenta_state = tangenta_state;
//                emit tangenta(tangenta_state);
//            }
//        }
//    });
}

Ago09Uart::~Ago09Uart()
{
}

QStringList Ago09Uart::portsAvailable()
{
    QStringList ports;

    QSerialPortInfo info;
    for(QSerialPortInfo &port : info.availablePorts()){
        ports.append(port.portName()+" = "+port.description());
    }

    return ports;
}

bool Ago09Uart::open(QString name, qint32 baud)
{
    if(port && port->isOpen())
        close();

    port = new QSerialPort(name);
    port->setBaudRate(baud);//
    port->setDataBits(QSerialPort::Data8);
    port->setStopBits(QSerialPort::TwoStop);
    port->setFlowControl(QSerialPort::NoFlowControl);//NoFlowControl
    port->setParity(QSerialPort::NoParity);

    connect(port, &QSerialPort::errorOccurred, this, &Ago09Uart::portError);
    connect(port, &QSerialPort::readyRead, this, &Ago09Uart::readyRead);
    connect(port, &QSerialPort::bytesWritten, [](qint64 bytes){
        _log.log(QString("Writen  \t%1").arg(bytes));
    });

    bool res = port->open(QSerialPort::ReadWrite);
    if(!res){
        qCritical() << port->errorString();
//        port->deleteLater();
//        port = nullptr;
        emit connectState(Ago09UartStates::error);
    }else{
//        timer->start(100);
        qDebug() << "Port opened " << name;
        setTargets(static_cast<targets_t>(targets));
        emit bksReady(isBKSReady());
        setCastState(false);
        emit connectState(Ago09UartStates::connected);
    }

    startThread();

    return res;
}

void Ago09Uart::portError(QSerialPort::SerialPortError error){
    if(error != QSerialPort::NoError){
        qCritical() << "Port error:" << error << port->errorString();
        if(port->isOpen()){
            port->close();
            qDebug() << "Port closed";
        }
        emit connectState(Ago09UartStates::error);
    }
}

void Ago09Uart::readyRead()
{
    while(port->bytesAvailable()){
        auto tm = QDateTime::currentMSecsSinceEpoch();
        if((tm-recv_buf_tm) > 100){
            recv_buf_i = 0;
        }
        recv_buf_tm = tm;

        port->read(reinterpret_cast<char*>(&recv_buf[recv_buf_i++]), 1);

        if(recv_buf_i == uart_bytes){
            processPacket();
        }
    }
}

void Ago09Uart::processPacket(){
    targets_t targs = *reinterpret_cast<targets_t*>(recv_buf);
    if(uart_bytes==4)
        targs = qToLittleEndian(qFromBigEndian(targs));
    else
        targs = qToLittleEndian(qFromBigEndian(static_cast<uint16_t>(targs)));

    _log.log(QString("ОтБКС   \t%1").arg(targs, uart_bytes*2, 16, QChar('0')));

    emit busyTargets(targs, cast_state);
}

void Ago09Uart::close()
{
    if(port && port->isOpen()){
        signalsWorker->stop();
        signalsThread->wait(1000);
        signalsThread->deleteLater();
//        signalsTimer.stop();
        checkPortTimer.stop();

        {
            std::unique_lock<std::mutex> lock(mtx);

            port->close();
            port->deleteLater();
            port = nullptr;
        }

        emit connectState(Ago09UartStates::disconnected);
        qDebug() << "Port closed";

//        thread.waitForFinished();
    }
}

void Ago09Uart::setTargets(targets_t targets)
{
    this->targets = targets;


    if(port && port->isOpen()){
        std::unique_lock<std::mutex> lock(mtx);

    targets_t trg = uart_bytes==4 ?
                ~qToBigEndian(qFromLittleEndian(targets)) :
                ~qToBigEndian(qFromLittleEndian(static_cast<uint16_t>(targets)));

#if (INTER_BYTE_DELAY == 0)
    char* data = reinterpret_cast<char*>(&trg);
    _log.log(QString("К БКС   \t%1").arg(targets, uart_bytes*2, 16, QChar('0')));
    port->write(data, uart_bytes);
    port->flush();
    port->waitForBytesWritten(10);
    qDebug() << "writen";
#else
    for(int i=0; i<uart_bytes; i++){
        auto wr = port->write((char*)&trg, 1);
        trg >>= 8;
        port->waitForBytesWritten(10);
        QThread::usleep(INTER_BYTE_DELAY);
    }
#endif

    }
}

bool Ago09Uart::isBKSReady()
{
    std::unique_lock<std::mutex> lock(mtx);
    bool ready = false;

    if(port && port->isOpen()){

        auto sign = port->pinoutSignals();
        ready = (sign & QSerialPort::DataSetReadySignal) != 0;

#ifdef PORT_DEBUG
        ready = true;
#endif
    }
    return ready;//
}

bool Ago09Uart::isTangentaPressed()
{
    std::unique_lock<std::mutex> lock(mtx);

    bool pressed = false;
    if(port && port->isOpen()){

        auto sign = port->pinoutSignals();
        pressed = (sign & QSerialPort::ClearToSendSignal) == 0;
#ifdef PORT_DEBUG
        pressed = tangentaEmu;
#endif
    }
    return pressed;
}

void Ago09Uart::setCastState(bool state)
{
    std::unique_lock<std::mutex> lock(mtx);

    cast_state = state;

    if(port && port->isOpen()){
        port->setRequestToSend(state);
    }

    _log.log(QString("Cast    \t%1").arg(state));
}

void Ago09Uart::setTargetsCnt(int val)
{
    targets_cnt = val;
    uart_bytes = targets_cnt >> 3;
}

void Ago09Uart::startThread()
{
    qDebug() << "Port thread start";

    connect(&checkPortTimer, &QTimer::timeout, this, &Ago09Uart::onCheckPortTimer);
    checkPortTimer.start(500);

    int sign_thrd_period = setts.value("port_period", 50).toInt();

//    connect(&signalsTimer, &QTimer::timeout, this, &Ago09Uart::onSignalsTimer);
//    signalsTimer.start(setts.value("port_period", 50).toInt());

//    thread = QtConcurrent::run([this,sign_thrd_period](){
//        while(port){
//            onSignalsTimer();
//            QThread::usleep(sign_thrd_period*1000);
//        }
//    });

    signalsThread = new QThread();
    signalsWorker = new Ago09SingalWorker(this, sign_thrd_period);
    signalsWorker->moveToThread(signalsThread);
    connect(signalsThread, &QThread::started, signalsWorker, &Ago09SingalWorker::run);
    connect(signalsWorker, &Ago09SingalWorker::stoped, signalsThread, &QThread::terminate);
    signalsThread->start();
    signalsThread->setPriority(QThread::TimeCriticalPriority);

    qDebug() << "Port thread started";

}

void Ago09Uart::onCheckPortTimer(){
//    auto tm = QDateTime::currentMSecsSinceEpoch();
    if(cast_state){ // && (tm - last_send_tm) >= TIMER_INTERVAL
        setTargets(static_cast<targets_t>(targets));
//        last_send_tm = tm;
    }
    if(port){
        if(!port->isOpen()){
            qDebug() << "Try open port...";
            emit connectState(Ago09UartStates::tryConnect);
            if(port->open(QSerialPort::ReadWrite)){
                setTargets(static_cast<targets_t>(targets));
                qDebug() << "Reopen port";
                emit connectState(Ago09UartStates::connected);
            }
        }
    }
}

void Ago09Uart::onSignalsTimer()
{
    bool tangenta_state = isTangentaPressed();
    if(tangenta_state != last_tangenta_state){
        setTargets(static_cast<targets_t>(targets));

        _log.log(QString("Танг    \t %1").arg(tangenta_state));
        last_tangenta_state = tangenta_state;
        emit tangenta(tangenta_state);
    }

    bool bks_state = isBKSReady();
    if(last_bks_state != bks_state){
        _log.log(QString("БКС     \t%1").arg(bks_state));
        last_bks_state = bks_state;
        emit bksReady(bks_state);
    }

}


Ago09SingalWorker::Ago09SingalWorker(Ago09Uart *uart, int period, QObject *parent): uart(uart), period(period)
{

}

Ago09SingalWorker::~Ago09SingalWorker()
{

}

void Ago09SingalWorker::run()
{
    while(running && uart){
        uart->onSignalsTimer();
        QThread::usleep(period*1000);
    }
    emit stoped();
}

void Ago09SingalWorker::stop()
{
    running = false;
}
