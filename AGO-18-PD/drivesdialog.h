#ifndef DRIVESDIALOG_H
#define DRIVESDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QStorageInfo>

namespace Ui {
class DrivesDialog;
}

class DrivesDialog : public QDialog
{
    Q_OBJECT

public:
    QString path = "";

    explicit DrivesDialog(QWidget *parent = nullptr);
    ~DrivesDialog();

private slots:
    void on_listDrives_currentRowChanged(int currentRow);

private:
    Ui::DrivesDialog *ui;
    QList<QStorageInfo> drives;
};

#endif // DRIVESDIALOG_H
