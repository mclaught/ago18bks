#include "wordwrapcheck.h"
#include "ui_wordwrapcheck.h"

WordWrapCheck::WordWrapCheck(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WordWrapCheck)
{
    ui->setupUi(this);
}

WordWrapCheck::~WordWrapCheck()
{
    delete ui;
}

void WordWrapCheck::setText(QString text)
{
    ui->label->setText(text);
}

void WordWrapCheck::setChecked(bool checked)
{
    ui->checkBox->setChecked(checked);
}

bool WordWrapCheck::isChecked()
{
    return ui->checkBox->isChecked();
}

QCheckBox *WordWrapCheck::getCheckBox()
{
    return ui->checkBox;
}
