/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Session.cpp
 * Author: user
 * 
 * Created on 20 сентября 2018 г., 11:41
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sstream>
#include <sys/types.h> 
#include <sys/wait.h>
#include <string.h>
#include "Session.h"
#include "log.h"
#include "../MyUDPServer.h"
#include "RTP.h"

Session::Session() : MyThread(){
    
}

Session::Session(uint32_t id, string url, string iface) : MyThread() {
    this->id = id;
    this->iface = iface;
    this->url = url;
    this->targetsMask = 0;
    
//    shared_buf = (shared_buf_t*)mmap(NULL, sizeof(shared_buf_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
//    shared_buf->targets = 0;
//    shared_buf->active = true;
    
    gettimeofday(&recv_tm,0);
}

void Session::log(string str, bool error){
    if(!error)
        cout << yellow << name << ": " << str << clrst << endl;
    else
        cerr << red << name << ": " << str << clrst << endl;
}

void Session::init(){
//    active = true;
    
    ostringstream name_stm;
    name_stm << "Session(" << id << ")";
    name = name_stm.str();
    log("Create " + url + " Iface: " + iface);
    
    if(!parseUrl(url, host, port)){
        log("Error URL parsing", true);
    }
    
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1){
        log("socket() "+string(strerror(errno)), true);
        return;
    }
    
    sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        .sin_addr = {
            .s_addr = INADDR_ANY
        }
    };
//    if(iface != "")
//        inet_aton(iface.c_str(), &(addr.sin_addr));
//    else
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1){
        log("bind() "+string(strerror(errno)), true);
        return;
    }
    
    struct ip_mreq mreq;
    inet_aton(host.c_str(), &(mreq.imr_multiaddr));
    if(iface != "")
        inet_aton(iface.c_str(), &(mreq.imr_interface));
    else
        mreq.imr_interface.s_addr = INADDR_ANY;
    if( setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 ){
        log("membership() "+string(strerror(errno)), true);
        return;
    }
    
    mpgError("Init", mpg123_init());

    int error;
    mh = mpg123_new(nullptr, &error);
    mpgError("New decoder", error);
    
    mpgError("New decoder", mpg123_param(mh, MPG123_FORCE_RATE, AUDIO_FREQ, 0));
    mpgError("New decoder", mpg123_param(mh, MPG123_FLAGS, MPG123_MONO_MIX, 0));
    
    mpgError("Open feed", mpg123_open_feed(mh));
    
//    shared_buf->active = true;
}

void Session::close_me(){
//    active = false;
    
    struct ip_mreq mreq;
    inet_aton(host.c_str(), &(mreq.imr_multiaddr));
    if(iface != "")
        inet_aton(iface.c_str(), &(mreq.imr_interface));
    else
        mreq.imr_interface.s_addr = INADDR_ANY;
    if( setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 ){
        log("drop membership: "+string(strerror(errno)), true);
        return;
    }
    
    close(sock);
    mpg123_close(mh);
    mpg123_delete(mh);
    
//    if(player)
    player.stop();
    
//    munmap(shared_buf, sizeof(shared_buf_t));
    
    log("closed");
}

void Session::exec(){
    init();
    while(!terminated){//
        run();

        timeval tv;
        gettimeofday(&tv,0);
        if((tv.tv_sec - recv_tm.tv_sec) > SESSION_TIMEOUT){
            terminated = true;
            log("timed out");
        }
    }
    close_me();
}

//bool Session::start(){
//    pid = fork();
//    
//    if(pid == 0){
//        exit(0);
//    }else if(pid < 0){
//        cerr << red << "Fork:" << strerror(errno) << clrst << endl;
//    }
//    cout << "New process " << pid << endl;
//    
//    return pid>0;
//}
//
//void Session::stop(){
//    shared_buf->active = false;
//    if(pid > 0)
//        waitpid(pid, NULL, 0);
//    cout << "Process stoped " << pid << endl;
//}

Session::Session(const Session&) : MyThread() {
}

Session::~Session() {
    log("destroyed");
}

bool Session::parseUrl(string url, string &host, uint16_t &port){
    auto p = url.find("//");
    if(p == string::npos)
        return false;
    
    string host_port = url.substr(p+2);
    p = host_port.find(":");
    if(p != string::npos){
        host = host_port.substr(0, p);
        string sPort = host_port.substr(p+1);
        port = atoi(sPort.c_str());
    }else{
        return false;
    }
    return true;
}

void Session::sendData(string data){
    
}

void Session::sendDataTo(string data, sockaddr_in addr){}

void Session::onTime(){
    
}

void Session::onRead(){
    if(tm_mp3_first==0)
        tm_mp3_first = GetTickCount_us();
    
    uint8_t packet[2048];
    sockaddr_in from_addr;
    memset(&from_addr, 0, sizeof(sockaddr_in));
    from_addr.sin_family = AF_INET;
    socklen_t addr_len = sizeof(sockaddr_in);
    
    ssize_t sz = recvfrom(sock, packet, 2048, MSG_DONTWAIT, (sockaddr*)&from_addr, &addr_len);
    if(sz == -1 && errno != EAGAIN){
        log(strerror(errno), true);
        return;
    }
    
    if(sz>0){
        gettimeofday(&recv_tm,0);
        
        //декодирование, воспроизведение
        
        //RTP* rtp = reinterpret_cast<RTP*>(data);
        int payload_sz = 0;
//        int seq_n = 0;
//        uint32_t rtp_time;
        RTP::rtp_hdr_t hdr;
        uint8_t* payload = RTP::decode(packet, sz, &payload_sz, &hdr);

        if(payload){
            char pcm[PCM_SZ];
            size_t pcm_sz = 0;
            int err = mpg123_decode(mh,
                                    payload,
                                    payload_sz,
                                    reinterpret_cast<unsigned char*>(pcm),
                                    PCM_SZ,
                                    &pcm_sz);
            //std::cout << dec << "Decoded " << pcm_sz << " err=" << err << endl;

            uint32_t tm = ntohl(hdr.time)/100;
            if(start_tm == 0)
                start_tm = tm;

            switch(err){
                case MPG123_NEW_FORMAT:{
//                    std::cout << yellow << "NEW FORMAT: " << clrst;

                    int encoding;
                    if(mpgError("Get format", mpg123_getformat(mh, &rate, &channels, &encoding))){
                        return;
                    }
//                    std::cout << yellow << rate << " " << channels << clrst << std::endl;

                    player.start(targetsMask, rate, channels);
                    break;
                }

                case MPG123_OK:
                case MPG123_NEED_MORE:{
                    //Write PCM data to device
                    if(channels && rate && pcm_sz){
                        int avail = player.freeFrames();
                        if(pcm_sz/2/channels <= static_cast<size_t>(avail)){
                            //std::cout << green << dec << "                        PLAY " << (tm-start_tm) << " " << avail << clrst << "\r";//std::endl;
                            player.play((int16_t*)pcm, pcm_sz/channels/2);
                            if(tm_mp3_play==0){
                                tm_mp3_play = GetTickCount_us();
//                                std::cout << green << dec << "PLAY DELAY " << (tm_mp3_play - tm_mp3_first) << clrst << endl;
                            }
                        }else{
//                            std::cout << green << dec << "\t\tSKIP " << pcm_sz << " " << avail << clrst << "\r";
                        }
                    }
                    break;
                }

                case MPG123_DONE:
                    terminate();
                    break;

                default:
                    mpgError("Decode", err);
            }
        }
    }else{
        usleep(100);
    }
}

bool Session::mpgError(string pref, int error)
{
    if(error != 0)
        std::cerr << red << pref << ": " << mpg123_plain_strerror(error) << clrst << std::endl;
    return error != 0;
}
//
//int Session::findTargetId(string tgt){
//    int i = 0;
//    for(auto t : targetsList){
//        if(t == tgt)
//            return i;
//        i++;
//    }
//    return -1;
//}

void Session::addTarget(int tgt){
    targetsMask |= (1 << tgt);//shared_buf->targets
    player.setTargets(targetsMask);
    log("targets = "+to_string(targetsMask));
//    int tgtId = findTargetId(tgt);
//    if(tgtId != -1){
//        //targets.insert(tgt);
//        targetsMask |= (1 << tgtId);
//    }
}

void Session::removeTarget(int tgt){
    targetsMask &= ~(1 << tgt);//shared_buf->targets
    player.setTargets(targetsMask);
    log("targets = "+to_string(targetsMask));
//    int tgtId = findTargetId(tgt);
//    if(tgtId != -1){
//        //targets.erase(tgt);
//        targetsMask &= ~(1 << tgtId);
//    }
}

void Session::addTargets(targets_t tgt_mask){
    targetsMask |= tgt_mask;//shared_buf->targets
    player.setTargets(targetsMask);
    log("targets = "+to_string(targetsMask));
}

void Session::removeTargets(targets_t tgt_mask){
    targetsMask &= ~tgt_mask;//shared_buf->targets
    player.setTargets(targetsMask);
    log("targets = "+to_string(targetsMask));
}

targets_t Session::getTargets(){
    return targetsMask;//shared_buf->targets
}

void Session::run(){
    fd_set rd_set;
    FD_ZERO(&rd_set);
    FD_SET(sock, &rd_set);
    timeval tv = {1, 0};

    int cnt = select(sock+1, &rd_set, nullptr, nullptr, &tv);
    if(cnt == -1){
        log("select() "+string(strerror(errno)), true);
        usleep(1000);
    }else if(cnt > 0){
        if(FD_ISSET(sock, &rd_set)){
            onRead();
        }
    }else{
        usleep(1000);
    }
}
