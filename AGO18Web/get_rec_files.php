<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$path = "/var/www/html/AGO18Web/rec";

$files = scandir($path, SCANDIR_SORT_DESCENDING);

if($_GET['cmd'] == "count"){
    echo count($files)-2;
}

if($_GET['cmd'] == "files"){
    $array = [];
    $start = $_GET['start'];
    $count = $_GET['count'];
    for($i=$start; $i<($start+$count); $i++){
        if($i >= count($files))
            break;
        
        $file = $files[$i];

        if(!is_dir($file)){
            $year = (int)substr($file, 4, 4);
            $mon = (int)substr($file, 8, 2);
            $day = (int)substr($file, 10, 2);
            $hour = (int)substr($file, 13, 2);
            $min = (int)substr($file, 15, 2);
            $sec = (int)substr($file, 17, 2);
            $in = (int)substr($file, 20, 2);
            $outs = (strlen($file) > 27) ? substr($file, 23, 8) : "0";

            $objFile = [
                "year" => $year,
                "mon"  => $mon,
                "day"  => $day,
                "hour" => $hour,
                "min"  => $min,
                "sec"  => $sec,
                "datetime" => strtotime("$year/$mon/$day $hour:$min:$sec"),
                "in"  => $in,
                "outs" => $outs,
                "path" => "rec/".$file
            ];

            $array[] = $objFile;

    //        echo "$day-$mon-$year $hour:$min:$sec  Ch:$ch<br>";
        }
    } 

    echo json_encode($array);
}