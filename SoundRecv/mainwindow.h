#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAudioOutput>
#include <QMainWindow>
#include <QThread>
#include <QUdpSocket>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define AUDIO_FRAME_MS		5
#define AUDIO_FREQ		48000
#define AUDIO_SAMPLES		(AUDIO_FRAME_MS * AUDIO_FREQ / 1000)
#define AUDIO_LEN_BYTES         AUDIO_SAMPLES * 2

#define BUF_SZ 1000

#pragma pack(push, 1)
typedef struct
{
  uint8_t adr_to;//номер платы назначения (адрес ПК 0x80)
  uint8_t adr_from;//номер платы отправителя
  uint8_t repeate_cnt;//счетчик повторов от 0
  uint8_t type;// тип пакета
  uint16_t len;//длина оставшейся части пакета
}pack_hdr_t;//заголовок любого пакета

typedef struct
{
  uint8_t sender;//номер канала отправителя.
  uint32_t recip;//бит. маска получателей
}audio_hdr_t;//добавочный заголовок аудиопакета для 32 получателей

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    audio_hdr_t audio_hdr;
    uint16_t pcm[AUDIO_SAMPLES*2];
}packet_audio_t;
#pragma pack(pop)

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onRead();
    void on_btPlay_clicked();

private:
    Ui::MainWindow *ui;
    QUdpSocket* udp;
    QAudioOutput* m_audioOut;
    QIODevice* m_audioDev;
    int16_t buf[AUDIO_SAMPLES*BUF_SZ];
    int buf_in = 0;
    int buf_out = 0;
    int buf_cnt = 0;
    bool stoped = true;
    uint8_t last_n = 0;
    int lost_n = 0;
};
#endif // MAINWINDOW_H
