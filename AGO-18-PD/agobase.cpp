#include "agobase.h"
#include "soundread.h"
#include "wavsaver.h"
#include <QFile>
#include <QSettings>
#include <QtConcurrent>
#include <scheduller.h>
#include "log.h"

extern QSettings setts;

AgoBase::AgoBase(QObject *parent) : QObject(parent)
{
    recActive = false;
}

bool AgoBase::isBusy()
{
    return micOn || isFilePlay() || recActive;
}

bool AgoBase::isMicOn()
{
    return micOn;
}

void AgoBase::startRec(QString fileName)
{
    recActive = true;

    setVolume(setts.value("micVolume", 100).toInt());
    emitOnVolume();

    setCastState(false);

    reader->start(false);
    rec_task = QtConcurrent::run([this, fileName](){
        QSharedPointer<WavSaver> wav = QSharedPointer<WavSaver>(new WavSaver(fileName.toLocal8Bit().toStdString(), AUDIO_FREQ));

        while(recActive){
            int16_t buf[AUDIO_SAMPLES*20];
            if(reader->fifo_read(buf, AUDIO_SAMPLES*20)){
                wav->write(buf, AUDIO_SAMPLES*20);
            }
        }
    });

    emit onStatusLabel("Идет запись");
    emit onState(Rec);
}

void AgoBase::stopRec()
{
    recActive = false;
    rec_task.waitForFinished();

    reader->stop();

    emit onStatusLabel("");
    emit onState(Stop);
}

void AgoBase::setVolume(int volume)
{
    if(recActive || micOn){
        reader->setVolume(volume);
        setts.setValue("micVolume", volume);
    }else if(player){
        player->setVolume(QAudio::convertVolume(volume/100.0,
                                                QAudio::LogarithmicVolumeScale,
                                                QAudio::LinearVolumeScale)*100);

        if(fileInfo){
            fileInfo->volume = volume;
            fileInfo->save();
        }
    }

}

QString AgoBase::defAudioPath()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::MusicLocation);
    if(path.endsWith("/"))
        path = path.remove(path.length()-1, 1);
    return path;
}

QString AgoBase::defTargetsPath()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    if(path.endsWith("/") || path.endsWith("\\"))
        path += +"targets.txt";
    else
        path += +"/targets.txt";
    return path;
}

void AgoBase::emitOnVolume(){
    if(recActive || micOn){
        emit onVolume(reader->volume());
    }else if(fileInfo && player){
        emit onVolume(fileInfo->volume);
    }else{
        emit onVolume(0);
    }
}

uint32_t AgoBase::getTragetsForFile(QString file)
{
    uint32_t outs = setts.value("outs", 0).toInt();
    if(file.isEmpty())
        return outs;
    if(!fileInfo)
        return outs;
    if(fileInfo->targets == 0)
        return outs;
    return fileInfo->targets;
}

void AgoBase::onDurationChanged(){
    qDebug()<< "Dur: " << player->duration();
    if(player)
        emit onProgress(player->duration(), 0, true);
}

void AgoBase::onPositionChanged(qint64 pos){
    qDebug() << pos;
    if(player)
        emit onProgress(player->duration(), pos, true);
}

// ========================== АГО-18 ==================
Ago18::Ago18(QObject *parent) : AgoBase(parent)
{

}

void Ago18::init()
{
    connectServer();
    connect(&connectTimer, &QTimer::timeout, this, &Ago18::onConnectTimer);
    connectTimer.start(1000);
}

int Ago18::targetsCount()
{
    return 32;
}

void Ago18::setTargets(targets_t targets)
{
    (void)targets;
}

void Ago18::onConnectTimer(){
    if(!is_connected)
        connectServer();
}

void Ago18::micStart(qint64 outs, bool tangenta)
{
    (void)tangenta;

    if(outs == 0){
        emit onStatusLabel("Надо выбрать направление");
        emit onState(Stop);
        return;
    }

    int port = 0;//rSt->getPort();
    if(port == 0)
        port = setts.value("port",0).toInt();
    int inCh = setts.value("InCh", 0).toInt();

    QString sAddr = setts.value("broadcast").toString();
    if(sAddr.isEmpty())
        sAddr = setts.value("IP").toString();
    QHostAddress addr(sAddr);
    if(addr.isNull() || port==0){
        emit onError("Ошибка настройки", "Ошибочный IP-адрес или порт БКС.");
        emit onState(Stop);
        return;
    }

    if(isFilePlay()){
        paused_pos = fileReader->getPos();
        pausedFile = fileInfo->path;
        fileReader->stop();
    }

    udp = new UDPChannel(addr, static_cast<uint16_t>(port), reader);
    int period = setts.value("period", AUDIO_FRAME_MS*8000).toInt();
    udp->start(static_cast<uint8_t>(inCh),
               static_cast<ch_mask_t>(outs), period);

    reader->start(true);
    emitOnVolume();

    emit onStatusLabel("Микрофон включен");
    emit onState(Mic);

    micOn = true;
}

void Ago18::micStop()
{
    reader->stop();

    micOn = false;

    int inCh = setts.value("InCh", 0).toInt();

    if(udp){
        udp->stop(static_cast<uint8_t>(inCh), 0);
        udp->deleteLater();
        udp = nullptr;
    }

    emitOnVolume();

    emit onStatusLabel("");
    emit onState(Stop);

    if(paused_pos && !pausedFile.isEmpty()){
        playFile(pausedFile, true);
        paused_pos = 0;
        pausedFile = "";
    }
}


void Ago18::connectServer(){
    if(rSt)
        rSt->deleteLater();
    if(stateWS){
        stateWS->close();
        stateWS->deleteLater();
    }

    if(setts.value("ago", 1).toInt() != 0)
        return;

    emit onStatusLabel("Нет соединения");

    rSt = new RemoteSetts(setts.value("IP","").toString(), this);
    connect(rSt, &RemoteSetts::uploadProgress, this, &Ago18::onUploadProgress);

//    getNames();
//    getClips();

    stateWS = new StateWS(setts.value("IP","").toString(), 8888, this);
    connect(stateWS, &StateWS::connected, this, &Ago18::onWSConnected);
    connect(stateWS, &StateWS::disconnected, this, &Ago18::onWSConnected);
    connect(stateWS, &StateWS::onActivity, this, &Ago18::onWSActivity);
    connect(stateWS, &StateWS::onNames, this, &Ago18::onWSNames);
    connect(stateWS, &StateWS::onClips, this, &Ago18::onWSClips);
}

void Ago18::onUploadProgress(qint64 sent, qint64 total){
    emit onProgress(total, sent, true);
}

void Ago18::onWSConnected(){
    emit onStatusLabel("Подключен");
    emit enableCtrls(true);
    is_connected = true;
}

void Ago18::onWSActivity(uint64_t outs){
    qDebug() << "Outs: " << outs;
    emit onActivity(static_cast<qint64>(outs));
}

void Ago18::onWSNames(int outs_cnt, QStringList names){
    emit createOutChecks(outs_cnt, names);
}

void Ago18::onWSClips(QStringList clips){
    emit onClips(clips);
}

QVector<QString> Ago18::getClips(){
    return rSt->getClips();
}

void Ago18::playClip(QString name, int source, uint32_t outs)
{
    stateWS->playClip(name, source, outs);
}

void Ago18::stopClip(int source)
{
    stateWS->stopClip(source);
}

bool Ago18::uploadFile(QString file)
{
    return rSt->uploadFile(setts.value("IP","").toString(), file) != 0;
}

void Ago18::playFile(QString fileName, bool cast)
{
    fileInfo = scheduller.findFile(fileName);

    if(!cast){
        previewFile(fileName);
        return;
    }

    auto targets = getTragetsForFile(fileName);

    int port = 0;//rSt->getPort();
    if(port == 0)
        port = setts.value("port",0).toInt();
    int inCh = setts.value("InCh", 0).toInt();

    QString sAddr = setts.value("broadcast").toString();
    if(sAddr.isEmpty())
        sAddr = setts.value("IP").toString();
    QHostAddress addr(sAddr);
    if(addr.isNull() || port==0){
        emit onError("Ошибка настройки", "Ошибочный IP-адрес или порт БКС.");
        emit onState(Stop);
        return;
    }

    if(targets == 0){
        emit onStatusLabel("Надо выбрать направление");
        emit onState(Stop);
        return;
    }

    fileReader = new FileSoundSource();
    connect(fileReader, &FileSoundSource::progress, this, &Ago18::fileSendPorgress, Qt::QueuedConnection);
    connect(fileReader, &FileSoundSource::complete, this, &Ago18::fileSendComplete, Qt::QueuedConnection);
    if(!fileReader->start(fileName, paused_pos)){
        emit onStatusLabel(fileReader->errorStr);
        emit onState(Stop);
        return;
    }

    udp = new UDPChannel(addr, static_cast<uint16_t>(port), fileReader);
    udp->start(static_cast<uint8_t>(inCh), targets, setts.value("period", AUDIO_FRAME_MS*3000).toInt());

    emit onProgress(0,0,true);
    emit onStatusLabel("Идет воспроизведение файла");
    emit onState(File);
    emit onActiveTargets(targets, true);
}


void Ago18::previewFile(QString fileName)
{
    if(QFile(fileName).exists()){

        player = new QMediaPlayer(this);
        connect(player, &QMediaPlayer::stateChanged, this, &Ago18::onPlayerState);
        connect(player, &QMediaPlayer::durationChanged, this, &Ago18::onDurationChanged);
        connect(player, &QMediaPlayer::positionChanged, this, &Ago18::onPositionChanged);

        if(fileInfo)
            setVolume(fileInfo->volume);
        emitOnVolume();

        player->setMedia(QUrl::fromLocalFile(fileName));
        player->play();
    }else{
        emit onState(Stop);
    }
}

void Ago18::onPlayerState(QMediaPlayer::State state){
    if(state == QMediaPlayer::PlayingState){
        emit onState(File);
        emit onStatusLabel("Воспроизведение:");
        emit onActiveTargets(0, true);
    }
    if(state == QMediaPlayer::StoppedState){
        emit onState(Stop);
        player->deleteLater();
        player = nullptr;
        emit onProgress(0,0,false);
        emit onStatusLabel("");
        fileInfo = nullptr;
        emit onActiveTargets(getTragetsForFile(""), false);
    }
}


void Ago18::fileSendPorgress(int total, int pos){
    emit onProgress(total, pos, true);
}

void Ago18::fileSendComplete(){
    emit onStatusLabel("");
    emit onProgress(0,0,false);
    emit onState(Stop);

    fileReader->deleteLater();
    fileReader = nullptr;
}

bool Ago18::isFilePlay()
{
    return fileReader != nullptr;
}

void Ago18::stopFile()
{
    if(player && player->state() == QMediaPlayer::PlayingState){
        player->stop();
    }else{
        onPlayerState(QMediaPlayer::StoppedState);
    }

    if(fileReader){
        fileReader->stop();
    }

    int inCh = setts.value("InCh", 0).toInt();

    if(udp){
        udp->stop(static_cast<uint8_t>(inCh), 0);
        udp->deleteLater();
        udp = nullptr;
    }
}

// ========================== АГО-09 ==================
Ago09::Ago09(int targets_cnt, QObject *parent) : AgoBase(parent), targets_cnt(targets_cnt)
{
    comPort.setTargetsCnt(targets_cnt);
    connect(&comPort, &Ago09Uart::connectState, this, &Ago09::connectState);
    connect(&comPort, &Ago09Uart::bksReady, this, &Ago09::onBKSReady);
    connect(&comPort, &Ago09Uart::tangenta, this, &Ago09::onTangenta);
    connect(&comPort, &Ago09Uart::busyTargets, this, &Ago09::onBusyTargets);
}

Ago09::~Ago09()
{
    comPort.close();
}

void Ago09::init()
{
    emit createOutChecks(targetsCount(), loadTargets());
    openComPort();
}

int Ago09::targetsCount()
{
    return targets_cnt;
}

void Ago09::setTargets(targets_t targets)
{
    comPort.setTargets(targets);
}

void Ago09::setCastState(bool state)
{
    comPort.setCastState(state);
}

void Ago09::micStart(qint64 outs, bool tangenta)
{
    if(player && player->state()==QMediaPlayer::PlayingState){
        player->pause();
    }

//#ifndef PORT_DEBUG
    if(!tangenta){
        emit onState(Stop);
        return;
    }
//#endif

    if(outs == 0){
        emit onStatusLabel("Надо выбрать направление");
    }else{
        emit onStatusLabel("Идет трансляция");
    }

    reader->start(true);

    emit onState(Mic);

    setTargets(static_cast<targets_t>(outs));
    setCastState(true);

    micOn = true;
    setVolume(setts.value("micVolume", 100).toInt());
    emitOnVolume();
}

void Ago09::micStop()
{
    reader->stop();

    micOn = false;

    emitOnVolume();

    setCastState(false);
//    setTargets(static_cast<targets_t>(0));
    emit onActiveTargets(0, false);

    emit onState(Stop);

    if(player && player->state()==QMediaPlayer::PausedState && fileInfo){
        playFile(fileInfo->path, cast);
    }
}

void Ago09::playFile(QString fileName, bool cast)
{
    if(QFile(fileName).exists()){
        fileInfo = scheduller.findFile(fileName);

        this->cast = cast;

        if(!player || player->state()!=QMediaPlayer::PausedState || !fileInfo){

            player = new QMediaPlayer(this);
            connect(player, &QMediaPlayer::stateChanged, this, &Ago09::onPlayerState);
            connect(player, &QMediaPlayer::durationChanged, this, &Ago09::onDurationChanged);
            connect(player, &QMediaPlayer::positionChanged, this, &Ago09::onPositionChanged);

            player->setMedia(QUrl::fromLocalFile(fileName));
        }

        if(fileInfo)
            setVolume(fileInfo->volume);
        emitOnVolume();

        player->play();
    }else{
        emit onState(Stop);
    }
}

void Ago09::onPlayerState(QMediaPlayer::State state){
    _log.log(QString("Player  \t%1").arg(state));

    if(state == QMediaPlayer::PlayingState){
        auto targets = getTragetsForFile(fileInfo->path);
        if(cast){
            comPort.setTargets(static_cast<targets_t>(targets));
            setCastState(true);
            emit onStatusLabel("Трансляция:");
        }else{
            emit onStatusLabel("Воспроизведение:");
        }
        emit onState(File);
//        emit onActiveTargets(targets, true);
    }else if(state == QMediaPlayer::StoppedState){
        setCastState(false);
        emit onState(Stop);
        player->deleteLater();
        player = nullptr;
        fileInfo = nullptr;
        emit onProgress(0,0,false);
        emit onStatusLabel("");
        emit onActiveTargets(0, false);//getTragetsForFile("")
        setVolume(setts.value("micVolume", 100).toInt());
    }else{
//        comPort.setCastState(false);
//        emit setActionFileChecked(false);
//        emit setActionPlayChecked(false);
//        emit enableButtons(true, BtnPreview);
//        emit enableButtons(true, BtnFile);
//        emit onStatusLabel("Пауза");
//        emit onActiveTargets(getTragetsForFile(""), false);
    }
}

bool Ago09::isFilePlay()
{
    return player != nullptr;
}

void Ago09::stopFile()
{
    if(player && player->state() == QMediaPlayer::PlayingState){
        player->stop();
    }else{
        onPlayerState(QMediaPlayer::StoppedState);
    }
}

void Ago09::setTangentaEmu(bool state)
{
    comPort.tangentaEmu = state;
}

bool Ago09::getTangentaEmu()
{
    return comPort.tangentaEmu;
}

QStringList Ago09::loadTargets(){
    QStringList targets;
    QString fileName = setts.value("targets_file", defTargetsPath()).toString();
    if(!fileName.isEmpty()){
        QFile file(fileName);
        if(file.open(QIODevice::ReadOnly)){
            char str[100];
            while(file.readLine(str,100)>0){
                targets.push_back(QString(str).trimmed());
            }

        }
    }
    return targets;
}

void Ago09::openComPort()
{
    comPortName = setts.value("COM", "").toString();
    if(comPortName.isEmpty()){
        emit on_actionSetts_triggered();
        return;
    }

    auto baud = setts.value("baud", 2400).toInt();
    if(!comPortName.isEmpty()){

        if(comPort.open(comPortName, baud)){
            comPort.setTargets(static_cast<targets_t>(setts.value("outs", 0).toInt()));//

            emit onStatusLabel("Связь установлена ("+comPortName+")");

        }else{
            emit onStatusLabel("Ошибка открытия порта "+comPortName);
        }
    }
}

void Ago09::connectState(Ago09Uart::Ago09UartStates state){
    switch(state){
    case Ago09Uart::Ago09UartStates::connected:
        emit onStatusLabel("Связь установлена ("+comPortName+")");
        break;
    case Ago09Uart::Ago09UartStates::disconnected:
        emit onStatusLabel("Связь отсутствует ("+comPortName+")");
        break;
    case Ago09Uart::Ago09UartStates::error:
        emit onStatusLabel("Ошибка связи ("+comPortName+")");
        break;
    case Ago09Uart::Ago09UartStates::tryConnect:
        emit onStatusLabel("Подключение к "+comPortName+"...");
        break;
    }
}

void Ago09::onBusyTargets(targets_t targs, bool cast)
{
    emit onActiveTargets(targs, cast);
}

void Ago09::onBKSReady(bool ready){
    emit onBKSActive(ready);
}

void Ago09::onTangenta(bool state){
    if(state && isFilePlay()){
        stopFile();
    }
    emit on_tangenta(state);
}
