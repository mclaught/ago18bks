/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyWSClient.h
 * Author: user
 *
 * Created on 27 мая 2020 г., 13:27
 */

#ifndef MYWSCLIENT_H
#define MYWSCLIENT_H

#include <stdint.h>
#include <string>
#include <mutex>
#include "json.hpp"

#pragma pack(push,1)
typedef struct{
    uint8_t opcode:4;
    uint8_t RSV:3;
    uint8_t FIN:1;
    
    uint8_t len:7;
    uint8_t mask:1;
} websocket_hdr_t;

typedef struct{
    websocket_hdr_t hdr;
    union{
        struct{
            char mask[4];
            char data[];
        }masked;
        struct{
            char data[];
        }unmasked;
    };
} websocket8_t;

typedef struct{
    websocket_hdr_t hdr;
    uint16_t len;
    union{
        struct{
            char mask[4];
            char data[];
        }masked;
        struct{
            char data[];
        }unmasked;
    };
} websocket16_t;

typedef struct{
    websocket_hdr_t hdr;
    uint64_t len;
    union{
        struct{
            char mask[4];
            char data[];
        }masked;
        struct{
            char data[];
        }unmasked;
    };
} websocket64_t;
#pragma pack(pop)

using namespace std;
using namespace nlohmann;

class MyWSClient {
public:
    MyWSClient(int _sock);
    MyWSClient(const MyWSClient& orig);
    virtual ~MyWSClient();
    
    int sock;
    bool enabled = false;
    string type;
    bool process(char* data, int len);
    void write(char* data, int len);
private:
    char packet[1024*1024];
    int pack_len = 0;
    int pend_len;
    bool handshaked = false;
    mutex sock_mutex;
    
    void parseHeader(string header);
    void parsePacket(char* data, int len);
    void parseJson(json js);
    void processCommand(json js);
    void createMask(char* mask);
    void mask(char* data, char* mask, int len);
};

#endif /* MYWSCLIENT_H */

