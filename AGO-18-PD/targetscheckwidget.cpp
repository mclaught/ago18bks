#include "targetscheckwidget.h"
#include "ui_targetscheckwidget.h"
#include "log.h"
#include "ago09uart.h"

#include <QSettings>
#include <QDebug>

extern QSettings setts;

TargetsCheckWidget::TargetsCheckWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TargetsCheckWidget)
{
    ui->setupUi(this);
}

TargetsCheckWidget::~TargetsCheckWidget()
{
    delete ui;
}

void TargetsCheckWidget::setTargets(QStringList targets, int outs_cnt){
    this->names = targets;

    QLayoutItem* item;
    while ( ( item = ui->destChLayout->layout()->takeAt( 0 ) ) != nullptr )
    {
        delete item->widget();
        delete item;
    }//    while(ui->destChLayout->itemAt(0)){

    dstCheck.clear();

//        auto widget = ui->destChLayout->itemAt(0)->widget();
//        if(widget){
//            ui->destChLayout->removeWidget(widget);
//        }
//    }

    int cols_cnt = setts.value("cols_cnt", 4).toInt();
    int fnt_sz = setts.value("targ_font_sz", 24).toInt();

    for(int i=0; i<outs_cnt; i++){
        int r = i/cols_cnt;
        int c = i%cols_cnt;

        WordWrapCheck* check = ui->destChLayout->itemAtPosition(r,c) ?
                    dynamic_cast<WordWrapCheck*>(ui->destChLayout->itemAtPosition(r,c)->widget()) :
                    nullptr;

        if(!check){
            check = new WordWrapCheck(nullptr);
            ui->destChLayout->addWidget(check, r, c);
            ui->destChLayout->setRowStretch(r, 1);
            ui->destChLayout->setRowMinimumHeight(r, 30);

            connect(check->getCheckBox(), &QCheckBox::clicked, [this](bool checked){
                (void)checked;
                emit onChecked(getOuts());
            });
        }

        if(i<names.size() && names[i] != "")
            check->setText(QString("%1").arg(names[i]));
        else
            check->setText(QString("%1").arg(i+1));

        dstCheck.push_back(check);
    }

    setStyleSheet(QString("QLabel {border-radius: 20px; color: white; padding: 2px; font-size: %1px;}").arg(fnt_sz));
}

void TargetsCheckWidget::setOuts(uint32_t outs){
    for(int i=0; i<dstCheck.length(); i++){
        dstCheck[i]->setChecked(outs & (1 << i));
    }
}

uint32_t TargetsCheckWidget::getOuts(){
    uint64_t outs = 0;
    for(int i=0; i<dstCheck.length(); i++){
        if(dstCheck[i]->isChecked()){
            outs |= (1 << i);
        }
    }
    return outs;
}

void TargetsCheckWidget::setChecksEnabled(uint32_t outs){
    for(int i=0; i<dstCheck.size(); i++){
        dstCheck[i]->setEnabled((outs & (1<<i)));
    }
}

void TargetsCheckWidget::setPlayState(bool play, uint32_t outs)
{
    qDebug() << play << outs;

    QString chck;
    for(int i=0; i<dstCheck.length(); i++){
        if(play && ((outs & (1<<i)) || dstCheck[i]->isChecked())){
            QString color = ((outs & (1<<i)) && dstCheck[i]->isChecked()) ? "red" : "green";
            chck += color[0];

            dstCheck[i]->setStyleSheet(QString("QLabel {"
                                       "background-color: %1;"
                                       "}").arg(color));
//            "border: 2px solid green;"
        }else{
            dstCheck[i]->setStyleSheet("");
            chck += "-";
        }
    }

    _log.log(QString("Widget  \t%1\t%2\t%3").arg(play).arg(outs,dstCheck.length()*2,16,QChar('0')).arg(chck));

//    setStyleSheet(play ? "QCheckBox::indicator:checked {"
//                  "image: url(:/icon/played.png);"
//                  "}" : "");
}
