/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RTP.cpp
 * Author: user
 * 
 * Created on 25 декабря 2019 г., 17:16
 */

#include "RTP.h"
#include "log.h"
#include <iostream>

using namespace std;

namespace RTP {

uint8_t* decode(uint8_t* packet, int pack_len, int* data_len, rtp_hdr_t* hdr){
    *hdr = *reinterpret_cast<rtp_hdr_t*>(packet);
    *data_len = 0;
    if(hdr->ver != 2 || hdr->x)
        return nullptr;
    
    if(hdr->type == 14){
        *data_len = pack_len - sizeof(rtp_hdr_t) - hdr->cc*sizeof(uint32_t);
        return packet + sizeof(rtp_hdr_t) + hdr->cc*sizeof(uint32_t);
    }
    
    rtcp_hdr_t* rtcp = reinterpret_cast<rtcp_hdr_t*>(packet);
    if(rtcp->type == 200){
        cout << green << "RTCP " << rtcp->snd_cnt << clrst << endl;
    }
    
    return nullptr;
}

}