var searchData=
[
  ['b_747',['b',['../structcodec__eq__coeff__t.html#a814619e93870ebdd89e34fce1170bb6b',1,'codec_eq_coeff_t']]],
  ['b0_748',['b0',['../structbiqd__hp__coeff__t.html#a7b0e32fb5195655ea7d67bd19bb06607',1,'biqd_hp_coeff_t::b0()'],['../structbiqd__lp__coeff__t.html#ac8810c7710c636c0ee257c32ad7ee1cc',1,'biqd_lp_coeff_t::b0()']]],
  ['b1_749',['b1',['../structbiqd__hp__coeff__t.html#a004f6ef45aa5f173edea8db55de67c20',1,'biqd_hp_coeff_t::b1()'],['../structbiqd__lp__coeff__t.html#a133e079f64e4d920fbdc5f613957f6f8',1,'biqd_lp_coeff_t::b1()']]],
  ['b2_750',['b2',['../structbiqd__hp__coeff__t.html#a43f7a2b1d54c7ff17dcdd25e81d24170',1,'biqd_hp_coeff_t::b2()'],['../structbiqd__lp__coeff__t.html#ad76278f8c29ad2244549418ac4436f7a',1,'biqd_lp_coeff_t::b2()']]],
  ['bitrate_751',['bitrate',['../structpd__start__t.html#af19689e094c2fc525c676cb10e6f6aca',1,'pd_start_t']]],
  ['bits_5fper_5fsample_752',['bits_per_sample',['../structwav__format__t.html#a3ec15dc8cd65a6545c86d9ca879c93e6',1,'wav_format_t']]],
  ['bitspersample_753',['bitsPerSample',['../class_my_p_c_m.html#a040b7232461b24d4a2d93565debaff04',1,'MyPCM']]],
  ['bitstream_5fsn_754',['bitstream_sn',['../structogg__hdr__t.html#a4f84a92cada01532f5c6f5541ae4d2a9',1,'ogg_hdr_t']]],
  ['blockalign_755',['blockAlign',['../class_my_p_c_m.html#aed54ce8302b9e86219f407bfa9b5cc4f',1,'MyPCM']]],
  ['byte_5frate_756',['byte_rate',['../class_my_p_c_m.html#aea1e4c8a7beae9ea3bbe39109f3eeca0',1,'MyPCM']]],
  ['bytes_5fper_5fsec_757',['bytes_per_sec',['../structwav__format__t.html#a43122364f6a249715fe902882ff5c9aa',1,'wav_format_t']]]
];
