class Rest{
    async get(query){
        var pr = new Promise(function(resolve, reject){
            $.get('command.php', {query: JSON.stringify(query)}, function(answer){
                console.info("ANSWER", answer);
//                let obj = JSON.parse(answer);
                resolve(answer);
            });
        });
        return pr;
    }
    
    async post(query){
        var pr = new Promise(function(resolve, reject){
            console.log(JSON.stringify(query));
            $.post('command.php', {query: JSON.stringify(query)}, function(answer){
                console.info("ANSWER", answer);
//                let obj = JSON.parse(answer);
                resolve(answer);
            });
        });
        return pr;
    }
    
    async get_settings(){
        var query = {
            command: "get_settings"
        };
        return await this.get(query);
    }
    
    async set_settings(setts){
        var query = {
            command: "set_settings",
            settings: setts
        };
        return await this.post(query);
    }
    
    async clear_boards(){
        var query = {
            command: "clear_boards"
        };
        return await this.get(query);
    }
    
    async get_boards(){
        var query = {
            command: "get_boards"
        };
        return await this.get(query);
    }
    
    async get_network(){
        var query = {
            command: "get_network"
        };
        return await this.get(query);
    }
    
    async get_comm_table(){
        var query = {
            command: 'get_comm_table'
        };
        return await this.get(query);
    }
    
    async set_comm_table(comm_table){
        var query = {
            command: "set_comm_table",
            comm_table: comm_table
        };
        return await this.post(query);
    }
    
    async get_events(){
        var query = {
            command: 'get_events'
        };
        return await this.get(query);
    }
    
    async get_snd_params(){
        var query = {
            command: 'get_snd_params'
        };
        return await this.get(query);
    }
    
    async set_snd_params(channel_type, channel, snd_params){
        var query = {
            command: 'set_snd_params',
            channel_type: channel_type,
            channel: channel,
            snd_params: snd_params
        };
        return await this.get(query);
    }
    
    async set_time(time){
         var query = {
            command: "set_time",
            time: time
        };
        return await this.post(query);
    }
    
    async play_clip(name, signal, autorew){
         var query = {
            command: "play_clip",
            clip: name,
            signal: signal,
            autorew: autorew
        };
        return await this.post(query);
    }
    
    async play_clip_outs(name, source, outs, autorew){
         var query = {
            command: "play_clip",
            clip: name,
            source: source,
            outs: outs,
            autorew: autorew
        };
        return await this.post(query);
    }
    
    async stop_clip(source){
        var query = {
            command: "stop_clip",
            source: source
        };
        return await this.post(query);
    }
    
    async stop_all_clips(){
        var query = {
            command: "stop_clip"
        };
        return await this.post(query);
    }
    
    async clip_state(source){
        var query = {
            command: "clip_state",
            source: source
        };
        return await this.post(query);
    }
    
    async clips_state(sources){
        var query = {
            command: "clips_state",
            sources: sources
        };
        return await this.post(query);
    }
   
    async get_bru_cfg(request = false){
         var query = {
            command: 'get_bru_cfg',
            request: request
        };
        return await this.get(query);
    }
   
    async set_bru_cfg(id, bru_cfg){
         var query = {
            command: 'set_bru_cfg',
            id: id,
            bru_cfg: bru_cfg
        };
        return await this.post(query);
    }
   
    async bru_measure(bru_id){
         var query = {
            command: 'bru_measure',
            bru_id: bru_id
        };
        return await this.post(query);
    }
    
    async bru_measure_cancel(bru_id){
         var query = {
            command: 'bru_measure_cancel'
        };
        return await this.post(query);
    }
   
    async bru_reset(bru_id){
         var query = {
            command: 'bru_reset',
            bru_id: bru_id
        };
        return await this.get(query);
    }
    
    async get_id(id){
         var query = {
            command: 'get_id',
            id: id
        };
        return await this.post(query);
    }
    
    async change_id(old_id, new_id, offset, um_mode){
         var query = {
            command: 'change_id',
            old_id: old_id,
            new_id: new_id,
            offset: offset,
            um_mode: um_mode
        };
        console.dir(query);
        return await this.post(query);
    }
    
    async set_id(new_id, um_mode){
        var query = {
            command: 'set_id',
            new_id: new_id,
            um_mode: um_mode
        };
        return await this.post(query);
    }
    
    async get_alarms(){
         var query = {
            command: 'get_alarms'
        };
        console.dir(query);
        return await this.post(query);
    }
    
    async set_alarms(alarms){
         var query = {
            command: 'set_alarms',
            alarms: alarms
        };
        return await this.post(query);
    }
    
    async get_inp_mode(){
         var query = {
            command: 'get_inp_mode'
        };
        return await this.post(query);
    }
    
    async set_inp_mode(value){
         var query = {
            command: 'set_inp_mode',
            inp_mode: value
        };
        return await this.post(query);
    }
}