class BRUCfgView extends Page{
    
    constructor(container){
        super(container);
        this.data = null;
        this.measure_active = false;
    }
    
    init(){
        var _this = this;
        
        console.log("BRUCfgView.init()");
        
        this.bru_offset = ('bru_offset' in settings.bks) ? parseInt(settings.bks.bru_offset) : 25;
        this.id_offset = ('id_offset' in settings.bks) ? parseInt(settings.bks.id_offset) : 8;
        console.info("id_offset", this.id_offset);
        
        this.line_dlg = new LineDialog($('#lines-modal-body'));
        this.ws_connected = false;
        this.create_ws();
        this.update();
        setInterval(function(){
            if(!_this.ws_connected){
                _this.create_ws();
            }
            if(_this.data.length === null || _this.data.length === 0){
                _this.update();
            }
        },1000);
        
        $('#btn-update').click(async function(){
            console.log("BRU update");
            await _this.update(true);
            alertify.success("Конфигурация обновлена");
        });
        
        $('#btn-dlg-measure').click(function(){
            $('#measureModal').modal('hide');
            _this.update();
        });
        
        super.init();
    }
    
    async create_ws(){
        console.log("Try WS connect");
        
        let _this = this;
        var network = (await (new Rest()).get_network()).network;
        this.ws = new WebSocket("ws://"+window.location.hostname+":"+network.ws_port);
        this.ws.onopen = function(){
            console.log("WS connection opened");
            
            let query = {
                type: "measure"
            };

            _this.ws.send(JSON.stringify(query));

            _this.ws_connected = true;
            $('.btn-measure').prop('disabled', !_this.ws_connected);
            $('#connection-alert').hide();
            
            _this.update();
        };
        this.ws.onclose = function(){
            _this.ws_connected = false;
            $('.btn-measure').prop('disabled', !_this.ws_connected);
            $('#connection-alert').show();
        }
        this.ws.onerror = function(error){
            _this.ws_connected = false;
            $('.btn-measure').prop('disabled', !_this.ws_connected);
            $('#connection-alert').show();
        }
        this.ws.onmessage = function(event){
            let json = JSON.parse(event.data);
            console.dir(json);
            
            let btn = $(".btn-line[bru_id='"+json.bru+"'][line_id='"+json.line+"']");
            console.dir(btn);
            if(json.state == "ok"){
                btn.removeClass("btn-secondary");
                btn.addClass("btn-success");
            }else if(json.state == "timeout"){
                btn.removeClass("btn-secondary");
                btn.addClass("btn-warning");
            }else if(json.state == "error"){
                btn.removeClass("btn-secondary");
                btn.addClass("btn-danger");
            }
            
            if(json.line == -1){
//                $(".measure-spinner").hide();
                _this.measure_active = false;
                $('.btn-measure').html(`
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="8" cy="8" r="8"/>
                    </svg>
                    Измерение
                `);
                $('.btn-measure').removeClass("btn-success");
                $('.btn-measure').addClass("btn-secondary");
                $('.disbl-on-measure').prop('disabled', false);
                
                switch(json.result){
                    case "failed":
                        alertify.error("Измерение не выполнено");
                        break;
                    case "partial":
//                        alertify.warning("Измерение выполнено частично");
                        _this.show_measure_modal("Измерение выполнено частично",
                            "Полученные настройки линий будут загружены в интерфейс, после чего их можно проверить и сохранить.");
                        break;
                    case "complete":
//                        alertify.success("Измерение завершено полностью");
                        _this.show_measure_modal("Измерение выполнено полностью",
                            "Полученные настройки линий будут загружены в интерфейс, после чего их можно проверить и сохранить.");
                        break;
                }
            }
        };
    }
    
    show_measure_modal(hdr, body){
        $('#measureModalLabel').html(hdr);
        $('#measure-modal-body').html(body);
        $('#measureModal').modal('show');
//        _this.update();
    }
    
    create_hdr_label(first, descr, value){
        let h = first ? '<h4>' : '<h6>';
        let cls = first ? 'col' : '';
        return $(h, {class: ''}).append(
            $('<span>', {class: 'badge badge-light mr-1'}).html(descr+': '+value)
        );
    }
    
    create_input(name, descr, unit, cls, value){
        let div = $('<div>', {class: "input-group mb-3 "+cls}).append(
            $('<div>', {class: "input-group-prepend"}).append(
                $('<span>', {class: "input-group-text", id: name+"-label"}).html(descr)
            ),
            $('<input>', {type: "text", class: "form-control disbl-on-measure "+name, placeholder: descr, "aria-label": descr, "aria-describedby": name+"-label"})
                    .val(value)
        );
        if(unit != ''){
            div.append(
                $('<div>', {class: "input-group-append"}).append(
                    $('<span>', {class: "input-group-text"}).html(unit)
                ),
            );
        }
        return div;
    }
    
    create_num_input(name, descr, unit, cls, value, min, max_lines, invalid_text){
        let div = $('<div>', {class: "input-group mb-3 "+cls}).append(
            $('<div>', {class: "input-group-prepend"}).append(
                $('<span>', {class: "input-group-text", id: name+"-label"}).html(descr)
            ),
            $('<input>', {type: "number", min: min, class: "form-control is-valid disbl-on-measure "+name, placeholder: descr, "aria-label": descr, "aria-describedby": name+"-label", max_lines: max_lines})
                    .val(value),
            $('<div>', {class: 'invalid-feedback'}).html(invalid_text),
            $('<div>', {class: 'valid-feedback'}).html('OK')
        );
        if(unit != ''){
            div.append(
                $('<div>', {class: "input-group-append"}).append(
                    $('<span>', {class: "input-group-text"}).html(unit)
                ),
            );
        }
        return div;
    }
    
    create_list(name, descr, items, cls, value){
        let select = $('<select>', {class: 'form-control disbl-on-measure '+name});
        for(let i=0; i<items.length; i++){
            console.log("item "+items[i]);
            select.append($('<option>', {value: i}).html(items[i]).prop('selected', i==value));//
        }
        let div = $('<div>', {class: "input-group mb-3 "+cls}).append(
            $('<div>', {class: "input-group-prepend"}).append(
                $('<span>', {class: "input-group-text", id: name+"-label"}).html(descr)
            ),
            select
        );
        return div;
    }
    
    check_line_objects(bru){
        for(let i=bru.line_min; i<=bru.line_max; i++){
            if(!bru.linemap.has(i)){
                bru.linemap.set(i, {
                            low_trshld_ohm: 0,
                            high_trshld_ohm: 0,
                            ref_value_ohm: 0,
                            state: 0,
                            range: 0,
                            line_id: i
                        });
            }
        }
    }
    
    create_lines(bru){
        this.check_line_objects(bru);
        let lines = $('<div>', {class: "btn-group", role: "group", 'aria-label': "Линии"});//class: "accordion",  , {id: "accordionLines"+i}
        for(let l=bru.line_min; l<=bru.line_max; l++){
            let line_id = l;
            lines.append(
                $('<button>', {type: "button", class: "btn btn-secondary btn-line disbl-on-measure", bru_id: bru.id, line_id: line_id}).html(l+1)
            );
        }
        
        console.log("Add btn-line handler");
        let _this = this;
        lines.find('.btn-line').click(function(){
            let row = $(this).parents('.params-row');
            let id = parseInt(row.attr('bru-id'));
            let line_id = parseInt($(this).attr('line_id'));
            console.log('Line '+id+"-"+line_id);

            for(let i=0; i<_this.data.length; i++){
                if(_this.data[i].id === id){
                    if(!_this.data[i].linemap.has(line_id)){
                        _this.data[i].linemap.set(line_id, {
                            low_trshld_ohm: 0,
                            high_trshld_ohm: 0,
                            ref_value_ohm: 0,
                            state: 0,
                            range: 0
                        });
                    }
                    
                    let line = _this.data[i].linemap.get(line_id);
                    line.bru_id = id;
                    let line_n = line_id+1;
                    console.log("Edit line "+line_id);
                    console.dir(line);
                    console.info('this', _this);
                    _this.line_dlg.show('БРУ '+(id-_this.bru_offset)+' линия '+line_n, _this.data[i].linemap.get(line_id), function(line){
                        console.log("Save line "+line.line_id);
                        console.dir(line);
                        _this.data[i].linemap.set(line.line_id, line);
                        console.dir(_this.data);
                    });
                }
            }
            
        });
        
        return lines;
    }
    
    create_bru_card(bru){
        let colr = bru.switch_state === 0xFF ? 'bg-secondary' : 'bg-danger';
        let btn_colr = bru.switch_state === 0xFF ? 'btn-outline-secondary' : 'btn-outline-danger';
        let card = $("<div>", {class: "card shadow"}).append(
            $("<div>", {class: "card-header "+colr, id: "heading"+bru.id}).append(
                $('<h5>', {class: "mb-0"}).append(
                    $('<button>', {
                        class: "btn "+btn_colr+" btn-lg col" + (bru.id===this.cur_bru_id ? "" : " collapsed"), 
                        type: "button", 
                        "data-toggle": "collapse", 
                        "data-target": "#collapse"+bru.id, 
                        "aria-expanded": false, 
                        "aria-controls": "collapse"+bru.id}).append(

                        $('<div>', {class: 'row'}).append(
//                            this.create_hdr_label(true, 'БРУ', bru.id),
                            $('<h4>', {style: 'color: white;'}).append(
                                $('<img>', {src: 'img/bru_white.svg', width: 24, height: 24}),
                                $('<span>', {class: 'ml-2', style: 'text-decoration: none;'}).html('БРУ '+(bru.id-this.bru_offset))
                            ),
                            $('<div>', {class: 'col'}),
                            this.create_hdr_label(false, 'Линии', (bru.line_min+1)+"-"+(bru.line_max+1)),
                            this.create_hdr_label(false, 'Режим', bru.mode===0 ? "Ожидание" : "Работа"),
                            this.create_hdr_label(false, 'Подключение', bru.switch_state===0xFF ? "Штатно" : (bru.switch_state)),
                            this.create_hdr_label(false, 'Резерв', bru.redund_um_id),//-this.id_offset
                        )
                    )
                )
            ),
            $('<div>', {id: "collapse"+bru.id, class: "collapse" + (bru.id===this.cur_bru_id ? " show" : ""), "aria-labelledby": "heading"+bru.id, "data-parent": "#accordionBRU", bru_id: bru.id}).append(
                $('<div>', {class: "card-body"}).append(
                    $('<form>', {class: "row params-row needs-validation", 'bru-id': bru.id}).append(
                        this.create_num_input('line_min', 'Мин линия', '', 'col-lg-4 col-md-6 col-12', bru.line_min+1, 1, bru.lines, 'Количество линий превышает допустимое'),
                        this.create_num_input('line_max', 'Макс линия', '', 'col-lg-4 col-md-6 col-12', bru.line_max+1, 1, bru.lines, 'Количество линий превышает допустимое'),
                        this.create_num_input('line_offset', 'Сдвиг', '', 'col-lg-4 col-md-6 col-12', bru.line_offset, 0, bru.lines, 'Смещение должно быть четным'),
                        this.create_input('test_mode_settle_time_ms', 'Время установления в режиме создания профиля', 'мсек', 'col-12', bru.test_mode_settle_time_ms),
                        this.create_input('work_mode_eq_range_settle_time_ms', 'Время установления в рабочем режиме на том же диапазоне', 'мсек', 'col-12', bru.work_mode_eq_range_settle_time_ms),
                        this.create_input('work_mode_diff_range_settle_time_ms', 'Время установления в рабочем режиме при переключении на другой диапазон', 'мсек', 'col-12', bru.work_mode_diff_range_settle_time_ms),
                        this.create_input('work_mode_cycle_period_sec', 'Период проверки линий в рабочем режиме', 'сек', 'col-6', bru.work_mode_cycle_period_sec),
                        this.create_input('redund_um_id', 'Резервный УМ', '', 'col-6', bru.redund_um_id),//-this.id_offset
                        this.create_input('max_meas_tolerance_proc', 'Макс. допуск измерения', '%', 'col-md-6 col-12', bru.max_meas_tolerance_proc),
                        this.create_input('max_devi_tolerance_proc', 'Макс. допуск отклонения', '%', 'col-md-6 col-12', bru.max_devi_tolerance_proc),
                        this.create_list('mode', 'Текущий режим работы', ['Ожидание', 'Работа'], 'col-md-12 col-12', bru.mode),

                        $('<h6>', {class: 'mt-3 ml-3'}).html('Линии'),
                        $('<div>', {class: 'col-12 lines-div', bru_id: bru.id}).append(
                            this.create_lines(bru)
                        ),

                        $('<div>', {class: 'row col-12 mt-2 ml-1'}).append(
                            $('<button>', {class: 'btn btn-secondary btn-measure', type: 'button'}).html(`
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8" cy="8" r="8"/>
                                </svg>
                                Измерение
                            `),
                            $('<button>', {class: 'btn btn-secondary btn-mainum ml-1 disbl-on-measure', type: 'button', bru_id: bru.id}).html(`
                                <div class="spinner-grow spinner-grow-sm" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                <span class="mainum-text">Переключить на основной УМ</span>
                            `),
                            $('<div>', {class: 'col'}),
                            $('<button>', {class: 'btn btn-success btn-save shadow-sm disbl-on-measure', type: 'button'}).html(`
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M15.354 2.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L8 9.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
                                  <path fill-rule="evenodd" d="M8 2.5A5.5 5.5 0 1 0 13.5 8a.5.5 0 0 1 1 0 6.5 6.5 0 1 1-3.25-5.63.5.5 0 1 1-.5.865A5.472 5.472 0 0 0 8 2.5z"/>
                                </svg> 
                                Сохранить
                            `)
                        )

                    )
                )
            )
        );

        card.find('.btn-mainum').prop('disabled', bru.switch_state===0xFF);
        card.find('.btn-mainum').find('.spinner-grow').hide();

        return card;
    }
    
    find_bru(bru_id){
        console.log("find bru "+bru_id);
        for(let bru of this.data){
            console.dir(bru);
            if(bru.id == bru_id)
                return bru;
        }
        return null;
    }
    
    build(){
        let div = $("<div>", {class: "accordion", id:"accordionBRU"});

        for(let i=0; i<this.data.length; i++){
            let bru = this.data[i];
            
            div.append(
                this.create_bru_card(bru)
            );
        }
        
        var _this = this;
        
        div.find('.collapse').on('show.bs.collapse', function(){
            let bru_id = parseInt($(this).attr('bru_id'));
            console.log("Shown "+bru_id);
            _this.cur_bru_id = bru_id;
        });
        
        div.find('.line_min,.line_max,.line_offset').on('change', function(){
            let bru_id = $(this).parents('form').attr("bru-id");
            let bru = _this.find_bru(bru_id);
            if(bru === null){
                console.log("BRU not found");
                return;
            }
            let max_lines = bru.lines;
            let line_min = parseInt($(this).parents('form').find('.line_min').val())-1;
            let line_max = parseInt($(this).parents('form').find('.line_max').val())-1;
            let line_offset = parseInt($(this).parents('form').find('.line_offset').val());
            let cnt = line_max - line_min + 1 + line_offset;
            console.log(line_min+" - "+line_max+" / "+max_lines+"  "+line_offset);
            
            let disbl = 0;
            if(cnt > max_lines){
                $(this).parents('form').find('.line_min').removeClass('is-valid');
                $(this).parents('form').find('.line_max').removeClass('is-valid');
                $(this).parents('form').find('.line_min').addClass('is-invalid');
                $(this).parents('form').find('.line_max').addClass('is-invalid');
                disbl |= 1;
            }else{
                $(this).parents('form').find('.line_min').removeClass('is-invalid');
                $(this).parents('form').find('.line_max').removeClass('is-invalid');
                $(this).parents('form').find('.line_min').addClass('is-valid');
                $(this).parents('form').find('.line_max').addClass('is-valid');
                
                bru.line_min = line_min;
                bru.line_max = line_max;
                $(".lines-div[bru_id='"+bru_id+"']").empty();
                $(".lines-div[bru_id='"+bru_id+"']").append(
                    _this.create_lines(bru)
                );
            }
            
            if((line_offset%2)==1){
                $(this).parents('form').find('.line_offset').removeClass('is-valid');
                $(this).parents('form').find('.line_offset').addClass('is-invalid');
                disbl |= 2;
            }else{
                $(this).parents('form').find('.line_offset').removeClass('is-invalid');
                $(this).parents('form').find('.line_offset').addClass('is-valid');
            }
            
            if(disbl > 0){
                $(this).parents('form').find('.btn-save').prop('disabled', true);
            }else{
                $(this).parents('form').find('.btn-save').prop('disabled', false);
            }
        });
        
        div.find('.btn-measure').click(async function(){
            let row = $(this).parents('.params-row');
            let bru_id = parseInt(row.attr('bru-id'));
            
            if(!_this.measure_active){
                let res = await (new Rest()).bru_measure(bru_id);
                if(res.result === "success"){
                    console.log("Measure "+bru_id);
                    
                    $(".btn-line").removeClass("btn-success");
                    $(".btn-line").removeClass("btn-danger");
                    $(".btn-line").addClass("btn-secondary");

                    $(this).html(`
                        <div class="spinner-grow spinner-grow-sm ml-1" role="status">
                            <span class="sr-only">...</span>
                        </div>
                        Отмена
                    `);
                    $(this).removeClass("btn-secondary");
                    $(this).addClass("btn-success");
                    
                    $('.disbl-on-measure').prop('disabled', true);
                    
                    _this.measure_active = true;
                }
            }else{
                let res = await (new Rest()).bru_measure_cancel(bru_id);
                if(res.result == "success"){
                    console.log("Measure cancel "+bru_id);
                }
            }
        });
        
        div.find('.btn-mainum').click(async function(){
//            alertify.success("Подождите...");
            $(this).find(".spinner-grow").show();
            $(this).find(".mainum-text").html("Подождите...");
            $(this).removeClass("btn-secondary");
            $(this).addClass("btn-warning");
            
            let bru_id = parseInt($(this).attr('bru_id'));
            let ans = await (new Rest()).bru_reset(bru_id);
            if(ans.result == "success"){
                $(this).removeClass("btn-warning");
                $(this).addClass("btn-success");
//                alertify.success("Успешно. Обновление конфигурации...");
//                let bru = _this.find_bru(bru_id);
//                bru.switch_state = 0xFF;
//                _this.setState(); 
            }else{
                $(this).removeClass("btn-warning");
                $(this).addClass("btn-danger");
            }
            $(this).find(".mainum-text").html("Обновление конфигурации...");
            _this.update(false);
        });
        
        div.find('.btn-save').click(async function(){
            let row = $(this).parents('.params-row');
            let id = row.attr('bru-id');
            
            let bru = _this.find_bru(id);
            if(bru === null){
                console.log("BRU not found");
                return;
            }
                
            _this.check_line_objects(bru);

            bru.line_offset = parseInt(row.find('.line_offset').val());
            bru.line_min = parseInt(row.find('.line_min').val())-1;
            bru.line_max = parseInt(row.find('.line_max').val())-1;
            bru.test_mode_settle_time_ms = parseInt(row.find('.test_mode_settle_time_ms').val());
            bru.work_mode_eq_range_settle_time_ms = parseInt(row.find('.work_mode_eq_range_settle_time_ms').val());
            bru.work_mode_diff_range_settle_time_ms = parseInt(row.find('.work_mode_diff_range_settle_time_ms').val());
            bru.work_mode_cycle_period_sec = parseInt(row.find('.work_mode_cycle_period_sec').val());
            bru.max_meas_tolerance_proc = parseInt(row.find('.max_meas_tolerance_proc').val());
            bru.max_devi_tolerance_proc = parseInt(row.find('.max_devi_tolerance_proc').val());
            bru.redund_um_id = parseInt(row.find('.redund_um_id').val());//+_this.id_offset
            bru.mode = parseInt(row.find('.mode').val());

            bru.line = [];
            for(let i=bru.line_min; i<=bru.line_max; i++){
                let line = bru.linemap.get(i);
                bru.line.push(line);
            }

            console.log("Save BRU");
            console.log(JSON.stringify(bru));

            let res = await (new Rest()).set_bru_cfg(parseInt(id), bru);
            if(res.result == 'success'){
                alertify.success('Сохранено');
                _this.setState();
            }else{
                alertify.error(res.message);
            }
        });
        
        return div;
    }
    
    async update(request = false){
        let obj = await (new Rest()).get_bru_cfg(request);
        if(obj.result == "success"){
            this.data = obj.bru_cfg;
            console.dir(this.data);
            
            if(this.data !== null && this.data.length > 0){
                for(let i=0; i<this.data.length; i++){
                    this.data[i].linemap = new Map();
                    for(let j=0; j<this.data[i].line.length; j++){
                        let line = this.data[i].line[j];
                        this.data[i].linemap.set(line.line_id, line);
                    }
                }
                
                $('#no-bru').hide();

                console.dir(this.data);
                this.setState();
            }else{
                $('#no-bru').show();
            }
        }
    }
}