#include "filesmodel.h"
#include "json.hpp"
#include <QBitmap>
#include <QDir>
#include <QIcon>
#include <QPainter>
#include <QDebug>

using namespace nlohmann;

FilesModel::FilesModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant FilesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Horizontal){
        if(role == Qt::DisplayRole){
            switch(section){
            case Cols::colName:
                return QString("Файл");
            case Cols::colTargets:
                return QString("Направления");
            case Cols::colVolume:
                return QString("Громкость");
            case Cols::colScheduller:
                return QString("Планировщик");
            default:
                return QVariant();
            }
        }else if(role == Qt::SizeHintRole){
            switch(section){
            case Cols::colName:
                return 300;
            default:
                return QVariant();
            }
        }
    }

    return QVariant();
}

bool FilesModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int FilesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return scheduller.items.count();
}

int FilesModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return colCount;
}

QVariant FilesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto file = scheduller.items[index.row()];

    if(role == Qt::DisplayRole){
        switch(index.column()){
        case Cols::colName:
            return file->name;
        case Cols::colTargets:{
            QString txt;
            for(int i=0; i<32; i++){
                if(file->targets & (1<<i)){
                    if(!txt.isEmpty())
                        txt += ",";
                    txt += QString("%1").arg(i+1);
                }
            }
            return txt;
        }
        case Cols::colVolume:
            return file->volume;
        case Cols::colScheduller:
            return !file->bad ? file->toTaskString() : "(!)Ошибка настройки файла";
        }
    }else if(role == Qt::EditRole){
        switch(index.column()){
        case Cols::colName:
            return file->name;
        case Cols::colTargets:
            return file->targets;
        case Cols::colVolume:
            return file->volume;
        case Cols::colScheduller:
            return file->toTaskString();
        }
    }else if(role == Qt::DecorationRole){
        switch(index.column()){
        case Cols::colName:
            return !file->bad ? QIcon(":/icon/speaker.png") : QIcon(":/icon/cancel.png");
        case Cols::colScheduller:
            return (file->active_task) ?
                        QIcon(":/icon/check.png") : QIcon(":/icon/uncheck.png");
        }
    }else if(role == PathRole){
        return QString(file->path);
    }else if(role == Qt::ToolTipRole){
        return !file->bad ? file->name : "Информация о файле разрушена. Обновите настройки файла.";
    }
    return QVariant();
}

bool FilesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        auto file = scheduller.items[index.row()];
        if(role == Qt::EditRole){
            switch(index.column()){
            case Cols::colName:
                file->name = value.toString();
                break;
            case Cols::colTargets:
                file->targets = value.toInt();
                break;
            case Cols::colVolume:
                file->volume = value.toInt();
                break;
            case Cols::colScheduller:
//                file = FileInfo::forFile(value.toString());
                break;
            }
            scheduller.items.replace(index.row(), file);
            file->save();
        }
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags FilesModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    Qt::ItemFlags fl = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

    if(!readonly && (index.column() == Cols::colName || index.column() == Cols::colVolume)){
        fl |= Qt::ItemIsEditable;
    }

    return fl; // FIXME: Implement me!
}

bool FilesModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    for(int i=0; i<count; i++){
        auto file = FileInfo::forNew(filesDir, QString("Новый файл %1").arg(i+1));
        scheduller.items.insert(row+i, file);
    }
    endInsertRows();
}

bool FilesModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    endInsertColumns();
}

bool FilesModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);

    for(int i=0; i<count; i++){
        scheduller.removeFile(row);
    }

    endRemoveRows();
}

bool FilesModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    endRemoveColumns();
}


int FilesModel::findByPath(QString path)
{
    for(int i=0; i<scheduller.items.count(); i++){
        if(scheduller.items[i]->path == path){
            return i;
        }
    }
    return -1;
}

void FilesModel::update(){
    beginResetModel();
    scheduller.load();
    endResetModel();
}

//===================== TargetsDelegate =====================
TargetsDelegate::TargetsDelegate(int outs_cnt, QObject *parent) : QItemDelegate(parent), outs_cnt(outs_cnt)
{

}

QSize TargetsDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QItemDelegate::sizeHint(option, index);
}

void TargetsDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem myOption = option;

    QPen pen = painter->pen();
    QBrush brush = painter->brush();

    drawBackground(painter, option, index);

    if(index.column() == FilesModel::colTargets){
        uint32_t targets = index.model()->data(index, Qt::EditRole).toInt();

        myOption.displayAlignment = Qt::AlignCenter | Qt::AlignVCenter;

        int w = myOption.rect.width()/8;
        int h = myOption.rect.height()/4;
        int sz = (w<h) ? w : h;
        int m = (myOption.rect.width()-sz*8)/6;
        if(m < 0)
            m = 0;
        int dx = outs_cnt > 16 ? myOption.rect.width()/10 : myOption.rect.width()/5;

        painter->setClipRect(myOption.rect);
//        painter->setBrush(option.backgroundBrush);
//        painter->fillRect(myOption.rect, option.backgroundBrush);

        for(int i=0; i<outs_cnt; i++){
            int r = i/4;
            int c = i%4;

            bool active = targets & (1 << i);

            int x = myOption.rect.x()+myOption.rect.width()/2-dx*1.5+c*dx;
            int y = myOption.rect.y()+r*sz;
            if(outs_cnt > 16){
                x = myOption.rect.x()+myOption.rect.width()/2-dx*3.5-m+c*dx;
                if(i>=16){
                    x = myOption.rect.x()+myOption.rect.width()/2+m+c*dx;
                    y = myOption.rect.y()+(r-4)*sz;
                }
            }

            pen.setColor(Qt::gray);
            pen.setWidth(1);
            painter->setPen(pen);
            brush.setStyle(Qt::NoBrush);
            painter->setBrush(brush);
            painter->drawEllipse(x+1, y+1, sz-2, sz-2);

            if(active){
                pen.setColor(Qt::green);
                painter->setPen(pen);
                brush.setStyle(Qt::SolidPattern);
                brush.setColor(Qt::green);
                painter->setBrush(brush);
                painter->drawEllipse(x+2, y+2, sz-4, sz-4);
            }
        }

        painter->setClipRect(myOption.rect, Qt::NoClip);
    }else if(index.column()==FilesModel::colVolume){
        int vol = index.model()->data(index, Qt::EditRole).toInt();

        const int margin = 5;
        int w = myOption.rect.width()-margin*2;
        int h = myOption.rect.height()-margin*2;
        int barH = h/3;
        int barX = myOption.rect.x()+margin;
        int barY = myOption.rect.y()+margin;
        int barV = w*vol/100;
        int txtX = barX;
        int txtY = barY+barH+margin;
        int txtW = w;
        int txtH = h-barH-margin;

        pen.setColor(Qt::gray);
        pen.setWidth(2);
        painter->setPen(pen);
        brush.setStyle(Qt::NoBrush);
        painter->setBrush(brush);
        painter->drawRect(barX,barY,w,barH);

        pen.setColor(Qt::green);
        pen.setWidth(0);
        painter->setPen(pen);
        brush.setStyle(Qt::SolidPattern);
        brush.setColor(Qt::green);
        painter->setBrush(brush);
        painter->drawRect(barX+2,barY+2,barV-4,barH-4);

        pen.setColor(Qt::gray);
        pen.setWidth(1);
        painter->setPen(pen);
        painter->drawText(txtX, txtY, txtW, txtH, Qt::AlignHCenter, QString("%1%").arg(vol));
    }else{
        QRect decoRect = QRect(option.rect.x(), option.rect.y(), 48, option.rect.height());
        QIcon icon = index.model()->data(index, Qt::DecorationRole).value<QIcon>();
        QPixmap pix = icon.pixmap(32);
        if(myOption.state & QStyle::State_Selected){
            QBitmap mask = pix.createMaskFromColor(Qt::transparent);
            pix = QPixmap(pix.size());
            pix.fill(Qt::black);
            pix.setMask(mask);
        }
        drawDecoration(painter, option, decoRect, pix);

        QRect displayRect = QRect(option.rect.x()+48, option.rect.y(), option.rect.width()-48, option.rect.height());
        drawDisplay(painter, option, displayRect, index.model()->data(index, Qt::DisplayRole).toString());
    }
    drawFocus(painter, myOption, myOption.rect);
}

//QWidget *TargetsDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//    if(index.column() == FilesModel::colVolume){
//        int h = option.rect.height()/3;

//        QSlider* slider = new QSlider(parent);
//        slider->setMaximum(100);
//        slider->setOrientation(Qt::Horizontal);
//        slider->setStyleSheet(QString("QSlider{ "
//                                      "height: %1px; "
//                                      "} "
//                                      "QSlider::handle:horizontal { "
//                                      "height: %2px; "
//                                      "background: white; "
//                                      "margin: 0 -15px; "
//                                      "border-radius: %3px; "
//                                      "} ").arg(h, h, h/2));
//        return slider;
//    }else{
//        return QItemDelegate::createEditor(parent, option, index);
//    }
//}

//void TargetsDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
//{
//    if(index.column() == FilesModel::colVolume){
//        QSlider* slider = dynamic_cast<QSlider*>(editor);
//        if(slider){
//            int vol = index.model()->data(index, Qt::EditRole).toInt();
//            slider->setValue(vol);
//        }
//    }else{
//        QItemDelegate::setEditorData(editor, index);
//    }
//}

//void TargetsDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
//{
//    if(index.column() == FilesModel::colVolume){
//        QSlider* slider = dynamic_cast<QSlider*>(editor);
//        if(slider){
//            int vol = slider->value();
//            model->setData(index, vol);
//        }
//    }else{
//        QItemDelegate::setModelData(editor, model, index);
//    }
//}
