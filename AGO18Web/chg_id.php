<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?1">
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        include "navbar.php";
        include "alert.html";
        ?>
        
        <div class="container">
            <br>
            <h1><img src="img/settings.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Настройка ID</h1>
            <br>
            
            <ul class="nav nav-tabs shadow mx-0">
                <li class="nav-item">
                  <a class="nav-link active" href="#" id="tab-set">Упрощенная установка ID</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#" id="tab-chg">Смена ID</a>
                </li>
            </ul>
            
            <div class="row shadow">
                <div class="card col-4 p-3 ">
                    <div class="card-body">
                        <div class="input-group mb-3" id="grp-block-type">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="block-type-lbl">Тип блока</span>
                            </div>
                            <select id="block-type" class="form-control">
                                <option value="um">Усилитель</option>
                                <option value="bru">БРУ</option>
                                <option value="other">Другое</option>
                            </select>
                            <div class="input-group-append"><span class="input-group-text"></span></div>
                        </div>
                        <div class="input-group mb-3" id="grp-old-id">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="old-id-lbl">Старый ID</span>
                            </div>
                            <input id="old-id" type="text" class="form-control" placeholder="Старый ID" aria-label="Старый ID" aria-describedby="old-id-lbl">
                            <div class="input-group-append"><span class="input-group-text"></span></div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="new-id-lbl">Новый ID</span>
                            </div>
                            <input id="new-id" type="text" class="form-control" placeholder="Новый ID" aria-label="Новый ID" aria-describedby="new-id-lbl">
                            <div class="input-group-append"><span class="input-group-text"></span></div>
                        </div>
                        <div class="input-group mb-3" id="grp-offset">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="offset-lbl">Смещение</span>
                            </div>
                            <input id="offset" type="text" class="form-control" placeholder="Смещение" aria-label="Смещение" aria-describedby="offset-lbl">
                            <div class="input-group-append"><span class="input-group-text"></span></div>
                        </div>
                        <div class="input-group mb-3" id="grp-um-mode">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="um-mode-lbl">Режим УМ</span>
                            </div>
                            <select id="um-mode" class="form-control" placeholder="Режим УМ" aria-label="Режим УМ" aria-describedby="um-mode-lbl">
                                <option value="0">Основной</option>
                                <option value="1">Резервный</option>
                            </select>
                            <div class="input-group-append"><span class="input-group-text"></span></div>
                        </div>
                        
                        <div class="row">
                            <button id="btn-change-id" class="btn btn-outline-success col-4 ml-3">Изменить</button>
                            <button id="btn-set-id" class="btn btn-outline-success col-4 ml-3" >Установить</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-8 card " id="card-scan">
                    <div class="card-header row">
                        <button class="btn btn-outline-dark mb-3 ml-3 col-2" id="scan-ids" >Опрос</button>
                        <div class="form-group form-check col ml-3 mt-1">
                            <input type="checkbox" class="form-check-input" id="broadcast" checked>
                            <label class="form-check-label" for="broadcast">Широковещательно</label>
                        </div>
                        <div class="input-group mb-3 col-9">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Сортировка</span>
                            </div>
                            <select class="form-control" id="sort-prm">
                                <option value="id" selected>ID</option>
                                <!--<option value="uid">UID</option>-->
                                <option value="descr">Описание</option>
                            </select>
                            <div class="input-group-append"><span class="input-group-text"></span></div>
                        </div>
                        <div class="col-3 mt-1">
                            <input type="checkbox" class="form-check-input" id="sort-desc">
                            <label class="form-check-label" for="sort-desc">По убыванию</label>
                        </div>
                    </div>
                    <div class="card-body mb-3" id="ids">
                        
                    </div>
                </div>
                
            </div>
        </div>
        
        <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript" src="js/alert.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/constants.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/rest.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/get_setts.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/MyView.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/ChIdView.js" crossorigin="anonymous"></script>
        <script>
            alertify.defaults.glossary.ok = 'OK';
            alertify.defaults.glossary.cancel = 'Отмена';    
            alertify.defaults.transition = "fade";
            alertify.defaults.theme.ok = "btn btn-outline-primary";
            alertify.defaults.theme.cancel = "btn btn-outline-danger"
            
            var chIdView = new ChIdView($("#ids"), "set");
            
            get_settings(function(setts){
                chIdView.init();
            });
            
            $("#tab-chg").click(function(){
                console.log("Chg");
                $("#tab-chg").addClass("active");
                $("#tab-set").removeClass("active");
                
                chIdView = new ChIdView($("#ids"), "chg");
                chIdView.init();
            });
            
            $("#tab-set").click(function(){
                console.log("Set");
                $("#tab-set").addClass("active");
                $("#tab-chg").removeClass("active");
                
                chIdView = new ChIdView($("#ids"), "set");
                chIdView.init();
            });
        </script>
    </body>
</html>
