#include "fileinfo.h"
#include "json.hpp"
#include "scheduller.h"
#include "log.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>

using namespace nlohmann;

FileInfo::FileInfo(QString filePath, QObject *parent) : QObject(parent),
    path(filePath)
{
    setInfoPath();

    QFileInfo info(path);
    name = info.baseName();

    reload();
}

FileInfo::FileInfo(QString dir, QString name, QObject *parent) : QObject(parent)
{
    path = dir + "/" + name + ".wav";
    setInfoPath();

    QFileInfo info(path);
    name = info.baseName();
}

void FileInfo::setInfoPath(){
    QFileInfo info(path);
    auto dir = info.absoluteDir();
    auto name = info.baseName();
    dir.setPath(dir.absoluteFilePath(infoDirName));
    dir.mkpath(dir.absolutePath());

    infoPath = dir.absoluteFilePath(name+".info");//path + ".info";
}

void FileInfo::reload(){
    QFile file(infoPath);
    if(file.open(QIODevice::ReadOnly)){
        auto bytes = file.readAll();
        auto txt = QString(bytes).toStdString();
        try{
            json js = json::parse(txt);
            if(js.is_object()){
                if(js.contains("targets") && js["targets"].is_number_integer()){
                    targets = js["targets"];
                }
                if(js.contains("name") && js["name"].is_string()){
                    name = QString::fromStdString(js["name"]);
                }
                if(js.contains("volume") && js["volume"].is_number_integer()){
                    volume = js["volume"];
                }
                if(js.contains("active_task") && js["active_task"].is_boolean()){
                    active_task = js["active_task"];
                }
                if(js.contains("active_task") && js["active_task"].is_boolean()){
                    active_task = js["active_task"];
                }
                if(js.contains("begin") && js["begin"].is_number_integer()){
                    begin = js["begin"];
                }
                if(js.contains("end") && js["end"].is_number_integer()){
                    end = js["end"];
                }
                if(js.contains("period") && js["period"].is_number_integer()){
                    period = js["period"];
                }
            }
        }catch(json::exception &e){
            _log.log(e.what());
            bad = true;
        }

        file.close();
    }
}

json FileInfo::toJson(){
    json js = json::object({
                               {"targets", targets},
                               {"name", name.toStdString()},
                               {"volume", volume},
                               {"active_task", active_task},
                               {"begin", begin},
                               {"end", end},
                               {"period", period},
                           });
    return js;
}

void FileInfo::save()
{
    QFile file(infoPath);
    if(file.open(QIODevice::WriteOnly)){
        json js = toJson();
        std::string txt = js.dump(4);
        file.write(txt.c_str(), txt.length());

        file.close();

        bad = false;
    }
}

PFileInfo FileInfo::forFile(QString filePath, QObject *parent)
{
    return PFileInfo(new FileInfo(filePath, parent));
}

PFileInfo FileInfo::forNew(QString dir, QString name, QObject *parent)
{
    return PFileInfo(new FileInfo(dir, name, parent));
}

bool FileInfo::isSignaled(qint64 time)
{
    bool inRange = false;
    if(begin <= end && time >= begin && time < end){
        inRange = true;
    }
    if(begin > end && (time >= begin || time < end)){
        inRange = true;
    }
    if(!inRange){
        last_signaled = 0;
    }

    return inRange && (last_signaled==0 || time > (last_signaled + period));
}

void FileInfo::resetTask()
{
    last_signaled = scheduller.currentTime();
}

QString FileInfo::toTaskString()
{
    if(begin || end || period){
        return QString("С %1 по %2 каждые %3").arg(
                    Scheduller::timeToStr(begin),
                    Scheduller::timeToStr(end),
                    Scheduller::timeToStr(period));
    }else{
        return "";
    }
}
