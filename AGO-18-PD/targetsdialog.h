#ifndef TARGETSDIALOG_H
#define TARGETSDIALOG_H

#include <QDialog>

namespace Ui {
class TargetsDialog;
}

class TargetsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TargetsDialog(QWidget *parent = nullptr);
    ~TargetsDialog();

    void setNames(QStringList names, int outs_cnt);
    void setOuts(uint64_t outs);
    uint64_t getOuts();
private:
    Ui::TargetsDialog *ui;
};

#endif // TARGETSDIALOG_H
