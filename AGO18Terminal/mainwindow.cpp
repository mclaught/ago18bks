#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtNetwork/QUdpSocket>
#include <QtNetwork/QNetworkDatagram>
#include <QElapsedTimer>
#include <QFile>
#include <QTcpSocket>
#include <QThread>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>
#include <QtConcurrent>
#include "mypcm.h"

#define CHANNELS    16

QUdpSocket* udpSock[CHANNELS];


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for(int i=0; i<CHANNELS; i++){
        udpSock[i] = new QUdpSocket(this);
        udpSock[i]->setSocketOption(QAbstractSocket::SendBufferSizeSocketOption, 10*1024*1024);
        udpSock[i]->bind(QHostAddress::Any, i==0 ? 45920 : 46000);
        connect(udpSock[i], &QIODevice::readyRead, [i,this](){
            packet_buf_t pack;
            int rd = udpSock[i]->readDatagram(reinterpret_cast<char*>(&pack), 1024);

            if(pack.cmd != SND_CHANK_REQ){
                QString dump;
                int cnt = 30;
                if((pack.hdr.len-1)<cnt)
                    cnt = pack.hdr.len-1;
                for(int j=0; j<cnt; j++)
                    dump += QString(" %1").arg(pack.data[j], 2, 16, QChar('0'));

                ui->logList->addItem(QString("cmd(%1 from:%2 to:%3)%4")
                                     .arg(pack.cmd)
                                     .arg(pack.hdr.adr_from, 2, 16, QChar(' '))
                                     .arg(pack.hdr.adr_to, 2, 16, QChar(' '))
                                     .arg(dump));
            }

            if(pack.hdr.type == PACK_CFG || pack.hdr.type == PACK_BRU_CFG){
                if(pack.cmd == 4){
//                    on_btOUT_CPB_clicked();
                }

                if(pack.cmd == BRU_GET_CFG && pack.hdr.adr_from!=23 && pack.hdr.adr_from!=24){
                    sendBRUCfg(23, 0, 0);
                    sendBRUCfg(24, 0, 1);
                }

                if(pack.cmd == BRU_TEST_MODE_START){
                    uint8_t addr = pack.hdr.adr_to;
                    send_BRU_TEST_MODE_START(addr);
                }

                if(pack.cmd == BRU_LINE_MEASURE){
                    uint8_t addr = pack.hdr.adr_to;
                    uint8_t line = pack.data[0];
                    send_BRU_LINE_MEASURE(addr, line);
                }

                if(pack.cmd == BRU_RESET_SWITCH_CFG){
                    uint8_t addr = pack.hdr.adr_to;
                    send_BRU_RESET_SWITCH_CFG(addr);
                }

                if(pack.cmd == GET_ID && pack.hdr.adr_from==8){
                    for(int i=0; i<5; i++){
                        send_GET_ID(pack.hdr.adr_from, i);
                    }
                }

                if(pack.cmd == BKS_GET_INP_MODE && pack.hdr.adr_from==8){
                    for(int i=0; i<8; i+=3){
                        send_BKS_GET_INP_MODE(i, (1<<i*2));
                    }
                }

                if(pack.cmd == BRP_SET_CRITICAL_ALARMS && pack.hdr.adr_from==8){
                    send_BRP_SET_CRITICAL_ALARMS(0);
                }

            }else if(pack.hdr.type == PACK_AUDIO){
                play_audio(reinterpret_cast<packet_audio_t*>(&pack));
            }else if(pack.hdr.type == PACK_GEN_CALL){
                if(pack.cmd == PING && i==0){//
                    qDebug() << "PING" << i;
                    sendPing(0, 0);
                    sendPing(1, 0);
                    sendPing(2, 0);
                    sendPing(8, 0);
                    sendPing(9, 3);
                    sendPing(26, 2);
                }
            }
        });
    }

    for(int i=0; i<32; i++){
        dstCheck[i] = new QCheckBox(this);
        dstCheck[i]->setText(QString("%1").arg(i));
        ui->destChLayout->addWidget(dstCheck[i], i / 4, i % 4);
    }

    ui->cbAlarm->addItem("T100");
    ui->cbAlarm->addItem("T125");
    ui->cbAlarm->addItem("SD");
    ui->cbAlarm->addItem("UM_ERROR");
    ui->cbAlarm->addItem("HW_ERROR");
    ui->cbAlarm->addItem("DS1621S_OVR60");
    ui->cbAlarm->addItem("BRU_UM_NOT_ANSWER");
    ui->cbAlarm->addItem("BRU_LINE_SHORT");
    ui->cbAlarm->addItem("BRU_LINE_OPEN");
    ui->cbAlarm->addItem("BRU_LINE_OVER_LIM");
    ui->cbAlarm->addItem("BRU_SWITCH_TO_BYPASS_ERROR");
    ui->cbAlarm->addItem("BRU_SWITCH_BACK_ERROR");
    ui->cbAlarm->addItem("BRU_REDUND_UM_ACTIVE");
}

MainWindow::~MainWindow()
{
    for(int i=0; i<CHANNELS; i++){
        udpSock[i]->close();
    }
    delete ui;
}

//void MainWindow::readUdp(){

//}

void MainWindow::on_pushButton_clicked()
{
    packet_buf_t pack;
    pack.hdr.len = sizeof(alarm_item_t)+1;
    pack.hdr.type = PACK_ALARM;
    pack.hdr.adr_to = 0x80;
    pack.hdr.adr_from = 1;
    pack.hdr.repeate_cnt = 1;

    pack.cmd = ALARM_MSG;

    pack.data[0] = 1;
    reinterpret_cast<alarm_item_t*>(pack.data+1)->alarm_code = DS1621S_OVR60;
    reinterpret_cast<alarm_item_t*>(pack.data+1)->param = 3;

//    pack.data[0] = 0;
//    pack.data[1] = 0;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len+1);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    QString fileName = ui->fileEdit->text();
    if(fileName == "")
        return;

    QFile file(fileName);
    file.open(QIODevice::ReadOnly);

    file.seek(0xae);//0xB7D2

    setGo(true);

    QElapsedTimer timer;
    timer.start();
    auto last_tm = timer.elapsed();
    auto stat_tm = timer.elapsed();
    uint32_t stat = 0;

    while(file.pos() < file.size() && go){


        packet_audio_t pack;
        pack.hdr.len = AUDIO_LEN_BYTES + sizeof(audio_hdr_t) + 1;
        pack.hdr.type = PACK_AUDIO;
        pack.hdr.adr_to = 2;
        pack.hdr.repeate_cnt = 0;

        pack.cmd = SND_STREAM;


        file.read(reinterpret_cast<char*>(pack.pcm), AUDIO_LEN_BYTES);


        for(int i=0; i<CHANNELS; i++){
            pack.hdr.adr_from = 0;
            pack.audio_hdr.sender = i;
            pack.audio_hdr.recip = 5;

            QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+1+sizeof(audio_hdr_t)+AUDIO_LEN_BYTES*2);

            qint64 sent = -1;
            while(sent == -1){
                sent = udpSock[i]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
            }
            if(sent > 0){
                stat += sent;
                if((timer.elapsed() - stat_tm) > 1000){
                    ui->labelStat->setText(QString("%1").arg(stat));
                    stat = 0;
                    stat_tm = timer.elapsed();
                }
            }else if(sent == -1){
                qDebug() << udpSock[i]->errorString();
            }

            QApplication::processEvents();
            //QThread::msleep(2);
        }

        while((timer.elapsed() - last_tm) < 5){
            QApplication::processEvents();
        }
        last_tm = timer.elapsed();
    }

    file.close();
}

void MainWindow::on_pushButton_3_clicked()
{
    setGo(false);
}

void MainWindow::on_pushButton_4_clicked()
{
    QString fileName = ui->fileEdit->text();
    if(fileName == "")
        return;

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)){
        QMessageBox::warning(this, "Ошибка", "Ошибка открытия файла");
    }


    setGo(true);

    QElapsedTimer timer;
    timer.start();
    auto last_tm = timer.elapsed();
    auto stat_tm = timer.elapsed();
    uint32_t stat = 0;

    while(file.pos() < file.size() && go){


        packet_audio_t pack;

        file.read(reinterpret_cast<char*>(&pack.hdr), sizeof(pack_hdr_t));
        file.read(reinterpret_cast<char*>(&pack.cmd), pack.hdr.len);

        QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+1+sizeof(audio_hdr_t)+AUDIO_LEN_BYTES*2);

        qint64 sent = -1;
        while(sent == -1){
            sent = udpSock[0]->writeDatagram(dgram, QHostAddress(ui->lineEditIP->text()), ui->lineEditPort->text().toInt());//("192.168.1.146")
        }
        if(sent > 0){
            stat += sent;
            if((timer.elapsed() - stat_tm) > 1000){
                ui->labelStat->setText(QString("%1").arg(stat));
                stat = 0;
                stat_tm = timer.elapsed();
            }
        }else if(sent == -1){
            qDebug() << udpSock[0]->errorString();
        }

        while((timer.elapsed() - last_tm) < 5){
            QApplication::processEvents();
        }
        last_tm = timer.elapsed();
    }

    file.close();
}

void MainWindow::tcpError(QAbstractSocket::SocketError socketError){
    QMessageBox::warning(this, "Ошибка TCP", tcp->errorString());
}

void MainWindow::on_pushButton_5_clicked()
{
    QString fileName = ui->fileEdit->text();
    if(fileName == "")
        return;

    tcp = new QTcpSocket();
    tcp->bind(QHostAddress::Any);
    connect(tcp, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), [=](QAbstractSocket::SocketError socketError){
        QMessageBox::warning(this, "Ошибка TCP", tcp->errorString());
        setGo(false);
    });
    connect(tcp, &QTcpSocket::connected, [=](){
        QFile file(fileName);
        if(!file.open(QIODevice::ReadOnly)){
            QMessageBox::warning(this, "Ошибка", "Ошибка открытия файла");
            return;
        }

        uint32_t sign = 0;
        file.read(reinterpret_cast<char*>(&sign), sizeof(uint32_t));
        if(sign != 'SUPO'){
            QMessageBox::warning(this, "Ошибка","Не тот формат");
            file.close();
            return;
        }

        file.read(reinterpret_cast<char*>(&bitrate), sizeof(uint32_t));

        setGo(true);

        pd_start_t pd_start = {20, 1, 32000, 1, {0, 0}, 0};
        pd_start.in_ch[0] = static_cast<uint8_t>(ui->spinSrcCh->value());
        for(int i=0; i<32; i++){
            if(dstCheck[i]->isChecked()){
                pd_start.outs |= (1 << i);
            }
        }
        pd_start.bitrate = static_cast<uint16_t>(bitrate);

        tcp->write(reinterpret_cast<char*>(&pd_start), sizeof(pd_start_t));

        QElapsedTimer timer;
        timer.start();
        auto last_tm = timer.elapsed();

        while(file.pos() < file.size() && go){
            const int pack_sz = 20*bitrate/8000;

            uint8_t buf[pack_sz];
            file.read(reinterpret_cast<char*>(buf), pack_sz);

            tcp->write(reinterpret_cast<char*>(buf), pack_sz);

            ui->labelStat->setText(QString("%1").arg(file.pos()));

            while((timer.elapsed() - last_tm) < 20){
                QApplication::processEvents();
            }
            last_tm = timer.elapsed();
        }

        tcp->disconnectFromHost();

        delete tcp;

        file.close();
    });
    tcp->connectToHost(QHostAddress(ui->lineEditIP->text()), ui->lineEditPort->text().toInt());

}


void MainWindow::on_toolButton_clicked()
{
    auto fileName = QFileDialog::getOpenFileName(this, "Выбрать файл", QDir::currentPath(), "Все(*.*);; OPUS(*.opus);; PCM(*.wav);; DAT(*.dat)");//"d:\\Work\\AGO-18\\01.opus";
    ui->fileEdit->setText(fileName);
}

void MainWindow::on_pushButton_6_clicked()
{
}

void MainWindow::on_pushButton_7_clicked()
{
    if(go)
        return;

    QString fileName = ui->fileEdit->text();
    if(fileName == "")
        return;

    pd_stream.status = 1;

    tcp = new QTcpSocket();

    tcp->bind(QHostAddress::Any);
    connect(tcp, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), [=](QAbstractSocket::SocketError socketError){
        QMessageBox::warning(this, "Ошибка TCP", tcp->errorString());
        setGo(false);
    });
    connect(tcp, &QTcpSocket::readyRead, [=](){
        tcp->read(reinterpret_cast<char*>(&pd_stream), sizeof(pd_stream_t));
        qDebug() << "Stream = " << pd_stream.status;
    });
    connect(tcp, &QTcpSocket::connected, [=](){
        MyPCM pcm(fileName, 5, true);
        if(pcm.error){
            QMessageBox::warning(this, "Ошибка PCM",pcm.error_str);
            tcp->disconnectFromHost();
            return;
        }

        setGo(true);

        pd_start_t pd_start = {5, 0, 32000, 1, {0, 0}, 0};
        pd_start.in_ch[0] = static_cast<uint8_t>(ui->spinSrcCh->value());
        for(int i=0; i<32; i++){
            if(dstCheck[i]->isChecked()){
                pd_start.outs |= (1 << i);
            }
        }
        pd_start.bitrate = pcm.byte_rate*8;

        tcp->write(reinterpret_cast<char*>(&pd_start), sizeof(pd_start_t));

        QElapsedTimer timer;
        timer.start();
        auto last_tm = timer.nsecsElapsed();

        while(pcm.file_pos < pcm.file_size && go){
            if(pd_stream.status == 0){
                QApplication::processEvents();
                continue;
            }

            const int pack_sz = pcm.frame_size;

            uint8_t buf[pack_sz];
            pcm.readFrame(reinterpret_cast<char*>(buf));

            tcp->write(reinterpret_cast<char*>(buf), pack_sz);

            ui->labelStat->setText(QString("%1").arg(pcm.file_pos));

            period = 5000*100/pd_stream.status;
            ui->period->setValue(period);
            while((timer.nsecsElapsed() - last_tm) < period*1000){//4950000 ui->period->value()
                QApplication::processEvents();
            }
            last_tm = timer.nsecsElapsed();
        }

        tcp->disconnectFromHost();

        delete tcp;
    });
    tcp->connectToHost(QHostAddress(ui->lineEditIP->text()), ui->lineEditPort->text().toInt());
}

void MainWindow::requestPacket(QUdpSocket* udp, int cnt){
    packet_buf_t pack;
    pack.hdr.adr_from = 0;
    pack.hdr.adr_to = 0xFF;
    pack.hdr.type = PACK_CFG;
    pack.hdr.len = 3;
    pack.cmd = SND_CHANK_REQ;
    pack.data[0] = cnt;

    udp->writeDatagram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+3, QHostAddress(BC_IP), 46000);

    req_tm = timer.nsecsElapsed();
}

void MainWindow::on_btnUDP_clicked()
{
    if(go)
        return;

    QString fileName = ui->fileEdit->text();
    if(fileName == "")
        return;

    pd_stream.status = 1;

    auto host = QHostAddress(ui->lineEditIP->text());
    auto port = ui->lineEditPort->text().toInt();

    QUdpSocket* udp = new QUdpSocket();

    udp->bind(QHostAddress::Any);
    connect(udp, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), [=](QAbstractSocket::SocketError socketError){
        QMessageBox::warning(this, "Ошибка UDP", udp->errorString());
        setGo(false);
    });
    connect(udp, &QTcpSocket::readyRead, [=](){
        packet_buf_t pack_req;
        if(udp->readDatagram(reinterpret_cast<char*>(&pack_req), 2048) > 0){
            if(pack_req.cmd == SND_CHANK_REQ){
                request_cnt = pack_req.data[0];
            }
        }
    });

    MyPCM pcm(fileName, 5, true);
    if(pcm.error){
        QMessageBox::warning(this, "Ошибка PCM",pcm.error_str);
        tcp->disconnectFromHost();
        return;
    }

    setGo(true);

    start_stop_t start_pack;
    start_pack.hdr.adr_from = 12;
    start_pack.hdr.adr_to = 0xFF;
    start_pack.hdr.type = PACK_CFG;
    start_pack.hdr.len = 6;
    start_pack.hdr.repeate_cnt = 0;
    start_pack.cmd = SET_RECIPNT_LIST;
    start_pack.src_ch = ui->spinSrcCh->value();
    start_pack.recipients = 0;

    for(int i=0; i<32; i++){
        if(dstCheck[i]->isChecked()){
            start_pack.recipients |= (1 << i);
        }
    }

    udp->writeDatagram(reinterpret_cast<char*>(&start_pack), sizeof(start_stop_t), host, port);

    timer.start();
    auto last_tm = timer.nsecsElapsed();

    while(pcm.file_pos < pcm.file_size && go){
        if(request_cnt > 0){
            const int pack_sz = pcm.frame_size;

            packet_audio_t audio_pack;
            audio_pack.hdr.adr_from = 6;
            audio_pack.hdr.adr_to = 0xFF;
            audio_pack.hdr.type = PACK_AUDIO;
            audio_pack.hdr.len = sizeof(audio_hdr_t)+AUDIO_SAMPLES*2+1;
            audio_pack.hdr.repeate_cnt = 0;
            audio_pack.cmd = SND_STREAM;
            audio_pack.audio_hdr.recip = 12;
            audio_pack.audio_hdr.sender = ui->spinSrcCh->value();
            pcm.readFrame(reinterpret_cast<char*>(audio_pack.pcm));
            udp->writeDatagram(reinterpret_cast<char*>(&audio_pack), sizeof(packet_audio_t)-AUDIO_SAMPLES*2, host, port);

            qDebug() << "req prop tm " << (timer.nsecsElapsed() - req_tm);

            ui->labelStat->setText(QString("%1").arg(pcm.file_pos));

            request_cnt--;
        }

        if(ui->reqEmul->checkState() == Qt::Checked && (timer.nsecsElapsed() - last_tm) >= ui->period->value()*1000){
            requestPacket(udp, 1);
            last_tm = timer.nsecsElapsed();
        }

        QApplication::processEvents();

//        while((timer.nsecsElapsed() - last_tm) < ui->period->value()*1000){//4950000 ui->period->value()
//            QApplication::processEvents();
//        }
//        last_tm = timer.nsecsElapsed();
    }

    start_pack.cmd = CLEAR_RECIPNT_LIST;
    udp->writeDatagram(reinterpret_cast<char*>(&start_pack), sizeof(start_stop_t), host, port);

    delete udp;
}

void MainWindow::setGo(bool _go){
    go = _go;

    ui->btnStop->setEnabled(_go);

    ui->pushButton_2->setEnabled(!_go);
    ui->pushButton_4->setEnabled(!_go);
    ui->pushButton_5->setEnabled(!_go);
    ui->pushButton_7->setEnabled(!_go);
    ui->btnUDP->setEnabled(!_go);

    ui->btnRequest->setEnabled(!_go);
    ui->btnStopAudio->setEnabled(_go);
}

void MainWindow::on_btnStop_clicked()
{
    setGo(false);
}

void MainWindow::closeEvent(QCloseEvent *event){
    if(!go)
        event->accept();
    else
        event->ignore();
}

void MainWindow::alarmSet(quint8 _set){

    int alrm = ui->cbAlarm->currentIndex();

    packet_buf_t pack;
    pack.hdr.len = 0;
    pack.hdr.type = PACK_ALARM;
    pack.hdr.adr_to = 0x80;
    pack.hdr.adr_from = 26;
    pack.hdr.repeate_cnt = 1;

    pack.cmd = ALARM_MSG;

    pack.data[0] = 0;
    for(int i=0; i<32; i++){
        if(dstCheck[i]->checkState() == Qt::Checked){
            reinterpret_cast<alarm_item_t*>(pack.data+1+pack.data[0]*sizeof(alarm_item_t))->alarm_code = alrm | _set;
            reinterpret_cast<alarm_item_t*>(pack.data+1+pack.data[0]*sizeof(alarm_item_t))->param = i;
            pack.data[0]++;
            pack.hdr.len = pack.data[0]*sizeof(alarm_item_t)+2;
        }
    }
//    if(alrm==HW_ERROR){
//        reinterpret_cast<alarm_item_t*>(pack.data+1)->param = 17;
//    }
    if(alrm==BRU_LINE_OVER_LIM){
        reinterpret_cast<alarm_item_t*>(pack.data+1)->param = 7;

        uint8_t meas[] = {0x08, 0x13, 0x00, 0x05, 0x0C, 0x00, 0x23, 0xA6, 0xA7, 0xBB, 0x45, 0x74, 0x53, 0x5A, 0x44, 0x07, 0x03, 0x00, 0x4C};
        QByteArray meas_dgram(reinterpret_cast<char*>(&meas), 19);
        auto sent = udpSock[0]->writeDatagram(meas_dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
        if(sent > -1){
            qDebug() << "sent" << sent;
        }else{
            qDebug() << "error" << udpSock[0]->errorString();
        }
    }

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::on_btnAlalrmSet_clicked()
{
    alarmSet(0);
}

void MainWindow::on_btnAlalrmReset_clicked()
{
    alarmSet(0x80);
}

void MainWindow::send_OUT_CPB(qint8 addr){
//    const uint8_t data[] = {0x00, 0x00, 0xb3, 0xc2, 0x00, 0x00, 0x40, 0x41, 0x00, 0x00, 0x00, 0x3f, \
//                            0x00, 0x00, 0x20, 0xc2, 0x00, 0x00, 0x20, 0x42, 0x00, 0x00, 0xa0, 0xc1, 0x00, 0x00, 0xa0, 0x41, \
//                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x3f, \
//                            0x2c, 0x01, 0x00, 0x00, 0x50, 0x46, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, \
//                            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, \
//                            0xb4, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, \
//                            0x04, 0x00, 0x1e, 0x3c, 0x64, \

//                            0x00, 0x00, 0x20, 0xc0, 0x00, 0x00, 0x20, 0xc0, \
//                            0x02, 0x00, 0x00, 0x00,
//                            0x00, 0x00, 0x80, 0x3f, 0xcd, 0xcc, 0x4c, 0x3e, 0xd0, 0x07, 0x00, \
//                            0x00, 0x00, 0x00, 0x80, 0x3f, 0xcd, 0xcc, 0x4c, 0x3e, 0x58, 0x1b, 0x00, 0x00, 0x00, 0x00, 0x80, \
//                            0x3f, 0x00, 0x00, 0x80, 0x3f, 0xcd, 0xcc, 0x4c, 0x3e, 0xd0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x80, \
//                            0x3f, 0xcd, 0xcc, 0x4c, 0x3e, 0x58, 0x1b, 0x00, 0x00, 0x00, 0x00, 0x80, 0x3f, 0x00, 0x00, 0x00, \
//                            0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00};

    snd_out_capab_init_packet_t pack;
    pack.hdr.len = sizeof(snd_out_capab_t)+sizeof(snd_out_cfg_t)+1;
    pack.hdr.type = PACK_CFG;
    pack.hdr.adr_to = 0x80;
    pack.hdr.adr_from = addr;
    pack.hdr.repeate_cnt = 0;

    pack.cmd = 5;

//    memcpy(pack.data, data, pack.hdr.len-1);
    pack.snd_capab.eq_capb.fmax_hz = 16000;
    pack.snd_capab.eq_capb.fmin_hz = 300;
    pack.snd_capab.eq_capb.max_band = 1;
    pack.snd_capab.eq_capb.max_gain_dB = 20;
    pack.snd_capab.eq_capb.max_out_gain_dB = 40;
    pack.snd_capab.eq_capb.min_band = 0;
    pack.snd_capab.eq_capb.min_gain_dB = -20;
    pack.snd_capab.eq_capb.min_out_gain_dB = -40;
    pack.snd_capab.eq_capb.sections = 2;

    pack.snd_capab.hpf_capb.number = 4;
    pack.snd_capab.hpf_capb.fcut[0] = 0;
    pack.snd_capab.hpf_capb.fcut[1] = 80;
    pack.snd_capab.hpf_capb.fcut[2] = 120;
    pack.snd_capab.hpf_capb.fcut[3] = 160;

    pack.snd_capab.outp_capb.max_dB = 5;
    pack.snd_capab.outp_capb.min_dB = -20;
    pack.snd_capab.outp_capb.step_dB = 1;

    pack.snd_capab.pa_capb.out_lvl_amount = 4;
    pack.snd_capab.pa_capb.out_lvl[0] = 0;
    pack.snd_capab.pa_capb.out_lvl[1] = 25;
    pack.snd_capab.pa_capb.out_lvl[2] = 50;
    pack.snd_capab.pa_capb.out_lvl[3] = 100;

    for(int i=0; i<2; i++){
        pack.snd_cfg.lvl[i] = 0.3;
        pack.snd_cfg.amplf_volt[i] = 1+i;
        pack.snd_cfg.fcut_indx[i] = 1+i;
        pack.snd_cfg.eq[i].sections = 2;
        pack.snd_cfg.eq[i].outGain = 0.6;
        for(int j=0; j<2; j++){
            pack.snd_cfg.eq[i].ec[j].freq = 500 + j*2000;
            pack.snd_cfg.eq[i].ec[j].band = 0.2+0.1*j;
            pack.snd_cfg.eq[i].ec[j].gain = 0;
        }
    }

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::send_IN_CPB(qint8 addr){
    snd_in_capab_init_packet_t pack;

    pack.hdr.len = sizeof(snd_in_capab_t) + sizeof(snd_in_cfg_t) + 1;
    pack.hdr.type = PACK_CFG;
    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 0x80;
    pack.hdr.repeate_cnt = 0;

    pack.cmd = 4;

    pack.snd_capab.inp_capb.min_dB = -80;
    pack.snd_capab.inp_capb.max_dB = 20;
    pack.snd_capab.inp_capb.step_dB = 1;

    pack.snd_capab.eq_capb.fmax_hz = 16000;
    pack.snd_capab.eq_capb.fmin_hz = 300;
    pack.snd_capab.eq_capb.max_band = 1;
    pack.snd_capab.eq_capb.max_gain_dB = 20;
    pack.snd_capab.eq_capb.max_out_gain_dB = 40;
    pack.snd_capab.eq_capb.min_band = 0;
    pack.snd_capab.eq_capb.min_gain_dB = -20;
    pack.snd_capab.eq_capb.min_out_gain_dB = -40;
    pack.snd_capab.eq_capb.sections = 0;

    pack.snd_cfg.lvl[0] = 2;
    pack.snd_cfg.lvl[1] = 3;
    pack.snd_cfg.eq[0].sections = 0;
    pack.snd_cfg.eq[0].outGain = 0;
    pack.snd_cfg.eq[1].sections = 0;
    pack.snd_cfg.eq[1].outGain = 0;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::sendBRUCfg(uint8_t id, uint8_t switch_state, int n)
{
    bru_cfg_packet_t pack;

    pack.hdr.adr_from = id;
    pack.hdr.adr_to = 0x80;
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_BRU_CFG;
    pack.hdr.len = sizeof(bru_cfg_t)+1;

    pack.cmd = BRU_GET_CFG;

    pack.bru_cfg.lines = 20;
    pack.bru_cfg.line_offset = 0;
    pack.bru_cfg.line_min = n==0 ? 0 : 16;
    pack.bru_cfg.line_max = n==0 ? 15 : 23;
    pack.bru_cfg.test_mode_settle_time_ms = 100;
    pack.bru_cfg.work_mode_eq_range_settle_time_ms = 110;
    pack.bru_cfg.work_mode_diff_range_settle_time_ms = 120;
    pack.bru_cfg.work_mode_cycle_period_sec = 10;
    pack.bru_cfg.mode = 0;
    pack.bru_cfg.max_meas_tolerance_proc = 30;
    pack.bru_cfg.max_devi_tolerance_proc = 30;
    pack.bru_cfg.switch_state = switch_state;
    pack.bru_cfg.redund_um_id = 1;
    for(int i=pack.bru_cfg.line_min; i<=pack.bru_cfg.line_max; i++){
        int line_index = i - pack.bru_cfg.line_min;
        pack.bru_cfg.line[line_index].low_trshld_ohm = line_index;
        pack.bru_cfg.line[line_index].high_trshld_ohm = 10.1;
        pack.bru_cfg.line[line_index].shortOhm = line_index;
        pack.bru_cfg.line[line_index].openOhm = 10.1;
        pack.bru_cfg.line[line_index].ref_value_ohm = 4.2;
        pack.bru_cfg.line[line_index].state = 0;
        pack.bru_cfg.line[line_index].range = 0;
    }

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::send_BRU_TEST_MODE_START(uint8_t addr)
{
    packet_buf_t pack;

    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 0x80;
    pack.hdr.len = 5;
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_BRU_CFG;

    pack.cmd = BRU_TEST_MODE_START;

    *reinterpret_cast<uint32_t*>(pack.data) = 1000;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::send_BRU_LINE_MEASURE(uint8_t addr, uint8_t line)
{
    QThread::msleep(1000);//qrand() % 4000

    bru_measure_packet_t pack;

    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 0x80;
    pack.hdr.len = sizeof(bru_measure_t)+1;
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_BRU_CFG;

    pack.cmd = BRU_LINE_MEAS_RESULT;

    pack.bru_measure.line = line;
    pack.bru_measure.lstate = 1;
    pack.bru_measure.mode = 2;
    pack.bru_measure.ref_val = 4;
    pack.bru_measure.val = 10;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::send_BRU_RESET_SWITCH_CFG(uint8_t addr)
{
    packet_buf_t pack;

    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 0x80;
    pack.hdr.len = 1+sizeof(uint32_t);
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_BRU_CFG;

    pack.cmd = BRU_RESET_SWITCH_CFG;

    *(uint32_t*)pack.data = 1000;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }

    QThread::usleep(500000);

    sendBRUCfg(addr, 0xFF, 0);
}

void MainWindow::send_GET_ID(uint8_t adr_to, uint8_t adr_from)
{
    QString descr = QString("Device %1").arg(adr_from);

    packet_buf_t pack;

    pack.hdr.adr_from = adr_from;
    pack.hdr.adr_to = adr_to;
    pack.hdr.len = 1+12+descr.length();
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_CFG;

    pack.cmd = GET_ID;

    for(int i=0; i<12; i++){
        pack.data[i] = adr_from+i;
    }

    memcpy(&pack.data[12], descr.toLocal8Bit().data(), descr.length());

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::play_audio(packet_audio_t *pack)
{
    if(m_audioDev){
        memcpy(&audio_buf[audio_buf_in], pack->pcm, AUDIO_SAMPLES*sizeof(uint16_t));
        audio_buf_in += AUDIO_SAMPLES;
        if(audio_buf_in >= AUDIO_SAMPLES*100){
            if(playThread.isRunning())
                playThread.waitForFinished();
            playThread = QtConcurrent::run([this](){
                m_audioDev->write(reinterpret_cast<char*>(audio_buf), AUDIO_SAMPLES*100*sizeof(uint16_t));
                audio_buf_in = 0;
            });
        }
    }
}

void MainWindow::send_BKS_GET_INP_MODE(int addr, int val)
{
    packet_buf_t pack;

    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 0x80;
    pack.hdr.len = 5;
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_CFG;

    pack.cmd = BKS_GET_INP_MODE;

    *reinterpret_cast<uint32_t*>(pack.data) = val;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent << val;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::send_BRP_SET_CRITICAL_ALARMS(int addr)
{
    packet_buf_t pack;

    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 0x80;
    pack.hdr.len = 1;
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_CFG;

    pack.cmd = BRP_SET_CRITICAL_ALARMS;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

void MainWindow::sendPing(int addr, int type){
    packet_buf_t pack;

    pack.hdr.adr_from = addr;
    pack.hdr.adr_to = 254;
    pack.hdr.len = 6;
    pack.hdr.repeate_cnt = 0;
    pack.hdr.type = PACK_GEN_CALL;

    pack.cmd = PING;

    *(uint32_t*)pack.data = 0;
    *(uint8_t*)(pack.data+sizeof(uint32_t)) = (uint8_t)type;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}


void MainWindow::on_btOUT_CPB_clicked()
{
    for(int i=0; i<16; i++){
        send_IN_CPB(i);
    }
    for(int i=0; i<16; i++){
        send_OUT_CPB(i);
    }
}



void MainWindow::on_btnRequest_clicked()
{
    QAudioFormat m_format;
    m_format.setSampleRate(48000);
    m_format.setChannelCount(1); //set channels to mono
    m_format.setSampleSize(16); //set sample sze to 16 bit
    m_format.setSampleType(QAudioFormat::SignedInt ); //Sample type as usigned integer sample
    m_format.setByteOrder(QAudioFormat::LittleEndian); //Byte order
    m_format.setCodec("audio/pcm"); //set codec as simple audio/pcm

    auto device = QAudioDeviceInfo::defaultOutputDevice();

    m_audioOut = new QAudioOutput(device, m_format, this);

//    m_audioDev = m_audioOut->start();
//    qDebug() << m_audioOut->periodSize();

    if(ui->reqEmul->isChecked()){
        connect(&reqTimer, &QTimer::timeout, [this](){
            requestPacket(udpSock[0], 6);
        });
        reqTimer.start(30);
    }

    setGo(true);
}

void MainWindow::on_btnStopAudio_clicked()
{
    if(ui->reqEmul->isChecked()){
        reqTimer.stop();
    }

    if(m_audioOut){
//        m_audioOut->stop();
        m_audioOut->deleteLater();
        m_audioOut = nullptr;
        m_audioDev = nullptr;
    }

    setGo(false);
}

void MainWindow::on_bt_REC_NUMBER_clicked()
{
    for(int i=0; i<3; i++){
        packet_buf_t pack;
        pack.hdr.len = 2;
        pack.hdr.type = PACK_CFG;
        pack.hdr.adr_to = 0x80;
        pack.hdr.adr_from = 1;
        pack.hdr.repeate_cnt = 1;

        pack.cmd = REC_NUMBER;

        pack.data[0] = 1;

        QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len+1);

        auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
        if(sent > -1){
            qDebug() << "sent" << sent;
        }else{
            qDebug() << "error" << udpSock[0]->errorString();
        }
    }
}

void MainWindow::on_bt_UM_MANUAL_ID_SET_clicked()
{
    packet_buf_t pack;
    pack.hdr.len = 2;
    pack.hdr.type = PACK_CFG;
    pack.hdr.adr_to = 0x80;
    pack.hdr.adr_from = 1;
    pack.hdr.repeate_cnt = 1;

    pack.cmd = UM_MANUAL_ID_SET;

    pack.data[0] = 1;

    QByteArray dgram(reinterpret_cast<char*>(&pack), sizeof(pack_hdr_t)+pack.hdr.len+1);

    auto sent = udpSock[0]->writeDatagram(dgram, QHostAddress(BC_IP), MAIN_PORT);//("192.168.1.146")
    if(sent > -1){
        qDebug() << "sent" << sent;
    }else{
        qDebug() << "error" << udpSock[0]->errorString();
    }
}

