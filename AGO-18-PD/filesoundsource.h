#ifndef FILESOUNDSOURCE_H
#define FILESOUNDSOURCE_H

#include "basesoundsource.h"
#include "mypcm.h"
#include <QFile>
#include <QFuture>
#include <QObject>
#include <QSharedPointer>
#include <atomic>

class FileSoundSource : public BaseSoundSource
{
    Q_OBJECT
public:
    FileSoundSource(QObject *parent = nullptr);

    QString errorStr;

    bool start(QString fileName, int pos = 0);
    virtual void stop()override;

    int getPos();
    void setPos(int pos);

private:
    QSharedPointer<MyPCM> pcm;
    QFuture<void> readThread;
    std::atomic_bool active;

signals:
    void progress(int total, int pos);
    void complete();
};

#endif // FILESOUNDSOURCE_H
