class EventsView extends Page{
//    evts = [];
    
    constructor(container){
        super(container);
        this.evts = [];
    }
    
    init(){
        this.update();
        var _this = this;
        super.init();
    }
    
    build(){
        var events = $("<div>");
        
        for(var i=0; i<this.evts.length; i++){
            var color = "primary";
            if(this.evts[i].type == 1)
                color = "warning";
            else if(this.evts[i].type == 2)
                color = "danger";
//            var src = "";
//            for(let j=0; j<32; j++){
//                if((this.evts[i].source & (1<<j))!=0){
//                    if(src == "")
//                        src = "Усилитель "+(Math.floor(j/2)+1)+" (канал "+(j%2 + 1)+"): ";
//                    else
//                        src = "Усилитель "+(Math.floor(j/2)+1)+": ";
//                }
//            }
//            if(src == "")
//                src = "Сервер:";
            
            let card = $(this.evt_card);
            card.addClass('alert-'+color);
//            card.find('.evt-source').html(src);
            card.find('.evt-text').html(this.evts[i].message);
            $("#events").append(
                card
//                $("<div>", {class: 'alert alert-'+color, role: 'alert'}).append(
//                    $('<h5>', {class: 'alert-heading'}).html(src),
//                    $('<p>').html(this.evts[i].message)
//                )
                
            );
//            events += "<div class='alert alert-"+color+"' role='alert'>"+src+" "+this.evts[i].message+"</div>";
        }
        return events;
    }
    
    set_json(json){
        this.evts = json;
        this.setState();
    }
    
    async update(){
        var _this = this;
        
        console.log("event_card: ");
        this.evt_card = await this.loadView("event_card.html");
        console.log(this.evt_card);
        
//        var obj = await (new Rest()).get_events();
//        if(obj.result == "success"){
//            _this.set_json(obj.events);
//        }
    }
}