/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PDUDPServer.cpp
 * Author: user
 * 
 * Created on 16 декабря 2019 г., 14:53
 */

#include "PDUDPServer.h"
#include "ConfigServer.h"
#include "MyUDPServer.h"
#include "UDPSoundSender.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sstream>
#include <iostream>
#include "cus/log.h"

PDUDPServer pd_udp_server;

void pd_udp_thread_func(PDUDPServer *__this);

PDUDPServer::PDUDPServer() {
}

PDUDPServer::PDUDPServer(const PDUDPServer& orig) {
}

PDUDPServer::~PDUDPServer() {
}

void PDUDPServer::error(string mes, bool _exit){
    log_error("PDUDPServer: %s", mes.c_str());
}

void PDUDPServer::start(){
    iface = conf_server.getNetParam("out_iface", "");
    port = conf_server.getNetParam("out_port", 0);
    if(port == 0){
        conf_server.error("no out_port specified", true);
        exit(1);
    }
    
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1){
        error(strerror(errno), true);
        exit(1);
    }
    
    struct ifreq ifr;
    ifr.ifr_ifru.ifru_addr.sa_family = AF_INET;
    strcpy(ifr.ifr_ifrn.ifrn_name, iface.c_str());
    if(ioctl(sock, SIOCGIFADDR, &ifr) == -1){
        error("ioctl(): "+string(strerror(errno)), true);
        exit(1);
    }
    cout << "Outer UDP socket binds to " << inet_ntoa(((sockaddr_in*)&ifr.ifr_ifru.ifru_addr)->sin_addr) << ":" << port << endl;
    
    sockaddr_in addr;
    memset(reinterpret_cast<void*>(&addr), 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr = reinterpret_cast<sockaddr_in*>(&ifr.ifr_ifru.ifru_addr)->sin_addr;
    addr.sin_port = htons(port);
    
    if(bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1){
        error("bind(): "+string(strerror(errno)), true);
        exit(1);
    }
    
    stringstream s; 
    s << "Cервер IP-пультов запущен " << inet_ntoa(addr.sin_addr) << ":" << port;
    conf_server.addServerEvent(ConfigServer::SERVER_EVT_PDUDP, ConfigServer::EVT_NORMAL, 0, s.str().c_str());//
    
    active = true;
    thrd = thread(pd_udp_thread_func, this);

    sched_param param;
    param.sched_priority = 0;
    int err = pthread_setschedparam(thrd.native_handle(), SCHED_OTHER, &param);//
    if(err != 0)
        cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;

//    printf("PD UDP server created. id=%d\n", thrd.native_handle());
}

void PDUDPServer::stop(){
    active = false;
    thrd.join();
}

void PDUDPServer::run(){
    char buf[2048];
    
    while(active){
        fd_set rd_set, er_set;
        FD_ZERO(&rd_set);
        FD_ZERO(&er_set);
        FD_SET(sock, &rd_set);
        FD_SET(sock, &er_set);
        
        timeval to = {1,0};
        
        int cnt = select(sock+1, &rd_set, NULL, &er_set, &to);
        if(cnt == -1){
            error("select(): "+string(strerror(errno)), false);
        }
        
        if(cnt > 0){
            if(FD_ISSET(sock, &rd_set)){
                sockaddr_in from_addr;
                memset(&from_addr, 0, sizeof(sockaddr_in));
                socklen_t socklen = sizeof(sockaddr_in);
                auto rd = recvfrom(sock, buf, 2048, 0, reinterpret_cast<sockaddr*>(&from_addr), &socklen);
                if(rd == -1){
                    error("recvfrom(): "+string(strerror(errno)), false);
                    return;
                }else if(rd > 0){
                    packet_command_t* packet = reinterpret_cast<packet_command_t*>(buf);
                    if(packet->hdr.type == PACK_AUDIO){
                        server.sendBuffer(packet, rd);
//                        auto tm = chrono::steady_clock::now();
//                        printf("pd_sent_tm = %d\n", (int)chrono::duration_cast<chrono::microseconds>(tm - last_sent_tm).count());
//                        last_sent_tm = tm;
                    }
                    
                    string sAddr = inet_ntoa(from_addr.sin_addr);
                    senders_t::iterator fnd = senders.find(sAddr);

                    if(fnd != senders.end()){
                        fnd->second->touch();
                    }
                    
//                    if(packet->hdr.type == PACK_AUDIO){
//                        if(fnd == senders.end()){
//                            auto sender = UDPSoundSender::getFreeSender();//PUDPSoundSender(new UDPSoundSender());
//                            sender->start(sock, from_addr, reinterpret_cast<start_stop_t*>(packet->data));
//                            senders[sAddr] = sender;
//                        }
//                    }
                        
                    if(packet->hdr.type == PACK_CFG){
                        if(packet->cmd == SET_RECIPNT_LIST){
                            if(fnd == senders.end()){
                                auto sender = UDPSoundSender::getFreeSender();//PUDPSoundSender(new UDPSoundSender());
                                sender->start(sock, from_addr, reinterpret_cast<start_stop_t*>(packet->data));
                                senders[sAddr] = sender;
                            }
                        }
                        if(packet->cmd == CLEAR_RECIPNT_LIST || packet->cmd == CLEAR_MASTER){
                            if(fnd != senders.end()){
                                fnd->second->stop();
                                senders.erase(fnd);
                            }
                        }
                    }
                }
            }
            if(FD_ISSET(sock, &er_set)){
                error("socket error: "+string(strerror(errno)), false);
                return;
            }
        }
        
        set<string> to_clear;
        for(pair<string, PUDPSoundSender> p : senders){
            if(p.second->isTimedOut()){
                p.second->stop();
                to_clear.insert(p.first);
            }
        }
        for(string a : to_clear){
            senders.erase(a);
        }
    }
}

void pd_udp_thread_func(PDUDPServer *__this){
    __this->run();
}
