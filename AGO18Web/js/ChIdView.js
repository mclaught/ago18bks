class ChIdView extends MyView{
    constructor(container, type){
        super(container);
        this.type = type;
    }
    
    set_scan_id(){
        let broadcast = $('#broadcast').prop('checked');
        let id = $("#old-id").val();
        $("#scan-ids").prop('disabled', id==='' && !broadcast);
    }
    
    init(){
        console.log("Init "+this.type);
        
        this.ids = null;
        this.scaning = false;
        
        var _this = this;
        
        switch(this.type){
            case "chg":
                $("#grp-old-id").show();
//                $("#grp-block-type").hide();
                $("#grp-offset").show();
//                $("#card-scan").show();
                $("#btn-change-id").show();
                $("#btn-set-id").hide();
                break;
            case "set":
                $("#grp-old-id").hide();
//                $("#grp-block-type").show();
                $("#grp-offset").hide();
//                $("#card-scan").hide();
                $("#btn-change-id").hide();
                $("#btn-set-id").show();
                break;
        }
        
        if('id_offset' in settings.bks)
            $('#offset').val(settings.bks.id_offset);
        else
            $('#offset').val(8);
        
        $("#old-id").on("input", function(event){
            if($("#old-id").val() !== "")
                $("#broadcast").prop('checked', false);
            _this.set_scan_id();
        });
        
        $("#broadcast").change(function(){
            if($(this).prop('checked'))
                $("#old-id").val("");
            _this.set_scan_id();
        });
        
        $("#scan-ids").click(async function(){
            let broadcast = $('#broadcast').prop('checked');
            if($("#old-id").val()==='' && !broadcast){
                alertify.error("Введите ID или выберите 'Широковещательно'");
                return;
            }
            
            _this.scaning = true;
            _this.setState();
            
            let old_id = parseInt($("#old-id").val())-1;
            if(broadcast)
                old_id = 0xFF;
            console.log(old_id);
            
            let obj = await (new Rest()).get_id(old_id);
            
            _this.scaning = false;
            
            if(obj.result === "success"){
                _this.ids = obj.ids;
                console.dir(_this.ids);
                _this.setState();
            }else{
                alertify.error(obj.message);
            }
        });
        
        $("#btn-change-id").click(async function(){
            let old_id = parseInt($("#old-id").val());
            let new_id = parseInt($("#new-id").val());
            let offset = parseInt($("#offset").val());
            let um_mode = parseInt($("#um-mode").val());
            
            let obj = await (new Rest()).change_id(old_id, new_id, offset, um_mode);
            console.dir(obj);
            if(obj.result === "success"){
                alertify.success("Успешно");
            }else{
                alertify.error(obj.message);
            }
        });
        
        $("#btn-set-id").click(async function(){
            let new_id = parseInt($("#new-id").val());
            let um_mode = parseInt($("#um-mode").val());
            console.info("new_id", new_id);
            
            if(!new_id){
                alertify.error("Установите новый ID");
                return;
            }
            
            alertify.confirm("Установка ID", 
                "Нажмите 'Далее', затем включите питание настраиваемой платы в течение 20 сек.",
                async function(){
                    $("#btn-set-id").html(`
                        <div class="spinner-grow spinner-grow-sm ml-1" role="status">
                            <span class="sr-only">...</span>
                        </div>
                    `);
                    
                    let obj = await (new Rest()).set_id(new_id, um_mode);
                    console.dir(obj);
                    if(obj.result === "success"){
                        alertify.success(`Установлено: id=${obj.new_id.id} (${obj.new_id.um_mode==0 ? "Основной" : "Резервный"})`);
                    }else{
                        alertify.error(obj.message);
                    }
                    
                    $("#btn-set-id").html("Установить");
                }, function(){}).set('labels', {ok:'Далее', cancel:'Отмена'});
            
        });
        
        $("#sort-prm,#sort-desc").on('change', function(){
            _this.setState();
        });
        
        $("#block-type").change(function(){
            console.log($(this).val());
            switch($(this).val()){
                case 'um':
                    $("#grp-um-mode").show();
                    $("#offset").val(("id_offset" in settings.bks) ? settings.bks.id_offset : 8);
                    break;
                    
                case 'bru':
                    $("#grp-um-mode").hide();
                    $("#um-mode").val(0);
                    $("#offset").val(("bru_offset" in settings.bks) ? settings.bks.bru_offset : 25);
                    break;
                    
                case 'other':
                    $("#grp-um-mode").hide();
                    $("#um-mode").val(0);
                    $("#offset").val(0);
                    break;
            }
            if($(this).val() === "um"){
            }else{
            }
        });
        
        super.init();
    }
    
    sort_scan(){
        let prm = $('#sort-prm').val();
        let desc = $('#sort-desc').prop('checked');
        console.log("Sort: "+prm+" "+desc);
        
        if(prm === null || this.ids === null)
            return;
        
        this.ids.sort(function(a,b){
            let prm_a;
            let prm_b;
            switch(prm){
                case 'id':
                    prm_a = a.id;
                    prm_b = b.id;
                    break;
                case 'uid':
                    prm_a = a.uid;
                    prm_b = b.uid;
                    break;
                case 'descr':
                    prm_a = a.descr;
                    prm_b = b.descr;
                    break;
            }
            if(desc){
                let x = prm_a;
                prm_a = prm_b;
                prm_b = x;
            }

            console.log(prm_a+" - "+prm_b);

            if(prm_a < prm_b)
                return -1;
            if(prm_a > prm_b)
                return 1;
            return 0;
        });
    }
    
    build(){
        let _this = this;
        
        if(this.scaning){
            return $('<div>', {class: "alert alert-warning", role:"alert"}).html("Сканирование...");
        }
        
        if(this.ids === null)
            return $("<span>");
        
        
        this.sort_scan();
        
        if(this.ids.length>0){
            let list = $("<div>", {class: ""});
            for(let i=0; i<this.ids.length; i++){
                list.append(
                    $('<div>', {class: 'card'}).append(
                        $('<div>', {class: 'row p-1'}).append(
                            $("<button>", {class: "col-1 btn btn-link id-link", dev_id: this.ids[i].id}).html(this.ids[i].id),
                            $("<span>", {class: "col-5"}).html(this.ids[i].uid),
                            $("<span>", {class: "col-6"}).html(this.ids[i].descr)
                        )
                    )
                );
            }
        
            list.find('.id-link').click(function(){
                let id = $(this).attr('dev_id');
                $('#old-id').val(id);
                $('#broadcast').prop('checked', false);
                _this.set_scan_id();
            });

            return list;
        }else{
            return $('<div>', {class: "alert alert-danger", role: "alert"}).html("Нет ответа");
        }
    }
}