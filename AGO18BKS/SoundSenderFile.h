/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SoundSenderFile.h
 * Author: user
 *
 * Created on 10 декабря 2019 г., 15:42
 */

#ifndef SOUNDSENDERFILE_H
#define SOUNDSENDERFILE_H

#include "BufferedSoundSender.h"
#include "mypcm.h"
#include <future>
#include <memory>
#include <shared_mutex>
#include <vector>

using namespace std;

class SoundSenderFile;

typedef shared_ptr<SoundSenderFile> PSoundSenderFile;
typedef vector<PSoundSenderFile> file_sender_pool_t;

class SoundSenderFile : public BufferedSoundSender {
public:
    SoundSenderFile();
    SoundSenderFile(const SoundSenderFile& orig);
    virtual ~SoundSenderFile();
    
    void start(string filename, uint8_t sender, ch_mask_t outs=0);
    virtual void stop()override;
    
    static PSoundSenderFile getFreeSender();
    static file_sender_pool_t getFileSenders();
    static void stopAll();

    void set_filename(string value);
    string get_filename();
    void read_file();

protected:
    virtual void cycle()override;
    virtual void onStop()override;
    
private:
    string _filename;
    shared_mutex filename_mutex;
    shared_ptr<MyPCM> pcm;
};

#endif /* SOUNDSENDERFILE_H */

