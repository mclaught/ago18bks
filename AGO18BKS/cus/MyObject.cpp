/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyObject.cpp
 * Author: user
 * 
 * Created on 19 сентября 2018 г., 19:49
 */

#include "MyObject.h"

MyObject::MyObject() {
}

MyObject::MyObject(const MyObject& orig) {
}

MyObject::~MyObject() {
}

void MyObject::join(Message mes, mes_handler handler) {
    links[mes].push_back(handler);
}

void MyObject::sendMessage(Message mes, void* params) {
    for(auto link : links[mes]){
        link(params);
    }
}

