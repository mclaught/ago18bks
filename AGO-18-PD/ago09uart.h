#ifndef AGO09UART_H
#define AGO09UART_H

#include <QFuture>
#include <QObject>
#include <QSerialPort>
#include <QThread>
#include <QTimer>
#include <mutex>

#define INTER_BYTE_DELAY    0   //мкс 0 - без задержки
#define TIMER_INTERVAL      500
//#define UART_BYTES 2
//#if (UART_BYTES == 2)
//    typedef uint16_t targets_t;
//#elif (UART_BYTES == 4)
//    typedef uint32_t targets_t;
//#endif

typedef uint32_t targets_t;

//#define PORT_DEBUG

class Ago09SingalWorker;

class Ago09Uart : public QObject
{
    Q_OBJECT
public:
    explicit Ago09Uart(QObject *parent = nullptr);
    ~Ago09Uart() override;

    static QStringList portsAvailable();

    bool open(QString name, qint32 baud);
    void close();
    void setTargets(targets_t targets);
    bool isBKSReady();
    bool isTangentaPressed();
    void setCastState(bool state);
    void setTargetsCnt(int val);

    typedef enum {
        disconnected,
        connected,
        error,
        tryConnect
    }Ago09UartStates;

    bool tangentaEmu = false;
private:
    int targets_cnt = 16;
    int uart_bytes = 2;
    QString name;

    QSerialPort* port = nullptr;
//    QTimer* timer = nullptr;
//    QFuture<void> thread;
    QThread* signalsThread;
    Ago09SingalWorker* signalsWorker;
    QTimer checkPortTimer;
    QTimer signalsTimer;
    uint32_t targets;
    qint64 last_send_tm = 0;
    bool last_bks_state = false;
    bool last_tangenta_state = false;
    std::mutex mtx;
    bool cast_state = false;
    uint8_t recv_buf[4];
    int recv_buf_i = 0;
    qint64 recv_buf_tm = 0;

    void startThread();
    void processPacket();
    bool open_port(QString name, qint32 baud);
signals:
    void bksReady(bool ready);
    void tangenta(bool state);
    void connectState(Ago09UartStates state);
    void busyTargets(targets_t targets, bool cast);
private slots:
    void onCheckPortTimer();
    void onSignalsTimer();
    void portError(QSerialPort::SerialPortError error);
    void readyRead();

    friend class Ago09SingalWorker;
};


class Ago09SingalWorker : public QObject{
    Q_OBJECT
public:
    explicit Ago09SingalWorker(Ago09Uart* uart, int period, QObject *parent = nullptr);
    ~Ago09SingalWorker() override;

    void run();
    void stop();

private:
    bool running = true;
    Ago09Uart* uart;
    int period;

signals:
    void stoped();
};

#endif // AGO09UART_H
