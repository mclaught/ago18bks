#include "mainthread.h"

#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MainThread* mainThread = new MainThread();
    mainThread->start();

    return a.exec();
}
