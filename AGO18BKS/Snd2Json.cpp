/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Snd2Json.cpp
 * Author: user
 * 
 * Created on 13 ноября 2020 г., 20:32
 */

#include <exception>
#include <math.h>
#include <sstream>
#include <iostream>

#include "Snd2Json.h"
#include "proto.h"

Snd2Json::Snd2Json(uint8_t *buf, std::string name) : _buf(buf) {
#ifdef SND2JSON_DEBUG
    std::cout << "Snd2Json(" << name << ")" << std::endl;
#endif
}

Snd2Json::Snd2Json(const Snd2Json& orig) {
}

Snd2Json::~Snd2Json() {
}

json Snd2Json::codec_capabilities_to_json(uint8_t *&p){
    json js = json::object(
                {
                    {"min_dB", reinterpret_cast<codec_capabilities_t*>(p)->min_dB},
                    {"max_dB", reinterpret_cast<codec_capabilities_t*>(p)->max_dB},
                    {"step_dB", reinterpret_cast<codec_capabilities_t*>(p)->step_dB}
                }
            );
    shift(p, sizeof(codec_capabilities_t), 1);
    return js;
}

json Snd2Json::eq_capabilities_to_json(uint8_t *&p){
    json js = json::object(
        {
            {"min_out_gain_dB", reinterpret_cast<eq_capabilities_t*>(p)->min_out_gain_dB},
            {"max_out_gain_dB", reinterpret_cast<eq_capabilities_t*>(p)->max_out_gain_dB},
            {"min_gain_dB", reinterpret_cast<eq_capabilities_t*>(p)->min_gain_dB},
            {"max_gain_dB", reinterpret_cast<eq_capabilities_t*>(p)->max_gain_dB},
            {"min_band", reinterpret_cast<eq_capabilities_t*>(p)->min_band},
            {"max_band", reinterpret_cast<eq_capabilities_t*>(p)->max_band},
            {"fmin_hz", reinterpret_cast<eq_capabilities_t*>(p)->fmin_hz},
            {"fmax_hz", reinterpret_cast<eq_capabilities_t*>(p)->fmax_hz},
            {"sections", reinterpret_cast<eq_capabilities_t*>(p)->sections}
        }
    );
    shift(p, sizeof(eq_capabilities_t), 2);
    return js;        
}

json Snd2Json::hpf_capabilities_to_json(uint8_t *&p){
    hpf_capabilities_t* hpf = reinterpret_cast<hpf_capabilities_t*>(p);
    shift(p, sizeof(uint32_t), 3);

    json fcut = json::array();
    for(int i=0; i<static_cast<int>(hpf->number); i++){
        fcut.push_back(hpf->fcut[i]);
        shift(p, sizeof(uint32_t), 4);
    }
    
    json js = json::object(
        {
            {"number", hpf->number},
            {"fcut", fcut}
        }
    );
    
    return js;
}

json Snd2Json::pa_capabilities_to_json(uint8_t* &p){
    pa_capabilities_t* pa = reinterpret_cast<pa_capabilities_t*>(p);
    shift(p, sizeof(uint8_t), 5);

    json arr = json::array();
    for(int i=0; i<(pa->out_lvl_amount); i++){
        arr.push_back(pa->out_lvl[i]);
        shift(p, sizeof(uint8_t), 6);
    }
    json js = json::object(
        {
            {"out_lvl_amount", pa->out_lvl_amount},
            {"out_lvl", arr}        
        }
    );
    return js;
}

json Snd2Json::snd_in_capab_to_json(uint8_t *&p){
    json j_in_capab = codec_capabilities_to_json(p);
    json j_eq_capb = eq_capabilities_to_json(p);
    json js = json::object({
        {"inp_capb", j_in_capab},
        {"eq_capb", j_eq_capb}
    });
    return js;
}

json Snd2Json::snd_out_capab_to_json(uint8_t* &p){
    json j_out_capab = codec_capabilities_to_json(p);
    json j_eq_capb = eq_capabilities_to_json(p);
    json j_hpf_capb = hpf_capabilities_to_json(p);
    json j_pa_capb = pa_capabilities_to_json(p);
    
    json js = json::object(
        {
            {"outp_capb", j_out_capab},
            {"eq_capb", j_eq_capb},
            {"hpf_capb", j_hpf_capb},
            {"pa_capb", j_pa_capb}
        }
    );
    return js;
}

json Snd2Json::eq_param_to_json(uint8_t *&p){
    eq_param_t* pp = reinterpret_cast<eq_param_t*>(p);
    float gain = round(20*log10(pp->gain));
    json js = json::object({
        {"gain", gain},
        {"band", pp->band},
        {"freq", pp->freq}
    });
    shift(p, sizeof(eq_param_t), 7);
    return js;
}

json Snd2Json::eq_common_to_json(uint8_t *&p){
    uint32_t cnt = *reinterpret_cast<uint32_t*>(p);
    shift(p, sizeof(uint32_t), 8);

    json ec = json::array();
    for(int i=0; i<static_cast<int>(cnt); i++){
        ec.push_back(eq_param_to_json(p));
    }
    float outGain = *(float*)p;
    json js = json::object({
        {"ec", ec},
        {"outGain", round(20*log10(outGain))}
    });
    shift(p, sizeof(float), 9);
    return js;
}

json Snd2Json::snd_in_cfg_to_json(uint8_t *&p, int index){
    json js = json::object();

    float lvl = reinterpret_cast<float*>(p)[index];
    js["lvl"] = lvl;
    shift(p, sizeof(float)*MAX_INP_CHANNELS, 10);

    for(int i=0; i<MAX_INP_CHANNELS; i++){
        json eq = eq_common_to_json(p);
        if(i == index)
            js["eq"] = eq;
    }

    return js;
}
    
json Snd2Json::snd_out_cfg_to_json(uint8_t *&p, int index){
    json js = json::object();

    float lvl = reinterpret_cast<float*>(p)[index];
    js["lvl"] = lvl;//round(20*log10(lvl));
    shift(p, sizeof(float)*MAX_INP_CHANNELS, 11);

    for(int i=0; i<MAX_INP_CHANNELS; i++){
        json eq = eq_common_to_json(p);
        if(i == index)
            js["eq"] = eq;
    }
    
    js["fcut_indx"] = reinterpret_cast<uint32_t*>(p)[index];
    shift(p, sizeof(uint32_t)*MAX_INP_CHANNELS, 12);
    
    js["amplf_volt"] = reinterpret_cast<uint8_t*>(p)[index];
    shift(p, sizeof(uint8_t)*MAX_INP_CHANNELS, 13);
    
    return js;
}

json Snd2Json::snd_out_capab_init_packet_to_json(uint8_t *&p, int index){
    json snd_capab = snd_out_capab_to_json(p);
    json snd_cfg = snd_out_cfg_to_json(p, index);
    return json::object(
        {
            {"snd_capab", snd_capab},
            {"snd_cfg", snd_cfg}
        }
    );
}

void Snd2Json::shift(uint8_t *&p, int sz, int dbg_n){
#ifdef SND2JSON_DEBUG    
    std::cout << dbg_n << ": " << (int)(p-_buf) << " + " << sz << " = ";
#endif
    
    p += sz;
//    std::cout << (int)(p-_buf) << std::endl;
    if((p - _buf) > MAX_SHIFT){
        std::stringstream stm;
        stm << "Bad packet N:" << dbg_n << " Shift:" << (p-_buf);
        throw std::runtime_error(stm.str());
    }
}
