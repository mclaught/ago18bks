/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainModel.cpp
 * Author: user
 * 
 * Created on 19 сентября 2018 г., 19:39
 */

#include "MainModel.h"
#include <mpg123.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netdb.h>
#include "messages.h"
#include <iostream>
#include <vector>
#include <sys/poll.h>
#include "../ConfigServer.h"
#include "log.h"
#include <unistd.h>

//#define CLIENT "US-MUS.03.02.2F"
//#define IFACE "192.168.1.1"
//#define USE_POLL

MainModel* main_model;

MainModel::MainModel() : MyThread() {
    
    string out_iface = conf_server.getNetParam("out_iface", "any");
    if(out_iface == ""){
        conf_server.error("no outer interface specified");
    }
    
    iface = waitForIface(out_iface);
    if(iface.empty()){
        cerr << red << "Out iface not found. CUS interface disabled." << clrst << endl;
//        out_iface = conf_server.getNetParam("in_iface", "any");
//        iface = waitForIface(out_iface);
//        if(iface.empty()){
//            exit(EXIT_FAILURE);
//        }
        terminated = true;
        return;
    }
    
    string mcast = conf_server.getNetParam("udp-multicast", "");
    if(mcast == ""){
        conf_server.error("no multicast address specified", true);
    }
    
    targets = conf_server.getCUSTargets();
    tgtName = "unknown";
    for(string target : targets){
        if(target != "" && target != "-"){
            if(tgtName == "unknown")
                tgtName = target;
            targets_set.insert(target);
        }
    }
    
    string server_addr = conf_server.getNetParam("cus-server-addr", "");
    uint16_t server_port = conf_server.getNetParam("cus-server-port", 0);
    
    udp = new UDPChannel(iface, mcast, tgtName);
    udp->join(MSG_SERVER_FOUND, [this](void* params){
        sockaddr_in *addr = (sockaddr_in*)params;
        string s_addr = inet_ntoa(addr->sin_addr);
        uint16_t s_port = ntohs(addr->sin_port);
        
        cout << green << "Server found: " << s_addr << ":" << s_port << clrst << endl;
        
        for(TCPChannel *tcp : tcps){
            tcp->setServerParams(s_addr, s_port);
        }
        conf_server.setNetParam("cus-server-addr", s_addr);
        conf_server.setNetParam("cus-server-port", (int)s_port);
    });
    channels.push_back(udp);
    
    for(string target : targets_set){
        if(target != "" && target != "-"){
            TCPChannel *tcp = new TCPChannel(target);
            tcp->setServerParams(server_addr, server_port);
            tcps.insert(tcp);
            channels.push_back(tcp);
        }
    }
    
    for(TCPChannel *tcp : tcps){
        tcp->join(MSG_START_SESSION, [this](void* params){
            uint32_t id = ((SessionsPrms*)params)->id;
            string url = ((SessionsPrms*)params)->url;
            string cliName = ((SessionsPrms*)params)->clientName;
            
            targets_t tgtMask = findTargets(cliName);
            if(tgtMask == 0)
                return;
            
            PSession sess = findSession(id);
            if(!sess.get()){
#ifndef SMART_PTR
                for(auto s : sessions)
                    delete s->second;
#endif
                
                sessions.clear();
                
//                PSession new_sess = PSession(new Session(id, url, iface));
                PSession new_sess = make_shared<Session>(id, url, iface);
                cout << "Add new session "<< id <<". URL: " << url << endl;
                sessions[id] = new_sess;
                sess = new_sess;
                new_sess->start();
            }
            if(sess.get()){
                cout << "Add " << cliName << " to " << id << endl;
                sess->addTargets(tgtMask);
//                uart_thread->setTargets(sess->getTargets());//
            }
        });
        tcp->join(MSG_FINISH_SESSION, [this](void* params){
            uint32_t id = ((SessionsPrms*)params)->id;
            string clientName = ((SessionsPrms*)params)->clientName;
            
            targets_t tgtMask = findTargets(clientName);
            if(tgtMask == 0)
                return;
            
            PSession sess = findSession(id);
            if(sess){
                cout << "Remove " << clientName <<" from " << id << endl;
                sess->removeTargets(tgtMask);
//                uart_thread->setTargets(sess->getTargets());//
                if(sess->getTargets() == 0){
#ifndef SMART_PTR
                    delete sess;
#endif
                    sess->terminate();
                    sessions.erase(id);
                    
                    cout << "Remove session " << id << endl;
                }
            }
        });
    }
    
    ok = true;
}

MainModel::MainModel(const MainModel&) : MyThread() {
}

MainModel::~MainModel() {
}

void MainModel::run(){
//    printf("UDP server created. id=%d\n", pthread_self());

    sched_param param;
    param.sched_priority = 0;
    int err = pthread_setschedparam(pthread_self(), SCHED_OTHER, &param);//main_thread.native_handle()
    if(err != 0)
        cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;

#ifdef USE_POLL
    pollfd fds[channels.size()];
    int i=0;
    for(Channel *ch : channels){
        fds[i].fd = ch->sock;
        fds[i].events = POLLIN;
        fds[i].revents = 0;
        i++;
    }
#endif    
    
    while(!terminated){
#ifdef USE_POLL
        i=0;
        for(Channel *ch : channels){
            fds[i].fd = ch->sock;
            fds[i].events = POLLIN;
            fds[i].revents = 0;
            i++;
        }
        
        int cnt = poll(fds, channels.size(), 1000);
        if(cnt > 0){
            i=0;
            for(Channel *ch : channels){
                if(ch->active && (fds[i].revents & POLLIN)){
                    ch->onRead();
                }
                i++;
            }
        }else if(cnt == -1){
            cerr << strerror(errno) << endl;
        }
#else
        
        fd_set rd_set, er_set;
        FD_ZERO(&rd_set);
        FD_ZERO(&er_set);
        timeval tv = {1, 0};
        
        int max_sock = 0;
        for(Channel *ch : channels){
            if(ch->active){
                FD_SET(ch->sock, &rd_set);
                FD_SET(ch->sock, &er_set);
                max_sock = std::max(max_sock, ch->sock);
            }
        }
        int cnt = select(max_sock+1, &rd_set, nullptr, &er_set, &tv);
        if(cnt == -1){
            cerr << red  << "CUS MainModel select: " << strerror(errno) << clrst << endl;
            for(Channel *ch : channels){
                if(dynamic_cast<TCPChannel*>(ch) && ch->active){// && !checkSocket(ch->sock)
                    ch->disconnect();
                }
            }
        }else if(cnt>0){
            for(Channel *ch : channels){
                if(ch->active && FD_ISSET(ch->sock, &rd_set)){
                    ch->onRead();
                }
                if(FD_ISSET(ch->sock, &er_set)){
                    ch->disconnect();
                }
            }
        }else{
            //cout << "Idle" << endl;
        }
#endif
        
        udp->run();
        
        bool tcp_configured = true;
        for(TCPChannel *tcp : tcps){
            if(!tcp->isConfigured())
                tcp_configured = false;
            
            if(tcp->state == TCPChannel::DISCONNECTED){
                tcp->connectServer();
            }
            
            tcp->run();
        }
        
        if(!tcps.empty()){
            if(!tcp_configured){
                udp->searchServer();
                if(last_tcp_configured != 0){
                    conf_server.addServerEvent(ConfigServer::SERVER_EVT_CUS_TCP, ConfigServer::EVT_WARNING, 0, "Сервер СЦО не доступен");
                    last_tcp_configured = false;
                }
            }
            if(tcp_configured && last_tcp_configured != 1){
                conf_server.addServerEvent(ConfigServer::SERVER_EVT_CUS_TCP, ConfigServer::EVT_NORMAL, 0, "Все TCP каналы СЦО подключены");
                last_tcp_configured = true;
            }
        }
            
        
//        for(auto s : sessions){
//            s.second->run();
//        }
    }
}

PSession MainModel::findSession(uint32_t id){
    Sessions::iterator fnd = sessions.find(id);
    if(fnd != sessions.end())
        return fnd->second;
    else
        return PSession(nullptr);
}

targets_t MainModel::findTargets(string tgt){
    int i = 0;
    targets_t mask = 0;
    for(auto t : targets){
        if(t == tgt){
            mask |= (1<<i);;
        }
        i++;
    }
    return mask;
}

string MainModel::waitForIface(string if_name){
    struct ifaddrs *ifaddr, *ifa;
    int s;
    char host[NI_MAXHOST];
    
    for(int i=0; i<10; i++){
        if (getifaddrs(&ifaddr) == -1)
        {
            perror("getifaddrs");
            exit(EXIT_FAILURE);
        }

        for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
            if (ifa->ifa_addr == NULL)
                continue;

            s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if(ifa->ifa_addr->sa_family==AF_INET){
                cout << "Check iface: " << string(ifa->ifa_name) << endl;

                if (s != 0)
                {
                    printf("getnameinfo() failed: %s\n", gai_strerror(s));
                    break;
                }

                if(string(ifa->ifa_name) == if_name){
                    cout << "CUS iface: " << string(ifa->ifa_name) << " : " << host << endl;
                    return host;
                }
            }
        }
        cerr << red << "Wait for iface..." << (10-i) << clrst << endl;
        usleep(1000000);
    }
    
    return "";
}

void MainModel::exec(){
    run();
}
