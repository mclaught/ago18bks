TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -Wno-unused-parameter -Wno-unused-result -Wno-missing-field-initializers -Wno-multichar

#DEFINES += "THREAD_SEND"

SOURCES += \
        AudioSaver.cpp \
        BRU2JSON.cpp \
        BaseSoundSender.cpp \
        BaseWriter.cpp \
        BufferedSoundSender.cpp \
        ConfigServer.cpp \
        MyUDPServer.cpp \
        MyWSClient.cpp \
        OGGWriter.cpp \
        PDServer.cpp \
        PDUDPServer.cpp \
        PacketParser.cpp \
        Snd2Json.cpp \
        SocketSoundSender.cpp \
        SoundSenderFile.cpp \
        UDPSoundSender.cpp \
        WSServer.cpp \
        clipsworker.cpp \
        cus/Channel.cpp \
        cus/MainModel.cpp \
        cus/MyObject.cpp \
        cus/MyPlayer.cpp \
        cus/MyThread.cpp \
        cus/RTP.cpp \
        cus/Session.cpp \
        cus/TCPChannel.cpp \
        cus/UDPChannel.cpp \
        cus/tinyxml2.cpp \
        json2bin.cpp \
        main.cpp \
        mypcm.cpp \
        sched_dl.cpp \
        wavsaver.cpp

HEADERS += \
    AudioSaver.h \
    BRU2JSON.h \
    BaseSoundSender.h \
    BaseWriter.h \
    BufferedSoundSender.h \
    ConfigServer.h \
    MyUDPServer.h \
    MyWSClient.h \
    OGGWriter.h \
    PDServer.h \
    PDUDPServer.h \
    PacketParser.h \
    Snd2Json.h \
    SocketSoundSender.h \
    SoundSenderFile.h \
    UDPSoundSender.h \
    WSServer.h \
    base64.h \
    clipsworker.h \
    cus/Channel.h \
    cus/MainModel.h \
    cus/MyObject.h \
    cus/MyPlayer.h \
    cus/MyThread.h \
    cus/RTP.h \
    cus/Session.h \
    cus/TCPChannel.h \
    cus/log.h \
    cus/messages.h \
    cus/tinyxml2.h \
    json.hpp \
    json2bin.h \
    mypcm.h \
    proto.h \
    sched_dl.h \
    wavsaver.h

LIBS += -lopus -logg -lmpg123 -lpthread -lstdc++fs -lcrypto
