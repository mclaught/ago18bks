#include "basesoundsource.h"

BaseSoundSource::BaseSoundSource(QObject *parent) : QObject(parent)
{

}

void BaseSoundSource::start()
{
    fifo_in = fifo_out = 0;
}


int BaseSoundSource::fifo_cnt(){
    std::lock_guard<std::mutex> lock(fifo_mutex);

    if(fifo_in >= fifo_out)
        return fifo_in - fifo_out;
    else
        return fifo_in + FIFO_SIZE - fifo_out;
}

void BaseSoundSource::fifo_add(int16_t *data, int cnt){
    std::lock_guard<std::mutex> lock(fifo_mutex);

    int tail = FIFO_SIZE - fifo_in;
    int min = std::min(tail, cnt);

    in_sum += cnt;

    memcpy(&fifo[fifo_in], data, static_cast<size_t>(min)*sizeof(int16_t));
    cnt -= min;

    if(cnt == 0){
        fifo_in += min;
    }else{
        memcpy(fifo, &data[min], static_cast<size_t>(cnt)*sizeof(int16_t));
        fifo_in = cnt;
    }
}

bool BaseSoundSource::fifo_read(int16_t *data, int cnt){
    if(cnt > fifo_cnt())
        return false;

    std::lock_guard<std::mutex> lock(fifo_mutex);

    out_sum += cnt;

    //int16_t src_data[src_cnt];
    int16_t* src_data = data;

    int tail = FIFO_SIZE - fifo_out;
    int min = std::min(tail, cnt);

    memcpy(src_data, &fifo[fifo_out], static_cast<size_t>(min)*sizeof(int16_t));
    cnt -= min;

    if(cnt == 0){
        fifo_out += min;
    }else{
        memcpy(&src_data[min], fifo, static_cast<size_t>(cnt)*sizeof(int16_t));
        fifo_out = cnt;
    }

    return true;
}
