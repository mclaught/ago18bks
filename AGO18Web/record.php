<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?1">
    </head>
    
    <body class="bg-light">
        <?php
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        
        $cur_page=basename(__FILE__);
        include "navbar.php";
        ?>
        
        <div class="container">
            <br>
            <h1><img src="img/audio_rec.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Файлы аудиозаписи</h1>
            <br>
            
            <nav aria-label="Page navigation example">
                <ul class="pagination pages">
                  <li class="page-item"><a class="page-link prev-page" href="#">Предыдущая</a></li>
                </ul>
            </nav>
            
             <!--col-xl-8 col-lg-9 col-md-10 col-sm-11 col-12-->
            <table class="table shadow">  
                <thead>
                <!--<th></th>-->
                <th>Файл</th>
                <th>Вход</th>
                <th style="min-width: 200px;">Выходы</th>
                </thead>
                <tbody id="files-tbl">
                    
                </tbody>
            </table>
            
            <nav aria-label="Page navigation example">
                <ul class="pagination pages">
                  <li class="page-item"><a class="page-link prev-page" href="#">Предыдущая</a></li>
                </ul>
            </nav>
        </div>
        
        <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/alert.js"></script>
        <script type="text/javascript" src="js/constants.js"></script>
        <script type="text/javascript" src="js/rest.js"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script type="text/javascript" src="js/howler.js"></script>
        <script>
            const filesOnPage = 20;
            var cur_page = 0;
            var pages_count;
            
            Math.trunc = Math.trunc || function(x) {
                if (isNaN(x)) {
                  return NaN;
                }
                if (x > 0) {
                  return Math.floor(x);
                }
                return Math.ceil(x);
            };
            
            function fmt(num){
                return ("0"+num).slice(-2);
            }
            
            function update_page(start){
                $(".page-a").removeClass("active");
                console.log("[pagenum="+(start/filesOnPage)+"]");
                $("[pagenum="+(start/filesOnPage)+"]").addClass("active");
                
                $("#files-tbl").html("");
                
                var formatter = new Intl.DateTimeFormat("ru", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                    second: "numeric"
                });

                $.get('get_rec_files.php',{cmd: "files", start: start, count: filesOnPage},function(answer){
                    console.log(answer);
                    
                    var array = JSON.parse(answer);
                    for(var i=0; i<array.length; i++){
                        var tr = $('<tr>');
                        $("#files-tbl").append(tr);
                        
//                        var tdImg = $('<td>');
//                        tdImg.append();
//                        tr.append(tdImg);

                        var tdDate = $('<td>');
                        tr.append(tdDate);
                        
                        var link = $('<a>',{
                            class: 'btn btn-outline-dark',
                            'data-toggle': "collapse", 
                            href: "#collapsePlayer"+i, 
                            role: "button", 
                            'aria-expanded': "true", 
                            'aria-controls': "collapsePlayer"+i
                        });
                        link.append($('<img>',{
                            src: 'img/play.svg',
                            width: 32,
                            height: 32
                        }));
                        link.append($('<span>',{
                            text: "   "
                        }));
                        link.append($('<span>',{
                            text: `${array[i].day}.${fmt(array[i].mon)}.${array[i].year} ${fmt(array[i].hour)}:${fmt(array[i].min)}:${fmt(array[i].sec)}`
//                            text: formatter.format(new Date(array[i].datetime*1000))
                        }));
                        tdDate.append(link);

                        var collapse = $('<div>',{
                            class: "collapse", 
                            id: "collapsePlayer"+i
                        });
                        tdDate.append(collapse);
                        
                        collapse.append($('<br>'));

                        var player = $('<audio>', {
                            controls: true
                        });
                        collapse.append(player);

                        player.append($('<source>',{
                            src: array[i].path,
                            //type: array[i].path.indexOf("opus")!==-1 ? "audio/ogg; codecs=opus" : "audio/wav"
                        }));
                        
                        player.append("Тег audio не поддерживается вашим браузером.")
                        
                        player.append("<a href='"+array[i].path+"'>Скачать</a>");
                        
//                        var howler_btn = $("<a>",{
//                            class: "btn btn-outline-dark btn-howler",
//                            sound: array[i].path,
//                            text: "Play"
//                        }); 
//                        collapse.append(howler_btn);

                        collapse.append("<p><a class='btn btn-sm btn-outline-dark' href='"+array[i].path+"'>Скачать</a></p>");//

                        var tdIn = $('<td>');
                        tdIn.append("<h3><span class='badge badge-pill badge-secondary'>"+(array[i].in + 1)+"</span></h3>");
                        tr.append(tdIn);

                        var tdOuts = $('<td>');
                        tr.append(tdOuts);
                        
                        var outs = parseInt(array[i].outs, 16);
                        for(var j=0; j<outsCnt; j++){
                            if((outs & (1 << j)) > 0){
                                var spanOut = $('<span>', {
                                    class: 'badge badge-secondary col-2',
                                    text: j+1
                                });
                                tdOuts.append(spanOut);
                            }
                        }
                    }
                    
//                    $(".btn-howler").click(function(){
//                        var sound = $(this).attr("sound");
//                        console.log(sound);
//                        var howl = new Howl({
//                            src: [sound],
//                            sprite: {
//                                blast: [0, 3000],
//                                laser: [4000, 1000],
//                                winner: [6000, 5000]
//                            }
//                        });
//                        howl.play('laser');
//                    });

                });
            }
            
            $.get('get_rec_files.php',{cmd: "count"},function(answer){
                var cnt = parseInt(answer,10);
                pages_count = Math.trunc(cnt/filesOnPage);
                if((cnt%filesOnPage) > 0)
                    pages_count++;
                console.log(pages_count);
                
                for(var i=0; i<pages_count; i++){
                    $(".pages").each(function(){
                        var li = $('<li>',{
                            class: "page-item page-a",
                            pagenum: i
                        });
                        if(i == cur_page){
                            li.addClass("active");
                        }
                        $(this).append(li);

                        var a = $('<a>',{
                            class: "page-link page-num", 
                            href: "#",
                            pagenum: i
                        });
                        li.append(a);

                        a.html(i+1);
                    });
                }
                
                $('.pages').append("<li class='page-item'><a class='page-link next-page' href='#'>Следующая</a></li>");
            
                $(".page-num").click(function(){
                    console.log("Click");
                    var start = $(this).attr('pagenum')*filesOnPage;
                    console.log(start);
                    update_page(start);
                });
                
                $(".prev-page").click(function(){
                    if(cur_page > 0){
                        cur_page--;
                        console.log(cur_page);
                        update_page(cur_page*filesOnPage);
                    }
                });
                
                $(".next-page").click(function(){
                    if(cur_page < (pages_count-1)){
                        cur_page++;
                        console.log(cur_page);
                        update_page(cur_page*filesOnPage);
                    }
                });
            });
            
            get_settings(function(setts){
                update_page(0);
            });
            
            
        </script>
    </body>    
</html>