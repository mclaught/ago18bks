/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConfigServer.h
 * Author: user
 *
 * Created on 27 ноября 2019 г., 12:23
 */

#ifndef CONFIGSERVER_H
#define CONFIGSERVER_H

#define SETTS_PATH  "/etc/ago18bks"
#define EVENTS_PATH  "/var/www/html/AGO18Web/events/"
#define MAX_EVENTS 100

#include <thread>
#include <list>
#include <set>
#include <map>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <condition_variable>
#include <future>
#include <chrono>

#include "proto.h"
#include "json.hpp"
#include "WSServer.h"

using namespace std;
using namespace nlohmann;

#define EVENTS_SOURCE_SERVER    0x00000000
#define PING_MAX_TM 10s
#define PING_PERIOD 1s

struct iface_t{
    string name;
    string addr;
    string bc_addr;
};

struct ch_activity_t{
    bool active;
    std::chrono::time_point<std::chrono::steady_clock> time;
};

struct board_ping_t{
    bool active;
    std::chrono::time_point<std::chrono::steady_clock> time;
    std::chrono::time_point<std::chrono::steady_clock> lastSysPingTm;
};

class ConfigServer {
public:
    typedef enum {
        EVT_NORMAL, EVT_WARNING, EVT_DANGER
    }event_type_t;
    
    enum {
        SERVER_EVT_UDP,
        SERVER_EVT_PD,
        SERVER_EVT_PDUDP,
        SERVER_EVT_FILE,
        SERVER_EVT_CUS_TCP
    };
    
    typedef struct {
        chrono::time_point<std::chrono::system_clock> time;
        event_type_t type;
        ch_mask_t channels;
        string message;
    }event_t;
    typedef map<uint64_t, event_t> events_map_t;
    
    json network;
    uint8_t server_addr = 0x80;
    uint8_t cus_in = 0;
    bool rec_enable = true;
    int timeout_rec = 3000;
    int timeout_iface = 3000;
    int events_log_period = 7;
    int bru_offset = 25;
    int id_offset = 8;
    string rtc = "/dev/rtc0";
    long min_disk_free = 300;
    chrono::seconds ping_period = PING_PERIOD;
    chrono::seconds ping_timeout = PING_MAX_TM;

//    codec_capabilities_t ins_capab[MAX_CHANNELS];
//    codec_capabilities_t outs_capab[CT_BITS];
    
    ConfigServer();
    ConfigServer(const ConfigServer&);
    virtual ~ConfigServer();

    void start();
    void stop();
    void run();

    int getRecParam(string name, int def);
    int getNetParam(string name, int def);
    string getNetParam(string name, const char* def);
    void saveNetwork();
    void setNetParam(string name, string val);
    void setNetParam(string name, int val);
//    void setNetParam(string name, const char* val);
    ch_mask_t getCTOutsInput(int input);
    ch_mask_t getActiveOuts();
    void setCTOutsInput(int input, ch_mask_t outs);
    void setChannelAlarms(int cnt, alarm_item_t* alarms, uint8_t addr_from, blocks_t dev_type);
    void addEvent(ch_mask_t channels, uint8_t addr, alarm_item_t *alarm, event_type_t type, const string msg);
    void addServerEvent(uint8_t alarm_code, event_type_t type, uint8_t param, const char* msg, ...);
    void removeEvent(uint8_t addr, alarm_item_t *alarm);
    void removeServerEvent(uint8_t alarm_code, event_type_t type, uint8_t param);
    void sendSndParams(json js);
    void setChannelActivity(int ch, ch_mask_t active);
    void ctInputFromJSON(int ch);
    int getStoreDays();
    vector<string> getCUSTargets();
    
    void setSndInCapabs(int ch, uint8_t *&capabs);
    void setSndInParams(int ch, uint8_t *&cfg, int index);
    void setSndOutCapabs(int ch, uint8_t *&capabs);
    void setSndOutParams(int ch, uint8_t *&cfg, int index);
    
    void error(string mes, bool _exit=false);
    
    json get_json_snd_params();
    void set_json_snd_params(json value);
    
    json get_settings();
    void set_settings(json value);
    
    json get_comm_table();
    void set_comm_table(json value);
    json get_comm_table_ch(int ch);
    
    void setBRUCfg(uint8_t id, bru_cfg_t* cfg, int sz);
    void setBRUCfg(uint8_t id, json cfg);
    json getBRUCfg(uint8_t id);
    void getBRUCfg(uint8_t id, bru_cfg_t* cfg);
    map<uint8_t, json> get_brus();
    json getBRUsJson();

    void select_ifaces();
    
    int start_clip_process(uint8_t in, ch_mask_t outs, string fileName, bool autorew=false);
    void stop_clip_process(int in);
    void stop_clip_process();
    void start_clip_for_signal(int signal, bool autorew=false);
    
    uint32_t getAlarmsBits();
    
    bool inRecEnabled(int i);
    
    long getAvailableSpace(const char* path);
    
    void setBoardPing(uint8_t addr, uint8_t dev_type, string ip);

//    string read_string(string name){};
//    int read_int(string name){};
//    void write_string(string name, string value){};
//    void write_int(string name, int value){};
//    vector<string> read_strings(string name){};
    
private:
    typedef struct prio_t{
        int ch;
        int prio;
//        friend bool operator<(prio_t a, prio_t b){
//            if(a.prio < b.prio)
//                return true;
//            if(a.prio > b.prio)
//                return false;
//            return a.ch < b.ch;

//        }
    }prio_t;
    
    int sock;
    bool active = true;
    thread thrd;
    swp_table_t swp_table;
    swp_table_t swp_table_def;
    shared_mutex swp_table_mutex;
    int prios[MAX_CHANNELS];
    atomic<ch_mask_t> alarmOuts;
    atomic<ch_mask_t> alarmIns[MAX_CHANNELS];
//    time_t activeChannels[MAX_CHANNELS];
    atomic<ch_mask_t> activeChannels[MAX_CHANNELS];
    atomic<uint64_t> channelsTime[MAX_CHANNELS];
//    atomic<bool> lastSentActiveChannels[MAX_CHANNELS];
//    mutex active_ch_mutex;
    events_map_t events_map;
    shared_mutex events_log_mutex;
    
    json comm_table;
    shared_mutex comm_table_mutex;
    json json_snd_params;
    shared_mutex json_snd_params_mutex;
    json settings;
    shared_mutex settings_mutex;

//    vector<string> alarms_strs;
    json alarms_defs;
    shared_mutex alarms_mutex;

    json clip_defs;
    shared_mutex clip_defs_mutex;
    
    WSServer* ws_server;
    
    map<uint8_t, json> brus;
    shared_mutex brus_mutex;
    
    future<void> measure_async;
    atomic<bool> bru_measure_active;

    bool state_changed;

    int clip_player_pid = 0;
    
    uint64_t insRecMask = 0xFFFFFFFFFFFFFFFF;
    
    typedef struct{
        uint8_t addr;
        uint8_t type;
        string ip_addr;
    }board_def_t;
    map<uint8_t, board_def_t> pingBoardMap;
    board_ping_t boardPingTime[256];
    shared_mutex boardPingMtx;
    chrono::steady_clock::time_point lastPingTm;
    chrono::steady_clock::time_point lastSysPingTm;
    bool startPing = false;
    
    void processSocket(int cli_sock);
    void processQuery(int cli_sock, string str);
    
    void loadFromFile();
    void saveToFile();
    
    bool ctFromJSON(json js);
    bool ctToJSON(json &js);
    void addCTHystory(json ct);
    swp_table_t getSWPTable();
    void sendSWPTable();
    
    void loadAlarms();
    void setAlarms(json js);
    json getAlarms();
    json getAlarmsStr(int n);
    bool isChannelAlarmed(int ch);
    ch_mask_t getChannelActive(int ch);
    bool isChannelTimedOut(int ch);
    void activeChannelsTimeout();
    void saveBoards();
    void systemPing(string ip, int secs);
    void checkBoardPings();
    string devTypeToNameBrdNum(blocks_t dev_type, uint8_t addr, int &brdNum);
    string alarmToStr(int alarm, int addr, int param, int ch, int brd, event_type_t &type, blocks_t dev_type);
    uint32_t alarmSource(int ch, int brd, alarm_t alarm, blocks_t dev_type);
    events_map_t getEvents();
    json eventsToJson();
    
    string loadNTPText();
    bool findNTPStr(string s_ntp, int &pos, int &len);
    string getNTP();
    void setNTP(string value);
    
    json backup();
    void restore(json js);
    void restore_snd_params(json js, string type);
    
    void send_ws(string type);
    void send_ws(string type, string data);
    void send_ws_input_state(int in);

    bool sndJSON();//deprecated
    void settsToJSON();//deprecated
    
    void bru_measure(int bru_id);

    void replace_tag(string &str, string tag, string val);

    friend void config_thread_func(ConfigServer* __this);
    string select_if(string type, string def, string &bc_addr);
    void enter_str_param(string descr, string& value);

    json get_clip_def(string name);
    
    int get_player_pid(int in);

    void writeEventsLog(event_t event);
    json eventsLogFiles();
    json eventsLogToJson(string fname);
};

extern ConfigServer conf_server;

#endif /* CONFIGSERVER_H */


