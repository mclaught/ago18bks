#ifndef UDPCHANNEL_H
#define UDPCHANNEL_H

#include "basesoundsource.h"

#include <QElapsedTimer>
#include <QFuture>
#include <QObject>
#include <QTimer>
#include <QUdpSocket>
#include <atomic>

#define MAX_INP_CHANNELS        2
#define CHAN_TO_BRD_ID(x)	((x) / MAX_INP_CHANNELS)
#define BRD_ID_TO_CHAN(x,y)     ((MAX_INP_CHANNELS * x) + y)

#pragma pack(push, 1)
typedef enum {
  PACK_AUDIO = 0, PACK_LOGS, PACK_ALARM, PACK_CFG, PACK_OTHER,
          PACK_TYPES_CNT
}pack_type_t;//тип пакета для быстрой коммутации

typedef enum {
    NO_ACTION = 0, SND_START_REQ, SND_STOP_REQ, SND_CHANK_REQ, SND_IN_CPB_REQ, SND_OUT_CPB_REQ, SND_IN_CURR_REQ, SND_OUT_CURR_REQ,//7
        EXT_SWT, SND_STREAM, AUDIO_CFG_IN, AUDIO_CFG_OUT, CONTROL_CFG, ALARM_MSG, LOG_MSG, SET_RECIPNT_LIST, CLEAR_RECIPNT_LIST,//16
        CH_BUSY_MSG, SET_MASTER, CLEAR_MASTER, REC_NUMBER, GET_TEMPERATURE, BRU_TEST_REQ, BRU_TEST_END, UM_SWITCH_REQ, BRU_FROM_BYPS_SWITCH_REQ,
        BRU_TO_BYPS_SWITCH_REQ, BRU_START_MEASURE, BRU_STOP_MEASURE, BRU_RAW_DATA, BRU_LINE_MEASURE, BRU_TEST_MODE_START, BRU_TEST_MODE_STOP,//32
        BRU_SAVE_CFG, SCREEN_MSG, BRU_LINE_MEAS_RESULT, BRU_GET_STATE, CHANGE_ID, GET_ID, UM_UNLOCK, FW_BOOT_MODE, FW_FILE_INFO, FW_GET_FILE,//42
        FW_UPDATE_BEGIN, FW_UPDATE_STATUS, FW_RESET, FW_INFO, SWPT_REQ, READ_FLASH_CFG, WRITE_FLASH_CFG, BRU_RESET_SWITCH_CFG, FAKE_ALARM,//51
        MON_INFO, HW_EQ, SW_EQ, SW_EQ_BYPASS, UM_FEEDBACK, HPF_CFG, PA_CFG, PA_CNTRL_ASCII, PA_CNTRL_BIN, LOG_CFG, GET_LOG_CFG, CLEAR_FLASH_CFG,
        BRU_MANUAL_SWITCH
}eth_cmd_t;

typedef uint32_t ch_mask_t;
#define CT_BITS (sizeof(ch_mask_t)*8)

typedef struct
{
  uint8_t adr_to;//номер платы назначения (адрес ПК 0x80)
  uint8_t adr_from;//номер платы отправителя
  uint8_t repeate_cnt;//счетчик повторов от 0
  uint8_t type;// тип пакета
  uint16_t len;//длина оставшейся части пакета
}pack_hdr_t;//заголовок любого пакета

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    uint8_t data[];
}packet_command_t;

typedef struct{
    uint8_t src_ch;
    ch_mask_t recipients;
//    uint8_t recipients_amount;
//    uint8_t recipients[MAX_CHANNELS];
}start_stop_t;

//typedef struct
//{
//  uint8_t ch_table[MAX_INP_CHANNELS];//номера каналов отправителя.
//  uint8_t streams;//кол-во потоков (каналов) (1 или 2. Другие значения - ошибка)
//}audio_hdr_t;//добавочный заголовок для аудиопакета
typedef struct
{
  uint8_t sender;//номер канала отправителя.
  ch_mask_t recip;//бит. маска получателей
}audio_hdr_t;//добавочный заголовок аудиопакета для 32 получателей

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    audio_hdr_t audio_hdr;
    int16_t pcm[AUDIO_SAMPLES*2];
}packet_audio_t;

#pragma pack(pop)


class UDPChannel : public QObject
{
    Q_OBJECT
public:
    explicit UDPChannel(QHostAddress addr, uint16_t port, BaseSoundSource* reader, QObject *parent = nullptr);

    void start(uint8_t src_ch, ch_mask_t recipients, int period);
    void stop(uint8_t src_ch, ch_mask_t recipients);
private:
    BaseSoundSource* _reader;
    QUdpSocket *sock;
    QHostAddress serverAddr;
    uint16_t serverPort;
    uint8_t src_ch;
    ch_mask_t recip;
    QElapsedTimer sendTimer;
    uint8_t master_brd_addr = 0xFF;
    uint8_t master_brd_chs[MAX_INP_CHANNELS];
    QTimer timer;
    int period;
    QFuture<void> sendFurute;
    std::atomic_bool running;
    int prebuf;
    qint64 sent = 0;
    qint64 lastSentTm = 0;

    void sendCommand(uint8_t *data, int len, uint8_t type, uint8_t cmd, int repeats=1, uint8_t addr = 0xFF);
    void sendSound();
    void selectMasterBoard(ch_mask_t outs);
    void unselectMasterBoard();
signals:
private slots:
    void readDgram();
    void onSoundReaderTimer();
    void onOtherTimer();
};

#endif // UDPCHANNEL_H
