<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");

$cus_enable = filter_input(INPUT_POST, 'cus_enable');
$udp_multicast = filter_input(INPUT_POST, 'udp_multicast');

$path = '/etc/ago18bks/network.json';

$json = json_decode(file_get_contents($path));

$jsonArr = get_object_vars($json);
$jsonArr['udp-multicast'] = $udp_multicast;
$json = (object)$jsonArr;

$json->cus_enable = intval($cus_enable);

$jsonText = json_encode($json, JSON_PRETTY_PRINT);
echo $jsonText."\n";
file_put_contents('files/network.json', $jsonText);
echo system("sudo cp -f files/network.json /etc/ago18bks");
