#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "agobase.h"
#include "keyboard.h"

#include <QDialog>
#include <QLabel>
#include <QLineEdit>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(AgoBase* ago, QWidget *parent = nullptr);
    ~SettingsDialog();

    void setAudioDevice(QString name);
    QString getAudioDevice();

    void setIP(QString ip);
    QString getIP();

    void setPort(int port);
    int getPort();

    void setInCh(int ch);
    int getInCh();

    void setFullScreen(bool value);
    bool getFullScreen();

    void setAudioFolder(QString value);
    QString getAudioFolder();

    void setCursor(bool value);
    bool getCursor();

    void setPerion(int value);
    int getPeriod();

    void setBroadcast(QString value);
    QString getBroadcast();

    void setAGOType(int val);
    int getAGOType();

    void setCOMPort(QString val);
    QString getCOMPort();

    void setTragetsFile(QString val);
    QString getTargetsFile();

    void setBaud(int val);
    int getBaud();

    void setKeyboard(bool val);
    bool getKeyboard();

    void setVolumeSlider(bool val);
    bool volumeSlider();

    void setPass(QString val);
    QString getPass();

    void setColsCnt(int cnt);
    int getColsCnt();

    void setTargsFntSz(int sz);
    int getTargsFntSz();

    void setPortPeriod(int val);
    int getPortPeriod();

    void setTargCnt(int val);
    int getTargCnt();

    bool getCloseApp();
private slots:
    void on_btBrowseFolder_clicked();

    void on_btIP_clicked();

    void on_btPort_clicked();

    void on_btInCh_clicked();

    void on_btServerSetts_clicked();

    void on_btPeriod_clicked();

    void on_btBroadcast_clicked();

    void on_cbAGO_currentIndexChanged(int index);

    void on_btTargetsFile_clicked();

    void on_btEditTargets_clicked();

    void on_btPass_clicked();

    void on_btPortPeriod_clicked();

    void on_edPortPeriod_textChanged(const QString &arg1);

    void on_cbCloseApp_stateChanged(int arg1);

private:
    AgoBase* ago;
    Ui::SettingsDialog *ui;
    bool pressed = false;
    QPointF pressPos;
    int pressScrollPos;

    void keyboardForEdit(QLabel *label, QLineEdit *edit, Keyboard::kb_type_t type=Keyboard::KB_TEXT);

protected:
    virtual void mousePressEvent(QMouseEvent *event)override;
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
};

#endif // SETTINGSDIALOG_H
