var searchData=
[
  ['data_100',['data',['../struct_channel_1_1send__queue__iten__t.html#a8fffbe0c46ec779cd5e16fe8e41eea37',1,'Channel::send_queue_iten_t::data()'],['../structpacket__command__t.html#a91d73e247ac7e58d193a6757c19fc081',1,'packet_command_t::data()'],['../structpd__packet__t.html#a19081115ce3c947feba644e507ecdb50',1,'pd_packet_t::data()']]],
  ['debug_5fsend_101',['DEBUG_SEND',['../proto_8h.html#aa6204bac407cf90809ece0bdf7b02c6b',1,'proto.h']]],
  ['decode_102',['decode',['../namespace_r_t_p.html#a2eafe0bcf42275be4edc25f013fdd880',1,'RTP']]],
  ['desc_103',['DESC',['../ago18bks_8d.html#a8fe220f0393691d2f97f058910c60af9',1,'ago18bks.d']]],
  ['disconnect_104',['disconnect',['../class_channel.html#afb74271d13687e37d881386b6f78cb12',1,'Channel::disconnect()'],['../class_t_c_p_channel.html#a096ef97d918987a0856d0586d3899ca8',1,'TCPChannel::disconnect()'],['../class_u_d_p_channel.html#aebb24eb182aa8b52dea7629b4c038e03',1,'UDPChannel::disconnect()']]],
  ['disconnected_105',['DISCONNECTED',['../class_t_c_p_channel.html#ad304f9e9bf9aaa65c7e2c8f28d822d0eae3e97106098d71e8198e66ecdbf4a30e',1,'TCPChannel']]],
  ['dlsr_106',['dlsr',['../struct_r_t_p_1_1rtcp__hdr__t.html#a7473e2e5587c07a33a58a16364bfc5c1',1,'RTP::rtcp_hdr_t']]]
];
