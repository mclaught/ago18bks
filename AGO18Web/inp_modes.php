<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?1">
        
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        $nb_add = "<button class='btn btn-outline-light my-2 my-sm-0' id='btn-save'>Сохранить</button>";
        include "navbar.php";
        include "alert.html";
        ?>

        <div class="container">
            <br>
            <h1><img src="img/input_black.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Режимы входов</h1>
            <br>
            
            <div class="card-body shadow">
                <div class="alert alert-primary" role="alert">
                    Включенная опция - расширенный режим для выбранного входа (32 выхода)
                </div>                            
                <div class="row" id="inp-modes">
                </div>
                <div class="alert" role="alert" id="inp-mode-result"></div>                            
            </div>
        </div>    
        
        <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript" src="js/alert.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/constants.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/rest.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/get_setts.js" crossorigin="anonymous"></script>
        <script>
            var rest = new Rest();
            
            async function loadInpMode(){
                get_settings(async function(setts){
                    let res = await (rest.get_inp_mode());
                    console.info("res", res);
                    let inp_mode = 0;
                    if(res.result === "success"){
                        inp_mode = res.inp_mode;
                        let ins = res.ins;
                        
                        $('#inp-modes').html('');
                        
                        for(let i=0; i<ins.length; i++){
                            if(ins[i]<insCnt){
                                let checked = (inp_mode & (1<<ins[i])) !== 0 ? "checked" : "";
                                let name = setts.ins[ins[i]].name!=="" ? setts.ins[ins[i]].name : `${ins[i]+1}`;
                                let html = `<div class="col-3">
                                    <input inp_n="${ins[i]}" type="checkbox" class="mr-3 inp-mode-check" ${checked}>
                                    <label class="inp-mode-lbl" inp_n="${ins[i]}">${name}</label>
                                </div>`;
                                $('#inp-modes').append(html);
                            }
                        }
                        
//                        alertify.success(`Опрошено ${res.count} устройств`);
                        let devs_str;
                        switch(res.count){
                            case 1:
                                devs_str = 'устройство';
                                break;
                            case 2:
                            case 3:    
                            case 4:    
                                devs_str = 'устройства';
                                break;
                            default:    
                                devs_str = 'устройств';
                        }
                        $("#inp-mode-result").html(`Опрошено ${res.count} ${devs_str}`);
                        $("#inp-mode-result").addClass('alert-success');
                    }else{
//                        alertify.error("Ошибка: "+res.message);
                        $("#inp-mode-result").html(res.message);
                        $("#inp-mode-result").addClass('alert-danger');
                    }
                });
            }
            
            $("#btn-save").click(async function(){
                let inp_mode = 0;
                $('.inp-mode-check').each(function(){
                    let n = $(this).attr('inp_n');
                    if($(this).prop('checked')){
                        inp_mode |= (1<<n);
                    }
                });
                console.info(`inp_mode = ${inp_mode}`);
                
                let resp = await rest.set_inp_mode(inp_mode);
                console.info("resp", resp);
                
                if(resp.result === "success"){
                    alertify.success("Настройки сохранены");
                }else{
                    altertify.error(`Ошибка: ${resp.message}`);
                }
                
                loadInpMode();
            });
            
            loadInpMode();
        </script>
    </body>
</html>
