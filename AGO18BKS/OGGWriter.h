/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OGGWriter.h
 * Author: user
 *
 * Created on 26 ноября 2019 г., 23:34
 */

#ifndef OGGWRITER_H
#define OGGWRITER_H

#include "BaseWriter.h"
#include <ogg/ogg.h>
#include <fstream>
#include <opus/opus.h>
#include "proto.h"

#define OPUS_BITRATE        32000
#define OPUS_PACKET_SIZE    ((OPUS_BITRATE * AUDIO_FRAME_MS) / 8000)
#define PAGE_PACKETS   (1000/AUDIO_FRAME_MS)

using namespace std;

class OGGWriter : public BaseWriter {
public:
    OGGWriter(string filename, string descr);
    OGGWriter(const OGGWriter& orig);
    virtual ~OGGWriter();
    
//    virtual void open();
    virtual void write(int16_t* pcm, int samples);
private:
#pragma pack(push, 1)    
    typedef struct{
        uint8_t sign[8] = {'O','p','u','s','H','e','a','d'};
        uint8_t version;
        uint8_t channels;
        uint16_t pre_skip;
        uint32_t sample_rate;
        uint16_t out_gain;
        uint8_t mapping_family;
    }opus_head_t;
    
    typedef struct{
        uint32_t length;
        char text[100];
    }opus_comment_t;
    
    typedef struct{
        uint8_t sign[8] = {'O','p','u','s','T','a','g','s'};
        uint32_t vendor_length;
        uint8_t vendor[8];
        uint32_t comment_list_length;
        uint8_t comments[1024];
    }opus_tags_t;
#pragma pack(pop)    
    
    OpusEncoder *opus;
    ogg_stream_state strm;
    ogg_packet packet;
    ofstream file;
    int pack_cnt = 0;
    
    void writePage(bool eof);
};

#endif /* OGGWRITER_H */

