#ifndef BASESOUNDSOURCE_H
#define BASESOUNDSOURCE_H

#include <QObject>
#include <mutex>

#define AUDIO_FREQ 48000
#define AUDIO_FRAME_MS  5
#define AUDIO_SAMPLES (AUDIO_FREQ * AUDIO_FRAME_MS / 1000)
#define FIFO_FRAMES 100
#define FIFO_SIZE (FIFO_FRAMES * AUDIO_SAMPLES)

class BaseSoundSource : public QObject
{
    Q_OBJECT
public:
    explicit BaseSoundSource(QObject *parent = nullptr);

    virtual void start();
    virtual void stop()=0;

    int fifo_cnt();
    bool fifo_read(int16_t *data, int cnt);

protected:
    void fifo_add(int16_t *data, int cnt);

    int in_sum = 0;
    int out_sum = 0;

private:
    int16_t fifo[FIFO_SIZE];
    int fifo_in = 0;
    int fifo_out = 0;
    std::mutex fifo_mutex;

signals:

};

#endif // BASESOUNDSOURCE_H
