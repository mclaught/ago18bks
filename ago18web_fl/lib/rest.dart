import 'dart:convert';

import 'package:ago18web_fl/comm_table.dart';
import 'package:ago18web_fl/settings.dart';
import 'package:http/http.dart';

class Rest{
  final String url = "http://192.168.1.158";

  Future<dynamic> _command(String cmd, Map<String, dynamic> query)async{
    query['command'] = cmd;

    Response resp = await post(url+"/command.php", body: {'query': jsonEncode(query)});
    if (resp.statusCode >= 300) {
      throw Exception(resp.reasonPhrase);
    }

    print(resp.body);

    return jsonDecode(resp.body);
  }

  Future<Settings> get_settings()async{
    dynamic json = await _command("get_settings", {});
    return Settings.fromJson(json['settings']);
  }

  Future<dynamic> get_comm_table()async{
    dynamic json = await _command("get_comm_table", {});
    List<CommTable> list = json["comm_table"]
  }
}