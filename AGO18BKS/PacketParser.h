/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PacketParser.h
 * Author: user
 *
 * Created on 26 ноября 2019 г., 14:52
 */

#ifndef PACKETPARSER_H
#define PACKETPARSER_H

#include <future>
#include <map>
#include <memory>
#include <shared_mutex>
#include <netinet/in.h>

#include "proto.h"
#include "AudioSaver.h"

class PacketParser {
public:
    PacketParser();
    PacketParser(const PacketParser&);
    virtual ~PacketParser();
    
    static PacketParser* get_instance(uint8_t type);
    static PacketParser* get_instance(pack_hdr_t* pack);
    static void clear_parsers();
    
    virtual void parse(pack_hdr_t* pack, sockaddr_in from_addr) = 0;
private:

};

class PacketParserAudio : public PacketParser {
public:
    PacketParserAudio();
    PacketParserAudio(const PacketParserAudio& orig);
    virtual ~PacketParserAudio();
    
    virtual void parse(pack_hdr_t* pack, sockaddr_in from_addr);
private:
    AudioSaver* savers[MAX_CHANNELS];
    uint8_t pack_n = 0;
    chrono::time_point<chrono::high_resolution_clock> last_recv;
    
    void split_channels(int16_t* buf, int16_t* ch1, int16_t* ch2);
    void save_audio(int16_t* pcm, int ch_n);
    AudioSaver* get_saver(int ch_n);
};

class PacketParserAlarm : public PacketParser{
public:
    PacketParserAlarm();
    PacketParserAlarm(const PacketParserAlarm& orig);
    virtual ~PacketParserAlarm();
    
    virtual void parse(pack_hdr_t* pack, sockaddr_in from_addr);
    
private:
};

typedef struct dev_id_t{
    uint8_t id;
    string uid;
    string descr;

//    friend bool operator<(const dev_id_t id1, const dev_id_t id2)noexcept{
//        if(id1.id < id2.id)
//            return true;
//        if(id1.id > id2.id)
//            return false;
//        if(id1.uid < id2.uid)
//            return true;
//        if(id1.uid > id2.uid)
//            return false;
//        if(id1.descr < id2.descr)
//            return true;
//        if(id1.descr > id2.descr)
//            return false;
//        return false;
//    }
}dev_id_t;

class PacketParserCfg : public PacketParser{
public:
    PacketParserCfg();
    PacketParserCfg(const PacketParserCfg& orig);
    virtual ~PacketParserCfg();
    
    virtual void parse(pack_hdr_t* pack, sockaddr_in from_addr);
    
    map<string, dev_id_t> get_id(uint8_t addr);
    future<void> change_id(uint8_t old_id, uint8_t new_id, uint8_t offset, uint8_t um_mode);
    future_status set_id(uint8_t id, uint8_t redundant, brd_id_t* new_brd_id);
    set<uint8_t> get_inp_mode(uint32_t* bits);
    void set_inp_mode(uint32_t bits);
    void set_critical_alarms(uint32_t bits);
private:
    void setSndCapabIn(packet_command_t* packet);
    void setSndCurrIn(packet_command_t* packet);
    void setSndCapabOut(packet_command_t* packet);
    void setSndCurrOut(packet_command_t* packet);
    void requestSoundPacket(packet_command_t* packet);
    void requestClip(packet_command_t* packet);
    
    shared_ptr<promise<void>> change_id_promise;
    shared_ptr<promise<brd_id_t>> set_id_promise;
    future<future_status> set_id_async;
    mutex set_id_mutex;
    
    shared_ptr<promise<void>> set_critical_alarms_promise;
    mutex set_critical_alarms_mutex;

    uint32_t inp_mode_bits = 0;
//    int inp_mode_cnt = 0;
    set<uint8_t> inp_mode_set;

    map<string, dev_id_t> found_ids;
    map<int, chrono::time_point<chrono::system_clock>> recnumTime;
};

class PacketParserBRU : public PacketParser{
public:
    PacketParserBRU();
    PacketParserBRU(const PacketParserBRU& orig);
    virtual ~PacketParserBRU();
    
    virtual void parse(pack_hdr_t* pack, sockaddr_in from_addr);
    
    future<uint32_t> start_test_mode(uint8_t bru_id);
    future<bru_measure_t> line_measure(uint8_t bru_id, uint8_t line_id);
    future<uint32_t> switch_cfg(uint8_t bru_id);

    map<uint8_t, bru_measure_t> get_measures();
    void add_measure(bru_measure_t measure);
private:
    shared_ptr<promise<uint32_t>> start_test_mode_promise;
    mutex start_test_mode_mutex;
    shared_ptr<promise<bru_measure_t>> line_measure_promise;
    mutex line_measure_mutex;
    shared_ptr<promise<uint32_t>> switch_cfg_promise;
    mutex switch_cfg_mutex;

    map<uint8_t, bru_measure_t> measures;
    shared_mutex measures_mutex;
};

class PacketParserGenCall : public PacketParser{
public:
    PacketParserGenCall();
    PacketParserGenCall(const PacketParserGenCall& orig);
    virtual ~PacketParserGenCall();
    
    virtual void parse(pack_hdr_t* pack, sockaddr_in from_addr);
    
private:
};


#endif /* PACKETPARSER_H */

