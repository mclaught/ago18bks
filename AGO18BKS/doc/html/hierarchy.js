var hierarchy =
[
    [ "audio_hdr_t", "structaudio__hdr__t.html", null ],
    [ "AudioSaver", "class_audio_saver.html", null ],
    [ "BaseSoundSender", "class_base_sound_sender.html", [
      [ "BufferedSoundSender", "class_buffered_sound_sender.html", [
        [ "MyPlayer", "class_my_player.html", null ],
        [ "SocketSoundSender", "class_socket_sound_sender.html", null ],
        [ "SoundSenderFile", "class_sound_sender_file.html", null ]
      ] ],
      [ "UDPSoundSender", "class_u_d_p_sound_sender.html", null ]
    ] ],
    [ "BaseWriter", "class_base_writer.html", [
      [ "OGGWriter", "class_o_g_g_writer.html", null ],
      [ "WavSaver", "class_wav_saver.html", null ]
    ] ],
    [ "biqd_hp_coeff_t", "structbiqd__hp__coeff__t.html", null ],
    [ "biqd_lp_coeff_t", "structbiqd__lp__coeff__t.html", null ],
    [ "codec_capabilities_t", "structcodec__capabilities__t.html", null ],
    [ "codec_eq_coeff_t", "structcodec__eq__coeff__t.html", null ],
    [ "codec_eq_t", "structcodec__eq__t.html", null ],
    [ "ConfigServer", "class_config_server.html", null ],
    [ "filter_t", "structfilter__t.html", null ],
    [ "hp_stage_t", "structhp__stage__t.html", null ],
    [ "lp_stage_t", "structlp__stage__t.html", null ],
    [ "MyObject", "class_my_object.html", [
      [ "Channel", "class_channel.html", [
        [ "TCPChannel", "class_t_c_p_channel.html", null ],
        [ "UDPChannel", "class_u_d_p_channel.html", null ]
      ] ]
    ] ],
    [ "MyPCM", "class_my_p_c_m.html", null ],
    [ "MyThread", "class_my_thread.html", [
      [ "MainModel", "class_main_model.html", null ],
      [ "Session", "class_session.html", null ]
    ] ],
    [ "MyUDPServer", "class_my_u_d_p_server.html", null ],
    [ "ogg_hdr_t", "structogg__hdr__t.html", null ],
    [ "pack_hdr_t", "structpack__hdr__t.html", null ],
    [ "packet_audio_t", "structpacket__audio__t.html", null ],
    [ "packet_command_t", "structpacket__command__t.html", null ],
    [ "PacketParser", "class_packet_parser.html", [
      [ "PacketParserAlarm", "class_packet_parser_alarm.html", null ],
      [ "PacketParserAudio", "class_packet_parser_audio.html", null ],
      [ "PacketParserCfg", "class_packet_parser_cfg.html", null ]
    ] ],
    [ "pd_packet_hdr_t", "structpd__packet__hdr__t.html", null ],
    [ "pd_packet_t", "structpd__packet__t.html", null ],
    [ "pd_start_t", "structpd__start__t.html", null ],
    [ "pd_stream_t", "structpd__stream__t.html", null ],
    [ "PDServer", "class_p_d_server.html", null ],
    [ "PDUDPServer", "class_p_d_u_d_p_server.html", null ],
    [ "RTP::rtcp_hdr_t", "struct_r_t_p_1_1rtcp__hdr__t.html", null ],
    [ "RTP::rtp_hdr_t", "struct_r_t_p_1_1rtp__hdr__t.html", null ],
    [ "Channel::send_queue_iten_t", "struct_channel_1_1send__queue__iten__t.html", null ],
    [ "SessionsPrms", "struct_sessions_prms.html", null ],
    [ "shared_buf_t", "structshared__buf__t.html", null ],
    [ "snd_capab_init_packet_t", "structsnd__capab__init__packet__t.html", null ],
    [ "snd_capab_t", "structsnd__capab__t.html", null ],
    [ "snd_cfg_packet_t", "structsnd__cfg__packet__t.html", null ],
    [ "snd_cfg_t", "structsnd__cfg__t.html", null ],
    [ "snd_lvl_t", "structsnd__lvl__t.html", null ],
    [ "start_stop_t", "structstart__stop__t.html", null ],
    [ "swp_table_packet_t", "structswp__table__packet__t.html", null ],
    [ "swp_table_t", "structswp__table__t.html", null ],
    [ "TragetPrms", "struct_traget_prms.html", null ],
    [ "wav_format_t", "structwav__format__t.html", null ]
];