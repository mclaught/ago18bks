var _audio_saver_8h =
[
    [ "ogg_hdr_t", "structogg__hdr__t.html", "structogg__hdr__t" ],
    [ "AudioSaver", "class_audio_saver.html", "class_audio_saver" ],
    [ "FORMAT", "_audio_saver_8h.html#ac8dc47e3b39c930caed4bfd05b4ba805", null ],
    [ "MAX_FILE_SIZE", "_audio_saver_8h.html#a649ad5274d988d17a275a3cead056746", null ],
    [ "PCM_FIFO_SIZE", "_audio_saver_8h.html#aea64de195d29c55e23fd16368bb3fc9f", null ],
    [ "REC_PATH", "_audio_saver_8h.html#adee659698bd21f15edc23ef23666e545", null ],
    [ "REC_STEP_SEC", "_audio_saver_8h.html#a1d35e823a33028865f5e7906a259c254", null ]
];