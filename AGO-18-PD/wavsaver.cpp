#include "wavsaver.h"
#include "iostream"

WavSaver::WavSaver(std::string fileName, uint32_t sample_rate)
{
    file.open(fileName, ios_base::out | ios_base::binary);
    if(file.is_open()){
        cout << "File " << fileName.c_str() << " is open" << endl;
        writeHeader(sample_rate);
    }
}

WavSaver::~WavSaver(){
    close();
}

void WavSaver::writeHeader(uint32_t sample_rate)
{
    const uint32_t RIFF = static_cast<uint32_t>('FFIR');
    const uint32_t WAVE = static_cast<uint32_t>('EVAW');
    const uint32_t fmt = static_cast<uint32_t>(' tmf');
    const uint32_t data_ = static_cast<uint32_t>('atad');

    uint32_t size = 0;

    file.write(reinterpret_cast<const char*>(&RIFF), 4);
    file.write(reinterpret_cast<const char*>(&size), 4);
    file.write(reinterpret_cast<const char*>(&WAVE), 4);

    wav_format_t format;
    format.compression = 1;
    format.channles = 1;
    format.sample_rate = sample_rate;
    format.bytes_per_sec = sample_rate*2;
    format.allign = 2;
    format.bits_per_sample = 16;
//    format.extra = 0;
//    memset(format.reserved, 0, 22);
    size = sizeof(wav_format_t);
    file.write(reinterpret_cast<const char*>(&fmt), 4);
    file.write(reinterpret_cast<const char*>(&size), 4);
    file.write(reinterpret_cast<const char*>(&format), sizeof(wav_format_t));

    size = 0;
    file.write(reinterpret_cast<const char*>(&data_), 4);
    file.write(reinterpret_cast<const char*>(&size), 4);
}

void WavSaver::write(int16_t* pcm, int samples)
{
//    qDebug() << tmr.nsecsElapsed();
//    tmr.start();
    file.write(reinterpret_cast<const char*>(pcm), samples*2);
    if(file.bad())
        cout << "File write error" << endl;

    //cout << "Write " << file.tellp() << "\r";
}

void WavSaver::close()
{
    uint32_t size;

    int fSize = static_cast<int>(file.tellp());

    file.seekp(4);
    size = static_cast<uint32_t>(fSize - 8);
    file.write(reinterpret_cast<const char*>(&size), 4);

    file.seekp(24 + sizeof(wav_format_t));
    size = static_cast<uint32_t>(static_cast<size_t>(fSize) - 28 - sizeof(wav_format_t));
    file.write(reinterpret_cast<const char*>(&size), 4);

    file.close();
    cout << "File closed" << endl;
}
