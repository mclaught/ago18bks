class LineDialog extends MyView{
    constructor(container){
        super(container);
        
        let _this = this;
        $('#btn-dlg-save').click(function(){
            _this.line.low_trshld_ohm = parseFloat($('#lineModal').find('.low_trshld_ohm').val());
            _this.line.high_trshld_ohm = parseFloat($('#lineModal').find('.high_trshld_ohm').val());
            _this.line.shortOhm = parseFloat($('#lineModal').find('.shortOhm').val());
            _this.line.openOhm = parseFloat($('#lineModal').find('.openOhm').val());
            _this.line.ref_value_ohm = parseFloat($('#lineModal').find('.ref_value_ohm').val());
            _this.line.state = parseInt($('#lineModal').find('.state').val());
            _this.line.range = parseInt($('#lineModal').find('.range').val());
            
            $('#lineModal').modal('hide');
            
            _this.onsave(_this.line);
        });
    }
    
    init(){
        super.init();
    }
    
    create_input(name, descr, unit, cls, value){
        let div = $('<div>', {class: "input-group mb-3 "+cls}).append(
            $('<div>', {class: "input-group-prepend"}).append(
                $('<span>', {class: "input-group-text", id: name+"-label"}).html(descr)
            ),
            $('<input>', {type: "text", class: "form-control "+name, placeholder: descr, "aria-label": descr, "aria-describedby": name+"-label"})
                    .val(value)
        );
        if(unit != ''){
            div.append(
                $('<div>', {class: "input-group-append"}).append(
                    $('<span>', {class: "input-group-text"}).html(unit)
                ),
            );
        }
        return div;
    }
    
    create_list(name, descr, items, cls, value){
        let select = $('<select>', {class: 'form-control '+name});
        for(let i=0; i<items.length; i++){
            select.append($('<option>', {value: i}).html(items[i]).prop('selected', i==value));//
        }
        let div = $('<div>', {class: "input-group mb-3 "+cls}).append(
            $('<div>', {class: "input-group-prepend"}).append(
                $('<span>', {class: "input-group-text", id: name+"-label"}).html(descr)
            ),
            select
        );
        return div;
    }
    
    build(){
        $('#lineModalLabel').html(this.hdr);
        
        let div = $('<div>');
        div.append(
            this.create_input('low_trshld_ohm', 'Норма. Нижний порог', 'Ом', 'col-6', Math.round(this.line.low_trshld_ohm*10)/10),
            this.create_input('high_trshld_ohm', 'Норма. Верхний порог', 'Ом', 'col-6', Math.round(this.line.high_trshld_ohm*10)/10),
            this.create_input('shortOhm', 'КЗ', 'Ом', 'col-6', Math.round(this.line.shortOhm*10)/10),
            this.create_input('openOhm', 'Обрыв', 'Ом', 'col-6', Math.round(this.line.openOhm*10)/10),
            this.create_input('ref_value_ohm', 'Референсное сопротивление', 'Ом', 'col-6', Math.round(this.line.ref_value_ohm*10)/10),
            this.create_list('state', 'Состояние линии', ['OK', 'КЗ', 'Обрыв', 'Не норма', 'Ошибка измерения', 'LINE_ERR_CONV', 'LINE_OUT_OF_RANGE'], 'col-6', this.line.state),
            this.create_list('range', 'Режим измерений', ['Низкий', 'Высокий', 'x10'], 'col-6', this.line.range),
        );
        return div;
    }
    
    show(hdr, line, onsave){
        this.hdr = hdr;
        this.line = line;
        this.onsave = onsave;
        
        this.setState();
        
        let _this = this;
        $('#lineModal').modal('show');
    }
}