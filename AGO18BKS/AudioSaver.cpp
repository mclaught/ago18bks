/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AudioSaver.cpp
 * Author: user
 * 
 * Created on 26 ноября 2019 г., 19:03
 */

#include "AudioSaver.h"
#include "ConfigServer.h"
#include "MyUDPServer.h"
#include "TimeSync.h"

#include "cus/log.h"
#include <string.h>
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <experimental/filesystem>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#define _GNU_SOURCE
#include <sched.h>
#include <pthread.h>

void saver_func(AudioSaver* __this);

AudioSaver::AudioSaver(int _channel) : channel(_channel) {
    svr_thread = new thread(saver_func, this);

    sched_param param;
    param.sched_priority = 0;
    if(pthread_setschedparam(svr_thread->native_handle(), SCHED_OTHER, &param))
        cerr << red << "sched_setscheduler " << strerror(errno) << clrst << endl;

//    printf("AudioSaver %d created. id=%d\n", channel, svr_thread->native_handle());
}

AudioSaver::AudioSaver(const AudioSaver&) {
}

AudioSaver::~AudioSaver() {
    active = false;

    {
        unique_lock<mutex> lock(sync_mutex);
        sync.notify_all();
    }

    svr_thread->join();
    delete svr_thread;

    printf("AudioSaver %d stoped\n", channel);
}

void AudioSaver::add_frame(int16_t* pcm){
    if(!timeSync.get_sync_ok()){
        return;
    }
    
    if(pcm_fifo_cnt < PCM_FIFO_SIZE){
        memcpy(&pcm_fifo[pcm_fifo_in].buf[pcm_fifo[pcm_fifo_in].s], pcm, AUDIO_SAMPLES*sizeof(uint16_t));

        pcm_fifo[pcm_fifo_in].s += AUDIO_SAMPLES;

        if(pcm_fifo[pcm_fifo_in].s == PCM_FIFO_PACKS*AUDIO_SAMPLES){
            pcm_fifo[pcm_fifo_in].s = 0;
            pcm_fifo_in++;
            if(pcm_fifo_in >= PCM_FIFO_SIZE)
                pcm_fifo_in = 0;
            pcm_fifo_cnt++;

            unique_lock<mutex> lock(sync_mutex);
            sync.notify_all();
        }

        last_write_tm = GetTickCount();

    }else{
        log_error("PCM FIFO overload");
    }
}

void AudioSaver::save_pcm(int16_t* pcm, int cnt){
    new_file();
    
    if(file){
        file->write(pcm, cnt);
    }
    
    last_write_tm = GetTickCount();
}

string AudioSaver::new_file(){
    time_t t = time(NULL);
    
    int time_step = t/REC_STEP_SEC;
    
    if(time_step != last_time_step){
        flush();
        last_time_step = time_step;
    }
    
    if(!file && conf_server.getAvailableSpace(REC_PATH) >= (conf_server.min_disk_free*1024*1024)){
        cout << "Free space: " << dec << conf_server.getAvailableSpace(REC_PATH) << endl;
        
        struct tm tm;
        localtime_r(&t, &tm);
        char str[100];
        
        int format = conf_server.getRecParam("format", 1);
        string ext = format==1 ? "opus" : "wav";
        uint32_t outs = conf_server.getCTOutsInput(channel);
        sprintf(str, "%s/rec-%04d%02d%02d-%02d%02d%02d-%02d-%08X.%s", 
                REC_PATH, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, 
                tm.tm_hour, tm.tm_min, tm.tm_sec, channel, outs, ext.c_str());
        file_name = string(str);
        
#ifdef SAVER_DEBUG
        cout << "New file " << file_name << endl;
#endif
        
        sprintf(descr, "Channel %d %02d-%02d-%04d %02d:%02d:%02d", channel, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
            
        if(format == 1){
            file = new OGGWriter(file_name, descr);//new OGGWriter(file_name);
        }else{
            file = new WavSaver(file_name.c_str(), AUDIO_FREQ);//new OGGWriter(file_name);
        }
        
        conf_server.addServerEvent(ConfigServer::SERVER_EVT_FILE, ConfigServer::EVT_NORMAL, channel, "Запись: %s", descr);
    }
    
    return file_name;
}

void AudioSaver::flush(){
    if(file){
        if(pcm_fifo[pcm_fifo_in].s > 0){
#ifdef SAVER_DEBUG
            printf("Save tail %d\n", pcm_fifo[pcm_fifo_in].s);
#endif
            save_pcm(pcm_fifo[pcm_fifo_in].buf, pcm_fifo[pcm_fifo_in].s);
        }

#ifdef SAVER_DEBUG
        cout << "Flush file" << endl;
#endif
        delete file;
        file = nullptr;
        
        conf_server.removeServerEvent(ConfigServer::SERVER_EVT_FILE, ConfigServer::EVT_NORMAL, channel);
        
        clearRecStorage();
    }
}

void AudioSaver::clearRecStorage(){
    int days = conf_server.getStoreDays();
    ostringstream stm;
    stm << "find " << REC_PATH << "/* -type f -mtime +" << days << " -delete";//
#ifdef SAVER_DEBUG
    cout << stm.str() << endl;
#endif
    system(stm.str().c_str());
    
    
//    namespace bfs = std::experimental::filesystem;
//
//    time_t tm = time(NULL) - 1*24*60*60;
//    cout << "Time: " << asctime(gmtime(&tm)) << endl;
//    
//    bfs::directory_iterator itt(bfs::path(REC_PATH));
//    for(; itt != bfs::directory_iterator(); itt++){
//        bfs::path path = itt->path();
//        if(bfs::is_regular_file(path)){
//            struct stat stt;
//            stat(path.string().c_str(), &stt);
//            
//            cout << "File " << path.string() << " ctime=" << asctime(gmtime(&stt.st_ctim.tv_sec)) << endl;
//            
//            if(stt.st_ctim.tv_sec < tm){
//                
//            }
//        }
//    }
}

void saver_func(AudioSaver* __this){
    while(__this->active){

        if((GetTickCount() - __this->last_write_tm) >= (uint64_t)conf_server.timeout_rec && __this->pcm_fifo_cnt == 0){
            __this->flush();
        }

        if(__this->pcm_fifo_cnt > 0){

            __this->save_pcm(__this->pcm_fifo[__this->pcm_fifo_out].buf, PCM_FIFO_PACKS*AUDIO_SAMPLES);

//            lock_guard<mutex> lock(__this->fifo_mutex);

            __this->pcm_fifo_out++;
            if(__this->pcm_fifo_out >= PCM_FIFO_SIZE)
                __this->pcm_fifo_out = 0;
            __this->pcm_fifo_cnt--;
        }else{
            unique_lock<mutex> lock(__this->sync_mutex);
            __this->sync.wait_for(lock, chrono::milliseconds(1000));
        }
    }
}
