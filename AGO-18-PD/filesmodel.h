#ifndef FILESMODEL_H
#define FILESMODEL_H

#include "fileinfo.h"
#include "scheduller.h"

#include <QAbstractTableModel>
#include <QItemDelegate>

//typedef struct FILE_INFO {
//    QString name;
//    QString path;
//    quint64 targets;
//    PSchedullerItem sched;
//} file_info_t;

class FilesModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum Cols{
        colName,
        colTargets,
        colVolume,
        colScheduller,
        colCount
    };
    enum Role {
        PathRole = Qt::UserRole + 0
    };

    bool readonly = true;

    explicit FilesModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    int findByPath(QString path);
    void update();
private:
    QString filesDir;
};

class TargetsDelegate : public QItemDelegate{
public:
    explicit TargetsDelegate(int outs_cnt, QObject *parent = nullptr);

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;

    void paint(QPainter *painter,
               const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

//    QWidget *createEditor(QWidget *parent,
//                          const QStyleOptionViewItem &option,
//                          const QModelIndex &index) const override;

//    void setEditorData(QWidget *editor, const QModelIndex &index) const override;

//    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

private:
    int outs_cnt = 0;
};

#endif // FILESMODEL_H
