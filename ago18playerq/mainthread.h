#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include <QElapsedTimer>
#include <QObject>
#include <QTimer>

class MainThread : public QObject
{
    Q_OBJECT
public:
    explicit MainThread(QObject *parent = nullptr);

    void start();
private:
    QTimer* timer;
    QElapsedTimer tm;

signals:

};

#endif // MAINTHREAD_H
