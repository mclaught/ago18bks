/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PacketParser.cpp
 * Author: user
 * 
 * Created on 26 ноября 2019 г., 14:52
 */

#include <iostream>
#include <string.h>
#include <experimental/filesystem>
#include <unistd.h>
#include <arpa/inet.h>

#include "PacketParser.h"
#include "MyUDPServer.h"
#include "OGGWriter.h"
#include "ConfigServer.h"
#include "BufferedSoundSender.h"
#include "SoundSenderFile.h"
#include "cus/log.h"

using namespace std;

PacketParser* parsers[PACK_TYPES_CNT] = {0,0,0,0,0,0};

PacketParser::PacketParser() {
}

PacketParser::PacketParser(const PacketParser&) {
}

PacketParser::~PacketParser() {
}

PacketParser* PacketParser::get_instance(uint8_t type){
    if(parsers[type])
        return parsers[type];
    
    switch(type){
        case PACK_AUDIO:
            parsers[type] = new PacketParserAudio();
            break;
            
        case PACK_ALARM:
            parsers[type] = new PacketParserAlarm();
            break;
            
        case PACK_CFG:    
            parsers[type] = new PacketParserCfg();
            break;
            
        case PACK_BRU_CFG:
            parsers[type] = new PacketParserBRU();
            break;
            
        case PACK_GEN_CALL:
            parsers[type] = new PacketParserGenCall();
            break;
            
        default:
            parsers[type] = nullptr;
    }
    
    return parsers[type];
}

PacketParser* PacketParser::get_instance(pack_hdr_t* pack){
    if(!pack)
        return nullptr;
    
    if(pack->type >= static_cast<uint8_t>(PACK_TYPES_CNT))
        return nullptr;
    
    return get_instance(pack->type);
}

void PacketParser::clear_parsers(){
    for(int i=0; i<static_cast<int>(PACK_TYPES_CNT); i++){
        if(parsers[i])
            delete parsers[i];
    }
    memset(parsers, 0, sizeof(PacketParser*)*PACK_TYPES_CNT);
}

//==============================================================================
PacketParserAudio::PacketParserAudio() : PacketParser(){
    memset(savers, 0, sizeof(AudioSaver*)*MAX_CHANNELS);
}

PacketParserAudio::PacketParserAudio(const PacketParserAudio& orig) : PacketParser(){
    
}

PacketParserAudio::~PacketParserAudio(){
    for(int i=0; i<MAX_CHANNELS; i++){
        if(savers[i])
            delete savers[i];
    }
}

void PacketParserAudio::parse(pack_hdr_t* pack, sockaddr_in from_addr){
    packet_audio_t* packet_audio = reinterpret_cast<packet_audio_t*>(pack);

//    printf("%d - %d\n", packet_audio->hdr.repeate_cnt, pack_n);
    auto tm =  chrono::high_resolution_clock::now();
    int period = chrono::duration_cast<chrono::microseconds>(tm - last_recv).count();
    last_recv = tm;
//    if(period > 160000)
//        printf("%d\n", period);

    if(packet_audio->hdr.repeate_cnt != pack_n){
        int lost = (int)packet_audio->hdr.repeate_cnt - (int)pack_n;
        if(lost < 0)
            lost += 256;
//        printf("Lost packets %d\n", lost);
    }
    pack_n = packet_audio->hdr.repeate_cnt+1;
    
//    cout << "Parse audio " << pack->len << " cmd=" << packet_audio->cmd << endl;
    if(packet_audio->cmd != SND_STREAM)
        return;

    conf_server.setChannelActivity(packet_audio->audio_hdr.sender, packet_audio->audio_hdr.recip);
//    if(packet_audio->audio_hdr.recip){
//        conf_server.setCTOutsInput(packet_audio->audio_hdr.sender, packet_audio->audio_hdr.recip);
//    }

    if(conf_server.inRecEnabled(packet_audio->audio_hdr.sender)){//conf_server.rec_enable
        save_audio(packet_audio->pcm, packet_audio->audio_hdr.sender);
    }
}

void PacketParserAudio::split_channels(int16_t* buf, int16_t* ch1, int16_t* ch2){
    for(int i=0; i<AUDIO_SAMPLES; i++){
        ch1[i] = buf[i<<1];
        ch2[i] = buf[(i<<1) | 1];
    }
}

void PacketParserAudio::save_audio(int16_t* pcm, int ch_n){
    AudioSaver* saver = get_saver(ch_n);
    if(saver){
        saver->add_frame(pcm);
    }
}

AudioSaver* PacketParserAudio::get_saver(int ch_n){
    if(ch_n >= MAX_CHANNELS || ch_n < 0)
        return nullptr;
    if(savers[ch_n] == nullptr){
        savers[ch_n] = new AudioSaver(ch_n);
    }
    return savers[ch_n];
}

//==============================================================================
PacketParserAlarm::PacketParserAlarm() : PacketParser(){
}

PacketParserAlarm::PacketParserAlarm(const PacketParserAlarm& orig) : PacketParser(){
}

PacketParserAlarm::~PacketParserAlarm(){
}
    
void PacketParserAlarm::parse(pack_hdr_t* pack, sockaddr_in from_addr){
    packet_command_t* packet = reinterpret_cast<packet_command_t*>(pack);
    
    int cnt = *reinterpret_cast<int8_t*>(packet->data);
    alarm_item_t* alarms = reinterpret_cast<alarm_item_t*>(packet->data+1);
    conf_server.setChannelAlarms(cnt, alarms, pack->adr_from, pack->adr_from > conf_server.bru_offset ? BRU : BKS_AUDIO);
    
//    if(packet->hdr.len < (MAX_INP_CHANNELS+1))
//        return;
//    
//    for(int i=0; i<MAX_INP_CHANNELS; i++){
//        int channel = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);
//        if(channel>=0 && channel<MAX_CHANNELS){
//            conf_server.setChannelAlarm(channel, packet->data[i]);
//        }
//    }
}

//==============================================================================
PacketParserCfg::PacketParserCfg() : PacketParser(){
}

PacketParserCfg::PacketParserCfg(const PacketParserCfg& orig) : PacketParser(){
}

PacketParserCfg::~PacketParserCfg(){
}
    
void PacketParserCfg::parse(pack_hdr_t* pack, sockaddr_in from_addr){
    try{
        packet_command_t* packet = reinterpret_cast<packet_command_t*>(pack);
        start_stop_t* start_stop = reinterpret_cast<start_stop_t*>(packet->data);
        switch(packet->cmd){
            case SET_RECIPNT_LIST:{
                if(start_stop->recipients){
                    conf_server.setCTOutsInput(start_stop->src_ch, start_stop->recipients);
                }
                break;
            }

            case CLEAR_RECIPNT_LIST:
                if(start_stop->recipients){
                    //conf_server.setCTOutsInput(start_stop->src_ch, 0);
                    conf_server.ctInputFromJSON(start_stop->src_ch);
                    conf_server.setChannelActivity(start_stop->src_ch, 0);
                }
                break;

            case SND_IN_CPB_REQ:
                if(packet->hdr.adr_from != conf_server.server_addr){
                    setSndCapabIn(packet);
                    server.start_requests.insert(static_cast<eth_cmd_t>(packet->cmd));
                }
                break;

            case SND_OUT_CPB_REQ:
                if(packet->hdr.adr_from != conf_server.server_addr){
                    setSndCapabOut(packet);
                    server.start_requests.insert(static_cast<eth_cmd_t>(packet->cmd));
                }
                break;

            case SND_IN_CURR_REQ:
                setSndCurrIn(packet);
                break;

            case SND_OUT_CURR_REQ:
                setSndCurrOut(packet);
                break;

            case SND_CHANK_REQ:
                requestSoundPacket(packet);
                break;

            case REC_NUMBER:
                if(packet->hdr.adr_from != conf_server.server_addr){
                    requestClip(packet);
                }
                break;

            case GET_ID:{
                if(packet->hdr.adr_from != conf_server.server_addr){
                    dev_id_t dev_id;
                    dev_id.id = packet->hdr.adr_from;
                    dev_id.uid = "";
                    if(packet->hdr.len >= 13){
                        for(int i=0; i<12; i++){
                            char dig[10];
                            sprintf(dig, "%02X", packet->data[i]);
                            dev_id.uid += dig;
                        }
                    }
                    if(packet->hdr.len > 13){
                        dev_id.descr = string(reinterpret_cast<char*>(&packet->data[12]), packet->hdr.len-13);
                    }
                    found_ids[dev_id.uid] = dev_id;
                }
                break;
            }

            case UM_MANUAL_ID_SET:
                if(packet->hdr.adr_from != conf_server.server_addr){
                    brd_id_t brd_id = *reinterpret_cast<brd_id_t*>(packet->data);

                    lock_guard<mutex> lock(set_id_mutex);
                    if(set_id_promise){
                        set_id_promise->set_value(brd_id);
                        set_id_promise = nullptr;
                    }
                }
                break;

        case BKS_GET_INP_MODE:
            if(packet->hdr.adr_from != conf_server.server_addr){
                inp_mode_bits |= *(uint32_t*)packet->data;
//                inp_mode_cnt++;
                inp_mode_set.insert(packet->hdr.adr_from);
            }
            break;
            
        case BRP_GET_CRITICAL_ALARMS: {
            uint32_t bits = conf_server.getAlarmsBits();
            if(*reinterpret_cast<uint32_t*>(packet->data) != bits){
                set_critical_alarms(bits);
            }
            break;
        }
        
        case BRP_SET_CRITICAL_ALARMS:
            if(packet->hdr.adr_from != conf_server.server_addr){
                lock_guard<mutex> lock(set_critical_alarms_mutex);
                if(set_critical_alarms_promise){
                    set_critical_alarms_promise->set_value();
                    set_critical_alarms_promise = nullptr;
                }
                server.start_requests.insert(static_cast<eth_cmd_t>(packet->cmd));
            }
            break;
        }
    }catch(std::exception &e){
        log_error("PacketParserCfg::parse(): %s", e.what());
    }
}

void PacketParserCfg::setSndCapabIn(packet_command_t* packet){
    if(packet->hdr.adr_from == conf_server.server_addr)
        return;
    
    for(int i=0; i<MAX_INP_CHANNELS; i++){
        uint8_t *p = packet->data;
        int ch = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);
        conf_server.setSndInCapabs(ch, p);
        conf_server.setSndInParams(ch, p, i);
    }
}

void PacketParserCfg::setSndCurrIn(packet_command_t* packet){
    if(packet->hdr.adr_from == conf_server.server_addr)
        return;

    uint8_t *p = packet->data;
    
    for(int i=0; i<MAX_INP_CHANNELS; i++){
        int ch = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);
        conf_server.setSndInParams(ch, p, i);
    }
}

void PacketParserCfg::setSndCapabOut(packet_command_t* packet){
//    if(packet->hdr.adr_from == conf_server.server_addr)
//        return;
    if(packet->hdr.len <= 1)
        return;

    for(int i=0; i<MAX_INP_CHANNELS; i++){
        uint8_t *p = packet->data;
        int ch = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);

//        cout << sizeof(snd_out_capab_t) << " " << sizeof(snd_out_cfg_t) << endl;

        conf_server.setSndOutCapabs(ch, p);
//        p += 3;
        conf_server.setSndOutParams(ch, p, i);
    }
}

void PacketParserCfg::setSndCurrOut(packet_command_t* packet){
    if(packet->hdr.adr_from == conf_server.server_addr)
        return;

    uint8_t *p = packet->data;
    
    for(int i=0; i<MAX_INP_CHANNELS; i++){
        int ch = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);
        conf_server.setSndOutParams(ch, p, i);
    }
}

void PacketParserCfg::requestSoundPacket(packet_command_t* packet){
    for(int i=0; i<MAX_INP_CHANNELS; i++){
        int brd_ch = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);
        int key = SOUND_SENDER_KEY(packet->hdr.adr_from, brd_ch);

        BaseSoundSender* sndr = BaseSoundSender::findSender(key);
        if(sndr)
            sndr->sendSoundPacket(packet->data[i]);
//        sound_sender_pool_t::iterator fnd = sound_sender_pool.find(key);
//        if(fnd != sound_sender_pool.end()){
//            fnd->second->sendSoundPacket(packet->data[i]);
//        }
    }
//    for(BaseSoundSender* snd : sound_sender_pool){
//        if(snd->master_brd_addr == packet->hdr.adr_from){
//            for(int i=0; i<MAX_INP_CHANNELS; i++){
//                int brd_ch = BRD_ID_TO_CHAN(packet->hdr.adr_from, i);
//                if(snd->master_brd_chs[i] == brd_ch){
//                    snd->sendSoundPacket(packet->data[i]);
//                    break;
//                }
//            }
//        }
//    }
}

//#include <dirent.h>
void PacketParserCfg::requestClip(packet_command_t* packet){
    namespace bfs = std::experimental::filesystem;

    int clipN = packet->data[0];
    auto cur_tm = chrono::system_clock::now();
    auto last_tm = recnumTime[clipN];
    if(chrono::duration_cast<chrono::milliseconds>(cur_tm - last_tm).count() > 1000){
        recnumTime[clipN] = cur_tm;
        conf_server.start_clip_for_signal(clipN);
    }

    server.sendCommand(reinterpret_cast<uint8_t*>(&clipN), 1, PACK_CFG, REC_NUMBER, 1, packet->hdr.adr_from); //

//    opendir();
//    auto ent = readdir();
//    ent->
    
    
//    string clip_path = "";
    
//    int i=0;
//    bfs::directory_iterator itt(bfs::path("/var/www/html/AGO18Web/clips"));
//    for(; itt != bfs::directory_iterator(); itt++){
//        bfs::path path = itt->path();
//        if(bfs::is_regular_file(path)){
//            if(i == clipN){
//                clip_path = path.string();
//                break;
//            }
//            i++;
//        }
//    }
    
//    if(clip_path != ""){
//        PSoundSenderFile sender = SoundSenderFile::getFreeSender();
//        sender->start(clip_path, BRD_ID_TO_CHAN(conf_server.server_addr, 0));
//    }
}

map<string, dev_id_t> PacketParserCfg::get_id(uint8_t addr)
{
    found_ids.clear();
    server.sendCommand(nullptr, 0, PACK_CFG, GET_ID, 3, addr);
    usleep(1000000);
    return found_ids;
}

future<void> PacketParserCfg::change_id(uint8_t old_id, uint8_t new_id, uint8_t offset, uint8_t um_mode){
    change_id_promise = make_shared<promise<void>>();
    id_cfg_t id_cfg;
    
    id_cfg.new_id = new_id;
    id_cfg.offset = offset;
    id_cfg.um_mode = um_mode;
    
    server.sendCommand(reinterpret_cast<uint8_t*>(&id_cfg), sizeof(id_cfg_t), PACK_CFG, CHANGE_ID, 1, old_id);
    
    return change_id_promise->get_future();
}

future_status PacketParserCfg::set_id(uint8_t id, uint8_t redundant, brd_id_t *new_brd_id)
{
    future<brd_id_t> set_id_future;
    {
        lock_guard<mutex> lock(set_id_mutex);
        set_id_promise = make_shared<promise<brd_id_t>>();
        set_id_future = set_id_promise->get_future();
    }

     brd_id_t brd_id = {id, redundant};

     for(int i=0; i<40; i++){
         server.sendCommand(reinterpret_cast<uint8_t*>(&brd_id), sizeof(brd_id_t), PACK_CFG, UM_MANUAL_ID_SET);
         auto status = set_id_future.wait_for(chrono::milliseconds(500));
         if(status == future_status::ready){
             *new_brd_id = set_id_future.get();
             return status;
         }else if(status == future_status::deferred){
             return status;
         }
     }
     return future_status::timeout;

//    set_id_async = async(std::launch::async, [this, id, redundant, new_brd_id]() -> future_status{
//    });

//    auto status = set_id_async.wait_for(chrono::milliseconds(20000));
//    if(status==future_status::timeout)
//        return status;
//    else
     //        return set_id_async.get();
}

set<uint8_t> PacketParserCfg::get_inp_mode(uint32_t* bits)
{
    inp_mode_bits = 0;
//    inp_mode_cnt = 0;
    inp_mode_set.clear();

    server.sendCommand(nullptr, 0, PACK_CFG, BKS_GET_INP_MODE, 3);

    usleep(200000);

    *bits = inp_mode_bits;
    return inp_mode_set;
}

void PacketParserCfg::set_inp_mode(uint32_t bits)
{
    server.sendCommand(reinterpret_cast<uint8_t*>(&bits), sizeof(uint32_t), PACK_CFG, BKS_SET_INP_MODE, 3);
}

void PacketParserCfg::set_critical_alarms(uint32_t bits){
    
    for(int i=0; i<30; i++){
        cout << "set_critical_alarms " << i << endl;
        
        future<void> ftr;
        {
            lock_guard<mutex> lock(set_critical_alarms_mutex);
            set_critical_alarms_promise = make_shared<promise<void>>();
            ftr = set_critical_alarms_promise->get_future();
        }
        
        server.sendCommand(reinterpret_cast<uint8_t*>(&bits), sizeof(uint32_t), PACK_CFG, BRP_SET_CRITICAL_ALARMS);
        
        auto res = ftr.wait_for(chrono::milliseconds(500));
        if(res == future_status::ready){
            cout << "set_critical_alarms OK" << endl;
            return;
        }
    }
    
    cout << "set_critical_alarms FAIL" << endl;
}

//==============================================================================
PacketParserBRU::PacketParserBRU() : PacketParser(){
}

PacketParserBRU::PacketParserBRU(const PacketParserBRU& orig) : PacketParser(){
}

PacketParserBRU::~PacketParserBRU(){
}

void PacketParserBRU::parse(pack_hdr_t* pack, sockaddr_in from_addr){
    packet_command_t* packet = reinterpret_cast<packet_command_t*>(pack);
    
    if(packet->hdr.adr_from == conf_server.server_addr)
        return;
    
    switch(packet->cmd){
        case BRU_GET_CFG:
            conf_server.setBRUCfg(pack->adr_from, reinterpret_cast<bru_cfg_t*>(packet->data), 2048);
            server.start_requests.insert(static_cast<eth_cmd_t>(packet->cmd));
            break;
            
        case BRU_TEST_MODE_START:{
            uint32_t measure_timeout = *reinterpret_cast<uint32_t*>(packet->data);
//            uint8_t bru_id = packet->hdr.adr_from;

            lock_guard<mutex> lock(start_test_mode_mutex);
            if(start_test_mode_promise){
                start_test_mode_promise->set_value(measure_timeout);
                start_test_mode_promise = nullptr;
            }
            break;
        }
        
        case BRU_LINE_MEAS_RESULT:{
            bru_measure_t* measure = reinterpret_cast<bru_measure_t*>(packet->data);

            lock_guard<mutex> lock(line_measure_mutex);
            if(line_measure_promise){
                line_measure_promise->set_value(*measure);
                line_measure_promise = nullptr;
            }
            add_measure(*measure);
            break;
        }
        
        case BRU_RESET_SWITCH_CFG:{
            uint32_t timeout = *reinterpret_cast<uint32_t*>(packet->data);

            lock_guard<mutex> lock(switch_cfg_mutex);
            if(switch_cfg_promise){
                switch_cfg_promise->set_value(timeout);
                switch_cfg_promise = nullptr;
            }
            break;
        }
    }
}

future<uint32_t> PacketParserBRU::start_test_mode(uint8_t bru_id){
    lock_guard<mutex> lock(start_test_mode_mutex);

    start_test_mode_promise = make_shared<promise<uint32_t>>();
    server.sendCommand(nullptr, 0, PACK_BRU_CFG, BRU_TEST_MODE_START, 1, bru_id);
    return start_test_mode_promise->get_future();
}

future<bru_measure_t> PacketParserBRU::line_measure(uint8_t bru_id, uint8_t line_id){
    lock_guard<mutex> lock(line_measure_mutex);

    line_measure_promise = make_shared<promise<bru_measure_t>>();
    server.sendCommand(&line_id, 1, PACK_BRU_CFG, BRU_LINE_MEASURE, 1, bru_id);
    return line_measure_promise->get_future();
}

future<uint32_t> PacketParserBRU::switch_cfg(uint8_t bru_id){
    lock_guard<mutex> lock(switch_cfg_mutex);

    switch_cfg_promise = make_shared<promise<uint32_t>>();
    server.sendCommand(nullptr, 0, PACK_BRU_CFG, BRU_RESET_SWITCH_CFG, 3, bru_id);
    return switch_cfg_promise->get_future();
}

map<uint8_t, bru_measure_t> PacketParserBRU::get_measures()
{
    shared_lock<shared_mutex> lock(measures_mutex);
    return measures;
}

void PacketParserBRU::add_measure(bru_measure_t measure)
{
    unique_lock<shared_mutex> lock(measures_mutex);
    measures[measure.line] = measure;
}

//==============================================================================
PacketParserGenCall::PacketParserGenCall() : PacketParser(){
}

PacketParserGenCall::PacketParserGenCall(const PacketParserGenCall& orig) : PacketParser(){
}

PacketParserGenCall::~PacketParserGenCall(){
}
    
void PacketParserGenCall::parse(pack_hdr_t* pack, sockaddr_in from_addr){
    if(pack->adr_from != conf_server.server_addr){
        packet_ping_t* pack_ping = (packet_ping_t*)pack;
        conf_server.setBoardPing(pack_ping->hdr.adr_from, pack_ping->dev_type, string(inet_ntoa(from_addr.sin_addr)));
    }
}
