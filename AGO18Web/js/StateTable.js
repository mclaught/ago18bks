class StateTable extends MyView{
//    tbl_array = [];
    
    constructor(container){
        super(container);
        this.tbl_array = [];
    }
    
    init(){
//        this.update();
        var _this = this;
//        setInterval(function(){
//            get_settings(function(setts){
//                _this.update();
//            });
//        }, 1000);
        super.init();
    }
    
    build(){
        
        var table = $(`
            <table class="table table-hover">
                <thead class="table-dark">
                    <tr id="commcols" style='height: 100px;'>
                    </tr>
                </thead>
                <tbody id="commtbl">
                </tbody>
            </table>
        `);
        
        var commcols = table.find("#commcols");
        var commtbl = table.find("#commtbl");
            
        for(var i=0; i<insCnt; i++){
            var ch = i<this.tbl_array.length ? this.tbl_array[i] : {in: i, out: 0, prio: 0, online: 0, alarm: 0};

            var inp_n = ch.in;
            var outs = ch.out;
            var prio = ch.prio;
            var online = ch.online;
            var alarm = ch.alarm;
            var in_alarm = ch.in_alarm;

            var newRow = $('<tr>', {});
            newRow.append(
                settings.ins==null || settings.ins[inp_n]==null || settings.ins[inp_n].name=="" ?
                    $('<th>', {
                        id: "in-hdr-"+inp_n,
                        style: 'max-width:40px; min-width:40px; text-align: center; vertical-align: middle; font-size:12pt;',
                        scope: 'row',
                        'class': in_alarm ? 'table-dark bg-danger' : 'table-dark',
                        'text': inp_n+1,
                        'data-toggle': "tooltip", 
                        'data-placement': "right", 
                        title: ""
                    }) :
                    $('<th>', {
                        id: "in-hdr-"+inp_n,
                        style: 'max-width:40px; min-width:40px; text-align: center; vertical-align: middle; font-size:8pt;',
                        scope: 'row',
                        'class': in_alarm ? 'table-dark bg-danger' : 'table-dark',
                        'text': settings.ins[inp_n].name,
                        'data-toggle': "tooltip", 
                        'data-placement': "right", 
                        title: settings.ins[inp_n].name
                    })
            );

            for(var j=0; j<outsCnt; j++){
                var active = (outs & (1<<j)) > 0;
                var onln = (online & (1<<j)) > 0;
                var color = active ? "#DDDD00" : "#FFFFFF";//warning
                var bg = active ? "bg-warning" : "bg-light";
                var text = active ? "+" : "-";
                if(onln){//active && online
                    color = "#00DD00";
                    bg = "bg-success";
                    text = "*";
                }
                if((alarm & (1<<j)) > 0){
                    color = "#FF0000";
                    bg = "bg-danger";
                    text = "X";
                }

                var newtd = $('<td>', {});
                newRow.append(newtd);

//                            var led = $('<div>', {
//                                'class': 'alert alert-secondary',
//                                'role': 'alert',
//                                'style': `background-color:${color}; font-size:8pt;`,
//                                text: text
//                            });
                var style = "font-size:8pt; width: 20pt; height: 20pt;";// border: 4px inset gray; background-color:"+color+"; 
                var led = $('<div>', {
                    'style': style,
                    id: "led-"+inp_n+"-"+j,
                    class: 'border border-dark rounded-circle shadow '+bg,
                    'data-toggle': "tooltip", 
                    'data-placement': "bottom", 
                    title: (settings.ins!=null && settings.ins[inp_n]!=null && settings.outs!=null && settings.outs[j]!=null) ? 
                        settings.ins[inp_n].name + " - " + settings.outs[j].name : ""
                });
                newtd.append(led);
            }

            commtbl.append(newRow);
        }

        commcols.append($('<th>', {
            style: 'text-align: center; vertical-align: middle; font-size:12pt;',
            scope: 'col',
            text: 'Вх\\Вых'
        }));
        for(var i=0; i<outsCnt; i++){
            commcols.append(
                settings.outs==null || settings.outs[i]==null || settings.outs[i].name=="" ?    
                    $('<th>', {
                        style: 'max-width:40px; min-width:40px; text-align: center; vertical-align: middle; font-size:12pt;',
                        class: 'cols',
                        id: "out-hdr-"+i,
                        'scope': 'col',
                        text: i+1,
                        'data-toggle': "tooltip", 
                        'data-placement': "bottom", 
                        title: ""
                    }) :
//                    .append(
//                        $('<div>').append($('<p>').html(i+1))
//                    ) :
                    $('<th>', {
                        class: 'cols-with-name',
                        id: "out-hdr-"+i,
                        'scope': 'col',
//                        'text': settings.outs[i].name,
                        'data-toggle': "tooltip", 
//                        'data-placement': "bottom", 
                        title: settings.outs[i].name
                    }).append(
                        $('<div>').append(
                            $('<p>').html(settings.outs[i].name)
                        )
                    )
            );
        }
        return table;
    }
    
    set_json(json){
        this.tbl_array = json;
        this.setState();
    }
    
    set_in_state(json){
        this.tbl_array[json.in].out = json.outs;
        this.tbl_array[json.in].online = json.active;
        this.setState();
    }
    
    async update(){
        
        var _this = this;
        
        var rest = new Rest();
        var objAnswer = await rest.get_comm_table();
        concole.log("StateTable.update:");
        console.info("SWTable", objAnswer.comm_table.sw_table);
        _this.set_json(objAnswer.comm_table.sw_table);
    }
}