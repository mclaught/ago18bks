/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PDServer.h
 * Author: user
 *
 * Created on 5 декабря 2019 г., 15:42
 */

#ifndef PDSERVER_H
#define PDSERVER_H

#include <thread>
#include <set>
#include <map>
#include "proto.h"
#include "SocketSoundSender.h"

using namespace std;

typedef enum{
    PD_START,
    PD_STREAM
}pd_command_t;

class PDServer {
public:
    PDServer();
    PDServer(const PDServer& orig);
    virtual ~PDServer();
    
    void start();
    void stop();
private:
    typedef map<int, PSocketSoundSender> clients_map_t;
    
    thread* thrd;
    bool active = true;
    int sock;
    string iface;
    uint16_t port;
    clients_map_t clients;
    
    void run();
    
    friend void pd_thread_func(PDServer *__this);
};

extern PDServer pd_server;

#endif /* PDSERVER_H */

