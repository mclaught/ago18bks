/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/class.cc to edit this template
 */

/* 
 * File:   TimeSync.cpp
 * Author: user
 * 
 * Created on 3 ноября 2022 г., 11:30
 */

#include "TimeSync.h"
#include "ConfigServer.h"

#include <stdlib.h>
#include <array>
#include <memory>
#include <iostream>
#include <fstream>
#include <string.h>
#include <unistd.h>
#include <iomanip>
#include <strstream>

TimeSync timeSync;

TimeSync::TimeSync() {
    rtc_set_tm = chrono::steady_clock::now() - RTC_SET_PERIOD;
}

TimeSync::TimeSync(const TimeSync& orig) {
}

TimeSync::~TimeSync() {
}

bool TimeSync::isRTCOk(){
    ifstream f;
    f.open(RTC_OK_FILE);
    bool res = false;
    if(f.good()){
        res = true;
        f.close();
    }
    return res;
}

bool TimeSync::isTimeSync(){
    ifstream f;
    f.open(TIME_OK_FILE);
    bool res = false;
    if(f.good()){
        res = true;
        f.close();
    }
    return res;
}

bool TimeSync::isTimeOk(){
    auto cur = chrono::system_clock::now().time_since_epoch();
//    cout << "Cur time: " << chrono::duration_cast<chrono::seconds>(cur).count() << endl;
    
    bool res = cur > 1722286800s;    
    return res;
}

bool TimeSync::syncRTC(){
    if(isRTCOk()){
        return true;
    }
    
    char cmd[256];
    sprintf(cmd, "hwclock -s -f %s && touch %s", conf_server.rtc.c_str(), RTC_OK_FILE);
    cout << cmd << endl;
    
    system(cmd);//(string("hwclock -s -f ")+conf_server.rtc.c_str()+" && touch "+RTC_OK_FILE).c_str()
    
    bool res = isRTCOk();
    if(res){
        cout << "RTC sync - OK" << endl;
    }else{
        cerr << "RTC sync - FAIL!" << endl;
    }
    return res;
}

bool TimeSync::syncNTP(){
    array<char, 1024> buffer;
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen("timedatectl", "r"), pclose);
    if (!pipe) {
        return false;
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    bool res = result.find("System clock synchronized: yes")!=std::string::npos;
#ifdef __DBG
    if(res){
        cout << "NTP sync - YES" << endl;
    }else{
        cout << "NTP sync - NO" << endl;
    }
#endif
    return res;
}

void TimeSync::setRTC(){
    cout << "Set RTC" << endl;
    
    char cmd[256];
    sprintf(cmd, "hwclock -w -f %s", conf_server.rtc.c_str());
    cout << cmd << endl;
    
    system(cmd);//(string("hwclock -w -f ")+conf_server.rtc).c_str()
}

void TimeSync::setRTCTime(string tm){
    char cmd[256];
    sprintf(cmd, "hwclock --set --date '%s' -f %s && hwclock -s -f %s && touch %s", 
            tm.c_str(), conf_server.rtc.c_str(), conf_server.rtc.c_str(), RTC_OK_FILE);
    cout << cmd << endl;
    system(cmd);
}

void TimeSync::setTimeSync(bool ok){
    if(ok)
        system((string("touch ")+TIME_OK_FILE).c_str());
    else
        unlink(TIME_OK_FILE);
}

TimeSync::time_sync_t TimeSync::sync_ok(){
    auto tm = chrono::steady_clock::now();
    
    if(tm >= next_tm){
//        cout << "SYNC OK?" << endl;
        if(syncNTP()){
            _sync_ok = SYNC_NTP;
//            cout << "Sync NTP - OK" << endl;
            if((tm - rtc_set_tm) >= RTC_SET_PERIOD){
                setRTC();
                rtc_set_tm = tm;
            }
        }else if(syncRTC()){
            _sync_ok = SYNC_RTC;
        }
        if(!isTimeOk()){
            _sync_ok = SYNC_NONE;
        }
        if(_sync_ok>SYNC_NONE && !isTimeSync()){
            setTimeSync(true);
        }
        if(_sync_ok==SYNC_NONE && isTimeSync()){
            setTimeSync(false);
        }
        next_tm = tm + (_sync_ok==SYNC_NTP ? 1min : 1s);
    }
    
    return _sync_ok;
}

TimeSync::time_sync_t TimeSync::get_sync_ok(){
    return _sync_ok;
}
