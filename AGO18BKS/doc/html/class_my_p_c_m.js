var class_my_p_c_m =
[
    [ "MyPCM", "class_my_p_c_m.html#afc3709f1745b9312e7565d5e7066950d", null ],
    [ "~MyPCM", "class_my_p_c_m.html#a4d36a6f9cb19ab3b29e4a39ece123eb5", null ],
    [ "eof", "class_my_p_c_m.html#a11c9b2055d003d6ba3837fdaad08bd72", null ],
    [ "needResample", "class_my_p_c_m.html#ae78f574ee171b9e5bf92e74d930f8695", null ],
    [ "readFrame", "class_my_p_c_m.html#a508716b4caa0c64d0bc683845ec5bb01", null ],
    [ "setError", "class_my_p_c_m.html#afa47e59b038e6b77d6e3b39753c23cee", null ],
    [ "audioFormat", "class_my_p_c_m.html#a75dccb39d8a9616075fee188b738fa82", null ],
    [ "bitsPerSample", "class_my_p_c_m.html#a040b7232461b24d4a2d93565debaff04", null ],
    [ "blockAlign", "class_my_p_c_m.html#aed54ce8302b9e86219f407bfa9b5cc4f", null ],
    [ "byte_rate", "class_my_p_c_m.html#aea1e4c8a7beae9ea3bbe39109f3eeca0", null ],
    [ "channels", "class_my_p_c_m.html#a8e3e0d29f5adcf2e157ef4f536c3fe13", null ],
    [ "error", "class_my_p_c_m.html#a2d07ecd5b5da04de0006b0b4249f5dbe", null ],
    [ "error_str", "class_my_p_c_m.html#a6287c4ec19927c6deafce0770c4d8552", null ],
    [ "file_pos", "class_my_p_c_m.html#a45b9b7711eccd1f70ef449a3fe95e87c", null ],
    [ "file_size", "class_my_p_c_m.html#a7057cf409f3efbab2c7aed612eb2b1e8", null ],
    [ "format", "class_my_p_c_m.html#ac2d0a8fa78577298305e2ffe29df8808", null ],
    [ "frame_size", "class_my_p_c_m.html#a0b4d8ad234a1841c1afeaea5cefb410d", null ],
    [ "isRIFF", "class_my_p_c_m.html#aadaa568a2dbd31d679c1209d3bad2302", null ],
    [ "sample_rate", "class_my_p_c_m.html#a4646f1ac67ae9cdfc2b7bfdc9664d831", null ]
];