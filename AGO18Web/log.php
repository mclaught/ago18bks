<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css?1">
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        include "navbar.php";
        
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ?>
        
        <div class="container">
<!--            <br>
            <h1><img src="img/info.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Лог сервера БКС</h1>
            <br>-->
            
            
            <div id="content-card" class="card shadow bg-light mt-3">
                <div class="card-header">
                    <h1><img src="img/info.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Лог сервера БКС</h1>
                </div>
                
                <div id="content" class="card-body m-3 overflow-auto" style="line-height:1em;font-size:14px;"></div>
                
                <div class="card-footer">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group dropup">
                            <button id="btn-filter" type="button" class="btn btn-outline-dark dropdown-toggle shadow" data-toggle="dropdown" aria-expanded="false">
                                <svg id="img-filter" style="width:16px;height:16px;" viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><path d="M60 96H36a5.997 5.997 0 0 1-6-6V55.722L.915 9.176A5.995 5.995 0 0 1 6 .001h83.998a5.995 5.995 0 0 1 5.086 9.175L66 55.723v34.276a5.997 5.997 0 0 1-6 6ZM42 84h12V54a5.96 5.96 0 0 1 .914-3.176l24.257-38.823H16.828l24.258 38.823A5.96 5.96 0 0 1 42 54Z" fill="#343a40" class="fill-000000"></path></svg>
                                Фильтр
                            </button>
                            <div class="dropdown-menu p-3 shadow">
                                <div class="form-group">
                                    <label for="since">Начало</label>
                                    <input id="since" type="datetime-local" class="form-control filter-control" aria-label="Начало" value="">
                                </div>           
                                <div class="form-group">
                                  <label for="until">Конец</label>
                                  <input id="until" type="datetime-local" class="form-control filter-control" aria-label="Конец" value="">
                                </div>           
                                <div class="form-group">
                                  <label for="lines-cnt">Количество записей</label>
                                  <input id="lines-cnt" type="number" class="form-control filter-control" aria-label="Количество строк" value="1000">
                                </div>    
                                <div class="btn-group" role="group">
                                    <button id="btn-update" class="btn btn-outline-primary" style="width:110px;">Обновить</button>
                                    <button id="btn-clear" class="btn btn-outline-danger" style="width:110px;">Сброс</button>
                                </div>
                            </div>
                        </div>
                        <button id="btn-load" class="btn btn-outline-success shadow">Сохранить в файл</button>
                        <div id="lines-count" class="alert alert-dark my-0 py-1 shadow"></div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/alert.js"></script>
        <script type="text/javascript" src="js/alertify.js"></script>
        <script>
            console.log("Start");
            
            function formatDate(str){
                if(str==="")
                    return "";
                let date = new Date(str);
                console.info("date", date);
                return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
            }
            
            function update(){
                let lines = $("#lines-cnt").val();
                let since = formatDate($("#since").val());
                let until = formatDate($("#until").val());
                console.info("lines", lines);
                console.info("since", since);
                $.get("read_log.php", {lines: lines, since: since, until: until}, function(res){
//                    let obj = JSON.parse(res);
//                    console.dir(obj);
                    $("#content").html(res);
//                    console.info("Count", $("#content").children().length);
                    $("#lines-count").html(`${$("#content").children().length} строк`);
                    $("#content").children().last().get(0).scrollIntoView();
                });
//                $.get("read_log.php", {action: "boots"}, function(answer){
//                    $("#boots").html("");
//                    let boots = answer.split("\r\n");
//                    console.info("boots", boots);
//                    for(let i=0; i<boots.length; i++){
//                        let boot = boots[i];
//                        if(boot === "")
//                            continue;
//                        
//                        let index = boot.indexOf(" ");
//                        let val = boot.substr(0, index);
//                        boot = boot.substr(index);
//                        
//                        index = boot.indexOf(" ");
//                        boot = boot.substr(index);
//                        
//                        $("#boots").append(`<option value="${val}">${boot}</option>`);
//                    }
//                });
            }
            
            function setSize(){
                console.info("size", $(window).height());
                $('#content-card').height($(window).height() - 100);
            }
            
            $("#btn-update").click(function(){
                update();
            });
            
            $(".filter-control").keypress(function(){
                update();
            });
            
            $('#btn-clear').click(function(){
                $("#since").val('');
                $("#until").val('');
                $("#lines-cnt").val('1000')
                update();
            });
            
            $( window ).resize(function(){
                setSize();
            });
            
            $("#btn-filter").hover(function(){
                $("#img-filter").find("path").attr("fill", "#f8f9fa");
            }, function(){
                $("#img-filter").find("path").attr("fill", "#343a40");
            });
            
            $("#btn-load").click(function(){
                let lines = $("#lines-cnt").val();
                let since = formatDate($("#since").val());
                let until = formatDate($("#until").val());
                let cmd = "read_log.php?action=load";
                if(lines!=="")
                    cmd += `&lines=${lines}`;
                if(since!=="")
                    cmd += `&since=${since}`;
                if(until!=="")
                    cmd += `&until=${until}`;
                location.href = cmd;
            });
            
            setSize();
            update();
        </script>
    </body>
</html>
