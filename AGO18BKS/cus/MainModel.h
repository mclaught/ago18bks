/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainModel.h
 * Author: user
 *
 * Created on 19 сентября 2018 г., 19:39
 */

#ifndef MAINMODEL_H
#define MAINMODEL_H

#include "MyObject.h"
#include "UDPChannel.h"
#include "TCPChannel.h"
#include "Session.h"
#include "MyThread.h"
#include <set>
#include <map>
#include <vector>
#include <memory>

#define SMART_PTR

using namespace std;

#ifdef SMART_PTR
typedef shared_ptr<Session> PSession;
#else
typedef Session* PSession;
#endif

class MainModel : public MyThread {
public:
    MainModel();
    MainModel(const MainModel&);
    virtual ~MainModel();
    
    bool ok = false;
    
    void run();
    virtual void exec();
private:
    vector<string> targets;
    set<string> targets_set;
    typedef map<uint32_t, PSession> Sessions;
    string tgtName = "";
    
    UDPChannel *udp;
    set<TCPChannel*> tcps;
    vector<Channel*> channels;
    Sessions sessions;
    string iface = "";
    int last_tcp_configured = -1;
    
    PSession findSession(uint32_t id);
    targets_t findTargets(string tgt);
    string waitForIface(string iface);
};

extern MainModel* main_model;

#endif /* MAINMODEL_H */

