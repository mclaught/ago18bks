/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BaseSoundSender.h
 * Author: user
 *
 * Created on 16 декабря 2019 г., 13:54
 */

#ifndef BASESOUNDSENDER_H
#define BASESOUNDSENDER_H

#include <memory>
#include <set>
#include <map>
#include <thread>
#include <atomic>
#include "proto.h"

#define SEND_FRAME_SERIES 6

using namespace std;

class BaseSoundSender;
typedef map<int, BaseSoundSender*> sound_sender_pool_t;

class BaseSoundSender {
public:
    BaseSoundSender(bool in_tread);
    BaseSoundSender(const BaseSoundSender& orig);
    virtual ~BaseSoundSender();
    
    uint8_t master_brd_addr;
    uint8_t master_brd_chs[MAX_INP_CHANNELS];

    static sound_sender_pool_t getPool();
    static BaseSoundSender* findSender(int key);

    virtual void start();
    virtual void stop();
    virtual void onStart();
    virtual void onStop();
    bool isBusy();
    int getInCh();
    virtual void sendSoundPacket(int cnt) = 0;
    virtual void timer_event();

protected:
    pd_start_t pd_start;
    thread thrd;
    atomic_bool active = true;
    virtual void run();
    virtual void cycle();
    void selectMasterBoard();
    void unselectMasterBoard();
    
private:
    chrono::time_point<chrono::high_resolution_clock> last_tm;
    chrono::time_point<chrono::high_resolution_clock> start_tm;
    int sent_cnt = 0;
    bool _in_thread = false;
    
    friend void ss_thread_func(BaseSoundSender* __this);

};
typedef shared_ptr<BaseSoundSender> PBaseSoundSender;

extern sound_sender_pool_t sound_sender_pool;

#define SOUND_SENDER_KEY(addr, ch)  ((addr << 8) | ch)

#endif /* BASESOUNDSENDER_H */

