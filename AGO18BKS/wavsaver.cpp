
#include "wavsaver.h"
#include "iostream"

WavSaver::WavSaver(std::string fileName, uint32_t sample_rate)
{
    file = fopen(fileName.c_str(), "wb");
    if(file){
        writeHeader(sample_rate);
    }
}

WavSaver::~WavSaver(){
    close();
}

void WavSaver::writeHeader(uint32_t sample_rate)
{
    const uint32_t RIFF = (uint32_t)'FFIR';
    const uint32_t WAVE = (uint32_t)'EVAW';
    const uint32_t fmt = (uint32_t)' tmf';
    const uint32_t data_ = (uint32_t)'atad';

    uint32_t size = 0;

    fwrite(reinterpret_cast<const char*>(&RIFF), 4, 1, file);
    fwrite(reinterpret_cast<const char*>(&size), 4, 1, file);
    fwrite(reinterpret_cast<const char*>(&WAVE), 4, 1, file);

    wav_format_t format;
    format.compression = 1;
    format.channles = 1;
    format.sample_rate = sample_rate;
    format.bytes_per_sec = sample_rate*2;
    format.allign = 2;
    format.bits_per_sample = 16;
//    format.extra = 0;
//    memset(format.reserved, 0, 22);
    size = sizeof(wav_format_t);
    fwrite(reinterpret_cast<const char*>(&fmt), 4, 1, file);
    fwrite(reinterpret_cast<const char*>(&size), 4, 1, file);
    fwrite(reinterpret_cast<const char*>(&format), 1, sizeof(wav_format_t), file);

    size = 0;
    fwrite(reinterpret_cast<const char*>(&data_), 4, 1, file);
    fwrite(reinterpret_cast<const char*>(&size), 4, 1, file);
}

void WavSaver::write(int16_t* pcm, int samples)
{
    fwrite(reinterpret_cast<const char*>(pcm), sizeof(int16_t), samples, file);
}

void WavSaver::close()
{
    uint32_t size;

    int fSize = ftell(file);

    fseek(file, SEEK_SET, 4);
    size = fSize - 8;
    fwrite(reinterpret_cast<const char*>(&size), 4, 1, file);

    fseek(file, SEEK_SET, 24 + sizeof(wav_format_t));
    size = fSize - 28 - sizeof(wav_format_t);
    fwrite(reinterpret_cast<const char*>(&size), 4, 1, file);

    fclose(file);
//    cout << "File closed" << endl;
}
