/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.cpp
 * Author: user
 * 
 * Created on 6 декабря 2019 г., 0:26
 */

#include "MyThread.h"

void my_thread_func(MyThread* __this);

MyThread::MyThread() {
}

MyThread::MyThread(const MyThread& orig) {
}

MyThread::~MyThread() {
}

void MyThread::start(){
    active = true;
    thrd = new thread(my_thread_func, this);
    delete thrd;
}

void MyThread::stop(){
    active = false;
    thrd->join();
}

void my_thread_func(MyThread* __this){
    __this->run();
}
