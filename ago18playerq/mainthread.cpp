#include "mainthread.h"

#include <QDebug>

MainThread::MainThread(QObject *parent) : QObject(parent)
{
    timer = new QTimer();
    connect(timer, &QTimer::timeout, [this](){
        qDebug() << tm.nsecsElapsed();
        tm.restart();
    });
}

void MainThread::start()
{
    tm.start();
    timer->start(15);
}
