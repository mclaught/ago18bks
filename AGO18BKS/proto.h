/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   proto.h
 * Author: user
 *
 * Created on 26 ноября 2019 г., 14:26
 */

#ifndef PROTO_H
#define PROTO_H

#include <stdint.h>
#include <set>

#define SIMPLE_SEND     0
#define THREAD_SEND     1
#define TIMER_SEND      2
#define PROCESS_SEND    3
#define UDP_SEND_TYPE   THREAD_SEND

//#define UDP_DEBUG 0
//#define CONFIG_DEBUG 0
//#define WS_DEBUG 0
//#define SAVER_DEBUG 0

#define MAX_INP_CHANNELS        2
#define MAX_CHANNELS            64//32
#define MAX_CHANNELS_USED       64

#define AUDIO_FRAME_MS		5
#define AUDIO_FREQ		48000
#define AUDIO_SAMPLES		(AUDIO_FRAME_MS * AUDIO_FREQ / 1000)
#define AUDIO_LEN_BYTES         AUDIO_SAMPLES * 2

#define EQ_SECTIONS                     2
#define HP_FCUT_VAR                     4UL

// преобразование номера платы в номер канала по его индексу
#define BRD_ID_TO_CHAN(x,y)     ((MAX_INP_CHANNELS * x) + y)

// преобразование номера канала в номер платы:
#define CHAN_TO_BRD_ID(x)	((x) / MAX_INP_CHANNELS)

// преобразование номера канала в индекс
#define CHAN_TO_INDX(x)         ((x) % MAX_INP_CHANNELS)

typedef uint32_t ch_mask_t;
#define CT_BITS (sizeof(ch_mask_t)*8)

typedef enum {  
    PACK_AUDIO = 0, PACK_LOGS, PACK_ALARM, PACK_CFG, PACK_OTHER, PACK_BRU_CFG, PACK_GEN_CALL,
          PACK_TYPES_CNT
}pack_type_t;//тип пакета для быстрой коммутации

typedef enum {
    NO_ACTION = 0, SND_START_REQ, SND_STOP_REQ, SND_CHANK_REQ, SND_IN_CPB_REQ, SND_OUT_CPB_REQ, SND_IN_CURR_REQ, SND_OUT_CURR_REQ, //7
    EXT_SWT, SND_STREAM, AUDIO_CFG_IN, AUDIO_CFG_OUT, CONTROL_CFG, ALARM_MSG, LOG_MSG, SET_RECIPNT_LIST, CLEAR_RECIPNT_LIST, //16
    CH_BUSY_MSG, SET_MASTER, CLEAR_MASTER, REC_NUMBER, GET_TEMPERATURE, BRU_TEST_REQ, BRU_TEST_END, UM_SWITCH_REQ, BRU_FROM_BYPS_SWITCH_REQ, //25
    BRU_TO_BYPS_SWITCH_REQ, BRU_START_MEASURE, BRU_STOP_MEASURE, BRU_RAW_DATA, BRU_LINE_MEASURE, BRU_TEST_MODE_START, BRU_TEST_MODE_STOP, //32
    BRU_SAVE_CFG, SCREEN_MSG, BRU_LINE_MEAS_RESULT, BRU_GET_STATE, CHANGE_ID, GET_ID, UM_UNLOCK, FW_BOOT_MODE, FW_FILE_INFO, FW_GET_FILE, //42
    FW_UPDATE_BEGIN, FW_UPDATE_STATUS, FW_RESET, FW_INFO, SWPT_REQ, READ_FLASH_CFG, WRITE_FLASH_CFG, BRU_RESET_SWITCH_CFG, FAKE_ALARM, //51
    MON_INFO, HW_EQ, SW_EQ, SW_EQ_BYPASS, UM_FEEDBACK, HPF_CFG, PA_CFG, PA_CNTRL_ASCII, PA_CNTRL_BIN, LOG_CFG, GET_LOG_CFG, CLEAR_FLASH_CFG, //63
    BRU_MANUAL_SWITCH, BRU_GET_CFG, UM_BUF_SYNC_PARAM, UM_BUF_SYNC_STAT, DPA_MON, DPA_MANUAL, MOT_CMD, UM_MANUAL_ID_SET, AUDIO_CAL_OUT, //72
    BKS_GET_INP_MODE, BKS_SET_INP_MODE, BRP_GET_CRITICAL_ALARMS, BRP_SET_CRITICAL_ALARMS, BRU_GET_CAL, BRU_SET_CAL, PING, //79
    CMD_NUMBER
}eth_cmd_t;

typedef enum {
  BKS_AUDIO = 0, BKS_CNTRL, BRU, UM_TRANS, UM_DPA, BRP, DEV_AMNT
}blocks_t;

#pragma pack(push, 1)

typedef struct
{
  uint8_t adr_to;//номер платы назначения (адрес ПК 0x80)
  uint8_t adr_from;//номер платы отправителя  
  uint8_t repeate_cnt;//счетчик повторов от 0
  uint8_t type;// тип пакета
  uint16_t len;//длина оставшейся части пакета
}pack_hdr_t;//заголовок любого пакета

//typedef struct
//{
//  uint8_t ch_table[MAX_INP_CHANNELS];//номера каналов отправителя. 
//  uint8_t streams;//кол-во потоков (каналов) (1 или 2. Другие значения - ошибка)  
//}audio_hdr_t;//добавочный заголовок для аудиопакета
typedef struct
{
  uint8_t sender;//номер канала отправителя.
  ch_mask_t recip;//бит. маска получателей
}audio_hdr_t;//добавочный заголовок аудиопакета для 32 получателей

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    audio_hdr_t audio_hdr;
    int16_t pcm[AUDIO_SAMPLES*2];
}packet_audio_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    uint32_t uptime;
    uint8_t dev_type;
}packet_ping_t;


typedef struct{
    uint8_t src_ch;
    ch_mask_t recipients;
//    uint8_t recipients_amount;
//    uint8_t recipients[MAX_CHANNELS];
}start_stop_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    uint8_t data[];
}packet_command_t;

typedef struct
{
  ch_mask_t sw_table[MAX_CHANNELS];//таблица коммутации
  uint8_t prio_code;//код сортировки приоритета, 0 - по возрастанию, 1 - по убыванию
  uint8_t prio_table[MAX_CHANNELS];//номера каналов, отсортированные в соотв. с prio_code
}swp_table_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    swp_table_t swp_table;
}swp_table_packet_t;

typedef enum {  
  v30V = 0, v60V, v100V
}out_voltage_t;//выходные напряжения усилителей

typedef struct
{
  float b0;
  float b1;
  float b2;
  float a1;
  float a2;
}biqd_hp_coeff_t;

typedef struct
{
  biqd_hp_coeff_t s1;
  biqd_hp_coeff_t s2;
  biqd_hp_coeff_t s3;
  biqd_hp_coeff_t s4;
}hp_stage_t;

typedef struct
{
  float b0;
  float b1;
  float b2;
  float a1;
  float a2;
}biqd_lp_coeff_t;

typedef struct
{
  biqd_lp_coeff_t s1;
  biqd_lp_coeff_t s2;
  biqd_lp_coeff_t s3;
  biqd_lp_coeff_t s4;
}lp_stage_t;

typedef struct
{
  hp_stage_t highpass;  
  lp_stage_t lowpass;
}filter_t;

typedef struct
{
  float in_lvl;
  float out_lvl;  
}snd_lvl_t;

typedef struct
{
  int16_t a;
  int16_t b;
  int16_t c;
}codec_eq_coeff_t;

typedef struct
{
  uint8_t eq_num;
  codec_eq_coeff_t cf[];//eq_num
}codec_eq_t;

//Структура для оперативной регулировки громкости. Используется с командами AUDIO_CFG_IN, AUDIO_CFG_OUT
typedef struct
{
  float lvl;//уровень громкости, дБ
  uint8_t ch_index;//индекс канала входа (AUDIO_CFG_IN), либо выхода (AUDIO_CFG_OUT)
}in_out_cfg_t;

//Настройка выходного напряжения усилителя. Используется с командой PA_CFG
typedef struct
{
  uint8_t ch_indx;//индекс канала усилителя
  uint8_t volt_indx;//out_voltage_t индекс выходного напряжения усилителя
}pa_cfg_t;

//настройка частоты среза HighPass фильтра усилителя. Используется с командой HPF_CFG
typedef struct
{
  uint8_t ch_index;//индекс канала усилителя
  uint8_t fcut_indx;//индекс частоты среза
}hpf_cfg_t;

//Используется для настройки эквалайзера с командой SW_EQ
typedef struct
{
  float gain;//усиление/ослабление, дБ
  float out_gain;//усиление на выходе секции, дБ
  float band;//
  uint32_t fcentr_hz;//центральная частота, Гц
  uint8_t ch_index;//индекс канала
  uint8_t eq_index;//индекс секции эквалайзера
}eq_cfg_t;

//Возможности аппаратного кодека по усилению
typedef struct
{
  float min_dB;//минимальный уровень усиления, дБ
  float max_dB;//максимальный уровень усиления, дБ
  float step_dB;//шаг изменения усиления, дБ
}codec_capabilities_t;

//Возможности эквалайзера (отдельно для каждого канала усилителя)
typedef struct
{
  float min_out_gain_dB;//минимальное усиление на выходе каждой секции
  float max_out_gain_dB;//максимальное усиление на выходе каждой секции
  float min_gain_dB;//максимальное ослабление каждой секции
  float max_gain_dB;//максимальный подъем каждой секции
  float min_band;//минимальная ширина полосы (коэфф) 
  float max_band;//максимальная ширина полосы (коэфф)
  uint32_t fmin_hz;//минимальная центральная частота
  uint32_t fmax_hz;//максимальная центральная частота
  uint32_t sections;//кол-во секций эквалайзера в каждом канале
}eq_capabilities_t;

//Возможности HighPass фильтра усилителей
typedef struct
{
  uint32_t number;//кол-во настроек частоты среза
  uint32_t fcut[100];//массив частот среза, текущее значение number == HP_FCUT_VAR
}hpf_capabilities_t;

//Кол-во уровней выходного напряжения усилителей
typedef struct
{
  uint8_t out_lvl_amount;//кол-во значений выходного напряжения (сейчас == 4)
  uint8_t out_lvl[100];//массив значений напряжений (сейчас 0,30,60,100). Для настройки используются индексы этих значений, а сами значения - для отображения в интерфейсе
}pa_capabilities_t;

typedef struct
{
  codec_capabilities_t inp_capb;//возможности по входу
  eq_capabilities_t eq_capb;
}snd_in_capab_t;

typedef struct
{
  codec_capabilities_t outp_capb;//..по выходу
  eq_capabilities_t eq_capb;
  hpf_capabilities_t hpf_capb;
  pa_capabilities_t pa_capb;
}snd_out_capab_t;

typedef struct
{
    float gain;
    float band;
    uint32_t freq;
}eq_param_t;

//конфигурация эквалайзеров в составе snd_out_cfg_t
typedef struct
{
    uint32_t sections;
    eq_param_t ec[EQ_SECTIONS];//реальное кол-во секций эквалайзера узнается из поля sections в eq_capabilities_t
    float outGain;
}eq_common_t;

//Конфигурация входов
typedef struct
{
  float lvl[MAX_INP_CHANNELS];
  eq_common_t eq[MAX_INP_CHANNELS];
}snd_in_cfg_t;

//Конфигурация выходов
typedef struct
{
  float lvl[MAX_INP_CHANNELS];
  eq_common_t eq[MAX_INP_CHANNELS];
  uint32_t fcut_indx[MAX_INP_CHANNELS];
  uint8_t amplf_volt[MAX_INP_CHANNELS];//заменить out_voltage_t на uint8_t
}snd_out_cfg_t;

typedef enum{
    PD_FRM_PCM,
    PD_FRM_OPUS
}pd_format_t;

typedef struct{
    uint8_t cmd;
    uint16_t len;
} pd_packet_hdr_t;

typedef struct{
    pd_packet_hdr_t hdr;
    uint8_t data[];
}pd_packet_t;

typedef struct{
    uint8_t frame_length_ms;
    uint8_t format;//pd_format_t
    uint32_t bitrate;
    uint8_t channels;
    uint8_t in_ch[MAX_INP_CHANNELS];
    ch_mask_t outs;
}pd_start_t;

typedef struct{
    uint32_t status;
}pd_stream_t;

typedef struct{
    snd_in_capab_t snd_capab;
    snd_in_cfg_t snd_cfg;
}snd_in_capab_init_packet_t;

typedef struct{
    snd_out_capab_t snd_capab;
    snd_out_cfg_t snd_cfg;
}snd_out_capab_init_packet_t;

typedef enum {
//    NO_ALARM = 0, ALARM_100C, ALARM_125C, ALARM_DS1621S_OVR60, ALARM_SD
    T100 = 0, T125, SD, UM_ERROR, HW_ERROR, DS1621S_OVR60, BRU_UM_NOT_ANSWER, BRU_LINE_SHORT, BRU_LINE_OPEN, BRU_LINE_OVER_LIM, 
    BRU_SWITCH_TO_BYPASS_ERROR, BRU_SWITCH_BACK_ERROR, BRU_REDUND_UM_ACTIVE, GEN_CALL_NO_ANSWER
}alarm_t;
const std::set<alarm_t> ampl_alarms = {HW_ERROR, DS1621S_OVR60, BRU_UM_NOT_ANSWER, BRU_SWITCH_TO_BYPASS_ERROR, BRU_SWITCH_BACK_ERROR};

#define SET_ALARM       0x00
#define CLEAR_ALARM     0x80
#define SET_ALARM_MASK  0x80
typedef struct
{
  uint8_t alarm_code;//код аварии. Для отмены аварии выставляется старший бит. Пример T125 - авария, (T125 | CLEAR_ALARM) - отмена
  uint8_t param;//номер канала/код ошибки
}alarm_item_t;

//----------- БРУ ---------------------
typedef enum {
  LINE_OK = 0, LINE_SHORT, LINE_OPEN, LINE_OVR_LIMITS, LINE_ERR_MEAS, LINE_ERR_CONV, LINE_OUT_OF_RANGE
}line_state_t;

typedef enum {
   MODE_LOW = 0, MOD_HIGH, MOD_HIGH_X10
}meas_mode_t;

typedef enum {
  MAIN_MOD_WAITING = 0, MAIN_MOD_WORKING
}main_mode_t;

typedef struct
{
  float low_trshld_ohm;//нижний порог отклонения сопротивления линии, Ом
  float high_trshld_ohm;//верхний порог отклонения, Ом
  float ref_value_ohm;//референсное значение сопротивления линии, Ом
  float shortOhm;
  float openOhm;
  uint8_t state;//состояние линии по результатам измерения line_state_t
  uint8_t range;//режим измерений (величина тока в линии) meas_mode_t
}line_t;

typedef struct
{
  uint8_t lines;//кол-во линий в профиле (ограничено аппаратно, сейчас 20)
  uint8_t line_offset;//сдвиг, если линии подключены не с 1-го входа (обычно 0)
  uint8_t line_min;//минимальный номер линии (индекс), контролируемый БРУ 
  uint8_t line_max;//максимальный номер линии (индекс), контролируемый БРУ
  uint32_t test_mode_settle_time_ms;//время установления в режиме создания профиля, мсек. 
  uint32_t work_mode_eq_range_settle_time_ms;//время установления в рабочем режиме на том же диапазоне, мсек.
  uint32_t work_mode_diff_range_settle_time_ms;//время установления в рабочем режиме при переключении на другой диапазон, мсек.
  uint32_t work_mode_cycle_period_sec;//период проверки линий в рабочем режиме, сек.
  uint8_t mode;//текущий режим работы. main_mode_t
  uint8_t max_meas_tolerance_proc;//макс. допуск для измерения в %
  uint8_t max_devi_tolerance_proc;//макс. допуск для отклонения в %
  uint8_t switch_state;//куда подключена нагрузка. 0xFF - штатно, иначе ID сгоревшего УМ.
  uint8_t redund_um_id;//номер резервного УМ
  line_t line[];//профиль трансляционных линий
}bru_cfg_t;
#define BRU_CFG_SIZE(p) (sizeof(uint8_t)*9 + sizeof(uint32_t)*4 + sizeof(line_t)*p->lines)

typedef struct
{
  float val;//измеренное значение импеданса, Ом
  float ref_val;//референсное значение импеданса, Ом
  uint8_t line;//номер (индекс) линии
  uint8_t lstate;//состояние линии по результатам измерения line_state_t
  uint8_t mode;//режим измерений (величина тока в линии) meas_mode_t
}bru_measure_t;

typedef struct
{
  uint8_t new_id;//новый ID блока
  uint8_t offset;//сдвиг выходных каналов относительно входных. По умолчанию 10 (актуально для УМ)
  uint8_t um_mode;//режим УМ (0 - основной, 1 - резервный)
}id_cfg_t;

typedef struct
{
    uint8_t id:             7;//порядковый номер блока от 1
    uint8_t isRedundant:    1;//для усилителей: 0 - основной, 1 - резервный, для БРУ 0 (не влияет)
}brd_id_t;

#pragma pack(pop)

#endif /* PROTO_H */

