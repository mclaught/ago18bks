#include "clipsdialog.h"
#include "ui_clipsdialog.h"

#include <QDir>

ClipsDialog::ClipsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ClipsDialog)
{
    ui->setupUi(this);
}

ClipsDialog::~ClipsDialog()
{
    delete ui;
}

QString ClipsDialog::getClip(QString title,QString dir)
{
    windowTitle() = title;
    auto clips = QDir(dir).entryList(QStringList() << "*.wav", QDir::Filters() | QDir::Files);
    ui->listClips->addItems(clips);

    if(exec()){
        auto item = ui->listClips->currentItem();
        if(item){
            return dir+"/"+item->text();
        }else{
            return "";
        }
    }else{
        return "";
    }
}

void ClipsDialog::on_listClips_itemDoubleClicked(QListWidgetItem *item)
{
    (void)item;
    accept();
}
