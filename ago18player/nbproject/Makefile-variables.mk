#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=None-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/None-Linux
CND_ARTIFACT_NAME_Debug=ago18player
CND_ARTIFACT_PATH_Debug=dist/Debug/None-Linux/ago18player
CND_PACKAGE_DIR_Debug=dist/Debug/None-Linux/package
CND_PACKAGE_NAME_Debug=ago18player.tar
CND_PACKAGE_PATH_Debug=dist/Debug/None-Linux/package/ago18player.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=ago18player
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/ago18player
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=ago18player.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/ago18player.tar
# Release_ARM configuration
CND_PLATFORM_Release_ARM=GNU-Linux
CND_ARTIFACT_DIR_Release_ARM=dist/Release_ARM/GNU-Linux
CND_ARTIFACT_NAME_Release_ARM=ago18player
CND_ARTIFACT_PATH_Release_ARM=dist/Release_ARM/GNU-Linux/ago18player
CND_PACKAGE_DIR_Release_ARM=dist/Release_ARM/GNU-Linux/package
CND_PACKAGE_NAME_Release_ARM=ago18player.tar
CND_PACKAGE_PATH_Release_ARM=dist/Release_ARM/GNU-Linux/package/ago18player.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
