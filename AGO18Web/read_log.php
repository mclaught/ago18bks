<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$action =   key_exists("action", $_GET) ? $_GET["action"] : "";

$cmd = "sudo /usr/bin/journalctl -u ago18bks --no-pager --all -o short-iso";

if($action === "load"){
    header('Content-Disposition: attachment; filename="bks_log.txt"');
    header("ContentType: text/plain");
    $cmd = "sudo /usr/bin/journalctl -u ago18bks --no-pager -o short";//
}

$lines = key_exists("lines", $_GET) ? $_GET["lines"] : "";
$since = key_exists("since", $_GET) ? $_GET["since"] : "";
$until = key_exists("until", $_GET) ? $_GET["until"] : "";

if($lines !== ""){
    $cmd .= " -n $lines";
}
if($since !== ""){
    $cmd .= " --since='$since'";
}
if($until !== ""){
    $cmd .= " --until='$until'";
}
if($action === "boots"){
    $cmd = "sudo /usr/bin/journalctl -u ago18bks --list-boots";
}

$res = [];
exec($cmd, $res);

foreach ($res as $line) {
    if($action === "load" || $action === "boots"){
        echo "$line\r\n";
        continue;
    }
    
    $n = strpos($line, ": ");
//    echo "n = $n<br>";
    if($n === false){
        echo "<span class='text-muted'>$line</span><br>";
        continue;
    }
    
    $hdr = substr($line, 0, $n);
    $str = substr($line, $n+2);
    
//    $parts = explode(":", $line);
//    if(count($parts) < 2){
//        echo "<b>$line</b><br>";
//        continue;
//    }
//    $hdr = $parts[0];
//    $str = "";
//    for($i=1; $i<count($parts); $i++){
//        $str .= $parts[$i];
//    }
    
    $parts2 = explode(" ", $hdr);
    if(count($parts2) < 3){
        echo "<span class='text-muted'>$line</span><br>";
        continue;
    }
//    print_r($parts2);
    
    $iTm = strtotime($parts2[0]);
    if($iTm==0){
        $iTm = intval($parts2[0]);
        
        if($iTm==0){
            echo "<span class='text-muted'>$line</span><br>";
            continue;
        }
    }
    $tm = date("d-m-Y H:i:s", $iTm);
    
    if(strpos($str, "[31m") !== FALSE){
        $str = "<span style='color: red;'>$str</span><br>";
    }else if(strpos($str, "[32m") !== FALSE){
        $str = "<span style='color: green;'>$str</span><br>";
    }else if(strpos($str, "[36m") !== FALSE){
        $str = "<span style='color: blue;'>$str</span><br>";
    }else if(strpos($str, "[33m") !== FALSE){
        $str = "<span style='color: rgb(128,128,0);'>$str</span><br>";
    }else if(strpos($str, "[35m") !== FALSE){
        $str = "<span style='color: magenta;'>$str</span><br>";
    }
//    $str = str_replace("\x1B[31m", "", $str);
//    $str = str_replace("\x1B[32m", "", $str);
//    $str = str_replace("\x1B[33m", "", $str);
//    $str = str_replace("\x1B[35m", "", $str);
//    $str = str_replace("\x1B[36m", "", $str);
//    $str = str_replace("\x1B[0m", "", $str);
//    $str = str_replace("\x1B", "", $str);
    $str = preg_replace("/\x1B\[[0-9]+m/", "", $str);
    
    echo "<div class='row text-dark' style='width:1024px;'>";
    if(strpos($parts2[2], "ago18bks")!==false){
        echo "<span class='col-4 text-dark'>$tm &nbsp;&nbsp; $parts2[2]</span>";
        echo "<span class='col text-dark'>$str</span><br>";
    }else{
        echo "<span class='col-4 text-secondary'>$tm &nbsp;&nbsp; $parts2[2]</span>";
        echo "<span class='col text-secondary'>$str</span><br>";
    }
    echo "</div>";
}
