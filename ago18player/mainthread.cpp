#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <mutex>
#include <sys/time.h>


#include "mainthread.h"

MainThread::MainThread(string fileName, string ip_addr, uint8_t in, ch_mask_t outs, uint8_t server_addr, bool autorew)
{
    this->fileName = fileName;
    broadcast = ip_addr;
    in_ch = in;
    this->outs = outs;
    this->server_addr = server_addr;
    this->autorew = autorew;
    
//    printf("MainThread %02X %02X", in_ch, this->outs);

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(sock == -1){
        printf("socket(): %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    int bc_on=1;
    if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &bc_on, sizeof(int)) == -1){
        printf("setsockopt(): %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    sockaddr_in my_addr;
    memset(&my_addr, 0, sizeof(sockaddr_in));
    my_addr.sin_family = AF_INET;
    //my_addr.sin_addr = ((sockaddr_in*)&ifr.ifr_ifru.ifru_addr)->sin_addr;//INADDR_ANY
    my_addr.sin_addr.s_addr = INADDR_ANY;
    my_addr.sin_port = 0;//htons(46000);
#ifdef DEBUG_PRINT
    cout << "Inner stream socket binds to " << inet_ntoa(my_addr.sin_addr) << ":" << 46000 << endl;
#endif

    if(bind(sock, (sockaddr*)&my_addr, sizeof(sockaddr_in)) == -1){
        printf("bind(): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    pcm = make_shared<MyPCM>(fileName, AUDIO_FRAME_MS, true);
    int rd = pcm->readFrame(reinterpret_cast<char*>(fifo), FIFO_SIZE*sizeof(int16_t));
    fifo_in = rd/sizeof(int16_t);
    if(fifo_in >= FIFO_SIZE)
        fifo_in -= FIFO_SIZE;
    fifo_out = 0;
    fifo_cnt = rd/sizeof(int16_t);
//    fifo_in = fifo_out = 0;
//    fifo_cnt = 0;

    terminated = false;
}

void MainThread::sendCommand(uint8_t* data, int len, uint8_t type, uint8_t cmd, int repeats, uint8_t addr){
    size_t sz = sizeof(pack_hdr_t)+1+len;
    uint8_t buf[sz];
    packet_command_t* p = reinterpret_cast<packet_command_t*>(buf);

    p->hdr.adr_from = server_addr;
    p->hdr.adr_to = addr;
    p->hdr.len = len+1;
    p->hdr.type = type;
    p->hdr.repeate_cnt = pack_n++;
    p->cmd = cmd;
    if(data)
        memcpy(p->data, data, len);

    sockaddr_in br_addr;
    memset(&br_addr, 0, sizeof(sockaddr_in));
    br_addr.sin_family = AF_INET;
    inet_aton(broadcast.c_str(), &br_addr.sin_addr);
    br_addr.sin_port = htons(46000);

    for(int i=0; i<repeats; i++){
//        p->hdr.repeate_cnt = i;

//        dumpPacket((pack_hdr_t*)buf, sizeof(pack_hdr_t)+sizeof(audio_hdr_t)+1);
        
        lock_guard<mutex> lock(sock_mutex);
        if(sendto(sock, buf, sz, 0, reinterpret_cast<sockaddr*>(&br_addr), sizeof(sockaddr_in)) == -1){
            printf("sendto(): %s\n", strerror(errno));
        }
        //usleep(1000);
    }
}

void MainThread::sendSoundPacket(int cnt){
//    packet_audio_t pack;

//    pack.audio_hdr.sender = in_ch;
//    pack.audio_hdr.recip = outs;

//    for(int i=0; i<cnt; i++){
//        {
//            memcpy(pack.pcm, &buffer[AUDIO_SAMPLES*i], AUDIO_SAMPLES*sizeof(int16_t));
//        }

//        sendCommand(reinterpret_cast<uint8_t*>(&pack.audio_hdr), sizeof(audio_hdr_t)+AUDIO_SAMPLES*sizeof(int16_t), 0, 9, 1, 0xFF);
//    }

//    sent+=cnt;

//    if(first && fifo_cnt < 10)
//        return;

//    first = false;

    packet_audio_t pack;

    pack.audio_hdr.sender = in_ch;
    pack.audio_hdr.recip = outs;

    for(int i=0; i<cnt; i++){
        if(fifo_cnt > 0){
            int to_send = fifo_cnt >= AUDIO_SAMPLES ? (int)AUDIO_SAMPLES : (int)fifo_cnt;
            
            memcpy(pack.pcm, &fifo[fifo_out], to_send*sizeof(int16_t));
            if(to_send < AUDIO_SAMPLES){
                memset(&pack.pcm[to_send], 0, (AUDIO_SAMPLES-to_send)*sizeof(int16_t));
            }

            fifo_out += to_send;
            if(fifo_out >= FIFO_SIZE)
                fifo_out = 0;
            fifo_cnt -= to_send;

            sendCommand(reinterpret_cast<uint8_t*>(&pack.audio_hdr), sizeof(audio_hdr_t)+AUDIO_SAMPLES*sizeof(int16_t), 0, 9, 1, 0xFF);
            sent++;
            
//            printf("fifo_cnt = %d\n", (int)fifo_cnt);
        }else{
            break;
        }
    }

}

int MainThread::read_block(int samples){
    int rd = pcm->readFrame(reinterpret_cast<char*>(&fifo[fifo_in]), sizeof(int16_t)*samples);
    int rd_smpl = rd/sizeof(int16_t);
    fifo_in += rd_smpl;
    if(fifo_in >= FIFO_SIZE)
        fifo_in -= FIFO_SIZE;
    fifo_cnt += rd_smpl;
    return rd_smpl;
}

void MainThread::read_file()
{
//    int rd = pcm->readFrame(reinterpret_cast<char*>(buffer), sizeof(int16_t)*AUDIO_SAMPLES*PACKS_CNT);

//    if(pcm->file_pos >= pcm->file_size){
//        terminated = true;
//    }

//    int _fifo_cnt = fifo_cnt;
//    if(pcm->file_pos < pcm->file_size && (FIFO_SIZE - _fifo_cnt) >= AUDIO_SAMPLES){//*9

//        int16_t buf[AUDIO_SAMPLES*9];
//        int rd = pcm->readFrame(reinterpret_cast<char*>(buf), sizeof(int16_t)*AUDIO_SAMPLES);
//        if(rd > (sizeof(int16_t)*AUDIO_SAMPLES))
//            rd = sizeof(int16_t)*AUDIO_SAMPLES;

//        addOpusPacket(reinterpret_cast<uint8_t*>(buf), rd);

//    }
    int _fifo_cnt = fifo_cnt;
    int free_cnt = FIFO_SIZE - _fifo_cnt;
    if((pcm->file_size - pcm->file_pos)/sizeof(int16_t) < free_cnt)
        free_cnt = (pcm->file_size - pcm->file_pos)/sizeof(int16_t);
    if(free_cnt){
        if((FIFO_SIZE-fifo_in) >= free_cnt){
            read_block(free_cnt);
//            int rd = pcm->readFrame(reinterpret_cast<char*>(&fifo[fifo_in]), sizeof(int16_t)*free_cnt);
//            fifo_in += rd/sizeof(int16_t);
//            if(fifo_in >= FIFO_SIZE)
//                fifo_in -= FIFO_SIZE;
//            fifo_cnt += rd/sizeof(int16_t);
        }else{
            free_cnt -= read_block(FIFO_SIZE-fifo_in);
//            int rd = pcm->readFrame(reinterpret_cast<char*>(&fifo[fifo_in]), sizeof(int16_t)*(FIFO_SIZE-fifo_in));
//            fifo_in += rd/sizeof(int16_t);
//            if(fifo_in >= FIFO_SIZE)
//                fifo_in -= FIFO_SIZE;
//            fifo_cnt += rd/sizeof(int16_t);
//            free_cnt -= rd/sizeof(int16_t);

            if(free_cnt){
                read_block(free_cnt);
//                rd = pcm->readFrame(reinterpret_cast<char*>(&fifo[fifo_in]), sizeof(int16_t)*free_cnt);
//                fifo_in += rd/sizeof(int16_t);
//                if(fifo_in >= FIFO_SIZE)
//                    fifo_in -= FIFO_SIZE;
//                fifo_cnt += rd/sizeof(int16_t);
            }
        }
    }

//    printf("Pos: %d / %d - %d\n", pcm->file_pos, pcm->file_size, _fifo_cnt);
    if(pcm->file_pos >= pcm->file_size){
        if(autorew){
            pcm = make_shared<MyPCM>(fileName, AUDIO_FRAME_MS, true);
        } else if(_fifo_cnt == 0){
            terminated = true;
        }
    }
}

void MainThread::addOpusPacket(const unsigned char* data, int32_t buf_len){
    int samples = buf_len/sizeof(int16_t);//pd_start.frame_length_ms * pd_start.channels * AUDIO_FREQ / 1000;

    if((FIFO_SIZE - fifo_cnt) < samples)
        return;

    int l1 = FIFO_SIZE-fifo_in;
    if(l1 > samples)
        l1 = samples;
    int l2 = samples - l1;

    memcpy(&fifo[fifo_in], data, l1*sizeof(int16_t));
    if(l2)
        memcpy(fifo, data + l1, l2*sizeof(int16_t));
    fifo_in = (fifo_in+samples)%FIFO_SIZE;
    fifo_cnt += samples;
}


void MainThread::exec()
{
    while(!terminated){
        read_file();
    }
}

void MainThread::dumpPacket(pack_hdr_t* pack, int len){
    int cnt = sizeof(pack_hdr_t) + 1 + pack->len;
    if(cnt > len)
        cnt = len;
    uint8_t* data = reinterpret_cast<uint8_t*>(pack);
    for(int i=0; i<cnt; i++){
        printf("%02X ", data[i]);
    }
    printf("\n");
}


