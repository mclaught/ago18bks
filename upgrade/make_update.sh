#!/bin/bash

cd place_files_here

tar -cf update.tar *
gpg -s update.tar

rm update.tar
mv update.tar.gpg ..

read -p "Update file name: " name

cd ..
mv update.tar.gpg $name.upd
