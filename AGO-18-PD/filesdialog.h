#ifndef FILESDIALOG_H
#define FILESDIALOG_H

#include "agobase.h"
#include "filesmodel.h"
#include "scheduller.h"

#include <QDialog>
#include <QFileSystemWatcher>

namespace Ui {
class FilesDialog;
}

class FilesDialog : public QDialog
{
    Q_OBJECT

public:
    enum RunType {
        RunPreview,
        RunCast
    };

    QString filePath;

    explicit FilesDialog(QStringList &names, RunType runType, AgoBase *ago, QWidget *parent = nullptr);
    ~FilesDialog();

    void loadFiles(QString dir);
private slots:
    void on_tblFiles_clicked(const QModelIndex &index);

    void on_btnClose_clicked();

    void on_btPlay_clicked();

    void onStatusLabel(QString text);
    void onProgress(qint64 max, qint64 val, bool visible);
    void onError(QString title, QString msg);
    void onVolume(int volume);
    void on_slVolume_valueChanged(int value);
    void onState(AgoBase::State state);

    void on_btSelectForCast_clicked();

    void on_commandLinkButton_2_clicked();

    void on_btDelete_clicked();

    void on_btUpdate_clicked();

    void on_btNewRec_clicked();

    void onFileChanged(const QString &path);
    void onDirChanged(const QString &path);

    void on_btExplorer_clicked();
    void onLevel(uint16_t lvl);

    void on_tblFiles_doubleClicked(const QModelIndex &index);

private:
    QString filesDir;
    Ui::FilesDialog *ui;
    FilesModel model;
    QStringList names;
    RunType runType;
    AgoBase* ago;
    QModelIndex curVolumeIndex;
    QMutex volMutex;
    QFileSystemWatcher watcher;

    QModelIndex selectedIndex();
    void showInExplorer(QString path);
};

#endif // FILESDIALOG_H
