var class_channel =
[
    [ "send_queue_iten_t", "struct_channel_1_1send__queue__iten__t.html", "struct_channel_1_1send__queue__iten__t" ],
    [ "CmdParams", "class_channel.html#ad1c5930cd81a3862ed0021e99c6dd910", null ],
    [ "Channel", "class_channel.html#af2b4b16288cbb2c592b1e0f6486c2430", null ],
    [ "Channel", "class_channel.html#a9721e1e26d3f59708a970bdc2718bdf9", null ],
    [ "~Channel", "class_channel.html#a5f15ebd302464069f1a9e3f0ded14482", null ],
    [ "checkCommand", "class_channel.html#a034b36b32a098ff347030a984acd054b", null ],
    [ "disconnect", "class_channel.html#afb74271d13687e37d881386b6f78cb12", null ],
    [ "onRead", "class_channel.html#a044edaab45eb80bb7f9c823717fe0658", null ],
    [ "onTime", "class_channel.html#a710ba5879bba48baea193a06f90d0ede", null ],
    [ "run", "class_channel.html#aca4cdccf021644b6cc0a5351a1382aa9", null ],
    [ "sendData", "class_channel.html#a4cdc1492eb7db29831814c3000ffd901", null ],
    [ "sendDataTo", "class_channel.html#adee56c71b8545d97ebd8242efc99a263", null ],
    [ "sendRequest", "class_channel.html#a5112af9c1a03dd7c0fb6fd3fe87dcf02", null ],
    [ "sendResponse", "class_channel.html#a689ec263f9769259c634d941b8b1e8e2", null ],
    [ "active", "class_channel.html#a1f56d8af226da204ee85121712197acf", null ],
    [ "name", "class_channel.html#ada27be4a604630621c5de998c7f4a418", null ],
    [ "sock", "class_channel.html#a273fe8bf3f5860c67f8a81aaed1433e4", null ]
];