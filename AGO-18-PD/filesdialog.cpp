#include "drivesdialog.h"
#include "filesdialog.h"
#include "keyboard.h"
#include "scheditemdialog.h"
#include "soundread.h"
#include "targetsdialog.h"
#include "ui_filesdialog.h"
#include <QDebug>
#include <QInputDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QProcess>
#include <QSettings>
#include <QStorageInfo>

extern QSettings setts;

FilesDialog::FilesDialog(QStringList &names, RunType runType, AgoBase* ago, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FilesDialog), names(names), runType(runType), ago(ago)
{
    ui->setupUi(this);

    model.readonly = (runType == RunCast);

    ui->tblFiles->setModel(&model);
    ui->tblFiles->setColumnWidth(FilesModel::colName, 800);
    ui->tblFiles->setColumnWidth(FilesModel::colTargets, 200);
    ui->tblFiles->setColumnWidth(FilesModel::colVolume, 200);
    ui->tblFiles->setItemDelegate(new TargetsDelegate(names.length()));

    if(runType == RunPreview){
        ui->frameCast->setVisible(false);
        ui->labelTitle->setText("Редактирование аудиозаписей");
    }else{
        ui->framePreview->setVisible(false);
        ui->slVolume->setVisible(false);
        ui->labelTitle->setText("Трансляция аудиозаписи");
    }

    ui->progressBar->setVisible(false);

    connect(ago, &AgoBase::onStatusLabel, this, &FilesDialog::onStatusLabel);
    connect(ago, &AgoBase::onState, this, &FilesDialog::onState);
    connect(ago, &AgoBase::onProgress, this, &FilesDialog::onProgress);
    connect(ago, &AgoBase::onError, this, &FilesDialog::onError);
    connect(ago, &AgoBase::onVolume, this, &FilesDialog::onVolume);

    connect(&watcher, &QFileSystemWatcher::fileChanged, this, &FilesDialog::onFileChanged);
    connect(&watcher, &QFileSystemWatcher::directoryChanged, this, &FilesDialog::onDirChanged);

    connect(reader, &SoundReader::on_level, this, &FilesDialog::onLevel);

    QString dir = setts.value("audio_folder", AgoBase::defAudioPath()).toString();
    if(watcher.directories().indexOf(dir) == -1){
        watcher.addPath(dir);
    }

    showFullScreen();
}

FilesDialog::~FilesDialog()
{
    delete ui;
}

void FilesDialog::loadFiles(QString dir){
    filesDir = dir;

    model.update();
    if(!filePath.isEmpty()){
        int i = model.findByPath(filePath);
        if(1 != -1){
            ui->tblFiles->selectRow(i);
        }
    }
}

void FilesDialog::on_tblFiles_clicked(const QModelIndex &index)
{
    if(runType == RunCast)
        return;

    if(runType == RunPreview){
        if(index.column()==FilesModel::colTargets){
            uint32_t targs = model.data(index, Qt::EditRole).toInt();

            qDebug() << "Click" << targs;

            TargetsDialog dlg(this);
            dlg.setNames(names, names.length());
            dlg.setOuts(targs);
            if(dlg.exec()){
                model.setData(index, QVariant::fromValue(dlg.getOuts()));
            }
        }else if(index.column() == FilesModel::colScheduller){
            if(index.isValid()){
                PFileInfo fileInfo = scheduller.items[index.row()];
                SchedItemDialog dlg(fileInfo, this);
                if(dlg.exec()){
                    dlg.save();
                    scheduller.update_tasks();
                    emit model.dataChanged(index, index, QVector<int>() << Qt::EditRole);
                }
            }
        }
    }

    filePath = model.data(index, FilesModel::PathRole).toString();

    curVolumeIndex = model.index(index.row(), FilesModel::colVolume);
    int vol = model.data(curVolumeIndex, Qt::EditRole).toInt();
    onVolume(vol);
}


void FilesDialog::on_btnClose_clicked()
{
    done(QDialog::Rejected);
}


void FilesDialog::on_btPlay_clicked()
{
    auto index = selectedIndex();
    if(index.isValid()){
        filePath = model.data(index, FilesModel::PathRole).toString();
        if(ago){
            if(!ago->isFilePlay()){
                curVolumeIndex = model.index(index.row(), FilesModel::colVolume);
                ago->playFile(filePath, false);
            }else{
                ago->stopFile();
            }
        }
    }
}

void FilesDialog::onStatusLabel(QString text)
{
    ui->lblStatus->setText(text);
}

void FilesDialog::onProgress(qint64 max, qint64 val, bool visible)
{
    ui->progressBar->setMaximum(max);
    ui->progressBar->setValue(val);
    ui->progressBar->setVisible(visible);
}

void FilesDialog::onError(QString title, QString msg){
    QMessageBox::warning(this, title, msg);
}

void FilesDialog::onVolume(int volume)
{
    QMutexLocker lock(&volMutex);
    ui->slVolume->setValue(volume);
}

void FilesDialog::on_slVolume_valueChanged(int value)
{
    if(volMutex.tryLock()){
        if(ago)
            ago->setVolume(value);
        if(!ago->recActive && !ago->isMicOn() && curVolumeIndex.isValid())
            model.setData(curVolumeIndex, value);
        volMutex.unlock();
    }
}

void FilesDialog::onState(AgoBase::State state)
{
    switch(state){
    case AgoBase::Stop:
        ui->btPlay->setEnabled(true);
        ui->btPlay->setChecked(false);
        ui->btPlay->setText("Прослушать");
        ui->btPlay->setIcon(QIcon(":/icon/headset.png"));

        ui->btNewRec->setEnabled(true);
        ui->btNewRec->setChecked(false);
        ui->btNewRec->setText("Новая запись");
        ui->btNewRec->setIcon(QIcon(":/icon/record.png"));

        ui->btDelete->setEnabled(true);
        ui->btnClose->setEnabled(true);

        if(curVolumeIndex.isValid()){
            onVolume(100);
        }
        break;

    case AgoBase::File:
        ui->btPlay->setEnabled(true);
        ui->btPlay->setChecked(true);
        ui->btPlay->setText("Стоп");
        ui->btPlay->setIcon(QIcon(":/icon/stop.png"));

        ui->btNewRec->setEnabled(false);
        ui->btNewRec->setChecked(false);
        ui->btNewRec->setText("Новая запись");
        ui->btNewRec->setIcon(QIcon(":/icon/record.png"));

        ui->btDelete->setEnabled(false);
        ui->btnClose->setEnabled(false);
        break;

    case AgoBase::Mic:
        ui->btPlay->setEnabled(false);
        ui->btPlay->setChecked(false);
        ui->btPlay->setText("Стоп");
        ui->btPlay->setIcon(QIcon(":/icon/stop.png"));

        ui->btNewRec->setEnabled(false);
        ui->btNewRec->setChecked(false);
        ui->btNewRec->setText("Новая запись");
        ui->btNewRec->setIcon(QIcon(":/icon/record.png"));

        ui->btDelete->setEnabled(false);
        ui->btnClose->setEnabled(false);
        break;

    case AgoBase::Rec:
        ui->btPlay->setEnabled(false);
        ui->btPlay->setChecked(false);
        ui->btPlay->setText("Стоп");
        ui->btPlay->setIcon(QIcon(":/icon/stop.png"));

        ui->btNewRec->setEnabled(true);
        ui->btNewRec->setChecked(true);
        ui->btNewRec->setText("Стоп");
        ui->btNewRec->setIcon(QIcon(":/icon/stop.png"));

        ui->btDelete->setEnabled(false);
        ui->btnClose->setEnabled(false);
        break;
    }
}


void FilesDialog::on_btSelectForCast_clicked()
{
    auto index = selectedIndex();
    if(index.isValid()){
        filePath = model.data(index, FilesModel::PathRole).toString();
        done(QDialog::Accepted);
    }
}

QModelIndex FilesDialog::selectedIndex(){
    auto selection = ui->tblFiles->selectionModel();
    qDebug() << "Selection:" << selection->hasSelection() << selection->selectedRows().length();
    if(selection && selection->hasSelection() && selection->selectedRows().length()){
        return selection->selectedRows().at(0);
    }
    return QModelIndex();
}

void FilesDialog::showInExplorer(QString path)
{
    const QFileInfo fileInfo(path);
    QStringList param;
//    param += "/e,";
    if (!fileInfo.isDir())
        param += QLatin1String("/select,");
    param += QDir::toNativeSeparators(fileInfo.canonicalFilePath());
    QProcess::startDetached("explorer.exe", param);
}


void FilesDialog::on_commandLinkButton_2_clicked()
{
    done(QDialog::Rejected);
}


void FilesDialog::on_btDelete_clicked()
{
    auto index = selectedIndex();
    if(index.isValid()){
        filePath = model.data(index, FilesModel::PathRole).toString();
        QString fileName = model.data(model.index(index.row(), FilesModel::colName)).toString();
        if(QMessageBox::question(this, "Удаление файла", QString("Удалить файл %1?").arg(fileName)) == QMessageBox::Yes && !filePath.isEmpty()){
            if(scheduller.removeFile(index.row())){
                model.update();
            }
//            model.removeRows(index.row(), 1);
        }
    }
}


void FilesDialog::on_btUpdate_clicked()
{
    loadFiles(filesDir);
}


void FilesDialog::on_btNewRec_clicked()
{
    if(!ago)
        return;

    if(!ago->recActive){
        ui->btNewRec->setChecked(false);

        auto dir = setts.value("audio_folder", AgoBase::defAudioPath()).toString();
        if(dir.isEmpty()){
            ui->lblStatus->setText("Надо выбрать папку для аудио-файлов");
            return;
        }

        QString fileName = "";
        if(!Keyboard::run(fileName, "Введите имя файла", this)){
            return;
        }
//        if(setts.value("keyboard", true).toBool()){
//            Keyboard kb("Введите имя файла", "", this);
//            kb.exec();
//            fileName = kb.getText();
//        }else{
//            fileName = QInputDialog::getText(this, "Введите имя файла", "Имя файла");
//        }


        if(fileName.isEmpty()){
            return;
        }
        if(!fileName.endsWith(".wav", Qt::CaseInsensitive))
            fileName += ".wav";
        fileName = dir+"/"+fileName;

        if(QFile(fileName).exists()){
            auto ans = QMessageBox::warning(this, "Предупреждение", "Файл уже существует. Заменить?",
                                 QMessageBox::StandardButtons() | QMessageBox::Yes | QMessageBox::No);
            if(ans == QMessageBox::No)
                return;
        }

        qDebug() << fileName;

        ago->startRec(fileName);

        filePath = fileName;

    }else{
        ago->stopRec();
    }
}

void FilesDialog::onFileChanged(const QString &path){
    if(!filesDir.isEmpty()){
        loadFiles(filesDir);
    }
}

void FilesDialog::onDirChanged(const QString &path){
    if(!filesDir.isEmpty()){
        loadFiles(filesDir);
    }
}

void FilesDialog::on_btExplorer_clicked()
{
    DrivesDialog dlg(this);
    if(dlg.exec()){
        auto index = selectedIndex();
        if(index.isValid()){
            filePath = model.data(index, FilesModel::PathRole).toString();
            showInExplorer(filePath);
        }else{
            showInExplorer(setts.value("audio_folder", AgoBase::defAudioPath()).toString());
        }

        if(!dlg.path.isEmpty())
            showInExplorer(dlg.path);

        QObject* win = parent();
        while(win && dynamic_cast<QMainWindow*>(win)==nullptr)
            win = win->parent();
        if(dynamic_cast<QMainWindow*>(win))
            dynamic_cast<QMainWindow*>(win)->showMinimized();
    }

}

void FilesDialog::onLevel(uint16_t lvl)
{
    ui->lvlGauge->setValue(lvl);
}

void FilesDialog::on_tblFiles_doubleClicked(const QModelIndex &index)
{
    if(index.column() == FilesModel::colName){
        QString val = model.data(index).toString();
        if(Keyboard::run(val, "Имя трека", this)){
            model.setData(index, val);
        }
    }
}

