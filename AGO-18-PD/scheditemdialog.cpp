#include "scheditemdialog.h"
#include "ui_scheditemdialog.h"

#include <QPushButton>

SchedItemDialog::SchedItemDialog(PFileInfo fileInfo, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SchedItemDialog), fileInfo(fileInfo)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Сохранить");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    ui->edBegin->setTime(fileInfo->begin);
    ui->edEnd->setTime(fileInfo->end);
    ui->edPeriod->setTime(fileInfo->period);
    ui->cbActive->setChecked(fileInfo->active_task);

    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
}

SchedItemDialog::~SchedItemDialog()
{
    delete ui;
}

void SchedItemDialog::save(){
    if(fileInfo){
        fileInfo->begin = ui->edBegin->getTime();
        fileInfo->end = ui->edEnd->getTime();
        fileInfo->period = ui->edPeriod->getTime();
        fileInfo->active_task = ui->cbActive->isChecked();
        fileInfo->save();
    }
}
