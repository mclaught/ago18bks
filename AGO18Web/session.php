<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$user = "-";
if( key_exists("login", $_POST))
    $user = $_POST["login"];
else if(key_exists("login", $_COOKIE))
    $user = $_COOKIE["login"];

$pass = "-";
if( key_exists("password", $_POST))
    $pass = $_POST["password"];
else if(key_exists("password", $_COOKIE))
    $pass = $_COOKIE["password"];

$users = file_get_contents('files/users');

$key = $user.":".$pass;
//echo $key;
file_put_contents("files/key", $key);

if($key!=":" && strpos($users, $user.":".$pass) !== FALSE){
    setcookie("login", $user, time()+60*60);
    setcookie("password", $pass, time()+60*60);
}else{
    header('Location: login.html');
}


