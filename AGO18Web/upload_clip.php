<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

//print_r($_POST);
//print_r($_FILES);

$uploaddir = 'clips/';

function pcgbasename($param, $suffix=null) { 
    if ( $suffix ) { 
        $tmpstr = ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR); 
        if ( (strpos($param, $suffix)+strlen($suffix) )  ==  strlen($param) ) { 
            return str_ireplace( $suffix, '', $tmpstr); 
        } else { 
            return ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR); 
        } 
    } else { 
        return ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR); 
    } 
}

function woExt( $name ) {
    $parts = explode('.', $name);
    $res = '';
    for($i=0; $i<count($parts)-1; $i++){
        if($res !== ''){
            $res .= '.';
        }
        $res .= $parts[$i];
    }
    return $res;
}

if(key_exists("command", $_POST)){
    $cmd = $_POST["command"];
    $uploaddir = $_POST["dir"]."/";

    if($cmd == "upload"){
        header("Location: clips.php");
        
        $basename = pcgbasename($_FILES['upload-clip']['name']);
        $uploadfile = "/tmp/" . $basename;//$new_file_prefix . "-" . 
        $nameWoExt = woExt($basename);
        $wavfile = $uploaddir . $nameWoExt . '.wav';//$_FILES['upload-clip']['name'] pathinfo($uploadfile, PATHINFO_FILENAME)
        $fileN = 1;
        while(file_exists($wavfile)){
            $wavfile = $uploaddir . $nameWoExt .'(' . $fileN . ')' . '.wav';
            $fileN++;
        }

//        echo "$uploadfile<br>";
//        echo "$wavfile<br>";

        echo '<pre>';
        if (move_uploaded_file($_FILES['upload-clip']['tmp_name'], $uploadfile)) {
            echo '<div class="alert alert-success" role="alert">';
            echo "Файл корректен и был успешно загружен.\n";
            echo "</div>";

            system("ffmpeg -i '$uploadfile' -f wav -acodec pcm_s16le -ar 48k -ac 1 '$wavfile'");
//            if(file_exists($wavfile))
//                unlink($uploadfile);
        } else {
            echo '<div class="alert alert-danger" role="alert">';
            echo "Ошибка при загрузке файла!\n";
            echo "</div>";
        }

        print "</pre>";

    }else if($cmd == "delete"){
        $filename = $_POST["filename"];
        if (!unlink($uploaddir . $filename)) {
            echo "DELETE_ERROR";
        }
        if (file_exists($uploaddir . $filename . ".json")) {
            unlink($uploaddir . $filename . ".json");
        }
        //    header("Location: clips.php");
    }else if($cmd == "rename"){
        $name_old = $_POST["name_old"];
        $name_new = $_POST["name_new"];
        $signal = $_POST["signal"];
        $in = $_POST["in"];
        
        if($name_old != $name_new){
            echo "$uploaddir$name_old -> $uploaddir$name_new\n";
            if(!rename($uploaddir.$name_old, $uploaddir.$name_new)){
                echo "RENAME_ERROR";
            }
        }
        
        $def = (object)[
            "signal" => intval($signal),
            "in" => intval($in)
        ];
        $js = json_encode($def);
        echo "$uploaddir$name_new.json <- $js\n";
        if(file_put_contents($uploaddir.$name_new.'.json', $js) === FALSE){
            echo "JSON_ERROR";
        }
    }
}