#include "drivesdialog.h"
#include "ui_drivesdialog.h"

#include <QStorageInfo>
#include <QPushButton>

DrivesDialog::DrivesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DrivesDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Открыть");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");
//    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    drives = QStorageInfo::mountedVolumes();
    for(int i=0; i<drives.count(); i++){
        if(drives.at(i).rootPath().startsWith("c:", Qt::CaseInsensitive)){
            drives.removeAt(i);
            break;
        }
    }
    foreach (const QStorageInfo &storage, drives) {
        if (storage.isValid() && storage.isReady()) {
//            qDebug() << storage.displayName() << storage.rootPath();
            QString name = storage.name();
            if(name.endsWith("/"))
                name = name.remove(name.length()-1,1);
            ui->listDrives->addItem(new QListWidgetItem(QIcon(":/icon/drive.png"),
                                                        QString("%1 [%2]").arg(name, storage.rootPath())));
        }
    }
}

DrivesDialog::~DrivesDialog()
{
    delete ui;
}

void DrivesDialog::on_listDrives_currentRowChanged(int currentRow)
{
//    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    QStorageInfo drive = drives.at(currentRow);
    path = drive.rootPath();
}

