/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SoundSender.cpp
 * Author: user
 * 
 * Created on 6 декабря 2019 г., 0:52
 */

#include "BufferedSoundSender.h"
#include "ConfigServer.h"
#include "MyUDPServer.h"
#include "PDServer.h"
#include "cus/log.h"
#include <iostream>
#include <string.h>

BufferedSoundSender::BufferedSoundSender()
#if UDP_SEND_TYPE == THREAD_SEND
    : BaseSoundSender(true)
#else
    : BaseSoundSender(false)
#endif
{
    pd_start.frame_length_ms = 0;
}

BufferedSoundSender::BufferedSoundSender(const BufferedSoundSender& orig) : BaseSoundSender(false) {
}

BufferedSoundSender::~BufferedSoundSender() {
    //delete thrd;
}

void BufferedSoundSender::start(){
    active = true;
    first = true;

    {
//        const lock_guard<recursive_mutex> lock(fifo_mutex);
        fifo_in = fifo_out = 0;
    }
    
    BaseSoundSender::start();
}

void BufferedSoundSender::stop(){
    BaseSoundSender::stop();
}

void BufferedSoundSender::iniOpus(){
    if(pd_start.format == PD_FRM_OPUS){
        int err = 0;
        opus = opus_decoder_create(AUDIO_FREQ, pd_start.channels, &err);
        if(err < 0){
            log_error("PDServer opus_decoder_create(): error %s", opus_strerror(err));
            opus = nullptr;
            return;
        }
    }else{
        opus = nullptr;
    }
    
    selectMasterBoard();
    
    sendStartStop(true);
}

void BufferedSoundSender::addOpusPacket(const unsigned char* data, opus_int32 buf_len){
    int samples = buf_len/sizeof(int16_t);//pd_start.frame_length_ms * pd_start.channels * AUDIO_FREQ / 1000;

    if((FIFO_SIZE - get_fifo_cnt()) < samples)
        return;
    
    if(pd_start.format == PD_FRM_OPUS && opus){
        opus_int16 buf[samples];
        int err = opus_decode(opus, data, buf_len, buf, samples, 0);
        if(err < 0){
            log_error("PDServer opus_decode(): error %s", opus_strerror(err));
            return;
        }

//        const lock_guard<recursive_mutex> lock(fifo_mutex);

        memcpy(&fifo[fifo_in], buf, samples*sizeof(opus_int16));
    
        fifo_in += samples;
        if(fifo_in >= FIFO_SIZE)
            fifo_in = 0;
        fifo_cnt += samples;
    }else{
        int l1 = FIFO_SIZE-fifo_in;
        if(l1 > samples)
            l1 = samples;
        int l2 = samples - l1;

//        const lock_guard<recursive_mutex> lock(fifo_mutex);

        memcpy(&fifo[fifo_in], data, l1*sizeof(int16_t));
        if(l2)
            memcpy(fifo, data + l1, l2*sizeof(int16_t));
        fifo_in = (fifo_in+samples)%FIFO_SIZE;
        fifo_cnt += samples;
    }
}

int BufferedSoundSender::get_fifo_cnt(){
//    const lock_guard<recursive_mutex> lock(fifo_mutex);
    return fifo_cnt;
}

void BufferedSoundSender::sendStartStop(bool start){
    start_stop_t start_stop;
    start_stop.src_ch = pd_start.in_ch[0];
    start_stop.recipients = pd_start.outs;
//    start_stop.recipients_amount = 0;
//    for(int i=0; i<MAX_CHANNELS; i++){
//        if(pd_start.channels & (1 << i)){
//            start_stop.recipients[start_stop.recipients_amount++] = i;
//        }
//    }
    server.sendCommand(reinterpret_cast<uint8_t*>(&start_stop), sizeof(start_stop_t), PACK_CFG, start ? SET_RECIPNT_LIST : CLEAR_RECIPNT_LIST);
}

void BufferedSoundSender::sendSoundPacket(int cnt){
    if(first && get_fifo_cnt() < 10)
        return;
    
    first = false;
    
    auto _fifo_cnt = get_fifo_cnt();
    if(_fifo_cnt >= AUDIO_SAMPLES*cnt){
//        printf("send: fifo_cnt=%d\n", get_fifo_cnt());

        packet_audio_t pack;

        pack.audio_hdr.sender = pd_start.in_ch[0];
        pack.audio_hdr.recip = pd_start.outs;
        
        for(int i=0; i<cnt; i++){
            {
//                const lock_guard<recursive_mutex> lock(fifo_mutex);

                memcpy(pack.pcm, &fifo[fifo_out], AUDIO_SAMPLES*sizeof(opus_int16));

                fifo_out += AUDIO_SAMPLES;
                if(fifo_out >= FIFO_SIZE)
                    fifo_out = 0;
                fifo_cnt -= AUDIO_SAMPLES;
            }
            
            server.sendCommand(reinterpret_cast<uint8_t*>(&pack.audio_hdr), sizeof(audio_hdr_t)+AUDIO_SAMPLES*2, PACK_AUDIO, SND_STREAM);
        }
    }
    
//    out_samples += AUDIO_SAMPLES*cnt;
//    if((GetTickCount() - last_stat_tm) > 1000){
//        printf("fifo_cnt=%d in-out=%d \n", _fifo_cnt/AUDIO_SAMPLES, in_samples - out_samples);
//        last_stat_tm = GetTickCount();
//    }
}

void BufferedSoundSender::onStart(){
    BaseSoundSender::onStart();
}

void BufferedSoundSender::onStop(){
    if(opus){
        opus_decoder_destroy(opus);
    }
    
    sendStartStop(false);
    
    unselectMasterBoard();
    
    BaseSoundSender::onStop();
}
