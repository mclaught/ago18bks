/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   messages.h
 * Author: user
 *
 * Created on 20 сентября 2018 г., 22:13
 */

#ifndef MESSAGES_H
#define MESSAGES_H

#include <string>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

    enum Message{
        MSG_SERVER_CONNECTED,
        MSG_SERVER_DISCONNECTED,
        MSG_START_SESSION,
        MSG_FINISH_SESSION,
        MSG_SEARCH_SERVER,
        MSG_SERVER_FOUND
    };

    struct SessionsPrms{
        uint32_t id;
        string url;
        string clientName;
    };
    struct TragetPrms{
        uint32_t id;
        string clientName;
    };

#ifdef __cplusplus
}
#endif

#endif /* MESSAGES_H */

