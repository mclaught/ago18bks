/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RTP.h
 * Author: user
 *
 * Created on 25 декабря 2019 г., 17:16
 */

#ifndef RTP_H
#define RTP_H

#include <stdint.h>


namespace RTP {

    #pragma pack(push, 1)
    typedef struct {
        uint8_t cc:4;
        uint8_t x:1;
        uint8_t p:1;
        uint8_t ver:2;

        uint8_t type:7;
        uint8_t marker:1;

        uint16_t seq;
        uint32_t time;
        uint32_t ssi;
        uint32_t csrc;
    }rtp_hdr_t;
    
    typedef struct {
        uint8_t rc:5;
        uint8_t p:1;
        uint8_t ver:2;
        
        uint8_t type;
        uint16_t len;
        uint32_t ssrc;
        uint64_t ntp_time;
        uint32_t rtp_time;
        uint32_t snd_cnt;
        uint32_t snd_bytes;
        uint32_t ssrc1;
        
        uint32_t lost_cnt:24;
        uint32_t lost_part:8;//fixed point n/256
        
        uint32_t max_seq;        
        uint32_t recv_deviation;
        uint32_t lsr;
        uint32_t dlsr;
    }rtcp_hdr_t;
    #pragma pack(pop)

    uint8_t* decode(uint8_t* packet, int pack_len, int* data_len, rtp_hdr_t* hdr);
};

#endif /* RTP_H */

