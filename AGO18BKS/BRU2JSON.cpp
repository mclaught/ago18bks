/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BRU2JSON.cpp
 * Author: user
 * 
 * Created on 25 ноября 2020 г., 16:21
 */

#include "BRU2JSON.h"
#include "proto.h"

BRU2JSON::BRU2JSON() {
}

BRU2JSON::BRU2JSON(const BRU2JSON& orig) {
}

BRU2JSON::~BRU2JSON() {
}

json BRU2JSON::line_to_json(line_t* line){
    json obj = json::object({
        {"low_trshld_ohm", line->low_trshld_ohm},
        {"high_trshld_ohm", line->high_trshld_ohm},
        {"ref_value_ohm", line->ref_value_ohm},
        {"shortOhm", line->shortOhm},
        {"openOhm", line->openOhm},
        {"state", line->state},
        {"range", line->range}
    });
    return obj;
}

json BRU2JSON::bru_cfg_to_json(bru_cfg_t* bru_cfg, int sz){
    if(BRU_CFG_SIZE(bru_cfg) > static_cast<long unsigned int>(sz))
        return json();
    
    json line_array = json::array();
    for(int l=bru_cfg->line_min; l<=bru_cfg->line_max; l++){
        int i = l - bru_cfg->line_min;
        if(i < bru_cfg->lines){
            json j_line = line_to_json(&bru_cfg->line[i]);
            j_line["line_id"] = l;
            line_array.push_back(j_line);
        }
    }
    
    json obj = json::object({
        {"lines", bru_cfg->lines},
        {"line_offset", bru_cfg->line_offset},
        {"line_min", bru_cfg->line_min},
        {"line_max", bru_cfg->line_max},
        {"line_max", bru_cfg->line_max},
        {"test_mode_settle_time_ms", bru_cfg->test_mode_settle_time_ms},
        {"work_mode_eq_range_settle_time_ms", bru_cfg->work_mode_eq_range_settle_time_ms},
        {"work_mode_diff_range_settle_time_ms", bru_cfg->work_mode_diff_range_settle_time_ms},
        {"work_mode_cycle_period_sec", bru_cfg->work_mode_cycle_period_sec},
        {"mode", bru_cfg->mode},
        {"max_meas_tolerance_proc", bru_cfg->max_meas_tolerance_proc},
        {"max_devi_tolerance_proc", bru_cfg->max_devi_tolerance_proc},        
        {"switch_state", bru_cfg->switch_state},
        {"redund_um_id", bru_cfg->redund_um_id},
        {"line", line_array}
    });
    
    return obj;
}

#define JS_INT(js, name) (js.contains(name) && js[name].is_number_integer()) ? static_cast<int>(js[name]) : 0;
#define JS_FLOAT(js, name) js.contains(name) && js[name].is_number_float() ? static_cast<float>(js[name]) : 0;
void BRU2JSON::json_to_line(json js, line_t* line){
    line->low_trshld_ohm = JS_FLOAT(js, "low_trshld_ohm");
    line->high_trshld_ohm = JS_FLOAT(js, "high_trshld_ohm");
    line->ref_value_ohm = JS_FLOAT(js, "ref_value_ohm");
    line->shortOhm = JS_FLOAT(js, "shortOhm");
    line->openOhm = JS_FLOAT(js, "openOhm");
    line->state = JS_INT(js, "state");
    line->range = JS_INT(js, "range");
}

void BRU2JSON::json_to_bru_cfg(json js, bru_cfg_t* bru_cfg, int sz){
    bru_cfg->lines = JS_INT(js, "lines");
    bru_cfg->line_offset = JS_INT(js, "line_offset");
    bru_cfg->line_min = JS_INT(js, "line_min");
    bru_cfg->line_max = JS_INT(js, "line_max");
    bru_cfg->test_mode_settle_time_ms = JS_INT(js, "test_mode_settle_time_ms");
    bru_cfg->work_mode_eq_range_settle_time_ms = JS_INT(js, "work_mode_eq_range_settle_time_ms");
    bru_cfg->work_mode_diff_range_settle_time_ms = JS_INT(js, "work_mode_diff_range_settle_time_ms");
    bru_cfg->work_mode_cycle_period_sec = JS_INT(js, "work_mode_cycle_period_sec");
    bru_cfg->mode = JS_INT(js, "mode");
    bru_cfg->max_meas_tolerance_proc = JS_INT(js, "max_meas_tolerance_proc");
    bru_cfg->max_devi_tolerance_proc = JS_INT(js, "max_devi_tolerance_proc");
    bru_cfg->switch_state = JS_INT(js, "switch_state");
    bru_cfg->redund_um_id = JS_INT(js, "redund_um_id");
            
    if(BRU_CFG_SIZE(bru_cfg) > static_cast<long unsigned int>(sz))
        return;
    
    int i=0;
    for(json o : js["line"]){
        if(i < bru_cfg->lines)
            json_to_line(o, &bru_cfg->line[i++]);
    }
}