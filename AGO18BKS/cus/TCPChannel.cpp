/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TCPChannel.cpp
 * Author: user
 * 
 * Created on 20 сентября 2018 г., 1:04
 */

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "TCPChannel.h"
#include <iostream>
#include <sys/ioctl.h>
#include <string>
#include <unistd.h>
#include <fcntl.h>
#include "log.h"

TCPChannel::TCPChannel() : Channel() {
}

bool SetSocketBlockingEnabled(int fd, bool blocking)
{
   if (fd < 0) return false;

   int flags = fcntl(fd, F_GETFL, 0);
   if (flags == -1) return false;
   flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
   return fcntl(fd, F_SETFL, flags) != -1;
}

TCPChannel::TCPChannel(string clientName) : Channel(), clientName(clientName){
    name = "TCPChannel "+clientName;
    state = DISCONNECTED;
    targetState = "free";

//    std::cout << cyan << "TCP channel created for " << clientName << clrst << std::endl;
    log("TCP channel created");
}

void TCPChannel::log(string str, bool error){
    if(!error)
        cout << cyan << name << ": " << str << clrst << endl;
    else
        cerr << red << name << ": " << str << clrst << endl;
}

bool TCPChannel::isConfigured(){
    return configured;
}

void TCPChannel::unconfig(){
    configured = false;
}

void TCPChannel::setServerParams(string address, uint16_t port){
    this->address = address;
    this->port = port;
    this->configured = !address.empty() && port>0;
}

void TCPChannel::connectServer()
{
//    if(!isConfigured()){
//        //cerr << "No server params specified" << endl;
//        return;
//    }
    if(address.empty() || port==0)
        return;
    
    log("Connecting to "+address);
    
    state = CONNECTING;
    
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
//        cerr << red << strerror(errno) << clrst << endl;
        log(string(strerror(errno)), true);
        onDisconnected();
        return;
    }
    
//    if(!SetSocketBlockingEnabled(sock, false)){
//        cerr << "SetSocketBlockingEnabled: " << strerror(errno) << endl;
//        onDisconnected();
//        return;
//    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    inet_aton(address.c_str(), &addr.sin_addr);
    addr.sin_port = htons(port);
    
    if(connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1 && errno != EINPROGRESS){
//        cerr << red << name << ": TCP connect: " << strerror(errno) << clrst << endl;
        log("connect() "+string(strerror(errno)), true);
        unconfig();
        onDisconnected();
        return;
    }
    
//    struct timeval tv = {5,0};
//    fd_set w_fds;
//    errno = EINPROGRESS;
//    while(errno == EINPROGRESS){
//        FD_ZERO(&w_fds);
//        FD_SET(sock, &w_fds);
//        if(select(sock+1, NULL, &w_fds, NULL, &tv) == -1 && errno != EINPROGRESS){
//            cerr << "select: " << strerror(errno) << endl;
//            //if(errno != EINPROGRESS)
//            port = 0;
//            onDisconnected();
//            return;
//        }
//    }
//    
//    if(!FD_ISSET(sock, &w_fds)){
//        cerr << "Timeout" << endl;
//        port = 0;
//        onDisconnected();
//        return;
//    }
//    
//    int err = 0;
//    socklen_t len = sizeof(err);
//    if(getsockopt(sock, SOL_SOCKET, SO_ERROR, &err, &len)){
//        cerr << "getsockopt: " << strerror(errno) << endl;
//        onDisconnected();
//        return;
//    }
//    
//    if(err){
//        cerr << "err: " << strerror(err) << endl;
//        port = 0;
//        onDisconnected();
//        return;
//    }
//    
//    if(!SetSocketBlockingEnabled(sock, true)){
//        cerr << "SetSocketBlockingEnabled: " << strerror(errno) << endl;
//        onDisconnected();
//        return;
//    }
    
    toConnecting = 2;
    
    onConnected();
}

TCPChannel::TCPChannel(const TCPChannel& orig) : Channel() {
}

TCPChannel::~TCPChannel() {
}

void TCPChannel::setState(string state)
{
//    std::cout << cyan << "New state: " << state << clrst << std::endl;
    log("State = "+state);
    targetState = state;

    CmdParams params;
    params["client"] = clientName;
    params["command"] = "state";
    params["action"] = "changed";
    params["state"] = state;
    sendRequest(params);
}

void TCPChannel::parseCommand(string data)
{
    XMLDocument xml;
    if(xml.Parse(data.c_str()) == XMLError::XML_SUCCESS){
        XMLElement* root = xml.RootElement();
        if(root){
            if(string(root->Name()) == "RESPONSE"){

                if(!root->Attribute("client", clientName.c_str()))
                    return;

                if(checkCommand(root, "connection", "connect")){
//                    state = CONNECTED;
//                    sendMessage(MSG_SERVER_CONNECTED, (void*)clientName.c_str());
                    if(root->Attribute("error")){
//                        close(sock);
                        onDisconnected();
                    }
                }
            }

            if(string(root->Name()) == "REQUEST"){
                CmdParams addParams;
                sockaddr_in addr;
                sendResponse(root, addr, addParams);

                if(checkCommand(root, "receive", "start")){
                    uint32_t id = (uint32_t)root->IntAttribute("session");
                    string url = root->Attribute("url");
                    setState("run");
                    
                    SessionsPrms prms = {id, url, clientName};
                    sendMessage(MSG_START_SESSION, &prms);
                }

                if(checkCommand(root, "receive", "finish")){
                    uint32_t id = (uint32_t)root->IntAttribute("session");
                    setState("free");
                    
                    SessionsPrms prms = {id, "", clientName};
                    sendMessage(MSG_FINISH_SESSION, &prms);
                }
            }
        }
    }else{
//        std::cerr << red << "TCP XML error: " << xml.ErrorStr() << clrst << std::endl;
        log("XML "+string(strerror(errno)), true);
        onDisconnected();
    }
}

void TCPChannel::sendData(string data){
//    std::cout << cyan << "Send TCP: " << data << clrst << std::endl;
    if(send(sock, data.c_str(), data.length(), 0) == -1){
        log("send() "+string(strerror(errno)), true);
        onDisconnected();
    }
}

void TCPChannel::sendDataTo(string data, sockaddr_in addr){
    sendData(data);
}

void TCPChannel::onConnected()
{
    log("Connected!");
    
    active = true;
    state = CONNECTED;
    sendMessage(MSG_SERVER_CONNECTED, (void*)clientName.c_str());

    CmdParams params;
    params["client"] = clientName;
    params["command"] = "connection";
    params["action"] = "connect";
    params["type"] = "target_in";
    params["state"] = targetState;
    sendRequest(params);
}

void TCPChannel::onDisconnected()
{
    active = false;
    state = DISCONNECTED;
    sendMessage(MSG_SERVER_DISCONNECTED, &clientName);
    close(sock);
    sock = 0;
    
    log("Disconnected");
    
}

void TCPChannel::onTime()
{
//    if(state == CONNECTING){
//        if(toConnecting > 0){
//            toConnecting--;
//        }else{
//            std::cerr << "! " << clientName << " timeout" << std::endl;
//            onDisconnected();
//        }
//    }

    if(state == CONNECTED){
        CmdParams params;
        params["client"] = clientName;
        params["command"] = "connection";
        params["action"] = "check";
        params["type"] = "target_in";
        params["state"] = targetState;
        sendRequest(params);
    }
}

void TCPChannel::onRead()
{
    char data[1024];
    
//    int available = 0;
//    if(ioctl(sock, FIONREAD, &available) == -1){
//        cerr << red << "ioctl: " << strerror(errno) << clrst << endl;
//        return;
//    }
    
    int l = recv(sock, data, 1024, MSG_DONTWAIT);
    if(l == -1){
        if(errno != EAGAIN)
//            cerr << red << "recv: " << strerror(errno) << clrst << endl;
            log("recv() "+string(strerror(errno)), true);
        return;
    }else if(l == 0){
        onDisconnected();
        return;
    }
    data[l] = 0;
    
//    std::cout << cyan << "TCP read: " << data << clrst << std::endl;
    parseCommand(string(data));
}

void TCPChannel::disconnect(){
    onDisconnected();
}
