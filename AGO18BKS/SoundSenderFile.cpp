/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SoundSenderFile.cpp
 * Author: user
 * 
 * Created on 10 декабря 2019 г., 15:42
 */

#include "SoundSenderFile.h"
#include "proto.h"
#include "ConfigServer.h"
#include "cus/log.h"
#include "clipsworker.h"

#include <iostream>
#include <unistd.h>

file_sender_pool_t file_sender_pool;
mutex file_sender_pool_mutex;

SoundSenderFile::SoundSenderFile() : BufferedSoundSender(){
}

SoundSenderFile::SoundSenderFile(const SoundSenderFile& orig) : BufferedSoundSender() {
}

SoundSenderFile::~SoundSenderFile() {
}

void SoundSenderFile::start(string filename, uint8_t source, ch_mask_t outs){
    cout << "Open file " << filename << endl;
    set_filename(filename);

    pcm = make_shared<MyPCM>(filename,AUDIO_FRAME_MS,1);

    pd_start.frame_length_ms = AUDIO_FRAME_MS;
    pd_start.channels = 1;
    pd_start.in_ch[0] = source;//BRD_ID_TO_CHAN(conf_server.server_addr, 0);
    pd_start.outs = outs > 0 ? outs : conf_server.getCTOutsInput(source);
    pd_start.bitrate = pcm->byte_rate*8;
    pd_start.format = 0;

    iniOpus();

    int rd = pcm->readFrame(reinterpret_cast<char*>(fifo), sizeof(opus_int16)*FIFO_SIZE);

    fifo_in = rd/sizeof(opus_int16) % FIFO_SIZE;
    fifo_out = 0;
    fifo_cnt = rd/sizeof(opus_int16);

    BufferedSoundSender::start();

    if(clips_worker.terminated)
        clips_worker.start();
}

void SoundSenderFile::read_file()
{
    if(pcm->file_pos < pcm->file_size && (FIFO_SIZE - get_fifo_cnt()) >= AUDIO_SAMPLES*9){

        opus_int16 buf[AUDIO_SAMPLES*9];
        int rd = pcm->readFrame(reinterpret_cast<char*>(buf), sizeof(opus_int16)*AUDIO_SAMPLES*9);
        if(rd > (sizeof(opus_int16)*AUDIO_SAMPLES*9))
            rd = sizeof(opus_int16)*AUDIO_SAMPLES*9;

        addOpusPacket(reinterpret_cast<uint8_t*>(buf), rd);

//        for(int i=0; i<n; i++){
//            memcpy(&fifo[fifo_in], &buf[AUDIO_SAMPLES*i], sizeof(opus_int16)*AUDIO_SAMPLES);

//            fifo_in += AUDIO_SAMPLES;
//            if(fifo_in >= FIFO_SIZE)
//                fifo_in = 0;
//            fifo_cnt += AUDIO_SAMPLES;
//        }

    }

    if(pcm->file_pos >= pcm->file_size && get_fifo_cnt() < AUDIO_SAMPLES){
        active = false;
    }
}

void SoundSenderFile::stop()
{
    BufferedSoundSender::stop();

    bool someBusy = false;
    for(auto sender : getFileSenders()){
        if(sender->isBusy()){
            someBusy = true;
            break;
        }
    }
    if(!someBusy)
        clips_worker.terminate();
}

void SoundSenderFile::cycle(){
    BufferedSoundSender::cycle();
}

void SoundSenderFile::onStop(){
    BufferedSoundSender::onStop();
}

PSoundSenderFile SoundSenderFile::getFreeSender(){
    lock_guard<mutex> lock(file_sender_pool_mutex);
    
    for(PSoundSenderFile s : file_sender_pool){
        if(!s->isBusy())
            return s;
    }
    
    PSoundSenderFile newFSender = PSoundSenderFile(new SoundSenderFile());
    file_sender_pool.push_back(newFSender);
    return newFSender;
}

file_sender_pool_t SoundSenderFile::getFileSenders()
{
    lock_guard<mutex> lock(file_sender_pool_mutex);
    return file_sender_pool;
}

void SoundSenderFile::stopAll(){
    file_sender_pool_t _file_sender_pool;
    {
        lock_guard<mutex> lock(file_sender_pool_mutex);
        _file_sender_pool = file_sender_pool;
    }
    
    for(PSoundSenderFile s : _file_sender_pool){
        if(s->isBusy()){
            s->stop();
            cout << "File sender stoped" << endl;
        }
    }
}

void SoundSenderFile::set_filename(string value)
{
    unique_lock<shared_mutex> lock(filename_mutex);
    _filename = value;
}

string SoundSenderFile::get_filename()
{
    shared_lock<shared_mutex> lock(filename_mutex);
    return _filename;
}
