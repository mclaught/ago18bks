/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketSoundSender.cpp
 * Author: user
 * 
 * Created on 9 декабря 2019 г., 20:03
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>
#include <string.h>
#include "SocketSoundSender.h"
#include "MyUDPServer.h"

socket_sender_pool_t socket_sender_pool;

SocketSoundSender::SocketSoundSender() : BufferedSoundSender(){
}

SocketSoundSender::SocketSoundSender(const SocketSoundSender& orig) : BufferedSoundSender() {
}

SocketSoundSender::~SocketSoundSender() {
}

void SocketSoundSender::start(int sock){
    this->sock = sock;
    pd_start.frame_length_ms = 0;
    pd_stream.status = 1;
    BufferedSoundSender::start();
}

bool SocketSoundSender::process(){
    if(pd_start.frame_length_ms == 0){
        int rd = recv(sock, reinterpret_cast<void*>(&pd_start), sizeof(pd_start_t), 0);
        if(rd > 0){
//            cout << "Recv len " << rd << endl;
            iniOpus();
            return true;
        }else if(rd < 0){
            cerr << "PDServer accept():" << strerror(errno) << endl;
            return true;
        }else{
//            cout << "TCP connection closed by host" << endl;
            return false;
        }
    }else{
        opus_int32 buf_len = pd_start.bitrate*pd_start.frame_length_ms/8000;
        uint8_t buf[buf_len];
        int rd = recv(sock, buf, buf_len, 0);
        if(rd > 0){
            addOpusPacket(buf, buf_len);
            return true;
        }else if(rd < 0){
            cerr << "PDServer accept():" << strerror(errno) << endl;
            return true;
        }else{
//            cout << "TCP connection closed by host" << endl;
            return false;
        }
    }
    
    return true;
}

void SocketSoundSender::streamControl(){
    int speed = (FIFO_CNT_AVG - get_fifo_cnt())*FIFO_CNT_K + 100;
    if(speed > 200)
        speed = 200;
    if(speed < 0)
        speed = 0;
    pd_stream.status = speed;
    
    if((GetTickCount() - last_stm_tm)>=100){
        if(send(sock, reinterpret_cast<void*>(&pd_stream), sizeof(pd_stream_t), 0) < 0){
            cerr << "PDServer send():" << strerror(errno) << endl;
        }
        last_stm_tm = GetTickCount();
    }
    
//    bool chgd = false;
//    if(pd_stream.status && fifo_cnt > (FIFO_SIZE-AUDIO_SAMPLES*100)){
//        pd_stream.status = 0;
//        chgd = true;
//    }
//    if(!pd_stream.status && fifo_cnt < (AUDIO_SAMPLES*100)){
//        pd_stream.status = 1;
//        chgd = true;
//    }
//    if(chgd){
//        if(send(sock, reinterpret_cast<void*>(&pd_stream), sizeof(pd_stream_t), 0) < 0){
//            cerr << "PDServer send():" << strerror(errno) << endl;
//        }
//    }
}

void SocketSoundSender::cycle(){
    streamControl();
    BufferedSoundSender::cycle();
}

PSocketSoundSender SocketSoundSender::getFreeSender(){
    for(PSocketSoundSender s : socket_sender_pool){
        if(!s->isBusy())
            return s;
    }
    
    PSocketSoundSender newSender = PSocketSoundSender(new SocketSoundSender());
    socket_sender_pool.push_back(newSender);
    return newSender;
}
