/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BRU2JSON.h
 * Author: user
 *
 * Created on 25 ноября 2020 г., 16:21
 */

#ifndef BRU2JSON_H
#define BRU2JSON_H

#include "json.hpp"
#include "proto.h"

using namespace nlohmann;

class BRU2JSON {
public:
    BRU2JSON();
    BRU2JSON(const BRU2JSON& orig);
    virtual ~BRU2JSON();
    
    json line_to_json(line_t* line);
    json bru_cfg_to_json(bru_cfg_t* bru_cfg, int sz);
    
    void json_to_bru_cfg(json js, bru_cfg_t* bru_cfg, int sz);
    void json_to_line(json js, line_t* line);
private:
};

#endif /* BRU2JSON_H */

