#!/bin/bash

echo Остановка сервера
service ago18bks stop
kill -9 `pidof ago18bks`
killall -s9 ago18player

#update-rc.d ago18bks.d remove
#rm -f /etc/init.d/ago18bks.d

echo Копируется служба
cp -f ago18bks /usr/sbin
chmod +x /usr/sbin/ago18bks

echo Копируется плеер
cp -f ago18player /usr/sbin
chmod +x /usr/sbin/ago18player

cp -f ago18bks.service /etc/systemd/system
chmod 644 /etc/systemd/system/ago18bks.service

cp -f www-data /etc/sudoers.d
chmod 644 /etc/sudoers.d/www-data

echo Копируется Web-интерфейс
cp -r -f AGO18Web /var/www/html
if [ ! -d /var/www/html/AGO18Web/clips/pd ]; then
    mkdir -p /var/www/html/AGO18Web/clips/pd
fi 
if [ ! -d /var/www/html/AGO18Web/clips/test ]; then
    mkdir -p /var/www/html/AGO18Web/clips/test
fi 
if [ ! -d /var/www/html/AGO18Web/events ]; then
    mkdir -p /var/www/html/AGO18Web/events
fi 
chown -R www-data:www-data /var/www

if [ ! -d /var/log/journal ]; then
	mkdir -p /var/log/journal
fi

#cp -f recclear_cron.sh /usr/sbin
#chmod +x /usr/sbin/recclear_cron.sh
#crontab crontab
#systemctl restart cron

echo Запуск сервера
#systemctl enable ago18bks
systemctl daemon-reload
systemctl start ago18bks
#systemctl daemon-reload

echo Установка завершена:...
echo Сервер `ago18bks -v`
echo Плеер `ago18player -v`
php /var/www/html/AGO18Web/print_web_ver.php


