/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/class.h to edit this template
 */

/* 
 * File:   TimeSync.h
 * Author: user
 *
 * Created on 3 ноября 2022 г., 11:30
 */

#ifndef TIMESYNC_H
#define TIMESYNC_H

#include <chrono>
#include <string>

//#define RTC_DEV "/dev/rtc0"
#define RTC_OK_FILE "/run/rtc_ok"
#define TIME_OK_FILE "/run/time_sync_ok"
#define RTC_SET_PERIOD  24h
//#define __DBG

using namespace std;

class TimeSync {
public:
    TimeSync();
    TimeSync(const TimeSync& orig);
    virtual ~TimeSync();
    
    typedef enum{SYNC_NONE, SYNC_RTC, SYNC_NTP} time_sync_t;
    
    time_sync_t sync_ok();
    time_sync_t get_sync_ok();
    void setRTC();
    void setRTCTime(string tm);
private:
    time_sync_t _sync_ok = SYNC_NONE;
    chrono::time_point<chrono::steady_clock> next_tm;
    chrono::time_point<chrono::steady_clock> rtc_set_tm;
    
    bool isRTCOk();
    bool isTimeSync();
    bool isTimeOk();
    bool syncRTC();
    bool syncNTP();
    void setTimeSync(bool ok);
};

extern TimeSync timeSync;

#endif /* TIMESYNC_H */

