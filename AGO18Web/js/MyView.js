class MyView{
//    container;
    
    constructor(container){
        this.container = container;
    }
    
    setState(){
        this.container.empty();
        var view = this.build();
        this.container.append(view);
    }
    
    init(){//override
        this.setState();
    }
    
    build(){//override
        
    }
}

class MyListView extends MyView{
    constructor(container){
        super(container);
    }
    
    setState(){
        this.container.empty();
        var cnt = this.getItemsCount();
        for(let i=0; i<cnt; i++){
            let view = this.build(i);
            this.container.append(view);
        }
    }
    
    getItemsCount(){//override
        
    }
    
    build(index){//override
        
    }
}

class Page extends MyView{
    async loadView(file){
        let pr = new Promise(function(resolve, reject){
            $.get(file, function(answer){
                resolve(answer);
            });
        });
        return pr;
    }
}