#!/bin/sh
### BEGIN INIT INFO
# Provides:          bksarec
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: BKS Audio recorder
# Description:       BKS Audio recorder
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="AGO-18 BKS"
NAME=ago18bks
GUARDNAME=ago18guardian
PLAYER=ago18player
DAEMON=/usr/sbin/${NAME}
GUARD=/usr/sbin/${GUARDNAME}

case $1 in
    start)
        echo "Starting ${DESC}..."

        cd /
        exec > /dev/null
        exec 2> /dev/null
        exec < /dev/null 

        .${DAEMON} &
	.${GUARD} &
        ;;

    stop)
        echo "Stoping ${DESC}..."
		
	killall ${PLAYER}
	killall ${GUARDNAME}

        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            kill `pidof ${NAME}`
        fi
        ;;

    restart)
	killall ${NAME}
	;;

    status)
        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi         
        ;;
esac


