#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release_WRT
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/AudioSaver.o \
	${OBJECTDIR}/BRU2JSON.o \
	${OBJECTDIR}/BaseSoundSender.o \
	${OBJECTDIR}/BaseWriter.o \
	${OBJECTDIR}/BufferedSoundSender.o \
	${OBJECTDIR}/ConfigServer.o \
	${OBJECTDIR}/MyUDPServer.o \
	${OBJECTDIR}/MyWSClient.o \
	${OBJECTDIR}/OGGWriter.o \
	${OBJECTDIR}/PDServer.o \
	${OBJECTDIR}/PDUDPServer.o \
	${OBJECTDIR}/PacketParser.o \
	${OBJECTDIR}/Snd2Json.o \
	${OBJECTDIR}/SocketSoundSender.o \
	${OBJECTDIR}/SoundSenderFile.o \
	${OBJECTDIR}/TimeSync.o \
	${OBJECTDIR}/UDPSoundSender.o \
	${OBJECTDIR}/WSServer.o \
	${OBJECTDIR}/clipsworker.o \
	${OBJECTDIR}/cus/Channel.o \
	${OBJECTDIR}/cus/MainModel.o \
	${OBJECTDIR}/cus/MyObject.o \
	${OBJECTDIR}/cus/MyPlayer.o \
	${OBJECTDIR}/cus/MyThread.o \
	${OBJECTDIR}/cus/RTP.o \
	${OBJECTDIR}/cus/Session.o \
	${OBJECTDIR}/cus/TCPChannel.o \
	${OBJECTDIR}/cus/UDPChannel.o \
	${OBJECTDIR}/cus/tinyxml2.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/mypcm.o \
	${OBJECTDIR}/wavsaver.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-rdynamic -std=c++17 -Wno-unused-parameter -Wno-unused-result -Wno-multichar
CXXFLAGS=-rdynamic -std=c++17 -Wno-unused-parameter -Wno-unused-result -Wno-multichar

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread -lopus -logg -lmpg123

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ago18bks

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ago18bks: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ago18bks ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/AudioSaver.o: AudioSaver.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/AudioSaver.o AudioSaver.cpp

${OBJECTDIR}/BRU2JSON.o: BRU2JSON.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BRU2JSON.o BRU2JSON.cpp

${OBJECTDIR}/BaseSoundSender.o: BaseSoundSender.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BaseSoundSender.o BaseSoundSender.cpp

${OBJECTDIR}/BaseWriter.o: BaseWriter.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BaseWriter.o BaseWriter.cpp

${OBJECTDIR}/BufferedSoundSender.o: BufferedSoundSender.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BufferedSoundSender.o BufferedSoundSender.cpp

${OBJECTDIR}/ConfigServer.o: ConfigServer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ConfigServer.o ConfigServer.cpp

${OBJECTDIR}/MyUDPServer.o: MyUDPServer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyUDPServer.o MyUDPServer.cpp

${OBJECTDIR}/MyWSClient.o: MyWSClient.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyWSClient.o MyWSClient.cpp

${OBJECTDIR}/OGGWriter.o: OGGWriter.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OGGWriter.o OGGWriter.cpp

${OBJECTDIR}/PDServer.o: PDServer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PDServer.o PDServer.cpp

${OBJECTDIR}/PDUDPServer.o: PDUDPServer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PDUDPServer.o PDUDPServer.cpp

${OBJECTDIR}/PacketParser.o: PacketParser.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PacketParser.o PacketParser.cpp

${OBJECTDIR}/Snd2Json.o: Snd2Json.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Snd2Json.o Snd2Json.cpp

${OBJECTDIR}/SocketSoundSender.o: SocketSoundSender.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SocketSoundSender.o SocketSoundSender.cpp

${OBJECTDIR}/SoundSenderFile.o: SoundSenderFile.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SoundSenderFile.o SoundSenderFile.cpp

${OBJECTDIR}/TimeSync.o: TimeSync.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TimeSync.o TimeSync.cpp

${OBJECTDIR}/UDPSoundSender.o: UDPSoundSender.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/UDPSoundSender.o UDPSoundSender.cpp

${OBJECTDIR}/WSServer.o: WSServer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/WSServer.o WSServer.cpp

${OBJECTDIR}/clipsworker.o: clipsworker.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/clipsworker.o clipsworker.cpp

${OBJECTDIR}/cus/Channel.o: cus/Channel.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/Channel.o cus/Channel.cpp

${OBJECTDIR}/cus/MainModel.o: cus/MainModel.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/MainModel.o cus/MainModel.cpp

${OBJECTDIR}/cus/MyObject.o: cus/MyObject.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/MyObject.o cus/MyObject.cpp

${OBJECTDIR}/cus/MyPlayer.o: cus/MyPlayer.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/MyPlayer.o cus/MyPlayer.cpp

${OBJECTDIR}/cus/MyThread.o: cus/MyThread.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/MyThread.o cus/MyThread.cpp

${OBJECTDIR}/cus/RTP.o: cus/RTP.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/RTP.o cus/RTP.cpp

${OBJECTDIR}/cus/Session.o: cus/Session.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/Session.o cus/Session.cpp

${OBJECTDIR}/cus/TCPChannel.o: cus/TCPChannel.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/TCPChannel.o cus/TCPChannel.cpp

${OBJECTDIR}/cus/UDPChannel.o: cus/UDPChannel.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/UDPChannel.o cus/UDPChannel.cpp

${OBJECTDIR}/cus/tinyxml2.o: cus/tinyxml2.cpp
	${MKDIR} -p ${OBJECTDIR}/cus
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cus/tinyxml2.o cus/tinyxml2.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/mypcm.o: mypcm.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mypcm.o mypcm.cpp

${OBJECTDIR}/wavsaver.o: wavsaver.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DWRT -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/wavsaver.o wavsaver.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
