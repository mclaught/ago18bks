#include "timeeditor.h"
#include "ui_timeeditor.h"
#include <QDateTime>

TimeEditor::TimeEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimeEditor)
{
    ui->setupUi(this);
}

TimeEditor::~TimeEditor()
{
    delete ui;
}

void TimeEditor::setTime(qint64 value){
    QTime tm = QTime::fromMSecsSinceStartOfDay(value*1000);
    ui->edSec->setValue(tm.second());
    ui->edMin->setValue(tm.minute());
    ui->edHr->setValue(tm.hour());
//    ui->edSec->setValue(value % 60);
//    value /= 60;
//    ui->edMin->setValue(value % 60);
//    value /= 60;
//    ui->edHr->setValue(value);
}

qint64 TimeEditor::getTime(){
//    return ui->edHr->value()*3600 + ui->edMin->value()*60 + ui->edSec->value();
    QTime tm;
    tm.setHMS(ui->edHr->value(), ui->edMin->value(), ui->edSec->value());
    return tm.msecsSinceStartOfDay()/1000;
}
