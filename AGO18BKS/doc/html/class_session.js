var class_session =
[
    [ "Session", "class_session.html#ad92ef09b872c9227e38a6efdd4d8a837", null ],
    [ "Session", "class_session.html#a4d881b9c0ded11818ba66b5f6bb5a5f4", null ],
    [ "Session", "class_session.html#a5517c92b5497ab681e627ae1e0d3e8f7", null ],
    [ "~Session", "class_session.html#a8753bb9dee966b7d39abc9b7237cd665", null ],
    [ "addTarget", "class_session.html#a0324d036f9990d96bfbce456e77b854d", null ],
    [ "addTargets", "class_session.html#a8c01f74cc8a3ecafa59ce6e0afabbd06", null ],
    [ "close_me", "class_session.html#a6a6444237d5fa1dc8d4fc8f9e5dc67b4", null ],
    [ "exec", "class_session.html#a5e89b88ecafdc865703a29bc4295e1c8", null ],
    [ "getTargets", "class_session.html#a43e3e2d96aa4eef7b7b18c88932c12a6", null ],
    [ "init", "class_session.html#a2ef42885182477e834362d92e770ad97", null ],
    [ "removeTarget", "class_session.html#aada0260ca14e655ecd30a88679e628ba", null ],
    [ "removeTargets", "class_session.html#aa8300a0adeb2e73649741287077d2619", null ]
];