class Settings {
  Bks _bks;
  List<Ins> _ins;
  int _insCnt;
  List<Outs> _outs;
  int _outsCnt;
  Rec _rec;
  Time _time;

  Bks get bks => _bks;
  List<Ins> get ins => _ins;
  int get insCnt => _insCnt;
  List<Outs> get outs => _outs;
  int get outsCnt => _outsCnt;
  Rec get rec => _rec;
  Time get time => _time;

  Settings({
      Bks bks, 
      List<Ins> ins, 
      int insCnt, 
      List<Outs> outs, 
      int outsCnt, 
      Rec rec, 
      Time time}){
    _bks = bks;
    _ins = ins;
    _insCnt = insCnt;
    _outs = outs;
    _outsCnt = outsCnt;
    _rec = rec;
    _time = time;
}

  Settings.fromJson(dynamic json) {
    _bks = json["bks"] != null ? Bks.fromJson(json["bks"]) : null;
    if (json["ins"] != null) {
      _ins = [];
      json["ins"].forEach((v) {
        _ins.add(Ins.fromJson(v));
      });
    }
    _insCnt = json["insCnt"];
    if (json["outs"] != null) {
      _outs = [];
      json["outs"].forEach((v) {
        _outs.add(Outs.fromJson(v));
      });
    }
    _outsCnt = json["outsCnt"];
    _rec = json["rec"] != null ? Rec.fromJson(json["rec"]) : null;
    _time = json["time"] != null ? Time.fromJson(json["time"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_bks != null) {
      map["bks"] = _bks.toJson();
    }
    if (_ins != null) {
      map["ins"] = _ins.map((v) => v.toJson()).toList();
    }
    map["insCnt"] = _insCnt;
    if (_outs != null) {
      map["outs"] = _outs.map((v) => v.toJson()).toList();
    }
    map["outsCnt"] = _outsCnt;
    if (_rec != null) {
      map["rec"] = _rec.toJson();
    }
    if (_time != null) {
      map["time"] = _time.toJson();
    }
    return map;
  }

}

class Time {
  String _ntp;
  int _time;

  String get ntp => _ntp;
  int get time => _time;

  Time({
      String ntp, 
      int time}){
    _ntp = ntp;
    _time = time;
}

  Time.fromJson(dynamic json) {
    _ntp = json["ntp"];
    _time = json["time"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["ntp"] = _ntp;
    map["time"] = _time;
    return map;
  }

}

class Rec {
  int _bandwidth;
  int _bitrate;
  int _format;
  int _signal;
  int _storeDays;

  int get bandwidth => _bandwidth;
  int get bitrate => _bitrate;
  int get format => _format;
  int get signal => _signal;
  int get storeDays => _storeDays;

  Rec({
      int bandwidth, 
      int bitrate, 
      int format, 
      int signal, 
      int storeDays}){
    _bandwidth = bandwidth;
    _bitrate = bitrate;
    _format = format;
    _signal = signal;
    _storeDays = storeDays;
}

  Rec.fromJson(dynamic json) {
    _bandwidth = json["bandwidth"];
    _bitrate = json["bitrate"];
    _format = json["format"];
    _signal = json["signal"];
    _storeDays = json["storeDays"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["bandwidth"] = _bandwidth;
    map["bitrate"] = _bitrate;
    map["format"] = _format;
    map["signal"] = _signal;
    map["storeDays"] = _storeDays;
    return map;
  }

}

class Outs {
  int _ch;
  String _name;
  String _target;

  int get ch => _ch;
  String get name => _name;
  String get target => _target;

  Outs({
      int ch, 
      String name, 
      String target}){
    _ch = ch;
    _name = name;
    _target = target;
}

  Outs.fromJson(dynamic json) {
    _ch = json["ch"];
    _name = json["name"];
    _target = json["target"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["ch"] = _ch;
    map["name"] = _name;
    map["target"] = _target;
    return map;
  }

}

class Ins {
  int _ch;
  String _name;

  int get ch => _ch;
  String get name => _name;

  Ins({
      int ch, 
      String name}){
    _ch = ch;
    _name = name;
}

  Ins.fromJson(dynamic json) {
    _ch = json["ch"];
    _name = json["name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["ch"] = _ch;
    map["name"] = _name;
    return map;
  }

}

class Bks {
  String _name;
  int _serverAddr;

  String get name => _name;
  int get serverAddr => _serverAddr;

  Bks({
      String name, 
      int serverAddr}){
    _name = name;
    _serverAddr = serverAddr;
}

  Bks.fromJson(dynamic json) {
    _name = json["name"];
    _serverAddr = json["serverAddr"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = _name;
    map["serverAddr"] = _serverAddr;
    return map;
  }

}