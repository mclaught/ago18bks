/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OGGWriter.cpp
 * Author: user
 * 
 * Created on 26 ноября 2019 г., 23:34
 */

#include "OGGWriter.h"
#include <math.h>
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include "ConfigServer.h"
#include "cus/log.h"

using namespace std;

OGGWriter::OGGWriter(string filename, string descr) : BaseWriter() {
    descr = "TITLE="+descr;
    
    //OPUS
    int error;
    opus = opus_encoder_create(AUDIO_FREQ, 1, OPUS_APPLICATION_AUDIO, &error);
    if(error){
        cerr << red << "OPUS error: " << error << clrst << endl;
    }
    
    error = opus_encoder_ctl(opus, OPUS_SET_VBR(0));
    if(error){
        cerr << red << "OPUS error: " << error << clrst << endl;
    }
    
    int bitrate = conf_server.getRecParam("bitrate", OPUS_BITRATE);
    error = opus_encoder_ctl(opus, OPUS_SET_BITRATE(bitrate));//OPUS_BITRATE
    if(error){
        cerr << red << "OPUS error: " << error << clrst << endl;
    }
    
    int signal = conf_server.getRecParam("signal", OPUS_SIGNAL_MUSIC);
    error = opus_encoder_ctl(opus, OPUS_SET_SIGNAL(signal));
    if(error){
        cerr << red << "OPUS error: " << error << clrst << endl;
    }
    
    int bandwidth = conf_server.getRecParam("bandwidth", OPUS_BANDWIDTH_FULLBAND);
    error = opus_encoder_ctl(opus, OPUS_SET_BANDWIDTH(bandwidth));
    if(error){
        cerr << red << "OPUS error: " << error << clrst << endl;
    }
    
    error = opus_encoder_ctl(opus, OPUS_SET_EXPERT_FRAME_DURATION(OPUS_FRAMESIZE_5_MS));
    if(error){
        cerr << red << "OPUS error: " << error << clrst << endl;
    }
    
    file.open(filename, ios_base::out | ios_base::binary);

    //OGG
    ogg_page page;
    srand(time(NULL));
    ogg_stream_init(&strm, rand());
    
    //Header
    opus_head_t opus_head;
    opus_head.version = 1;
    opus_head.channels = 1;
    opus_head.pre_skip = 0x138;
    opus_head.sample_rate = AUDIO_FREQ;
    opus_head.out_gain = 0;
    opus_head.mapping_family = 0;
    
    packet.b_o_s = 1;
    packet.e_o_s = 0;
    packet.granulepos = 0;
    packet.packetno = 0;
    packet.bytes = sizeof(opus_head_t);
    packet.packet = reinterpret_cast<unsigned char*>(&opus_head);
    ogg_stream_packetin(&strm, &packet);
    
    if(ogg_stream_flush(&strm, &page)){
        file.write(reinterpret_cast<char*>(page.header), page.header_len);
        file.write(reinterpret_cast<char*>(page.body), page.body_len);
    }
    
    //Tags
    opus_tags_t tags;
    tags.vendor_length = 8;
    memcpy(reinterpret_cast<char*>(tags.vendor), "AGO18BKS", 8);
    tags.comment_list_length = 2;
    
    uint8_t* comment = reinterpret_cast<uint8_t*>(tags.comments);
    reinterpret_cast<opus_comment_t*>(comment)->length = descr.length();
    strncpy(reinterpret_cast<opus_comment_t*>(comment)->text, descr.c_str(), descr.length());
    comment += (descr.length()+4);
    
    packet.b_o_s = 0;
    packet.e_o_s = 0;
    packet.granulepos = 0;
    packet.packetno = 1;
    packet.bytes = 24+8+descr.length();//sizeof(opus_tags_t)
    packet.packet = reinterpret_cast<unsigned char*>(&tags);
    ogg_stream_packetin(&strm, &packet);
    
    if(ogg_stream_flush(&strm, &page)){
        file.write(reinterpret_cast<char*>(page.header), page.header_len);
        file.write(reinterpret_cast<char*>(page.body), page.body_len);
    }
    
    packet.granulepos = AUDIO_FREQ;
    packet.packetno = 2;
}

OGGWriter::OGGWriter(const OGGWriter& orig) : BaseWriter() {
}

OGGWriter::~OGGWriter() {
    if(pack_cnt)
        writePage(false);
    file.close();
    
    opus_encoder_destroy(opus);
}

void OGGWriter::writePage(bool eof){
    ogg_page page;
    if(ogg_stream_pageout(&strm, &page)){
        cout << "OGG write page" << endl;
        file.write(reinterpret_cast<char*>(page.header), page.header_len);
        file.write(reinterpret_cast<char*>(page.body), page.body_len);
    }
}

void OGGWriter::write(int16_t* pcm, int samples){
    uint8_t opus_pack[OPUS_PACKET_SIZE];
    int res = opus_encode(opus, reinterpret_cast<const opus_int16*>(pcm), samples, opus_pack, OPUS_PACKET_SIZE);
    if(res > 0){
        packet.packet = opus_pack;
        packet.bytes = OPUS_PACKET_SIZE;

        ogg_stream_packetin(&strm, &packet);

        packet.packetno++;
        packet.granulepos += samples;
        packet.b_o_s = 0;
        pack_cnt++;

        if(pack_cnt == PAGE_PACKETS){
            writePage(false);
            pack_cnt = 0;
        }
    }else if(res < 0){
        cerr << red << "OPUS error: " << res << clrst << endl;
        return;
    }

}
