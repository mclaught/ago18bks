#include <unistd.h>

#include "SoundSenderFile.h"
#include "clipsworker.h"

#define FLAG_FILE "/run/.ago18bks_busy"

ClipsWorker clips_worker;

ClipsWorker::ClipsWorker() : MyThread()
{
}

void ClipsWorker::start()
{
    FILE* file = fopen(FLAG_FILE,"wb");
    if(file)
        fclose(file);
    MyThread::start();
}

void ClipsWorker::terminate()
{
//    printf("CliipsWworler.terminate()\n");
    MyThread::terminate();
    unlink(FLAG_FILE);
}

void ClipsWorker::exec()
{
    while(!terminated){
        auto fileSenders = SoundSenderFile::getFileSenders();

        for(auto sender : fileSenders){
            if(sender->isBusy()){
                sender->read_file();
            }
        }

        usleep(1000);
    }
}
