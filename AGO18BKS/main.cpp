/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 26 ноября 2019 г., 13:50
 */

#include <cstdlib>
#include <syslog.h>
#include <signal.h>
#ifndef NO_STACK_TRACE
#include <execinfo.h>
#endif
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <fstream>
#include <stdarg.h>
#include <iomanip>

#include "MyUDPServer.h"
#include "ConfigServer.h"
#include "PDServer.h"
#include "PDUDPServer.h"
#include "SoundSenderFile.h"
#include "cus/MainModel.h"
#include "cus/log.h"
#include "clipsworker.h"
#include "proto.h"
#include "sched_dl.h"

#if UDP_SEND_TYPE == THREAD_SEND
    #define VER "24.09.03-02"
#elif UDP_SEND_TYPE == TIMER_SEND
    #define VER "24.09.03-02-tmr"
#else
    #define VER "24.09.03-02-smpl"
#endif

using namespace std;

//void log_error(const char* str)
//{
//    ERR(str);
//    syslog(LOG_DEBUG | LOG_LOCAL0, "%s", str.c_str());
//}

void log_error(const char* msg, ...){
    va_list args;
    va_start(args, msg);
    
    char str[256];
    sprintf(str, msg, va_arg(args, void *));
    
    ERR(str);
//    syslog(LOG_DEBUG | LOG_LOCAL0, "%s", str);
}

static void signal_error(int sig, siginfo_t *si, void *ptr)
{
#ifndef NO_STACK_TRACE
    void* ErrorAddr;
    void* Trace[16];
    int x;
    int TraceSize;
    char** Messages;
    
    // запишем в лог что за сигнал пришел


#if __WORDSIZE == 64 // если дело имеем с 64 битной ОС
    // получим адрес инструкции которая вызвала ошибку
    cerr << "Signal: " << strsignal(sig) << "(" << sig << ")" << ", Addr: 0x" << hex << (uint64_t) si->si_addr << endl;
//    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%lu\n", strsignal(sig), (uint64_t)si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_RIP];
#else 
    // получим адрес инструкции которая вызвала ошибку
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%X\n", strsignal(sig), (int) si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_EIP];
#endif

    // произведем backtrace чтобы получить весь стек вызовов 
    TraceSize = backtrace(Trace, 16);
    Trace[1] = ErrorAddr;

    // получим расшифровку трасировки
    Messages = backtrace_symbols(Trace, TraceSize);
    if (Messages)
    {
        log_error("== Backtrace ==");

        // запишем в лог
        for (x = 1; x < TraceSize; x++)
        {
            log_error(Messages[x]);
        }

        log_error("== End Backtrace ==");
        free(Messages);
    }
#endif
    
    closelog();
    _exit(0);
}

void UnsubscribeSignals();

void stop_servers(){
    UnsubscribeSignals();
    
//    clips_worker.terminate();
    SoundSenderFile::stopAll();
    conf_server.stop();
//    pd_server.stop();
//    pd_udp_server.stop();
    if(main_model)
        delete main_model;
    server.stop();
    exit(0);
}

chrono::time_point<chrono::high_resolution_clock> last_tm;
void start_timer(int period){

    struct itimerval tv;
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = period;  // when timer expires, reset to 100ms
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = period;   // 100 ms == 100000 us
    if(setitimer(ITIMER_REAL, &tv, NULL))
        cerr << red << "settimer: " << strerror(errno) << clrst << endl;
}

void AlrmHandler(int sig)
{
#if UDP_SEND_TYPE == TIMER_SEND
    auto senders = BaseSoundSender::getPool();
    for(auto s : senders){
        if(s.second->isBusy())
            s.second->timer_event();
    }
#endif
}

void TermHandler(int sig)
{
    cerr << "caught TERM signal: " << strsignal(sig) << endl;
    stop_servers();
    //exit(0);
}

void HupHandler(int sig)
{
    cerr << "caught HUP signal: " << strsignal(sig) << endl;
//    syslog(LOG_DEBUG | LOG_LOCAL0, "caught HUP signal: %s", strsignal(sig));
    stop_servers();
}

void IntHandler(int sig)
{
    cout << strsignal(sig) << endl;
//    syslog(LOG_DEBUG | LOG_LOCAL0, "caught INT signal: %s", strsignal(sig));
    stop_servers();
    //exit(0);
}

#if UDP_SEND_TYPE == TIMER_SEND
    int ign_sig_cnt = 6;
    int ign_sig[] = {SIGUSR2, SIGPIPE, SIGTSTP, SIGPROF, SIGCHLD, SIGCONT, 0};//SIGALRM,
#else
    int ign_sig_cnt = 7;
    int ign_sig[] = {SIGUSR2, SIGPIPE, SIGTSTP, SIGPROF, SIGALRM, SIGCHLD, SIGCONT, 0};//
#endif
    int fatal_sig[] = {SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGIOT, SIGBUS, SIGFPE, SIGSEGV, SIGSTKFLT, SIGPWR, SIGSYS, 0};
    
int ConfigureSignalHandlers()
{

    for (int i = 0; ign_sig[i]; i++)
        signal(ign_sig[i], SIG_IGN);

    for (int i = 0; fatal_sig[i]; i++)
    {
        struct sigaction sigact;
        sigact.sa_flags = SA_SIGINFO;
        sigact.sa_sigaction = signal_error;
        sigemptyset(&sigact.sa_mask);
        
        sigaction(fatal_sig[i], &sigact, 0);
    }

#if UDP_SEND_TYPE == TIMER_SEND
    struct sigaction sigstpSA;
    sigstpSA.sa_handler = AlrmHandler;
    sigemptyset(&sigstpSA.sa_mask);
    sigstpSA.sa_flags = 0;
    sigaction(SIGALRM, &sigstpSA, NULL);
#endif

    struct sigaction sigtermSA;
    sigtermSA.sa_handler = TermHandler;
    sigemptyset(&sigtermSA.sa_mask);
    sigtermSA.sa_flags = 0;
    sigaction(SIGTERM, &sigtermSA, NULL);

    struct sigaction sigusr1SA;
    sigusr1SA.sa_handler = HupHandler;
    sigemptyset(&sigusr1SA.sa_mask);
    sigusr1SA.sa_flags = 0;
    sigaction(SIGUSR1, &sigusr1SA, NULL);

    struct sigaction sighupSA;
    sighupSA.sa_handler = HupHandler;
    sigemptyset(&sighupSA.sa_mask);
    sighupSA.sa_flags = 0;
    sigaction(SIGHUP, &sighupSA, NULL);

    struct sigaction sigintSA;
    sigintSA.sa_handler = IntHandler;
    sigemptyset(&sigintSA.sa_mask);
    sigintSA.sa_flags = 0;
    sigaction(SIGINT, &sigintSA, NULL);
    
    return 0;
}

void UnsubscribeSignals(){
    for (int i = 0; fatal_sig[i]; i++){
        signal(fatal_sig[i], SIG_IGN);
    }
}

void run(){
    printf("============================\n");
    printf("AGO-18 BKS %s\n\n", VER);
    
    time_t t = time(NULL);
    struct tm tm;
    localtime_r(&t, &tm);
    printf("Local time: %02d:%02d %s(%.1f)\n", tm.tm_hour, tm.tm_min, tm.tm_zone, (float)tm.tm_gmtoff/3600.0);
    gmtime_r(&t, &tm);
    printf("GM time: %02d:%02d %s(%.1f)\n", tm.tm_hour, tm.tm_min, tm.tm_zone, (float)tm.tm_gmtoff/3600.0);

    sched_param param;
#if UDP_SEND_TYPE == TIMER_SEND
    param.sched_priority = 22;
    if(sched_setscheduler(0, SCHED_RR, &param))
#else
    param.sched_priority = 0;
    if(sched_setscheduler(0, SCHED_OTHER, &param))
#endif
        cerr << red << "sched_setscheduler " << strerror(errno) << clrst << endl;

//    printf("Main thread. id=%d\n", pthread_self());

//    unsigned int flags = 0;
//    sched_attr attr;
//    attr.size = sizeof(sched_attr);
//    attr.sched_flags = 0;
//    attr.sched_nice = 0;
//    attr.sched_priority = 0;
//    attr.sched_policy = SCHED_DEADLINE;
//    attr.sched_runtime = 10*1000*1000;
//    attr.sched_deadline = 30*1000*1000;
//    attr.sched_period = 30*1000*1000;

//    if(sched_setattr(0, &attr, flags) < 0){
//        cerr << red << "sched_setattr: " << strerror(errno) << clrst << endl;
//    }

    conf_server.start();
//    pd_server.start();
//    pd_udp_server.start();
    
    if(conf_server.getNetParam("cus_enable", 0)==1){
        main_model = new MainModel();
        if(main_model->ok){
            main_model->start();
        }
    }
    
    server.start();

//    clips_worker.start();

//    server.run();

#if UDP_SEND_TYPE == TIMER_SEND
    start_timer(SEND_FRAME_SERIES*AUDIO_FRAME_MS*1000);
#endif
    
    while(true)
        sleep(1);
}

void kill_old(){
    ifstream pidfile;
    pidfile.open(PIDFILE);
    if(pidfile.is_open()){
        int pid = 0;
        pidfile >> pid;
        pidfile.close();
        
        kill(pid, SIGTERM);

//        char stop_cmd[20];
    //        sprintf(stop_cmd, "kill -9 %d; rm %s", pid, PIDFILE);
//        system(stop_cmd);
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    ConfigureSignalHandlers();
    
    if(argc == 1){
        run();
        return 0;
    }else if(argc == 2){
        string cmd = string(argv[1]);
        if(cmd == "-v"){
            cout << VER << endl;
            return 0;
        }else if(cmd == "-i"){
            conf_server.select_ifaces();
        }else if(cmd == "start"){
            //kill_old();
            
            auto pid = fork();
            if(pid == -1){
                cerr << strerror(errno) << endl;
                return -1;
            }else if(pid == 0){
                chdir("/");
//                close(STDIN_FILENO);
//                close(STDOUT_FILENO);
//                close(STDERR_FILENO);
                run();
                return 0;
            }else{
                ofstream pidfile;
                pidfile.open(PIDFILE);
                pidfile << pid;
                pidfile.close();
                return 0;
            }
        }else if(cmd == "stop"){
            kill_old();
            
            return 0;
        }else if(cmd == "status"){
            ifstream pidfile;
            pidfile.open(PIDFILE);
            if(pidfile.is_open()){
                pidfile.close();
                return 0;
            }else{
                return 1;
            }
        }
    }
    

    return 0;
}

