#ifndef MYPCM_H
#define MYPCM_H

#include <string>

using namespace std;

class MyPCM
{
private:
    string file_name;
    ifstream* file;
    int frame_duration;
    bool mono;

    void openFile(string fName);
    bool readChunk(ifstream* file, int level);
    bool checkSampleRate();
public:
    bool error;
    string error_str;
    int frame_size;
    int file_size;
    int file_pos;
    bool isRIFF;
    uint32_t format;
    uint16_t audioFormat;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t channels;
    uint16_t blockAlign;
    uint16_t bitsPerSample;

    MyPCM(string fileName, int frameDuration, int mono);
    ~MyPCM();

    void setError(string err, bool fatal=true);
    int readFrame(char* buf, int max_size);
    bool eof();
    bool needResample();
    //void resample(EqList eq_list);
//    void deleteTemp();
//    void copyTemp(string newFileName);
};

#endif // MYPCM_H
