var searchData=
[
  ['b_32',['b',['../structcodec__eq__coeff__t.html#a814619e93870ebdd89e34fce1170bb6b',1,'codec_eq_coeff_t']]],
  ['b0_33',['b0',['../structbiqd__hp__coeff__t.html#a7b0e32fb5195655ea7d67bd19bb06607',1,'biqd_hp_coeff_t::b0()'],['../structbiqd__lp__coeff__t.html#ac8810c7710c636c0ee257c32ad7ee1cc',1,'biqd_lp_coeff_t::b0()']]],
  ['b1_34',['b1',['../structbiqd__hp__coeff__t.html#a004f6ef45aa5f173edea8db55de67c20',1,'biqd_hp_coeff_t::b1()'],['../structbiqd__lp__coeff__t.html#a133e079f64e4d920fbdc5f613957f6f8',1,'biqd_lp_coeff_t::b1()']]],
  ['b2_35',['b2',['../structbiqd__hp__coeff__t.html#a43f7a2b1d54c7ff17dcdd25e81d24170',1,'biqd_hp_coeff_t::b2()'],['../structbiqd__lp__coeff__t.html#ad76278f8c29ad2244549418ac4436f7a',1,'biqd_lp_coeff_t::b2()']]],
  ['basesoundsender_36',['BaseSoundSender',['../class_base_sound_sender.html',1,'BaseSoundSender'],['../class_base_sound_sender.html#a00830721b758753d89fc3686d5452afa',1,'BaseSoundSender::BaseSoundSender()'],['../class_base_sound_sender.html#aabc4916f5355415186e0ef38ea439f10',1,'BaseSoundSender::BaseSoundSender(const BaseSoundSender &amp;orig)']]],
  ['basesoundsender_2ecpp_37',['BaseSoundSender.cpp',['../_base_sound_sender_8cpp.html',1,'']]],
  ['basesoundsender_2eh_38',['BaseSoundSender.h',['../_base_sound_sender_8h.html',1,'']]],
  ['basewriter_39',['BaseWriter',['../class_base_writer.html',1,'BaseWriter'],['../class_base_writer.html#adeb6e758fcb0aaee73f51315fa9acc9d',1,'BaseWriter::BaseWriter()'],['../class_base_writer.html#ad5b9a5e8943d52a9e239b4c26ba8bae4',1,'BaseWriter::BaseWriter(const BaseWriter &amp;orig)']]],
  ['basewriter_2ecpp_40',['BaseWriter.cpp',['../_base_writer_8cpp.html',1,'']]],
  ['basewriter_2eh_41',['BaseWriter.h',['../_base_writer_8h.html',1,'']]],
  ['biqd_5fhp_5fcoeff_5ft_42',['biqd_hp_coeff_t',['../structbiqd__hp__coeff__t.html',1,'']]],
  ['biqd_5flp_5fcoeff_5ft_43',['biqd_lp_coeff_t',['../structbiqd__lp__coeff__t.html',1,'']]],
  ['bitrate_44',['bitrate',['../structpd__start__t.html#af19689e094c2fc525c676cb10e6f6aca',1,'pd_start_t']]],
  ['bits_5fper_5fsample_45',['bits_per_sample',['../structwav__format__t.html#a3ec15dc8cd65a6545c86d9ca879c93e6',1,'wav_format_t']]],
  ['bitspersample_46',['bitsPerSample',['../class_my_p_c_m.html#a040b7232461b24d4a2d93565debaff04',1,'MyPCM']]],
  ['bitstream_5fsn_47',['bitstream_sn',['../structogg__hdr__t.html#a4f84a92cada01532f5c6f5541ae4d2a9',1,'ogg_hdr_t']]],
  ['blockalign_48',['blockAlign',['../class_my_p_c_m.html#aed54ce8302b9e86219f407bfa9b5cc4f',1,'MyPCM']]],
  ['brd_5fid_5fto_5fchan_49',['BRD_ID_TO_CHAN',['../proto_8h.html#a40ebc51e4b09451fdd39982e72227f04',1,'proto.h']]],
  ['bufferedsoundsender_50',['BufferedSoundSender',['../class_buffered_sound_sender.html',1,'BufferedSoundSender'],['../class_buffered_sound_sender.html#a0f132fb1cac1bb35a4831706b9b539a6',1,'BufferedSoundSender::BufferedSoundSender()'],['../class_buffered_sound_sender.html#aa30e921aac90f9c487cbd2681699694f',1,'BufferedSoundSender::BufferedSoundSender(const BufferedSoundSender &amp;orig)']]],
  ['bufferedsoundsender_2ecpp_51',['BufferedSoundSender.cpp',['../_buffered_sound_sender_8cpp.html',1,'']]],
  ['bufferedsoundsender_2eh_52',['BufferedSoundSender.h',['../_buffered_sound_sender_8h.html',1,'']]],
  ['byte_5frate_53',['byte_rate',['../class_my_p_c_m.html#aea1e4c8a7beae9ea3bbe39109f3eeca0',1,'MyPCM']]],
  ['bytes_5fper_5fsec_54',['bytes_per_sec',['../structwav__format__t.html#a43122364f6a249715fe902882ff5c9aa',1,'wav_format_t']]]
];
