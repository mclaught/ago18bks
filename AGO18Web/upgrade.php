<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?1" />
        
        <style>
            .log-text{
                font-size: 12px;
            }
            .cmd-text{
                font-size: 12px;
                font-weight: 600;
            }
        </style>
    </head>
    <body class="bg-light">
        <div class="container mt-3">
            <h1><img src="img/settings.svg" width="32" height="32""><span>&nbsp;&nbsp;&nbsp;</span>Обновление ПО</h1>
            <div class="row m-3">
                <a class="btn btn-outline-dark col-2" href="settings.php">&lt;Назад</a>
                <span class="col"></span>
            </div>
            <div class="card shadow p-5">
                <?php
                const fingerprint = "D97BA23B7A1156CE4A2C9F5F12DC9874EC20343B";
                
                function print_lines($txt, $color){
                    if(!$txt)
                        return;
                    $lines = explode("\n", $txt);
                    foreach ($lines as $str){
                        echo "<p class='m-0 log-text' style='color:$color;'>$str</p>";
                    }
//                    echo "<p class='m-0 log-text' style='color:$color;'>$txt</p>";
                    ob_flush();
                    flush();
                }
                
                function exec_print($cmd, $search = null){
//                    echo "<p class='cmd-text'>$cmd</p>";
                    $proc = proc_open($cmd,[
                            1 => ['pipe','w'],
                            2 => ['pipe','w'],
                        ],$pipes);
                    
                    stream_set_blocking($pipes[1], false);
                    stream_set_blocking($pipes[2], false);
                    
                    $found = 1;
                    $status = proc_get_status($proc);

                    while($status["running"] == 1){
//                        print_r($status);
                        $out = stream_get_line($pipes[1], 100, "\n"); //stream_get_contents($pipes[1]);
                        print_lines($out, "black");
                        $err = stream_get_line($pipes[2], 100, "\n"); //stream_get_contents($pipes[2]);
                        print_lines($err, "gray");
                        if(isset($search)){
                            if(strpos($out, $search) || strpos($err, $search)){
                                $found = 0;
                            }
                        }
                        
                        $status = proc_get_status($proc);
//                        print_r($status);
                    }
                    $out = stream_get_contents($pipes[1]);
                    print_lines($out, "black");
                    $err = stream_get_contents($pipes[2]);
                    print_lines($err, "grey");
                    if(isset($search)){
                        if(strpos($out, $search) || strpos($err, $search)){
                            $found = 0;
                        }
                    }
                    
//                    print_r($status);
                    
                    if(!isset($search)){
                        $found = $status["exitcode"];
                    }
                    
                    fclose($pipes[1]);
                    fclose($pipes[2]);
                    
                    proc_close($proc);
                    return $found;
                    
//                    $res = [];
//                    $code = 0;
//                    $res_text = exec($cmd, $res, $code);
//                    foreach ($res as $str) {
//                        echo "<p>$str</p>";
//                    }
//                    return $code;
                }
                
                set_time_limit(0);
                ob_start();

                $uploaddir = '/var/www/html/AGO18Web/files';
                $uploadfile = $uploaddir . "/" . basename($_FILES['file']['name']);

                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                    echo "<div class='alert alert-primary mt-3' role='alert'><h6>Проверка ключей:...</h6></div>";
                    flush();
                    ob_flush();
                    $found = exec_print("gpg --list-keys", fingerprint);
                    if($found == 0){
                        echo "<div class='alert alert-success mt-1' role='alert'>OK</div>";
                    }
                    flush();
                    ob_flush();
                    
                    if($found != 0){
                        echo "<div class='alert alert-primary mt-3' role='alert'><h6>Импорт ключей:...</h6></div>";
                        flush();
                        ob_flush();
                        $import = exec_print("gpg --import files/key.asc");
                        $found = exec_print("gpg --list-keys", fingerprint);
                        if($import != 0 || $found != 0){
                            echo "<div class='alert alert-danger mt-1' role='alert'>Ключ отсутствует!</div>";
                            exit;

                        }
                        else{
                            echo "<div class='alert alert-success mt-1' role='alert'>OK</div>";
                        }
                        flush();
                        ob_flush();
                    }
                    
                    echo "<div class='alert alert-primary mt-3' role='alert'><h6>Проверка цифровой подписи:...</h6></div>";
                    flush();
                    ob_flush();
                    if(exec_print("gpg --verify $uploadfile") != 0){
                        echo "<div class='alert alert-danger mt-1' role='alert'>Отсутствует цифровая подпись разработчика!</div>";
                        exit;
                    }else{
                        echo "<div class='alert alert-success mt-1' role='alert'>OK</div>";
                    }
                    flush();
                    ob_flush();
                    
                    echo "<div class='alert alert-primary mt-3' role='alert'><h6>Распаковка файлов:...</h6></div>";
                    flush();
                    ob_flush();
                    if(exec_print("gpg --decrypt $uploadfile | tar -x  -C /tmp && ls -l /tmp") != 0){//
                        echo "<div class='alert alert-danger mt-1' role='alert'>Ошибка при распаковке. Возможно файл поврежден.</div>";
                        exit;
                    }else{
                        echo "<div class='alert alert-success mt-1' role='alert'>OK</div>";
                    }
                    flush();
                    ob_flush();
                    
                    echo "<div class='alert alert-primary mt-3' role='alert'><h6>Установка:...</h6></div>";
                    flush();
                    ob_flush();
                    if(file_exists("/tmp/install.sh")){
                        if(exec_print("cd /tmp && chmod +x install.sh && sudo ./install.sh") != 0){
                            echo "<div class='alert alert-danger mt-1' role='alert'>Ошибка при установке.</div>";
                            exit;
                        }else{
                            echo "<div class='alert alert-success mt-1' role='alert'>Установка обновления завершена!</div>";
                        }
                        unlink("/tmp/install.sh");
                    }else{
                        echo "<div class='alert alert-danger mt-1' role='alert'>Файл установочного скрипта отсутствует.</div>";
                        exit;
                    }
                    flush();
                    ob_flush();

//                    $cmd = "sudo $uploaddir/install.sh";
    //                echo "<p>$cmd</p>";
//                    exec_print($cmd);
                } else {
                    echo "<div class='alert alert-danger mt-1' role='alert'>Ошибка при загрузке файла</div>";
                    exit;
                }
                
                ob_end_flush();
                ?>
            </div>        
            <div class="row m-3">
                <a class="btn btn-outline-dark col-2" href="settings.php">&lt;Назад</a>
                <span class="col"></span>
            </div>
        </div>
    </body>
</html>
