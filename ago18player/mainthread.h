#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include "MyThread.h"
#include "mypcm.h"

#include <mutex>
#include <atomic>

using namespace std;

#define AUDIO_FRAME_MS		5
#define AUDIO_FREQ		48000
#define AUDIO_SAMPLES		(AUDIO_FRAME_MS * AUDIO_FREQ / 1000)
#define AUDIO_LEN_BYTES         AUDIO_SAMPLES * 2
#define FIFO_SIZE (1000*AUDIO_SAMPLES)

#pragma pack(push, 1)

typedef uint32_t ch_mask_t;

typedef struct
{
  uint8_t adr_to;//номер платы назначения (адрес ПК 0x80)
  uint8_t adr_from;//номер платы отправителя
  uint8_t repeate_cnt;//счетчик повторов от 0
  uint8_t type;// тип пакета
  uint16_t len;//длина оставшейся части пакета
}pack_hdr_t;//заголовок любого пакета

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    uint8_t data[];
}packet_command_t;

typedef struct
{
  uint8_t sender;//номер канала отправителя.
  ch_mask_t recip;//бит. маска получателей
}audio_hdr_t;//добавочный заголовок аудиопакета для 32 получателей

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    audio_hdr_t audio_hdr;
    int16_t pcm[AUDIO_SAMPLES*2];
}packet_audio_t;

#pragma pack(pop)

class MainThread : public MyThread
{
public:
    MainThread(string fileName, string ip_addr, uint8_t in, ch_mask_t outs, uint8_t server_addr, bool autorew);

    int sent = 0;

    void read_file();
    void sendSoundPacket(int cnt);
private:
    int sock;
    string fileName;
    uint8_t server_addr = 0x80;
    string broadcast;
    mutex sock_mutex;
    bool first = true;
    atomic_int fifo_cnt = 0;
    uint8_t in_ch;
    ch_mask_t outs;
    int16_t fifo[FIFO_SIZE];
    int fifo_in = 0;
    int fifo_out = 0;
    shared_ptr<MyPCM> pcm;
    uint8_t pack_n = 0;
//    int16_t buffer[AUDIO_SAMPLES*1000];
    bool autorew = false;

    virtual void exec()override;
    void sendCommand(uint8_t *data, int len, uint8_t type, uint8_t cmd, int repeats, uint8_t addr);
    void addOpusPacket(const unsigned char *data, int32_t buf_len);
    int read_block(int samples);
    void dumpPacket(pack_hdr_t* pack, int len);
};

#endif // MAINTHREAD_H
