#ifndef FILEINFO_H
#define FILEINFO_H

#include <QObject>
#include <QSharedPointer>
#include "json.hpp"

using namespace nlohmann;

const QString infoDirName = "info";

class FileInfo;
typedef QSharedPointer<FileInfo> PFileInfo;
class FileInfo : public QObject
{
    Q_OBJECT
public:

    QString name;
    QString path;
    QString infoPath;
    quint64 targets = 0;
    int volume = 100;
    bool active_task = false;
    qint64 begin = 0;
    qint64 end = 0;
    qint64 period = 0;
    bool bad = false;

    explicit FileInfo(QString filePath, QObject *parent = nullptr);
    explicit FileInfo(QString dir, QString name, QObject *parent = nullptr);
    void save();

    static PFileInfo forFile(QString filePath, QObject *parent = nullptr);
    static PFileInfo forNew(QString dir, QString name, QObject *parent = nullptr);
    bool isSignaled(qint64 time);
    void resetTask();
    QString toTaskString();
    json toJson();
    void reload();
private:
    qint64 last_signaled = 0;
    void setInfoPath();
signals:

};

#endif // FILEINFO_H
