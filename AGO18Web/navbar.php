<?php 
include 'session.php';

$pages = [
    [
        "page" => "index.php",
        "name" => "Состояние"
    ],
    [
        "page" => "events.php",
        "name" => "События"
    ],
    [
        "page" => "record.php",
        "name" => "Запись"
    ],
    [
        "page" => "dropdownSetts",
        "name" => "Настройки",
        "items" => [
            [
                "page" => "settings.php",
                "name" => "Общие настройки"
            ],
            [
                "page" => "comm_table.php",
                "name" => "Таблица коммутации"
            ],
            [
                "page" => "sound.php",
                "name" => "Настройки звука"
            ],
            [
                "page" => "clips.php",
                "name" => "Аудио-клипы"
            ],
            [
                "page" => "bru_cfg.php",
                "name" => "БРУ"
            ],
            [
                "page" => "chg_id.php",
                "name" => "Настройка ID"
            ],
            [
                "page" => "inp_modes.php",
                "name" => "Режимы входов для UART"
            ]
        ],
    ],
    [
        "page" => "log.php",
        "name" => "Лог"
    ]
];
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#" id="bksName">БКС</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php
        foreach ($pages as $pg){
            $link = $pg['page'];
            $name = $pg['name'].($cur_page == $link ? "<span class='sr-only'>(current)</span>" : "");
            $active = $cur_page == $link ? 'active' : '';
            
            if(strpos($link, "dropdown") === FALSE){
                echo "<li class='nav-item $active'>";
                echo "<a class='nav-link' href='$link'>$name</a>";
                echo "</li>";
            }else{
                $items = $pg['items'];
                
                foreach ($items as $item){
                    $item_link = $item['page'];
                    if($cur_page == $item_link)
                        $active = 'active';
                }
                
                echo "<li class='nav-item dropdown $active'>";
                echo "  <a class='nav-link dropdown-toggle' href='#' id='$link' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                echo "      $name";
                echo "  </a>";
                echo "  <div class='dropdown-menu' aria-labelledby='$link'>";
                
                foreach ($items as $item){
                    $item_link = $item['page'];
                    $item_name = $item['name'];
                    $item_active = $cur_page == $item_link ? 'active' : '';
                    echo "<a class='dropdown-item $item_active' href='$item_link'>$item_name</a>";
                }
                
                echo "  </div>";
                echo "</li>";
            }
        }
        ?>
    </ul>
    <?php  
    if(isset($nb_add))
        echo $nb_add;
    ?>
      
    <a class="btn text-light" href="login.html" title="Выход">
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-down-right-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
        <path fill-rule="evenodd" d="M5.172 5.172a.5.5 0 0 1 .707 0l4.096 4.096V6.5a.5.5 0 1 1 1 0v3.975a.5.5 0 0 1-.5.5H6.5a.5.5 0 0 1 0-1h2.768L5.172 5.879a.5.5 0 0 1 0-.707z"/>
      </svg>      
    </a>  
      
<!--    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Поиск">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
    </form>-->
  </div>
</nav>
