#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"
#include <QSettings>
#include <QMessageBox>
#include "clipsdialog.h"
#include "filesdialog.h"
#include "filesoundsource.h"
#include "keyboard.h"
#include "remotesetts.h"
#include "scheduller.h"
#include "wavsaver.h"
//#include "json.hpp"
#include <QCoreApplication>
#include <QDebug>
#include <QFileDialog>
#include <QFuture>
#include <QMessageBox>
#include <QScreen>
#include <QShortcut>
#include <QtConcurrent>
#include <QStandardPaths>
#include <QInputDialog>
#include "log.h"

QSettings setts("Elcom", "AGO18PD");

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->btClose->setVisible(false);

//    setWindowFlags(Qt::WindowStaysOnTopHint);
    showFullScreen();
    showCursor();

    if(setts.value("audio_folder", AgoBase::defAudioPath()).toString().isEmpty()){
        auto paths =  QStandardPaths::standardLocations(QStandardPaths::MusicLocation);
        if(paths.size())
            setts.setValue("audio_folder", paths.at(0));
    }

    QShortcut *shortcut = new QShortcut (QKeySequence(static_cast<int>(Qt::CTRL) + Qt::Key_S  ), this);
    connect (shortcut, SIGNAL(activated()), this, SLOT(on_actionSetts_triggered()));

    ui->volGauge->setValue(0);
    createSoundReader();
    ui->slVolume->setValue(setts.value("micVolume", 100).toInt());
    ui->slVolume->setVisible(false);

    cbBKSOn = new QCheckBox(this);
    cbBKSOn->setText("БКС");
    ui->statusbar->addWidget(cbBKSOn);

    cbTangenta = new QCheckBox(this);
    cbTangenta->setText("Мик");
    ui->statusbar->addWidget(cbTangenta);

    statusLabel = new QLabel(this);
    ui->statusbar->addWidget(statusLabel);

    playPorgress = new QProgressBar();
    playPorgress->setVisible(false);
    playPorgress->setTextVisible(false);
    ui->statusbar->addWidget(playPorgress);

    initAgo();

    connect(&scheduller, &Scheduller::signaled, [this](PFileInfo item){
        if(ago && !ago->isBusy() && item.get()){
            item->resetTask();
            qDebug() << "Signaled:" << item->path;
            ago->playFile(item->path, item->targets ? item->targets : getOuts());
        }
    });
    connect(&scheduller, &Scheduller::periodic, [this](qint64 time){
        auto tm = QTime::fromMSecsSinceStartOfDay(time*1000);
        ui->labelTime->setText(tm.toString("HH:mm:ss"));
    });
    connect(&scheduller, &Scheduller::badFile, [this](QString name){
        onStatusLabel("Разрушена информация о клипах. Проверьте настройки аудио-клипов.");
    });

    scheduller.load();
    scheduller.start();
    if(scheduller.hasBadFiles){
        playFile(false);
    }
}

MainWindow::~MainWindow()
{
    scheduller.stop();

    if(ago){
        ago->deleteLater();
        ago = nullptr;
    }
    statusLabel = nullptr;

    delete ui;
}

void MainWindow::onError(QString title, QString msg){
    QMessageBox::warning(this, title, msg);
}

void MainWindow::initAgo(){
    if(ago)
        ago->deleteLater();

    if(setts.value("ago", 1).toInt() == 0){
        ago = new Ago18(this);
    }else{
        ago = new Ago09(setts.value("targets_cnt", 16).toInt(), this);
        ui->actionUpload->setVisible(false);
        ui->cbClips->setVisible(false);
        ui->btPlay->setVisible(false);
    }

    connect(ago, &AgoBase::onStatusLabel, this, &MainWindow::onStatusLabel);
    connect(ago, &AgoBase::onState, this, &MainWindow::onState);
    connect(ago, &AgoBase::onProgress, this, &MainWindow::onProgress);
    connect(ago, &AgoBase::enableCtrls, this, &MainWindow::enableCtrls);
    connect(ago, &AgoBase::onActivity, this, &MainWindow::onActivity);
    connect(ago, &AgoBase::createOutChecks, this, &MainWindow::createOutChecks);
    connect(ago, &AgoBase::onClips, this, &MainWindow::onClips);
    connect(ago, &AgoBase::on_actionSetts_triggered, this, &MainWindow::on_actionSetts_triggered);
    connect(ago, &AgoBase::onBKSActive, this, &MainWindow::onBKSActive);
    connect(ago, &AgoBase::onError, this, &MainWindow::onError);
    connect(ago, &AgoBase::on_tangenta, this, &MainWindow::on_tangenta);
    connect(ago, &AgoBase::onVolume, this, &MainWindow::onVolume);
    connect(ago, &AgoBase::onActiveTargets, this, &MainWindow::onActiveTargets);

    ago->init();
}

void MainWindow::createSoundReader(){
    reader = new SoundReader(setts.value("audio_device", "").toString(), this);
    connect(reader, &SoundReader::onDebug, this, [this](QString str){
        statusLabel->setText(str);
    });
    auto devs = reader->devList();
    qDebug() << "Audio devs:";
    for(int i=0; i<devs.size(); i++){
        qDebug() << devs[i].deviceName();
    }

    auto dev = SoundReader::defDevice();
    auto fmt = SoundReader::usedFormat();
    setWindowTitle(
                QString("АГО-18-ПД (%1    %2Гц, %3бит, %4)").arg(dev.deviceName())
                    .arg(fmt.sampleRate()).arg(fmt.sampleSize()).arg(fmt.codec())
                );

    ui->actionStart->setEnabled(!dev.isNull());

    connect(reader, &SoundReader::on_level, this, &MainWindow::on_level);

    ui->slVolume->setEnabled(reader->isVolumeSupported());
}

void MainWindow::onStatusLabel(QString text)
{
    if(statusLabel)
        statusLabel->setText(text);
}

void MainWindow::onState(AgoBase::State state)
{
    switch(state){
    case AgoBase::Stop:
        ui->actionStart->setChecked(false);
        ui->actionStart->setEnabled(true);
        ui->actionFile->setChecked(false);
        ui->actionFile->setEnabled(true);
        ui->actionUpload->setEnabled(true);
        ui->btFiles->setEnabled(true);
        ui->btSetts->setEnabled(true);
        break;

    case AgoBase::Mic:
        ui->actionStart->setChecked(true);
        ui->actionStart->setEnabled(true);
        ui->actionFile->setChecked(false);
        ui->actionFile->setEnabled(false);
        ui->actionUpload->setEnabled(false);
        ui->btFiles->setEnabled(false);
        ui->btSetts->setEnabled(false);
        break;

    case AgoBase::File:
        ui->actionStart->setChecked(false);
        ui->actionStart->setEnabled(true);
        ui->actionFile->setChecked(true);
        ui->actionFile->setEnabled(true);
        ui->actionUpload->setEnabled(false);
        ui->btFiles->setEnabled(false);
        ui->btSetts->setEnabled(false);
        break;

    case AgoBase::Rec:
        ui->actionStart->setChecked(false);
        ui->actionStart->setEnabled(false);
        ui->actionFile->setChecked(false);
        ui->actionFile->setEnabled(false);
        ui->actionUpload->setEnabled(false);
        ui->btFiles->setEnabled(true);
        ui->btSetts->setEnabled(false);
        break;
    }
}

void MainWindow::onProgress(qint64 total, qint64 val, bool visible)
{
    playPorgress->setMaximum(static_cast<int>(total));
    playPorgress->setValue(static_cast<int>(val));
    playPorgress->setVisible(visible);
}


void MainWindow::createOutChecks(int outs_cnt, QStringList names){
    this->names = names;
    auto outs = setts.value("outs", 0).toInt();
    while(this->names.size() < outs_cnt){
        this->names.push_back(QString::number(this->names.size()+1));
    }
    ui->outChecks->setTargets(this->names, outs_cnt);
    ui->outChecks->setOuts(outs);
    connect(ui->outChecks, &TargetsCheckWidget::onChecked, this, &MainWindow::onCheckOut);

    if(ago){
        ago->setTargets(outs);
    }

//    dstCheck.clear();
//    auto outs = setts.value("outs", 0).toInt();
//    for(int i=0; i<outs_cnt; i++){
//        WordWrapCheck* check = new WordWrapCheck(this);

//        if(i<names.size() && names[i] != "")
//            check->setText(QString("%1").arg(names[i]));
//        else
//            check->setText(QString("%1").arg(i+1));
//        ui->destChLayout->addWidget(check, i / 4, i % 4);
//        ui->destChLayout->setRowStretch(i / 4, 1);
//        ui->destChLayout->setRowMinimumHeight(i/4, 30);

//        connect(check->getCheckBox(), &QCheckBox::clicked, [this](bool checked){
//            (void)checked;
//            auto outs = getOuts();
//            setts.setValue("outs", outs);
//            if(ago)
//                ago->setTargets(static_cast<targets_t>(outs));
//        });

//        check->setChecked(outs & (1 << i));

//        dstCheck.push_back(check);
//    }
}

void MainWindow::onCheckOut(uint64_t outs){
    if(!ago->isBusy()){
        setts.setValue("outs", QVariant::fromValue(outs));
    }
    if(ago)
        ago->setTargets(static_cast<targets_t>(outs));
}

void MainWindow::onActiveTargets(uint32_t outs, bool active)
{
//    ui->outChecks->setOuts(outs);
    ui->outChecks->setPlayState(active, outs);
//    if(active){
//        outFlashTimer.start(500);
//    }else{
//        outFlashTimer.stop();
//        ui->outChecks->setEnabled(true);
//    }
}

void MainWindow::onClips(QStringList clips)
{
    ui->cbClips->clear();

    for(auto &clip : clips){
        qDebug() << clip;
        ui->cbClips->addItem(clip);
    }
//    ui->cbClips->addItem("< Обновить... >");
    ui->cbClips->setCurrentIndex(-1);
}

void MainWindow::onBKSActive(bool active)
{
    cbBKSOn->setChecked(active);
}

void MainWindow::enableCtrls(bool enable){
    ui->actionStart->setEnabled(enable);
    ui->btPlay->setEnabled(enable);
}

void MainWindow::onActivity(qint64 outs)
{
    ui->outChecks->setChecksEnabled(~outs);
//    for(int i=0; i<dstCheck.size(); i++){
//        dstCheck[i]->setEnabled(!(outs & (1<<i)));
//    }
    file_is_play = (outs>0);
//        if(!file_is_play)
        ui->btPlay->setChecked(file_is_play);
}

//void MainWindow::getNames()
//{

//    dstCheck.clear();
//    auto names = rSt->getOutNames();
//    auto outs = setts.value("outs", 0).toInt();
//    for(int i=0; i<rSt->outsCnt; i++){
////        auto checkLayout = new QHBoxLayout(this);
////        auto check = new QCheckBox(this);
////        auto checkLabel = new QLabel(this);
////        checkLabel->setWordWrap(true);
////        checkLayout->addWidget(check);
////        checkLayout->addWidget(checkLabel);
//        WordWrapCheck* check = new WordWrapCheck(this);

//        if(i<names.size() && names[i] != "")
//            check->setText(QString("%1").arg(names[i].c_str()));
//        else
//            check->setText(QString("%1").arg(i+1));
//        ui->destChLayout->addWidget(check, i / 4, i % 4);
//        ui->destChLayout->setRowStretch(i / 4, 1);
//        ui->destChLayout->setRowMinimumHeight(i/4, 30);

//        connect(check->getCheckBox(), &QCheckBox::clicked, [this](bool checked){
//            setts.setValue("outs", getOuts());
//        });

//        check->setChecked(outs & (1 << i));

//        dstCheck.push_back(check);
//    }
//}

void MainWindow::getClips(){
    ui->cbClips->clear();

    if(!ago)
        return;

    auto clips = ago->getClips();
    for(auto &clip : clips){
        qDebug() << clip;
        ui->cbClips->addItem(clip);
    }
//    ui->cbClips->addItem("< Обновить... >");
    ui->cbClips->setCurrentIndex(-1);
}

uint32_t MainWindow::getOuts(){
    return ui->outChecks->getOuts();
//    uint32_t outs = 0;
//    for(int i=0; i<dstCheck.size(); i++){
//        if(dstCheck[i]->isChecked())
//            outs |= (1 << i);
//    }
//    return outs;
}

void MainWindow::on_btStart_clicked()
{
}

void MainWindow::on_btStop_clicked()
{
}

void MainWindow::on_level(uint16_t lvl){
    ui->volGauge->setValue(lvl);
}

void MainWindow::on_tangenta(bool state){
    cbTangenta->setChecked(state);

    if(ago){
        if(state)
            ago->micStart(getOuts(), true);
        else
            ago->micStop();
    }
}

void MainWindow::onVolume(int volume)
{
    QMutexLocker lock(&volMutex);
    ui->slVolume->setValue(volume);
}

void MainWindow::on_actionStart_triggered()
{
    if(ago){
//        if(!ago->isMicOn())
//            ago->micStart(getOuts(), false);
//        else
//            ago->micStop();
        _log.log("Button");
        ago->setTangentaEmu(!ago->getTangentaEmu());
    }
}

void MainWindow::on_actionStop_triggered()
{
}

void MainWindow::showCursor(){
    if(setts.value("cursor", true).toBool())
        setCursor(Qt::PointingHandCursor);
    else
        setCursor(Qt::BlankCursor);
}

void MainWindow::on_actionSetts_triggered()
{
    QString pass = setts.value("password", "").toString();
    if(!pass.isEmpty()){
        Keyboard kb("Пароль", "", this, Keyboard::KB_PASS);
        if(kb.exec()==QDialog::Rejected || kb.getText()!=pass){
            return;
        }
    }

    SettingsDialog dlg(ago, this);

    dlg.setAudioDevice(setts.value("audio_device", "").toString());
    dlg.setAGOType(setts.value("ago", 1).toInt());
    dlg.setIP(setts.value("IP").toString());
    dlg.setPort(setts.value("port", 47000).toInt());
    dlg.setInCh(setts.value("InCh", 0).toInt());
    dlg.setAudioFolder(setts.value("audio_folder", AgoBase::defAudioPath()).toString());
    dlg.setFullScreen(isFullScreen());
    dlg.setCursor(setts.value("cursor", true).toBool());
    dlg.setPerion(setts.value("period", AUDIO_FRAME_MS*3000).toInt());
    dlg.setBroadcast(setts.value("broadcast").toString());
    auto comPortName = setts.value("COM", "").toString();
    dlg.setCOMPort(comPortName);
    dlg.setTragetsFile(setts.value("targets_file", AgoBase::defTargetsPath()).toString());
    dlg.setBaud(setts.value("baud", 2400).toInt());
    dlg.setKeyboard(setts.value("keyboard", true).toBool());
    dlg.setVolumeSlider(ui->slVolume->isVisible());
    dlg.setPass(pass);
    dlg.setColsCnt(setts.value("cols_cnt", 4).toInt());
    dlg.setTargsFntSz(setts.value("targ_font_sz", 24).toInt());
    dlg.setPortPeriod(setts.value("port_period", 50).toInt());
    auto targsCnt = setts.value("targets_cnt", 16).toInt();
    dlg.setTargCnt(targsCnt);

    if(dlg.exec()){

        setts.setValue("audio_device", dlg.getAudioDevice());
        setts.setValue("ago", dlg.getAGOType());
        setts.setValue("IP", dlg.getIP());
        setts.setValue("port", dlg.getPort());
        setts.setValue("InCh", dlg.getInCh());
        setts.setValue("audio_folder", dlg.getAudioFolder());
        setts.setValue("cursor", dlg.getCursor());
        setts.setValue("period", dlg.getPeriod());
        setts.setValue("broadcast", dlg.getBroadcast());
        auto newComPortName = dlg.getCOMPort();
        setts.setValue("COM", newComPortName);
        setts.setValue("targets_file", dlg.getTargetsFile());
        setts.setValue("baud", dlg.getBaud());
        setts.setValue("keyboard", dlg.getKeyboard());
        setts.setValue("cols_cnt", dlg.getColsCnt());
        setts.setValue("targ_font_sz", dlg.getTargsFntSz());
        auto newTargsCnt = dlg.getTargCnt();
        setts.setValue("targets_cnt", newTargsCnt);

        if(reader && reader->getDeviceName()!=dlg.getAudioDevice()){
            reader->deleteLater();
            createSoundReader();
        }

        auto port_period = dlg.getPortPeriod();
        if(port_period){
            setts.setValue("port_period", port_period);
        }

        if(dlg.getPass() != setts.value("password", "").toString()){
            Keyboard kb("Введите пароль для проверки", "", this, Keyboard::KB_PASS);
            if(kb.exec()==QDialog::Rejected || kb.getText()!=dlg.getPass()){
                QMessageBox::warning(this, "Пароль", "Пароль введен не верно и не будет сохранен.");
            }else{
                setts.setValue("password", dlg.getPass());
            }
        }

        if(newComPortName != comPortName || newTargsCnt != targsCnt){
//            initAgo();
            auto res = QMessageBox::information(this, "Параметры изменены", "Изменены параметры, требующие перезапуска приложения.\r\nПерезапустить сейчас?",
                                                QMessageBox::Yes | QMessageBox::No);
            if(res == QMessageBox::Yes){
                QCoreApplication::quit();
            }
        }

        if(dlg.getFullScreen()){
            showFullScreen();
//            setWindowFlags(Qt::WindowStaysOnTopHint);
        }else{
            showNormal();
        }

        showCursor();

        ui->slVolume->setVisible(dlg.volumeSlider());

        if(dlg.getCloseApp()){
            exit(0);
        }
    }
}

void MainWindow::on_slVolume_valueChanged(int value)
{
    if(volMutex.try_lock()){
        if(ago)
            ago->setVolume(value);
        volMutex.unlock();
    }
}

void MainWindow::on_actionRemoteSetts_triggered()
{
//    RemoteSetts rSt(setts.value("IP","").toString(), this);
//    for(QString name : rSt.getOutNames()){
//        qDebug() << name;
//    }
}

void MainWindow::on_btPlay_clicked()
{
    QString name = ui->cbClips->currentText();
    if(name.isEmpty()){
        statusLabel->setText("Файл не выбран");
        return;
    }

//    if(name == "< Обновить... >"){
//        getClips();
//    }

//    ui->btPlay->setChecked(file_is_play);

    if(ago){
        if(ui->btPlay->isChecked())
            ago->playClip(name, setts.value("InCh", 0).toInt(), getOuts());
        else
            ago->stopClip(setts.value("InCh", 0).toInt());
    }
}

void MainWindow::on_cbClips_currentTextChanged(const QString &arg1)
{
    (void)arg1;
}

void MainWindow::on_btClose_clicked()
{
    exit(0);
}

void MainWindow::on_actionUpload_triggered()
{
    auto dir = setts.value("audio_folder", AgoBase::defAudioPath()).toString();
    if(dir.isEmpty()){
        statusLabel->setText("Надо выбрать папку для аудио-файлов");
        on_actionSetts_triggered();
        return;
    }

    statusLabel->setText("Загрузка файла");
    playPorgress->setVisible(true);

    auto file = ClipsDialog(this).getClip("Файл для загрузки",
                                          setts.value("audio_folder", AgoBase::defAudioPath()).toString());//QFileDialog::getOpenFileNames(this, "Выберите файлы для загрузки", setts.value("audio_folder", AgoBase::defAudioPath()).toString());
    bool res = ago ? ago->uploadFile(file) != 0 : false;

    statusLabel->setText(res ? "Файл загружен" : "Ошибки при загрузке");
    playPorgress->setVisible(false);

    getClips();
}

void MainWindow::on_actionRecord_triggered()
{
    if(!ago)
        return;

    if(!ago->recActive){
        ui->actionRecord->setChecked(false);

        auto dir = setts.value("audio_folder", AgoBase::defAudioPath()).toString();
        if(dir.isEmpty()){
            statusLabel->setText("Надо выбрать папку для аудио-файлов");
            on_actionSetts_triggered();
            return;
        }

        QString fileName = "";
        if(setts.value("keyboard", true).toBool()){
            Keyboard kb("Введите имя файла", "", this);
            kb.exec();
            fileName = kb.getText();
        }else{
            fileName = QInputDialog::getText(this, "Введите имя файла", "Имя файла");
        }


        if(fileName.isEmpty()){
            return;
        }
        if(!fileName.endsWith(".wav", Qt::CaseInsensitive))
            fileName += ".wav";
        fileName = dir+"/"+fileName;

        if(QFile(fileName).exists()){
            auto ans = QMessageBox::warning(this, "Предупреждение", "Файл уже существует. Заменить?",
                                 QMessageBox::StandardButtons() | QMessageBox::Yes | QMessageBox::No);
            if(ans == QMessageBox::No)
                return;
        }

        qDebug() << fileName;

        ago->startRec(fileName);

    }else{
        ago->stopRec();
    }
}

void MainWindow::playFile(bool cast){

    if(ago){
        if(!ago->isFilePlay()){
//            QString fileName = ClipsDialog(this).getClip("Файл для воспроизведения", setts.value("audio_folder", AgoBase::defAudioPath()).toString());//QFileDialog::getOpenFileName(this, "Файл для прослушивания", setts.value("audio_folder", AgoBase::defAudioPath()).toString());
            FilesDialog dlg(names, cast? FilesDialog::RunCast : FilesDialog::RunPreview, ago, this);
            dlg.loadFiles(setts.value("audio_folder", AgoBase::defAudioPath()).toString());
            auto res = dlg.exec();
            if(res == QDialog::Accepted){
                qDebug() << "Play file:" << dlg.filePath;
                ago->playFile(dlg.filePath, cast);
            }else{
                ago->stopFile();
            }
        }else{
            ago->stopFile();
        }
    }
}

void MainWindow::on_actionPlay_triggered()
{
    playFile(false);
}

void MainWindow::on_actionFile_triggered()
{
    playFile(true);
}


