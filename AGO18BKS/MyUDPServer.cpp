/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyUDPServer.cpp
 * Author: user
 * 
 * Created on 26 ноября 2019 г., 13:51
 */

#include "MyUDPServer.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <stdbool.h>
#include <sys/ioctl.h>
#include "PacketParser.h"
#include "ConfigServer.h"
#include "cus/log.h"
#include <sstream>

MyUDPServer server;
chrono::steady_clock::time_point _start_tm;

void main_thread_func(MyUDPServer* __this);

MyUDPServer::MyUDPServer() {
}

MyUDPServer::MyUDPServer(const MyUDPServer& ) {
}

MyUDPServer::~MyUDPServer() {
    PacketParser::clear_parsers();
}

string MyUDPServer::waitForIface(string if_name){
    struct ifaddrs *ifaddr, *ifa;
    int s;
    char host[NI_MAXHOST];
    
    for(int i=0; i<10; i++){
        if (getifaddrs(&ifaddr) == -1)
        {
            perror("getifaddrs");
            exit(EXIT_FAILURE);
        }

        for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
            if (ifa->ifa_addr == NULL)
                continue;

            s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if(ifa->ifa_addr->sa_family==AF_INET){
                cout << "Check iface: " << string(ifa->ifa_name) << endl;

                if (s != 0)
                {
                    printf("getnameinfo() failed: %s\n", gai_strerror(s));
                    break;
                }

                if(string(ifa->ifa_name) == if_name){
                    cout << "BKS iface: " << string(ifa->ifa_name) << " : " << host << endl;
                    return host;
                }
            }
        }
        cerr << red << "Wait for iface " << if_name << " ..." << (10-i) << clrst << endl;
        usleep(3000000);
    }
    
    return "";
}

void MyUDPServer::error(string mes, bool _exit){
    log_error("MyUDPServer: %s\n", mes.c_str());
}

void MyUDPServer::start(){
    json js_ports = conf_server.network["in_ports"];
    if(js_ports.is_array()){
        for(int i=0; i<LISTEN_SOCKS; i++){
            if(i < static_cast<int>(js_ports.size())){
                ports[i] = js_ports[i];
            }else{
                ports[i] = 0;
            }
        }
    }else{
        conf_server.error("network.json in_ports format error", true);
    }
    
    iface = conf_server.getNetParam("in_iface","");
    auto host = waitForIface(iface);

    for(int i=0; i<LISTEN_SOCKS; i++){
 
        sock[i] = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if(sock[i] == -1){
            error("socket(): "+string(strerror(errno)), true);
            exit(EXIT_FAILURE);
        }

        int bc_on=1;
        if(setsockopt(sock[i], SOL_SOCKET, SO_BROADCAST, &bc_on, sizeof(int)) == -1){
            error("setsockopt(): "+string(strerror(errno)), false);
            exit(EXIT_FAILURE);
        }

        {//for(int n=6; n>=1; n--)
            int n = 1;
            int buf_sz;
            socklen_t buf_sz_sz = sizeof(int);
            if(getsockopt(sock[i], SOL_SOCKET, SO_RCVBUF,(void *)&buf_sz, &buf_sz_sz) == -1){
                error("getsockopt(SO_RCVBUF): "+string(strerror(errno)), false);
            }else{
//                printf("old socket input buffer = %d\n", buf_sz);
            }

            buf_sz = n*1024*1024;
            buf_sz_sz = sizeof(int);
            if(setsockopt(sock[i], SOL_SOCKET, SO_RCVBUF,(void *)&buf_sz, sizeof(int)) == -1){
                error("setsockopt(SO_RCVBUF): "+string(strerror(errno)), false);
            }

            if(getsockopt(sock[i], SOL_SOCKET, SO_RCVBUF,(void *)&buf_sz, &buf_sz_sz) == -1){
                error("getsockopt(SO_RCVBUF): "+string(strerror(errno)), false);
            }else{
                printf("New socket input buffer = %d\n", buf_sz);
            }
        }



    //    struct ifreq ifr;
    //    ifr.ifr_ifru.ifru_addr.sa_family = AF_INET;
    //    strcpy(ifr.ifr_ifrn.ifrn_name, iface.c_str());
    //    if(ioctl(sock, SIOCGIFADDR, &ifr) == -1){
    //        cerr << "ioctl(SIOCGIFADDR):" << strerror(errno) << endl;
    //        return;
    //    }

        sockaddr_in my_addr;
        memset(&my_addr, 0, sizeof(sockaddr_in));
        my_addr.sin_family = AF_INET;
        //my_addr.sin_addr = ((sockaddr_in*)&ifr.ifr_ifru.ifru_addr)->sin_addr;//INADDR_ANY
        my_addr.sin_addr.s_addr = INADDR_ANY;
        my_addr.sin_port = htons(ports[i]);
        cout << "Inner stream socket" << i <<" binds to " << inet_ntoa(my_addr.sin_addr) << ":" << ports[i] << endl;

        if(bind(sock[i], (sockaddr*)&my_addr, sizeof(sockaddr_in)) == -1){
            error("bind(): "+string(strerror(errno)), true);
            exit(EXIT_FAILURE);
        }

    //    ifr.ifr_ifru.ifru_addr.sa_family = AF_INET;
    //    strcpy(ifr.ifr_ifrn.ifrn_name, iface.c_str());
    //    if(ioctl(sock, SIOCGIFBRDADDR, &ifr) == -1){
    //        cerr << "ioctl(SIOCGIFBRDADDR):" << strerror(errno) << endl;
    //        return;
    //    }
    }
    
    broadcast = conf_server.getNetParam("in_broadcast", "");
    if(broadcast == ""){
        conf_server.error("no in_broadcast specified", true);
        exit(EXIT_FAILURE);
    }
    
    cout << "Broadcast address " << broadcast << endl;
    
    main_thread = thread(main_thread_func, this);
    
    conf_server.addServerEvent(ConfigServer::SERVER_EVT_UDP, ConfigServer::EVT_NORMAL, 0, "UDP-сервер запущен");// %s:%d, inet_ntoa(my_addr.sin_addr), port
    
//    dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG))->set_critical_alarms(conf_server.getAlarmsBits());
//    sendCommand(nullptr, 0, PACK_CFG, SND_IN_CPB_REQ, 3, 0xFF);
//    sendCommand(nullptr, 0, PACK_CFG, SND_OUT_CPB_REQ, 3, 0xFF);
//    sendCommand(nullptr, 0, PACK_BRU_CFG, BRU_GET_CFG, 3, 0xFF);

//    sched_param param;
//    param.sched_priority = 1;
//    int err = pthread_setschedparam(main_thread.native_handle(), SCHED_FIFO, &param);//
//    if(err != 0)
//        cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;
    
//    printf("UDP server created. id=%d\n", main_thread.native_handle());
}

void MyUDPServer::stop(){
    active = false;
    main_thread.join();
    
    isStoped = true;
}

void MyUDPServer::parse_packet(pack_hdr_t* pack, sockaddr_in from_addr){
#ifdef UDP_DEBUG
    cout << "RECV: ";
    dumpPacket(pack);
#endif

    if((pack->len + sizeof(pack_hdr_t)) > MAX_PACKET_SIZE){
        perror("Too big packet\n");
        return;
    }

    PacketParser* parser = PacketParser::get_instance(pack);
    if(!parser){
//        ostringstream stm;
//        stm << "Packet has not processed. type = " << (int)pack->type;
//        error(stm.str(), false);
        return;
    }
    
    parser->parse(pack, from_addr);
}

void MyUDPServer::sendCommand(uint8_t* data, int len, uint8_t type, uint8_t cmd, int repeats, uint8_t addr){
    size_t sz = sizeof(pack_hdr_t)+1+len;
    uint8_t buf[sz];
    packet_command_t* p = reinterpret_cast<packet_command_t*>(buf);
    
    p->hdr.adr_from = conf_server.server_addr;
    p->hdr.adr_to = addr;
    p->hdr.len = len+1;
    p->hdr.type = type;
    p->cmd = cmd;
    if(data)
        memcpy(p->data, data, len);
    
    sockaddr_in br_addr;
    memset(&br_addr, 0, sizeof(sockaddr_in));
    br_addr.sin_family = AF_INET;
    inet_aton(broadcast.c_str(), &br_addr.sin_addr);
    br_addr.sin_port = type == PACK_AUDIO ? htons(ports[0]) : htons(ports[1]);
    
    int sock_n = type == PACK_AUDIO ? 0 : 1;

    
    for(int i=0; i<repeats; i++){
        p->hdr.repeate_cnt = i;
        
        if(cmd != SND_STREAM){
#ifdef UDP_DEBUG
            cout << "Send command " << (eth_cmd_t)cmd << ": ";
            MyUDPServer::dumpPacket(reinterpret_cast<pack_hdr_t*>(p));
#endif
        }
        
        lock_guard<mutex> lock(sock_mutex[sock_n]);
        if(sendto(sock[sock_n], buf, sz, 0, reinterpret_cast<sockaddr*>(&br_addr), sizeof(sockaddr_in)) == -1){
            error("sendto(): "+string(strerror(errno)));
        }
        //usleep(1000);
    }
}

void MyUDPServer::sendBuffer(packet_command_t* packet, int len){
    uint8_t type = packet->hdr.type;
    int sock_n = type == PACK_AUDIO ? 0 : 1;
        
    sockaddr_in br_addr;
    memset(&br_addr, 0, sizeof(sockaddr_in));
    br_addr.sin_family = AF_INET;
    inet_aton(broadcast.c_str(), &br_addr.sin_addr);
    br_addr.sin_port = type == PACK_AUDIO ? htons(ports[0]) : htons(ports[1]);
    
    lock_guard<mutex> lock(sock_mutex[sock_n]);
    if(sendto(sock[sock_n], packet, len, 0, reinterpret_cast<sockaddr*>(&br_addr), sizeof(sockaddr_in)) == -1){
        error("sendCommand sendto(): "+string(strerror(errno)));
    }
}

void MyUDPServer::run(){
    start_tm = GetTickCount();
    
    sched_param param;
#if UDP_SEND_TYPE == SIMPLE_SEND
    param.sched_priority = 22;
    int err = pthread_setschedparam(pthread_self(), SCHED_FIFO, &param);//main_thread.native_handle()
#else
    param.sched_priority = 0;
    int err = pthread_setschedparam(pthread_self(), SCHED_OTHER, &param);//main_thread.native_handle()
#endif
    if(err != 0)
        cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;

    uint64_t last_stat_tm = GetTickCount();
    uint32_t recv_sum = 0;
    _start_tm = chrono::steady_clock::now();
    while(active)
    try{
                
        int sock_max = 0;
        
        fd_set rd_set, er_set;
        FD_ZERO(&rd_set);
        FD_ZERO(&er_set);
        
        for(int i=0; i<LISTEN_SOCKS; i++){
            FD_SET(sock[i], &rd_set);
            FD_SET(sock[i], &er_set);
            if(sock[i] > sock_max)
                sock_max = sock[i];
        }
        
        timeval to = {1, 0};
        
        auto sel_socks = select(sock_max+1, &rd_set, NULL, &er_set, &to);
        if(sel_socks == -1){
            cerr << red << "select():" << strerror(errno) << clrst << endl;
        }
        if(sel_socks > 0){
            for(int i=0; i<LISTEN_SOCKS; i++){
                if(FD_ISSET(sock[i], &rd_set)){
//                    int sz;
//                    ioctl(sock[i], FIONREAD, &sz );
//                    printf("FIONREAD=%d\n", sz);
                    
                    sockaddr_in from_addr;
                    memset(&from_addr, 0, sizeof(sockaddr_in));
                    socklen_t socklen = sizeof(sockaddr_in);
                    auto rd = recvfrom(sock[i], reinterpret_cast<void*>(&buf), MAX_PACKET_SIZE, 0, reinterpret_cast<sockaddr*>(&from_addr), &socklen);
                    if(rd == -1 && errno != EAGAIN){
                        cerr << red << "recvfrom(sock" << i << ")" << strerror(errno) << clrst << endl;
                        continue;
                    }
                    if(rd > 0){
                        pack_hdr_t* pack = reinterpret_cast<pack_hdr_t*>(buf);
//                        cout << "recv " << rd << " type=" << (int)pack->type << " len=" << (int)pack->len << endl;
//                        dumpPacket(pack);
                        if(rd >= static_cast<ssize_t>(sizeof(pack_hdr_t)+pack->len)){
                            parse_packet(pack, from_addr);
                        }else{
                            cerr << red << "Packet length error" << clrst << endl;
                        }

                        recv_sum += rd;
                        if((GetTickCount() - last_stat_tm) > 1000){
                            //cout << "Stream speed: " << recv_sum*8 << endl;
                            recv_sum = 0;
                            last_stat_tm = GetTickCount();
                        }
                    }
                }

                if(FD_ISSET(sock[i], &er_set)){
                    ostringstream stm;
                    stm << "socket" << i << " error";
                    error(stm.str());
                }
            }
        }
        
        sendStartRequests();
        
    }catch(system_error &e){
        log_error("MyUPDServer::run(): %s", e.what());
    }

    cout << "UDP server stoped" << endl;

    for(int i=0; i<LISTEN_SOCKS; i++)
        close(sock[i]);
    
    unlink(PIDFILE);
    
    isStoped = true;
}

void MyUDPServer::sendStartReq(uint8_t* data, int len, pack_type_t type, eth_cmd_t cmd){
    if(start_requests.find(cmd) == start_requests.end()){
//        cout << "Send initial request: cmd=" << cmd << endl;
        sendCommand(data, len, type, cmd);
    }
}

void MyUDPServer::sendStartRequests(){
    uint32_t cur_tm = GetTickCount();
    if((cur_tm - start_req_tm)<1000 || (cur_tm - start_tm) > 60000 || start_requests.size() >= 4){
        return;
    }
    
    auto bits = conf_server.getAlarmsBits();
    sendStartReq(reinterpret_cast<uint8_t*>(&bits), sizeof(uint32_t), PACK_CFG, BRP_SET_CRITICAL_ALARMS);
    sendStartReq(nullptr, 0, PACK_CFG, SND_IN_CPB_REQ);
    sendStartReq(nullptr, 0, PACK_CFG, SND_OUT_CPB_REQ);
    sendStartReq(nullptr, 0, PACK_BRU_CFG, BRU_GET_CFG);
    
    start_req_tm = GetTickCount();
}

void MyUDPServer::dumpPacket(pack_hdr_t* pack){
    chrono::steady_clock::time_point _now = chrono::steady_clock::now();
    int tm = chrono::duration_cast<chrono::milliseconds>(_now - _start_tm).count();
    printf("%d: ", tm);

    int cnt = sizeof(pack_hdr_t) + 1 + pack->len;
    uint8_t* data = reinterpret_cast<uint8_t*>(pack);
    for(int i=0; i<cnt; i++){
        printf("%02X ", data[i]);
    }
    printf("\n");
}

void main_thread_func(MyUDPServer* __this){
    __this->run();
}

uint32_t GetTickCount() 
{ 
    struct timeval tv; 
    gettimeofday(&tv,NULL); 
    return (tv.tv_sec*1000+tv.tv_usec/1000); 
}

uint64_t GetTickCount_us() 
{ 
    struct timeval tv; 
    gettimeofday(&tv,NULL); 
    return (static_cast<uint64_t>(tv.tv_sec)*1000000 + static_cast<uint64_t>(tv.tv_usec)); 
}
