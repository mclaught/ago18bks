var class_t_c_p_channel =
[
    [ "State", "class_t_c_p_channel.html#ad304f9e9bf9aaa65c7e2c8f28d822d0e", [
      [ "DISCONNECTED", "class_t_c_p_channel.html#ad304f9e9bf9aaa65c7e2c8f28d822d0eae3e97106098d71e8198e66ecdbf4a30e", null ],
      [ "CONNECTING", "class_t_c_p_channel.html#ad304f9e9bf9aaa65c7e2c8f28d822d0ea4efa28b2144eaf894a73b3076056c8b6", null ],
      [ "CONNECTED", "class_t_c_p_channel.html#ad304f9e9bf9aaa65c7e2c8f28d822d0ea075dabe4bd7dd7edb7247cff3462bd46", null ],
      [ "PLAY", "class_t_c_p_channel.html#ad304f9e9bf9aaa65c7e2c8f28d822d0ea05e0d54e028071f4b7a08247ef1bcf4e", null ]
    ] ],
    [ "TCPChannel", "class_t_c_p_channel.html#ac80cde1f4df3cce0e1d49ed222adcc60", null ],
    [ "TCPChannel", "class_t_c_p_channel.html#a97789803d1952b5f6d027e55d5a418a0", null ],
    [ "TCPChannel", "class_t_c_p_channel.html#a2a38aa6a1a8d148727452317a07d9d12", null ],
    [ "~TCPChannel", "class_t_c_p_channel.html#a8f7a906d1623231bafc5ad8fdcbd8997", null ],
    [ "connectServer", "class_t_c_p_channel.html#a3f260effcb9e21a798758c51525afb29", null ],
    [ "disconnect", "class_t_c_p_channel.html#a096ef97d918987a0856d0586d3899ca8", null ],
    [ "isConfigured", "class_t_c_p_channel.html#a51f22425c8d7f196b36df79df382061b", null ],
    [ "onRead", "class_t_c_p_channel.html#a619dc979994ef00ecac460f557257596", null ],
    [ "setServerParams", "class_t_c_p_channel.html#af1e03db7a44226efc18797618d3635d2", null ],
    [ "setState", "class_t_c_p_channel.html#a38f7fc81ac29580f7bfe7fc752f21076", null ],
    [ "clientName", "class_t_c_p_channel.html#a8c6dee1c7b6b61626f967cb8ffaf830d", null ],
    [ "state", "class_t_c_p_channel.html#a82894bb63c382ee005ed2a330eb43196", null ],
    [ "targetState", "class_t_c_p_channel.html#a1f9ea2aa2ca496fbe928732e0ee364c5", null ]
];