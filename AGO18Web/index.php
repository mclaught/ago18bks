<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css?1">
        
        <style>
            th.cols{
                max-width:40px; 
                min-width:40px; 
                /*vertical-align: middle;*/ 
            }
            th.cols div{
                width: 40px;
                height: 100px;
                margin: 0 auto;
                padding: 0;
            }
            th.cols div p{
                font-size:12pt;
                text-align: center; 
                position: relative;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                font-size:10pt; 
                margin: 0 auto;
            }
            th.cols-with-name {
                max-width:40px; 
                min-width:40px; 
            }
            th.cols-with-name div{
                width: 100px;
                height: 40px;
                position: relative;
                top: 50%;
                left: 50%;
                transform: translateX(-50%) translateY(-50%) rotate(-90deg);
            }
            th.cols-with-name div p{
/*                width: 100px;
                height: 40px;
                vertical-align: middle; */
                text-align: center;
                position: relative;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                font-size:10pt; 
                margin: 0 auto;
            }
        </style>
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        include "navbar.php";
        ?>
        
        <div class="container">
            <br>
            <h1><img src="img/info.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Текущее состояние</h1>
            <br>
            
            <div class="alert alert-danger" role="alert" id="connection-alert">
              Отсутствует соединение с сервером!
            </div>
            
            <div id="commtable"></div>
            
            <h3>События</h3>
            <div style="height:400px;line-height:1em;overflow:scroll;padding:5px;" id="events">
            </div>
        </div>
        
        <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/alert.js"></script>
        <script type="text/javascript" src="js/alertify.js"></script>
        <script type="text/javascript" src="js/constants.js"></script>
        <script type="text/javascript" src="js/rest.js"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script type="text/javascript" src="js/MyView.js"></script>
        <script type="text/javascript" src="js/StateTable.js"></script>
        <script type="text/javascript" src="js/EventsView.js"></script>
        <script type="text/javascript" src="js/MainPage.js"></script>
        <script>
            var sock = null;
            var stateTable;
            var eventsView;
            var ws_connected = false;
            
            async function connect_ws(){
                var network = (await (new Rest()).get_network()).network;
                console.dir(network);
                
                if(sock != null)
                    sock.close();
                
                let url = "ws://"+window.location.hostname+":"+network.ws_port;
                console.log(url);
                sock = new WebSocket(url);
                sock.onopen = function(e){
                    console.log("WS connection opened");
                    alertify.success("Соединение установлено");
                    
                    let query = {
                        type: "state_page"
                    };
                    
                    get_settings(function(setts){
                        stateTable.init();
                        eventsView.init();
                        
                        sock.send(JSON.stringify(query));

                        ws_connected = true;
                        $('#connection-alert').hide();
                    });
                    
                };
                sock.onclose = function(event){
                    alertify.warning("Соединение закрыто: "+event.code+" "+event.reason);
                    ws_connected = false;
                    $('#connection-alert').show();
                };
                sock.onerror = function(error){
                    alertify.error(error.message);
                    ws_connected = false;
                    $('#connection-alert').show();
                };
                sock.onmessage = function(event){
                    let json = JSON.parse(event.data);
                    console.info("onmessage", json);

                    if(json.comm_table != null)
                        stateTable.set_json(json.comm_table.sw_table);
                    if(json.in_state != null)
                        stateTable.set_in_state(json.in_state);
                    if(json.events != null)
                        eventsView.set_json(json.events);
                };
            }
            
            async function update(){
                console.dir(window.location);

                stateTable = new StateTable($("#commtable"));
                eventsView = new EventsView($("#events"));

                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();//
                });

                connect_ws();
                setInterval(function(){
                    if(!ws_connected){
                        connect_ws();
                    }
                }, 1000);

            }
            
            update();
        </script>
    </body>
</html>
