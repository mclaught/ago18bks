#ifndef REMOTESETTS_H
#define REMOTESETTS_H

#include <QNetworkReply>
#include <QObject>

#include "json.hpp"

using namespace nlohmann;

class RemoteSetts : public QObject
{
    Q_OBJECT

    QString server_ip;
    QNetworkReply* reply;
    QByteArray getAnswer(QUrl url);
public:
    explicit RemoteSetts(QString ip, QObject *parent = nullptr);

    QByteArray query(QString cmd);
    QByteArray query(QString cmd, json data);

    json getNetSetts();
    int getPort();
    int getWSPort();
    json getSetts();
    std::vector<std::string> getOutNames();
    QVector<QString> getClips();
    bool playClip(QString name, uint8_t source, uint64_t outs);
    bool stopClip(QString name, uint8_t source);
    int uploadFile(QString ip, QString fileName);

    json setts;
    json network;
    int outsCnt;
signals:
    void uploadProgress(qint64 sent, qint64 total);
private slots:
    void replyFinished(QNetworkReply *rep);
    void slotReadyRead();
    void slotError(QNetworkReply::NetworkError error);
    void slotSslErrors(QList<QSslError> &errors);
    void onProgress(qint64 sent, qint64 total);
};

#endif // REMOTESETTS_H
