#include "agobase.h"
#include "scheduller.h"
#include <QFile>
#include <QDateTime>
#include <QFileInfo>
#include <QDebug>
#include <QSettings>
#include <QAudioDeviceInfo>

const qint64 tm_k[] = {1, 60, 3600};
extern QSettings setts;
Scheduller scheduller;

Scheduller::Scheduller(QObject *parent) : QObject(parent)
{
//    audioDir = QDir(setts.value("audio_folder", AgoBase::defAudioPath()).toString());
//    infoDir.setPath(audioDir.absoluteFilePath(infoDirName));
//    infoDir.mkpath(infoDir.absolutePath());

    connect(&timer, &QTimer::timeout, this, &Scheduller::onTimer);
}

void Scheduller::onTimer(){
    auto tm = Scheduller::currentTime();
    emit periodic(tm);

    auto fnd = findSignalled(tm);
    if(fnd.get()){
        emit signaled(fnd);
    }
}

void Scheduller::load()
{
    items.clear();
    audioDir = QDir(setts.value("audio_folder", AgoBase::defAudioPath()).toString());
    auto files = audioDir.entryList(QStringList() << "*.*", QDir::Filters() | QDir::Files);
    for(auto &file : files){
        if(!file.endsWith(".info")){
            items.push_back(FileInfo::forFile(audioDir.absoluteFilePath(file)));
        }
    }

    update_tasks();
}

void Scheduller::save()
{
    for(auto &item : items){
        item->save();
    }
}

PFileInfo Scheduller::findSignalled(qint64 time)
{
    QMutexLocker lock(&tasksMutex);

    for(auto &itm : active_tasks){
        if(itm->active_task && itm->isSignaled(time)){
            return itm;
        }
    }
    return PFileInfo();
}

void Scheduller::addItem(PFileInfo item)
{
    items.push_back(item);
}

void Scheduller::removeItem(int index)
{
    items.remove(index);
}

PFileInfo Scheduller::findFile(QString path)
{
    for(auto &item : items){
        if(item->path == path)
            return item;
    }
    return PFileInfo();
}

void Scheduller::start()
{
    timer.start(1000);
}

void Scheduller::stop()
{
    timer.stop();
}

void Scheduller::update_tasks()
{
    QMutexLocker lock(&tasksMutex);

    active_tasks.clear();
    hasBadFiles = false;
    for(auto &item : items){
        if(item->bad){
            emit badFile(item->name);
            hasBadFiles = true;
            continue;
        }
        if(item->active_task){
            active_tasks.push_back(item);
        }
    }
}

qint64 Scheduller::timeFromStr(QString str)
{
    qint64 time = 0;
    auto parts = str.split(":");
    for(int i=0; i<parts.length(); i++){
        bool ok = false;
        qint64 iPart = parts[parts.length()-i-1].toInt(&ok);
        if(ok){
            time += tm_k[i]*iPart;
        }
    }
    return time;
}

QString Scheduller::timeToStr(qint64 time)
{
    auto tm = QTime::fromMSecsSinceStartOfDay(time*1000);
    if(tm.second() == 0)
        return tm.toString("H:mm");
    else
        return tm.toString("H:mm:ss");
//    QString str = "";
//    while(time){
//        qint64 t = time % 60;
//        time /= 60;
//        if(!str.isEmpty()){
//            str = ":"+str;
//        }
//        str = QString("%1").arg(t, 2, 10, QLatin1Char('0')) + str;
//    }
//    return str;
}

qint64 Scheduller::currentTime(){
    auto tm = QDateTime::currentDateTime();
    tm.setTimeSpec(Qt::UTC);
    return tm.toLocalTime().toSecsSinceEpoch() % 86400;
}

bool Scheduller::removeFile(int index){
    if(index < 0 || index >= items.length())
        return false;

    auto info = items[index];
    QFile file(info->path);
    if(file.exists())
        file.remove();
    QFile infoFile(info->infoPath);
    if(infoFile.exists())
        infoFile.remove();
    items.remove(index);

    return true;
}
