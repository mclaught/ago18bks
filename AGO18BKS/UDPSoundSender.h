/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPSoundSender.h
 * Author: user
 *
 * Created on 16 декабря 2019 г., 15:28
 */

#ifndef UDPSOUNDSENDER_H
#define UDPSOUNDSENDER_H

#include "BaseSoundSender.h"
#include <netinet/in.h>

class UDPSoundSender;
typedef shared_ptr<UDPSoundSender> PUDPSoundSender;
class UDPSoundSender : public BaseSoundSender {
public:
    UDPSoundSender();
    UDPSoundSender(const UDPSoundSender& orig);
    virtual ~UDPSoundSender();
    
    static PUDPSoundSender getFreeSender();

    void start(int _sock, sockaddr_in _addr, start_stop_t* start_stop);
    void stop();
    void touch();
    bool isTimedOut();
protected:
    virtual void sendSoundPacket(int cnt);
    virtual void onStart();
    virtual void onStop();
    
private:
    int sock;
    sockaddr_in addr;
    time_t last_active_time;
    uint64_t req_tm;
};

#endif /* UDPSOUNDSENDER_H */

