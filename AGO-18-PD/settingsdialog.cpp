#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "soundread.h"
#include <QAudioDeviceInfo>
#include <QDebug>
#include <QFileDialog>
#include <QMouseEvent>
#include <qdesktopservices.h>
#include "ago09uart.h"
#include "targetseditor.h"
#include "version.h"
#include "QScreen"
#include "QDesktopWidget"
#include "QDebug"
#include <QScrollBar>

#define Q(x) #x
#define QUOTE(x) Q(x)


SettingsDialog::SettingsDialog(AgoBase *ago, QWidget *parent) :
    QDialog(parent), ago(ago),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Сохранить");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    for(auto dev : SoundReader::devList()){
        ui->cbDevice->addItem(dev.deviceName());
    }

    auto fmt = SoundReader::usedFormat();
    ui->edFormat->setText(QString("%1Гц, %2бит, %3").arg(fmt.sampleRate()).arg(fmt.sampleSize()).arg(fmt.codec()));
    ui->edVersion->setText(QUOTE(VERSION));

    ui->cbAGO->addItems({
                            "АГО-18",
                            "АГО-09"
                        });

    ui->cbCOM->addItems(Ago09Uart::portsAvailable());

    ui->cbBaud->addItems({
                             "1200",
                             "2400",
                             "4800",
                             "9600",
                             "19200",
                             "38400",
                             "57600",
                             "115200"
                         });

    for(int i=1; i<=4; i++){
        ui->cbColsCnt->addItem(QString("%1").arg(i));
    }

    for(int i=10; i<=40; i+=2){
        ui->cbTargetsFontSz->addItem(QString("%1").arg(i));
    }

    ui->cbTargCnt->addItems({
                                "16",
                                "32"
                            });

    ui->cbCloseApp->setChecked(false);

    setMinimumHeight(QGuiApplication::primaryScreen()->availableGeometry().height());
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::setAudioDevice(QString name)
{
    ui->cbDevice->setCurrentText(name);
}

QString SettingsDialog::getAudioDevice()
{
    return ui->cbDevice->currentText();
}

void SettingsDialog::setIP(QString ip)
{
    ui->edIP->setText(ip);
}

QString SettingsDialog::getIP()
{
    return ui->edIP->text();
}

void SettingsDialog::setPort(int port)
{
    ui->edPort->setText(QString::number(port));
}

int SettingsDialog::getPort()
{
    return ui->edPort->text().toInt();
}

void SettingsDialog::setInCh(int ch)
{
    ui->edInCh->setText(QString::number(ch));
}

int SettingsDialog::getInCh()
{
    return ui->edInCh->text().toInt();
}

void SettingsDialog::setFullScreen(bool value)
{
    ui->cbFullScreen->setChecked(value);
}

bool SettingsDialog::getFullScreen()
{
    return ui->cbFullScreen->isChecked();
}

void SettingsDialog::setAudioFolder(QString value)
{
    ui->edAudioFolder->setText(value);
}

QString SettingsDialog::getAudioFolder()
{
    return ui->edAudioFolder->text();
}

void SettingsDialog::setCursor(bool value)
{
    ui->cbCursor->setChecked(value);
}

bool SettingsDialog::getCursor()
{
    return ui->cbCursor->isChecked();
}

void SettingsDialog::setPerion(int value)
{
    ui->edPeriod->setText(QString::number(value));
}

int SettingsDialog::getPeriod()
{
    return ui->edPeriod->text().toInt();
}

void SettingsDialog::setBroadcast(QString value)
{
    ui->edBroadcast->setText(value);
}

QString SettingsDialog::getBroadcast()
{
    return ui->edBroadcast->text();
}

void SettingsDialog::setAGOType(int val)
{
    ui->cbAGO->setCurrentIndex(val);
}

int SettingsDialog::getAGOType()
{
    return ui->cbAGO->currentIndex();
}

void SettingsDialog::setCOMPort(QString val)
{
    if(val.isEmpty()){
        ui->cbCOM->setCurrentIndex(-1);

        for(int i=0; i<ui->cbCOM->count(); i++){
            if(ui->cbCOM->itemText(i).indexOf("CP210")!=-1){
                ui->cbCOM->setCurrentIndex(i);
                break;
            }
        }
        return;
    }

    for(int i=0; i<ui->cbCOM->count(); i++){
        QString name = ui->cbCOM->itemText(i);
        if(name.indexOf(val+" = ")==0){
            ui->cbCOM->setCurrentIndex(i);
            return;
        }
    }

    ui->cbCOM->insertItem(0, val+" = (Порт не найден)");
    ui->cbCOM->setCurrentIndex(0);
}

QString SettingsDialog::getCOMPort()
{
    QString name = ui->cbCOM->currentText();
    auto parts = name.split(" = ");
    if(parts.length() >= 2){
        return parts[0];
    }else{
        return "";
    }
}

void SettingsDialog::setTragetsFile(QString val)
{
    ui->edTargetsFile->setText(val);
}

QString SettingsDialog::getTargetsFile()
{
    return ui->edTargetsFile->text();
}

void SettingsDialog::setBaud(int val)
{
    ui->cbBaud->setCurrentText(QString("%1").arg(val));
}

int SettingsDialog::getBaud()
{
    return ui->cbBaud->currentText().toInt();
}

void SettingsDialog::setKeyboard(bool val)
{
    ui->cbKeyboard->setChecked(val);
}

bool SettingsDialog::getKeyboard()
{
    return ui->cbKeyboard->isChecked();
}

void SettingsDialog::setVolumeSlider(bool val)
{
    ui->cbVolume->setChecked(val);
}

bool SettingsDialog::volumeSlider()
{
    return ui->cbVolume->isChecked();
}

void SettingsDialog::setPass(QString val)
{
    ui->edPass->setText(val);
}

QString SettingsDialog::getPass()
{
    return ui->edPass->text();
}

void SettingsDialog::setColsCnt(int cnt)
{
    ui->cbColsCnt->setCurrentIndex(cnt-1);
}

int SettingsDialog::getColsCnt()
{
    return ui->cbColsCnt->currentIndex()+1;
}

void SettingsDialog::setTargsFntSz(int sz)
{
    if(sz<10 || sz>40)
        return;

    int i = (sz-10)/2;
    ui->cbTargetsFontSz->setCurrentIndex(i);
}

int SettingsDialog::getTargsFntSz()
{
    return ui->cbTargetsFontSz->currentIndex()*2+10;
}

void SettingsDialog::setPortPeriod(int val)
{
    ui->edPortPeriod->setText(QString("%1").arg(val));
}

int SettingsDialog::getPortPeriod()
{
    return ui->edPortPeriod->text().toInt();
}

void SettingsDialog::setTargCnt(int val)
{
    ui->cbTargCnt->setCurrentIndex(val==16 ? 0 : 1);
}

int SettingsDialog::getTargCnt()
{
    return ui->cbTargCnt->currentIndex()==0 ? 16 : 32;
}

bool SettingsDialog::getCloseApp()
{
    return ui->cbCloseApp->isChecked();
}

void SettingsDialog::on_btBrowseFolder_clicked()
{
    auto folder = QFileDialog::getExistingDirectory(this, "Выберите папку для аудио-файлов", ui->edAudioFolder->text());
    if(!folder.isEmpty())
        ui->edAudioFolder->setText(folder);

}

void SettingsDialog::keyboardForEdit(QLabel* label, QLineEdit* edit, Keyboard::kb_type_t type){
    if(!ui->cbKeyboard->isChecked())
        return;

    Keyboard kb(label->text(), edit->text(), this, type);
    if(kb.exec()==QDialog::Accepted)
        edit->setText(kb.getText());
}

void SettingsDialog::mousePressEvent(QMouseEvent *event)
{
    qDebug() << event->button() << event->localPos();
    pressed = true;
    pressPos = event->localPos();
    pressScrollPos = ui->scrollArea->verticalScrollBar()->value();
}

void SettingsDialog::mouseReleaseEvent(QMouseEvent *event)
{
    (void)event;
    pressed = false;
}

void SettingsDialog::mouseMoveEvent(QMouseEvent *event)
{
    if(pressed){
        auto pos = event->localPos();
        int dy = pos.y() - pressPos.y();
        ui->scrollArea->verticalScrollBar()->setValue(pressScrollPos-dy);
    }
}

void SettingsDialog::on_btIP_clicked()
{
    keyboardForEdit(ui->labelIP, ui->edIP);
}


void SettingsDialog::on_btPort_clicked()
{
    keyboardForEdit(ui->labelPort, ui->edPort);
}

void SettingsDialog::on_btInCh_clicked()
{
    keyboardForEdit(ui->labelInCh, ui->edInCh);
}

void SettingsDialog::on_btServerSetts_clicked()
{
    QDesktopServices::openUrl(QUrl("http://"+getIP()));
    reject();
    close();
}

void SettingsDialog::on_btPeriod_clicked()
{
    keyboardForEdit(ui->labelPeriod, ui->edPeriod);
}

void SettingsDialog::on_btBroadcast_clicked()
{
    keyboardForEdit(ui->labelBroadcast, ui->edBroadcast);
}

void SettingsDialog::on_cbAGO_currentIndexChanged(int index)
{
    ui->edIP->setVisible(index==0);
    ui->edBroadcast->setVisible(index==0);
    ui->edPort->setVisible(index==0);
    ui->edInCh->setVisible(index==0);
    ui->labelIP->setVisible(index==0);
    ui->labelBroadcast->setVisible(index==0);
    ui->labelPort->setVisible(index==0);
    ui->labelInCh->setVisible(index==0);
    ui->btIP->setVisible(index==0);
    ui->btBroadcast->setVisible(index==0);
    ui->btPort->setVisible(index==0);
    ui->btInCh->setVisible(index==0);
    ui->btServerSetts->setVisible(index==0);
    ui->labelPeriod->setVisible(index==0);
    ui->edPeriod->setVisible(index==0);
    ui->btPeriod->setVisible(index==0);

    ui->cbCOM->setVisible(index==1);
    ui->labelCOM->setVisible(index==1);
    ui->labelPortPeriod->setVisible(index==1);
    ui->edPortPeriod->setVisible(index==1);
    ui->btPortPeriod->setVisible(index==1);
    ui->labelTargetsFile->setVisible(index==1);
    ui->edTargetsFile->setVisible(index==1);
    ui->btTargetsFile->setVisible(index==1);
    ui->labelTargCnt->setVisible(index==1);
    ui->cbTargCnt->setVisible(index==1);
}


void SettingsDialog::on_btTargetsFile_clicked()
{
    QFileInfo info(ui->edTargetsFile->text());
    QString dir = info.absoluteDir().path();

    QString file = QFileDialog::getOpenFileName(this, "Выберите файл направлений", dir, "Текстовые файлы(*.txt)");
    if(!file.isEmpty()){
        ui->edTargetsFile->setText(file);
    }
}

void SettingsDialog::on_btEditTargets_clicked()
{
    TargetsEditor dlg(ui->edTargetsFile->text(), ago->targetsCount(), this);
    if(dlg.exec()){
        dlg.save();
    }
}


void SettingsDialog::on_btPass_clicked()
{
    keyboardForEdit(ui->labelPass, ui->edPass, Keyboard::KB_PASS);
}



void SettingsDialog::on_btPortPeriod_clicked()
{
    keyboardForEdit(ui->labelPortPeriod, ui->edPortPeriod, Keyboard::KB_NUMERIC);
}


void SettingsDialog::on_edPortPeriod_textChanged(const QString &arg1)
{
    bool ok = false;
    arg1.toInt(&ok);
    if(!arg1.isEmpty() && !ok)
        ui->edPortPeriod->setText("");
}

void SettingsDialog::on_cbCloseApp_stateChanged(int arg1)
{
    if(arg1 == Qt::Checked){
        QCoreApplication::quit();
    }
}
