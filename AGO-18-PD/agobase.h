#ifndef AGOBASE_H
#define AGOBASE_H

#include "remotesetts.h"
#include "ago09uart.h"
#include "statews.h"
#include "ago09uart.h"
#include "filesoundsource.h"
#include "fileinfo.h"

#include <QMediaPlayer>
#include <QObject>
#include <QTimer>
#include <udpchannel.h>
#include <atomic>
#include <QFuture>

class AgoBase : public QObject
{
    Q_OBJECT
public:
//    enum Buttons{
//        BtnStart=1,
//        BtnRec=2,
//        BtnPreview=4,
//        BtnFile=8,
//        BtnUpload=16,
//        BtnPlay2=32
//    };
    enum State {
        Stop,
        Mic,
        File,
        Rec
    };
    typedef quint32 ButtonsSet;

    explicit AgoBase(QObject *parent = nullptr);

    bool isBusy();
    std::atomic<bool> recActive;

    virtual void init() = 0;
    virtual int targetsCount() = 0;
    virtual void setTargets(targets_t targets) = 0;
    virtual void setCastState(bool state) = 0;
    virtual void micStart(qint64 outs, bool tangenta) = 0;
    virtual void micStop() = 0;
    bool isMicOn();
    virtual QVector<QString> getClips(){return QVector<QString>();}
    virtual void playClip(QString name, int source, uint32_t outs){(void)name; (void)source; (void)outs;}
    virtual void stopClip(int source){(void)source;}
    virtual bool uploadFile(QString file){(void)file; return false;}
    virtual void playFile(QString fileName, bool cast) = 0;
    virtual bool isFilePlay() = 0;
    virtual void stopFile() = 0;
    virtual void setTangentaEmu(bool state){(void)state;}
    virtual bool getTangentaEmu(){return false;}

    void startRec(QString fileName);
    void stopRec();
    void setVolume(int volume);

    static QString defAudioPath();
    static QString defTargetsPath();

private:
    QFuture<void> rec_task;

protected:
    bool micOn = false;
//    uint32_t targets = 0;
    QMediaPlayer* player = nullptr;
    PFileInfo fileInfo = nullptr;

    void emitOnVolume();
    uint32_t getTragetsForFile(QString file);
signals:
    void onStatusLabel(QString text);
    void onProgress(qint64 max, qint64 val, bool visible);
    void enableCtrls(bool enable);
    void onActivity(qint64 outs);
    void createOutChecks(int outs_cnt, QStringList names);
    void onClips(QStringList clips);
    void on_actionSetts_triggered();
    void onBKSActive(bool active);
    void onError(QString title, QString msg);
//    void enableButtons(bool enable, ButtonsSet ignore);
    void onState(State state);
    void on_tangenta(bool state);
//    void setActionStartChecked(bool checked);
//    void setActionFileChecked(bool checked);
//    void setActionPlayChecked(bool checked);
//    void setActionRecChecked(bool checked);
    void onVolume(int volume);
    void onActiveTargets(uint32_t outs, bool active);
protected slots:
    void onDurationChanged();
    void onPositionChanged(qint64 pos);
};

class Ago18 : public AgoBase {
    Q_OBJECT
public:
    explicit Ago18(QObject *parent = nullptr);
    virtual void init() override;
    virtual int targetsCount()override;
    virtual void setTargets(targets_t targets)override;
    virtual void setCastState(bool state)override{(void)state;}
    virtual void micStart(qint64 outs, bool tangenta)override;
    virtual void micStop()override;
    virtual QVector<QString> getClips()override;
    virtual void playClip(QString name, int source, uint32_t outs)override;
    virtual void stopClip(int source)override;
    virtual bool uploadFile(QString file)override;
    virtual void playFile(QString fileName, bool cast)override;
    virtual bool isFilePlay()override;
    virtual void stopFile()override;
private slots:
    void fileSendPorgress(int total, int pos);
    void fileSendComplete();
    void onConnectTimer();
    void onUploadProgress(qint64 sent, qint64 total);
    void onWSConnected();
    void onWSActivity(uint64_t outs);
    void onWSNames(int outs_cnt, QStringList names);
    void onWSClips(QStringList clips);
    void onPlayerState(QMediaPlayer::State state);
private:
    bool is_connected = false;
    RemoteSetts* rSt = nullptr;
    StateWS* stateWS = nullptr;
    QTimer connectTimer;
    UDPChannel* udp = nullptr;
    FileSoundSource* fileReader = nullptr;
    int paused_pos = 0;
    QString pausedFile = "";

    void connectServer();
    void previewFile(QString fileName);
};

class Ago09 : public AgoBase {
    Q_OBJECT
public:
    explicit Ago09(int targets_cnt, QObject *parent = nullptr);
    ~Ago09()override;
    virtual void init() override;
    virtual int targetsCount()override;
    virtual void setTargets(targets_t targets)override;
    virtual void setCastState(bool state)override;
    virtual void micStart(qint64 outs, bool tangenta)override;
    virtual void micStop()override;
    virtual void playFile(QString fileName, bool cast)override;
    virtual bool isFilePlay()override;
    virtual void stopFile()override;
    virtual void setTangentaEmu(bool state)override;
    virtual bool getTangentaEmu()override;
private slots:
    void onBKSReady(bool ready);
    void onTangenta(bool state);
    void onPlayerState(QMediaPlayer::State state);
    void connectState(Ago09Uart::Ago09UartStates state);
    void onBusyTargets(targets_t targets, bool cast);
private:
    QString comPortName;
    Ago09Uart comPort;
    bool cast = false;
    int targets_cnt;

    QStringList loadTargets();
    void openComPort();
};

#endif // AGOBASE_H
