<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?1">
        
        <style>
            .sm-text{
                font-size: 12px;
            }
            .white-text{
                color: white;
            }
        </style>
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        include "navbar.php";
        include "alert.html";
        ?>
        
        <div class="container-fluid" id="main-container">
            <br>
            <h1><img src="img/sound.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Настройки звука</h1>
            <div id="snd_params" class="row mb-5"></div>
            <br>
            
            
            
        </div>
        
        <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/alertify.min.js"></script>
        <script src="js/alert.js" crossorigin="anonymous"></script>
        <script src="js/constants.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/rest.js"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script type="text/javascript" src="js/MyView.js"></script>
        <script type="text/javascript" src="js/SoundSetts.js"></script>
        <script>
            var snd_params = new SoundParams($("#snd_params"));
            
            get_settings(function(settings){
                snd_params.init();
            });

            /*
            var snd_params;
            
            function setPrmRange(prm, min, max, step){
                console.dir(prm);
                console.log("Set range "+" "+min+" "+max+" "+step);
                $("#"+prm).attr('min', min);
                $("#"+prm).attr('max', max);
                $("#"+prm).attr('step', step);
                
                $("#"+prm+"-min").html(min+"dB");
                $("#"+prm+"-max").html(max+"dB");
            }
            
            function setPrmVal(p, v){
                var lblId = p.attr('id')+"-value";
                var descr = p.attr('descr');
                
                p.val(v);
                
                $("#"+lblId).html(v+"dB");//${descr}  
                
                p.on('change', function(){
                    setPrmVal($(this), $(this).val());
                })
            }
            
            function createParam(prm, ch, type){
                var div = $('<div>',{
                    class: 'row'
                });
                
                var ctrl_class = type==1 ? "ctrl-out" : "ctrl-in";
                
                var optCnt = (prm.max - prm.min)/prm.step + 1;
                
                if(prm.type == null){
                    div.append($('<label>',{
                        class: 'col-5',
                        for: prm.name,
                        text: prm.descr+": ",
                        id: prm.name+"-label"
                    }));

                    div.append($('<label>',{
                        class: 'col-5',
                        id: prm.name+"-value"
                    }));

                    var slider = $('<input>',{
                        class: 'col-12 form-control-range '+ctrl_class,
                        type: "range", 
                        min: prm.min, 
                        max: prm.max, 
                        step: prm.step, 
                        id: prm.name,
                        descr: prm.descr,
                        ch: ch
                    });
                    div.append(slider);
                    
                    div.append("<label class='col-3' id='"+prm.name+"-min'>0dB</label><span class='col-6'></span><label class='col-3 text-right' id='"+prm.name+"-max'>10dB</label>");
                }else if(prm.type == "space"){
                    div.append($('<div>',{
                        class: 'col-12',
                        height: 50
                    }));
                }else if(prm.type == "volt"){
                    var html = "<div class='input-group mb-3'>\n\
                                    <div class='input-group-prepend'>\n\
                                        <span class='input-group-text'>"+prm.descr+"</span>\n\
                                    </div>\n\
                                    <select class='form-control "+ctrl_class+"' id='"+prm.name+"' ch='"+ch+"'>\n\
                                        <option value='0'>0В</option>\n\
                                        <option value='1'>30В</option>\n\
                                        <option value='2'>60В</option>\n\
                                        <option value='3'>100В</option>\n\
                                    </select>\n\
                                </div>";
                    div.append(html);
                }
                
                //updatePrmLabel(slider);
                
//                var select = $('<select>',{
//                    class: "form-control", 
//                    id: `${prm.name}`
//                });
//                for(var i=0; i<optCnt; i++){
//                    var x = prm.min + i*prm.step;
//                    select.append($('<option>',{
//                        text: `${x}`
//                    }));
//                }
//                div.append(select);
                
                return div;
            }
            
            function createCard(ch, type, img_name, params){
                //var wd = Math.round(12/params.length);
                var s = "";
                
                var s_type = type==1 ? "Выход" : "Вход";
                var prms = type==0 ? settings.ins : settings.outs;
                var card_name = (ch >= prms.length || prms[ch].name == "") ? s_type+" "+(ch+1) : prms[ch].name;
                
                var div = $('<div>',{
                    class: 'col-6'
                });
                
                var card = $('<div>', {
                    class: 'card shadow', 
                    //style: 'width: 18rem;'
                });
                div.append(card);
                
                var card_header = $('<div>',{
                    class: "card-header"
                });
                card.append(card_header);
                
                var h5 = $('<h5>',{
                    class: 'card-title'
                });
                card_header.append(h5);
                
                var img = $('<img>',{
                    src:"img/"+img_name, 
                    class: "card-img-left", 
                    width: "32", 
                    height: "32"
                });
                h5.append(img);
                
                var title = $('<span>',{
                    text: "   "+card_name
                });
                h5.append(title);

                var card_body = $('<div>', {
                    class: 'card-body'
                });
                card.append(card_body);

                var form = $('<form>');
                card_body.append(form);
                
                var form_row = $('<div>',{
                    class: "form-row"
                });
                form.append(form_row);
                
                for(var i=0; i<params.length; i++){
                    var form_group = $('<div>',{
                        class: "form-group col-md-"+params[i].wd
                    });
                    form_row.append(form_group);
                    
                    form_group.append(createParam(params[i], ch, type));
                }
                
                var btnClass = type==1 ? "btn-save-out" : "btn-save-in";
                var btn = $('<a>',{
                    class: "btn btn-outline-success "+btnClass, 
                    ch: ch,
                    text: 'Сохранить'
                });
                form.append(btn);
                
                return div;
            }
            
            function update(){
                var chCnt = (insCnt > outsCnt) ? insCnt : outsCnt;
                for(var i=0; i<chCnt; i++){
                    var row = $('<div>',{
                        class: 'row'
                    });
                    $("#main-container").append(row);

                    if(i < insCnt){
                        row.append(createCard(i, 0, "input.svg", [
                           {name: "in-vol-"+i, descr: "Уровень", min: 0, max: 10, step: 1, wd: 12},
                           //{name: "", descr: "", min: 0, max: 10, step: 1, wd: 12},
                        ]));
                    }else{
                        row.append("<div class='col-6'></div>");
                    }

                    if(i < outsCnt){
                        row.append(createCard(i, 1, "output.svg", [
                           {name: "out-vol-"+i, descr: "Громкость", min: 0, max: 10, step: 1, wd: 12},
                           //{name: "out-treble-"+i, descr: "ВЧ", min: 0, max: 10, step: 1, wd: 5},
                           //{name: "", descr: "", type: "space", wd: 2},
                           //{name: "out-bass-"+i, descr: "НЧ", min: 0, max: 10, step: 1, wd: 5},
                           {name: "out-volt-"+i, descr: "Напряжение", type: "volt", wd: 12}
                        ]));
                    }else{
                        row.append("<div class='col-6'></div>");
                    }

                   $("#main-container").append($('<div>',{
                       height: 10
                   }));
                }

                var query = {
                    command: 'get_snd_params'
                };
                var strQuery = JSON.stringify(query);
                $.get('command.php', {query: strQuery}, function(answer){
                    console.log(answer);
                    
                    var obj = JSON.parse(answer);
                    snd_params = obj.snd_params;
                    if(obj.result == "success"){
                        console.dir(snd_params);
                        var ins = snd_params.ins;
                        var outs = snd_params.outs;

                        if(ins != null){
                            console.dir(ins);
                            for(var i=0; i<ins.length; i++){
                                if(ins[i] != null){
                                    setPrmRange("in-vol-"+i, ins[i].min, ins[i].max, ins[i].step);
                                    setPrmVal($("#in-vol-"+i), ins[i].vol);
                                }
                            }
                        }

                        if(outs != null){
                            console.dir(outs);
                            for(var i=0; i<outs.length; i++){
                                if(outs[i] != null){

                                    setPrmRange("out-vol-"+i, outs[i].min, outs[i].max, outs[i].step);

                                    setPrmVal($("#out-vol-"+i), outs[i].vol);
                                    setPrmVal($("#out-treble-"+i), outs[i].treble);
                                    setPrmVal($("#out-bass-"+i), outs[i].bass);
                                    setPrmVal($("#out-volt-"+i), outs[i].voltage);
                                }
                            }
                        }
                    }
                });
                
                function save_outs(){
                    var ch = parseInt($(this).attr("ch"), 10);
                    console.log("Save out"+ch);
                    
                    snd_params.outs[ch].vol = parseFloat($("#out-vol-"+ch).val());
                    snd_params.outs[ch].treble = parseInt($("#out-treble-"+ch).val(), 10);
                    snd_params.outs[ch].bass = parseInt($("#out-bass-"+ch).val(), 10);
                    snd_params.outs[ch].voltage = parseInt($("#out-volt-"+ch).val(), 10);

                    var query = {
                        command: 'set_snd_params',
                        channel_type: "outs",
                        channel: ch,
                        snd_params: snd_params.outs[ch]
                    };
                    var strQuery = JSON.stringify(query);
                    console.log(strQuery);

                    $.post('command.php', {query: JSON.stringify(query)}, function(answer){
                        var obj = JSON.parse(answer);
                        console.log(obj.result);
                        if(obj.result == "success"){
                            //showAlert("Сохранение", "Сохранено успешно");
                            alertify.success("Сохранено успешно");
                        }else{
                            //showAlert("Сохранение", "Ошибка сохранения");
                            alertify.error("Ошибка сохранения");
                        }
                    });
                }
                
                function save_ins(){
                    var ch = parseInt($(this).attr("ch"), 10);
                    console.log("Save in"+ch);
                    
                    if(snd_params == null)
                        snd_params = {
                            ins: [],
                            outs: []
                        };
                    
                    snd_params.ins[ch] = {
                        vol: parseInt($("#in-vol-"+ch).val(), 10)
                    };

                    var query = {
                        command: 'set_snd_params',
                        channel_type: "ins",
                        channel: ch,
                        snd_params: snd_params.ins[ch]
                    };
                    var strQuery = JSON.stringify(query);
                    console.log(strQuery);

                    $.post('command.php', {query: JSON.stringify(query)}, function(answer){
                        var obj = JSON.parse(answer);
                        console.log(obj.result);
                        if(obj.result == "success"){
                            //showAlert("Сохранение", "Сохранено успешно");
                            alertify.success("Сохранено успешно");
                        }else{
                            //showAlert("Сохранение", "Ошибка сохранения");
                            alertify.error("Ошибка сохранения");
                        }
                    });
                }
                
                $(".btn-save-out").click(save_outs);
                $(".ctrl-out").change(save_outs);

                $(".btn-save-in").click(save_ins);
                $(".ctrl-in").change(save_ins);
            }
            
            get_settings(function(setts){
                update();
            });
            */
        </script>
    </body>
</html>



