#ifndef WORDWRAPCHECK_H
#define WORDWRAPCHECK_H

#include <QCheckBox>
#include <QWidget>

namespace Ui {
class WordWrapCheck;
}

class WordWrapCheck : public QWidget
{
    Q_OBJECT

public:
    explicit WordWrapCheck(QWidget *parent = nullptr);
    ~WordWrapCheck();

    void setText(QString text);
    void setChecked(bool checked);
    bool isChecked();
    QCheckBox* getCheckBox();

private:
    Ui::WordWrapCheck *ui;
};

#endif // WORDWRAPCHECK_H
