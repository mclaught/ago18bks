#ifndef LOG_H
#define LOG_H

#include <QFile>
#include <QObject>

class Log : public QObject
{
    Q_OBJECT
public:
    explicit Log(QObject *parent = nullptr);

    void log(QString str);
private:
    QFile* file = nullptr;

signals:

};

extern Log _log;

#endif // LOG_H
