#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCheckBox>
#include <QElapsedTimer>
#include <QMainWindow>
#include <QTcpSocket>
#include <QTimer>
#include <QUdpSocket>
#include <QAudioOutput>
#include <QFuture>

QT_BEGIN_NAMESPACE

class QAudioOutput;
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define MAIN_PORT       46000
#define MAX_PACKET_SIZE (2048)
#define AUDIO_FRAME_MS		5
#define AUDIO_FREQ		48000
#define AUDIO_SAMPLES		AUDIO_FRAME_MS * AUDIO_FREQ / 1000
#define AUDIO_LEN_BYTES         AUDIO_SAMPLES * 2
#define MAX_INP_CHANNELS        2
#define BC_IP   "192.168.0.255"

typedef enum {
  PACK_AUDIO = 0, PACK_LOGS, PACK_ALARM, PACK_CFG, PACK_OTHER, PACK_BRU_CFG, PACK_GEN_CALL,
          PACK_TYPES_CNT
}pack_type_t;//тип пакета для быстрой коммутации

typedef enum {
    NO_ACTION = 0, SND_START_REQ, SND_STOP_REQ, SND_CHANK_REQ, SND_IN_CPB_REQ, SND_OUT_CPB_REQ, SND_IN_CURR_REQ, SND_OUT_CURR_REQ, //7
    EXT_SWT, SND_STREAM, AUDIO_CFG_IN, AUDIO_CFG_OUT, CONTROL_CFG, ALARM_MSG, LOG_MSG, SET_RECIPNT_LIST, CLEAR_RECIPNT_LIST, //16
    CH_BUSY_MSG, SET_MASTER, CLEAR_MASTER, REC_NUMBER, GET_TEMPERATURE, BRU_TEST_REQ, BRU_TEST_END, UM_SWITCH_REQ, BRU_FROM_BYPS_SWITCH_REQ, //25
    BRU_TO_BYPS_SWITCH_REQ, BRU_START_MEASURE, BRU_STOP_MEASURE, BRU_RAW_DATA, BRU_LINE_MEASURE, BRU_TEST_MODE_START, BRU_TEST_MODE_STOP, //32
    BRU_SAVE_CFG, SCREEN_MSG, BRU_LINE_MEAS_RESULT, BRU_GET_STATE, CHANGE_ID, GET_ID, UM_UNLOCK, FW_BOOT_MODE, FW_FILE_INFO, FW_GET_FILE, //42
    FW_UPDATE_BEGIN, FW_UPDATE_STATUS, FW_RESET, FW_INFO, SWPT_REQ, READ_FLASH_CFG, WRITE_FLASH_CFG, BRU_RESET_SWITCH_CFG, FAKE_ALARM, //51
    MON_INFO, HW_EQ, SW_EQ, SW_EQ_BYPASS, UM_FEEDBACK, HPF_CFG, PA_CFG, PA_CNTRL_ASCII, PA_CNTRL_BIN, LOG_CFG, GET_LOG_CFG, CLEAR_FLASH_CFG, //63
    BRU_MANUAL_SWITCH, BRU_GET_CFG, UM_BUF_SYNC_PARAM, UM_BUF_SYNC_STAT, DPA_MON, DPA_MANUAL, MOT_CMD, UM_MANUAL_ID_SET, AUDIO_CAL_OUT, //72
    BKS_GET_INP_MODE, BKS_SET_INP_MODE, BRP_GET_CRITICAL_ALARMS, BRP_SET_CRITICAL_ALARMS, BRU_GET_CAL, BRU_SET_CAL, PING, //79
    CMD_NUMBER
}eth_cmd_t;

#pragma pack(push, 1)

typedef struct
{
  uint8_t adr_to;//номер платы назначения (адрес ПК 0x80)
  uint8_t adr_from;//номер платы отправителя
  uint8_t repeate_cnt;//счетчик повторов от 0
  uint8_t type;// тип пакета
  uint16_t len;//длина оставшейся части пакета
}pack_hdr_t;//заголовок любого пакета

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    uint8_t data[MAX_PACKET_SIZE];
}packet_buf_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    uint8_t src_ch;
    uint32_t recipients;
//    uint8_t recipients_amount;
//    uint8_t recipients[MAX_CHANNELS];
}start_stop_t;

typedef struct
{
  uint8_t sender;//номер канала отправителя.
  uint32_t recip;//бит. маска получателей
}audio_hdr_t;//добавочный заголовок аудиопакета для 32 получателей

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    audio_hdr_t audio_hdr;
    uint16_t pcm[AUDIO_SAMPLES*2];
}packet_audio_t;

typedef struct{
    uint8_t cmd;
    uint16_t len;
}pd_packet_hdr_t;

typedef struct{
    pd_packet_hdr_t hdr;
    uint8_t data[];
}pd_packet_t;

typedef struct{
    uint8_t frame_length_ms;
    uint8_t format;
    uint32_t bitrate;
    uint8_t channels;
    uint8_t in_ch[2];
    uint32_t outs;
}pd_start_t;

typedef enum{
    PD_START,
    PD_STREAM
}pd_command_t;

typedef struct{
    uint32_t status;
}pd_stream_t;

typedef enum {
//    NO_ALARM = 0, ALARM_100C, ALARM_125C, ALARM_DS1621S_OVR60, ALARM_SD
    T100 = 0, T125, SD, UM_ERROR, HW_ERROR, DS1621S_OVR60, BRU_UM_NOT_ANSWER, BRU_LINE_SHORT, BRU_LINE_OPEN, BRU_LINE_OVER_LIM,
    BRU_SWITCH_TO_BYPASS_ERROR, BRU_SWITCH_BACK_ERROR, BRU_REDUND_UM_ACTIVE
}alarm_t;

typedef struct
{
  uint8_t alarm_code;//код аварии. Для отмены аварии выставляется старший бит. Пример T125 - авария, (T125 | CLEAR_ALARM) - отмена
  uint8_t param;//номер канала/код ошибки
}alarm_item_t;

typedef struct
{
  float min_dB;//минимальный уровень усиления, дБ
  float max_dB;//максимальный уровень усиления, дБ
  float step_dB;//шаг изменения усиления, дБ
}codec_capabilities_t;

typedef struct
{
  float min_out_gain_dB;//минимальное усиление на выходе каждой секции
  float max_out_gain_dB;//максимальное усиление на выходе каждой секции
  float min_gain_dB;//максимальное ослабление каждой секции
  float max_gain_dB;//максимальный подъем каждой секции
  float min_band;//минимальная ширина полосы (коэфф)
  float max_band;//максимальная ширина полосы (коэфф)
  uint32_t fmin_hz;//минимальная центральная частота
  uint32_t fmax_hz;//максимальная центральная частота
  uint32_t sections;//кол-во секций эквалайзера в каждом канале
}eq_capabilities_t;

typedef struct
{
  uint32_t number;//кол-во настроек частоты среза
  uint32_t fcut[4];//массив частот среза, текущее значение number == HP_FCUT_VAR
}hpf_capabilities_t;

typedef struct
{
  uint8_t out_lvl_amount;//кол-во значений выходного напряжения (сейчас == 4)
  uint8_t out_lvl[4];//массив значений напряжений (сейчас 0,30,60,100). Для настройки используются индексы этих значений, а сами значения - для отображения в интерфейсе
}pa_capabilities_t;

typedef struct
{
  codec_capabilities_t inp_capb;//возможности по входу
  eq_capabilities_t eq_capb;
}snd_in_capab_t;

typedef struct
{
  codec_capabilities_t outp_capb;//..по выходу
  eq_capabilities_t eq_capb;
  hpf_capabilities_t hpf_capb;
  pa_capabilities_t pa_capb;
}snd_out_capab_t;

typedef struct
{
    float gain;
    float band;
    uint32_t freq;
}eq_param_t;

typedef struct
{
    uint32_t sections;
    float outGain;
}eq_in_common_t;

typedef struct
{
    uint32_t sections;
    eq_param_t ec[2];//реальное кол-во секций эквалайзера узнается из поля sections в eq_capabilities_t
    float outGain;
}eq_out_common_t;

typedef struct
{
  float lvl[MAX_INP_CHANNELS];
  eq_in_common_t eq[MAX_INP_CHANNELS];
}snd_in_cfg_t;

typedef struct
{
  float lvl[MAX_INP_CHANNELS];
  eq_out_common_t eq[MAX_INP_CHANNELS];
  uint32_t fcut_indx[MAX_INP_CHANNELS];
  uint8_t amplf_volt[MAX_INP_CHANNELS];//заменить out_voltage_t на uint8_t
}snd_out_cfg_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    snd_in_capab_t snd_capab;
    snd_in_cfg_t snd_cfg;
}snd_in_capab_init_packet_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    snd_out_capab_t snd_capab;
    snd_out_cfg_t snd_cfg;
}snd_out_capab_init_packet_t;

typedef struct
{
  float low_trshld_ohm;//нижний порог отклонения сопротивления линии, Ом
  float high_trshld_ohm;//верхний порог отклонения, Ом
  float ref_value_ohm;//референсное значение сопротивления линии, Ом
  float shortOhm;
  float openOhm;
  uint8_t state;//состояние линии по результатам измерения line_state_t
  uint8_t range;//режим измерений (величина тока в линии) meas_mode_t
}line_t;

typedef struct
{
  uint8_t lines;//кол-во линий в профиле (ограничено аппаратно, сейчас 20)
  uint8_t line_offset;//сдвиг, если линии подключены не с 1-го входа (обычно 0)
  uint8_t line_min;//минимальный номер линии (индекс), контролируемый БРУ
  uint8_t line_max;//максимальный номер линии (индекс), контролируемый БРУ
  uint32_t test_mode_settle_time_ms;//время установления в режиме создания профиля, мсек.
  uint32_t work_mode_eq_range_settle_time_ms;//время установления в рабочем режиме на том же диапазоне, мсек.
  uint32_t work_mode_diff_range_settle_time_ms;//время установления в рабочем режиме при переключении на другой диапазон, мсек.
  uint32_t work_mode_cycle_period_sec;//период проверки линий в рабочем режиме, сек.
  uint8_t mode;//текущий режим работы. main_mode_t
  uint8_t max_meas_tolerance_proc;//макс. отклонение в %
  uint8_t max_devi_tolerance_proc;//макс. допуск для отклонения в %
  uint8_t switch_state;//куда подключена нагрузка. 0xFF - штатно, иначе ID сгоревшего УМ.
  uint8_t redund_um_id;//номер резервного УМ
  line_t line[20];//профиль трансляционных линий
}bru_cfg_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    bru_cfg_t bru_cfg;
}bru_cfg_packet_t;

typedef struct
{
  float val;//измеренное значение импеданса, Ом
  float ref_val;//референсное значение импеданса, Ом
  uint8_t line;//номер (индекс) линии
  uint8_t lstate;//состояние линии по результатам измерения line_state_t
  uint8_t mode;//режим измерений (величина тока в линии) meas_mode_t
}bru_measure_t;

typedef struct{
    pack_hdr_t hdr;
    uint8_t cmd;
    bru_measure_t bru_measure;
}bru_measure_packet_t;

#pragma pack(pop)


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    virtual void closeEvent(QCloseEvent *event) override;
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void tcpError(QAbstractSocket::SocketError socketError);
    void on_toolButton_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_btnUDP_clicked();

    void on_btnStop_clicked();

    void on_btnAlalrmSet_clicked();

    void on_btnAlalrmReset_clicked();

    void on_btOUT_CPB_clicked();

    void on_btnRequest_clicked();

    void on_btnStopAudio_clicked();

    void on_bt_REC_NUMBER_clicked();

    void on_bt_UM_MANUAL_ID_SET_clicked();

private:
    Ui::MainWindow *ui;
    bool go = false;
    QCheckBox* dstCheck[32];
    QTcpSocket* tcp;
    uint32_t bitrate = 0;
    pd_stream_t pd_stream;
    int period = 5000;
    int request_cnt = 0;
    QElapsedTimer timer;
    qint64 req_tm;
    QTimer reqTimer;
    QAudioOutput* m_audioOut = nullptr;
    QIODevice* m_audioDev = nullptr;
    int16_t audio_buf[AUDIO_SAMPLES*100];
    int audio_buf_in = 0;
    QFuture<void> playThread;

//    void readUdp();
    void requestPacket(QUdpSocket *udp, int cnt);
    void setGo(bool _go);
    void alarmSet(quint8 _set);
    void send_OUT_CPB(qint8 addr);
    void send_IN_CPB(qint8 addr);
    void sendBRUCfg(uint8_t id, uint8_t switch_state, int n);
    void send_BRU_TEST_MODE_START(uint8_t addr);
    void send_BRU_LINE_MEASURE(uint8_t addr, uint8_t line);
    void send_BRU_RESET_SWITCH_CFG(uint8_t addr);
    void send_GET_ID(uint8_t adr_to, uint8_t adr_from);
    void play_audio(packet_audio_t* pack);
    void send_BKS_GET_INP_MODE(int addr, int val);
    void send_BRP_SET_CRITICAL_ALARMS(int addr);
    void sendPing(int addr, int type);
};
#endif // MAINWINDOW_H
