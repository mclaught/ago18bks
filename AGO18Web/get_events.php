<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$path = "/var/www/html/AGO18Web/events";

if($_GET["cmd"] == "files"){
    $array = [];
    
    $files = scandir($path, SCANDIR_SORT_DESCENDING);
    
    for($i=0; $i<count($files); $i++){
        $file = $files[$i];
        
        if(!is_dir($file)){
            $info = pathinfo($file);
            $array[] = $info['filename'];
        }
    }
    
    echo json_encode($array);
}

if($_GET['cmd'] == 'log'){
    $file = $path.'/'.$_GET['file'].'.log';
    echo file_get_contents($file);
}
    
