<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?6">
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        include "navbar.php";
        include "alert.html";
        
        $max_size = ini_get('upload_max_filesize');
        $max_size = str_replace("K", "000", $max_size);
        $max_size = str_replace("M", "000000", $max_size);
        $max_size = str_replace("G", "000000000", $max_size);
//        echo "max sz = ".$max_size."<br>";
        
        $clip_sources = '';
        
        $i = 0;
        ?>
        
        <div class="container" id="main-container">
            <?php
//            include_once 'upload_clip.php';
            
            function cmp($a, $b){
                $s1 = $a->signal;
                $s2 = $b->signal;
                if($s1 == -1)
                    $s1 = 1000000;
                if($s2 == -1)
                    $s2 = 1000000;
                if ($s1 == $s2) {
                    return 0;
                }
                return ($s1 < $s2) ? -1 : 1;
            }
            
            function show_clips($dir){
                global $clip_sources, $i;
                
                ?>
                    <div class="row pl-5">
                        <span class="col-1"></span>
                        <label class="col">Файл</label>
                        <!--<span class="col-1"></span>-->
                        <label class="col-1">Сигнал</label>
                        <label class="col-1">Вход</label>
                        <span class="col-2"></span>
                    </div>
                <?php
                
                $clips = scandir($dir);
                
                $defs = [];
                foreach ($clips as $clip){
                    if(!is_dir("$dir/$clip") && pathinfo("$dir/$clip", PATHINFO_EXTENSION)!="json"){
                        $def_file = "$dir/$clip.json";
                        $def = (object)[
                            "signal" => -1,
                            "in" => -1
                        ];
                        if(file_exists($def_file)){
                            $def_str = file_get_contents($def_file);
                            $def = json_decode($def_str);
                        }
                        $def->dir = $dir;
                        $def->clip = $clip;
                        $defs[] = $def;
                        
                        if($clip_sources != ''){
                            $clip_sources .= ',';
                        }
                        $clip_sources .= $def->in;
//                        echo $clip_sources;
                    }
                }
                
                usort($defs,"cmp");
                
                foreach ($defs as $d) {
                    ?>
                    <div class="clip-line pr-3" dir="<?php echo $d->dir; ?>">
                        <input type="hidden" name="command" value="delete" />
                        <input type="hidden" name="filename" class="filename-old" value="<?php echo $d->clip; ?>"/>
                        <input type="hidden" name="dir" class="filename-dir" value="<?php echo "$d->dir"; ?>"/>
                        <div class="row mb-3 ml-3">
                            <div class="col input-group">
                                <div class="input-group-append">
                                    <a class="btn btn-secondary btn-extra pt-1" title="Дополнительно" data-toggle="collapse" 
                                       href="#clipCollapse<?php echo $i; ?>" role="button" 
                                       aria-expanded="false" aria-controls="clipCollapse<?php echo $i; ?>">
                                        <svg width="20" height="20" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g fill="#ffffff" class="fill-241f20"><path d="M24 0C10.745 0 0 10.745 0 24s10.745 24 24 24 24-10.745 24-24S37.255 0 24 0zm0 44C12.954 44 4 35.046 4 24S12.954 4 24 4s20 8.954 20 20-8.954 20-20 20z"></path><path d="M35 22h-9v-8.998h-4V22h-9v4h9v8.998h4V26h9z"></path></g></svg>
                                    </a>
                                </div>    
                                <div class="play-stop-btns input-group-append" style="display: none;">
                                    <button class="btn btn-success btn-play-outs pt-1" source="<?php echo $d->in; ?>" filename="<?php echo $d->clip; ?>" signal="<?php echo $d->signal; ?>" filedir="<?php echo "$d->dir"; ?>">
                                        <svg width="20" height="20" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M-838-2232H562v3600H-838z" fill="none"></path>
                                            <path d="M16 10v28l22-14z" fill="#ffffff" class="fill-000000"></path>
                                            <path d="M0 0h48v48H0z" fill="none"></path>
                                        </svg>
                                    </button>
                                    <button style="display: none;" class='btn btn-danger btn-stop pt-1' title="Остановить" source="<?php echo $d->in; ?>" filename="<?php echo $d->clip; ?>" filedir="<?php echo $d->dir; ?>" signal="<?php echo $d->signal; ?>">
                                        <svg width="20" height="20" baseProfile="tiny" version="1.2" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16 8v8H8V8h8m0-2H8c-1.1 0-2 .9-2 2v8c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2z" fill="#ffffff" class="fill-000000"></path>
                                        </svg>
                                    </button>
                                </div>
                                <input class='form-control filename-new file-params' value="<?php echo $d->clip; ?>">
                            </div>    
                            <input class='form-control clip-signal file-params col-1' 
                                   title="Сигнал" value="<?php echo $d->signal!=-1 ? $d->signal : ""; ?>" 
                                   dir="<?php echo $d->dir; ?>">
                            <input class='form-control clip-in file-params col-1' title="Вход" value="<?php echo $d->in!=-1 ? ($d->in+1) : ""; ?>" dir="<?php echo $d->dir; ?>">
                            <div class="btn-group col-2 ">
                                <button class='btn btn-primary btn-rename' title="Сохранить клип" type="button" disabled="disabled">
                                    <svg width="20" height="20" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5 21h14a2 2 0 0 0 2-2V8a1 1 0 0 0-.29-.71l-4-4A1 1 0 0 0 16 3H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2zm10-2H9v-5h6zM13 7h-2V5h2zM5 5h2v4h8V5h.59L19 8.41V19h-2v-5a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v5H5z" fill="#FFFFFF" class="fill-000000"></path>
                                    </svg>
                                </button>
                                <button class='btn btn-secondary btn-del' title="Удалить">
                                    <svg width="20" height="20" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                        <g data-name="Layer 17"><path d="M24 31H8a3 3 0 0 1-3-3V9a1 1 0 0 1 2 0v19a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1V9a1 1 0 0 1 2 0v19a3 3 0 0 1-3 3ZM28 7H4a1 1 0 0 1 0-2h24a1 1 0 0 1 0 2Z" fill="#ffffff" class="fill-101820"></path><path d="M20 7a1 1 0 0 1-1-1V3h-6v3a1 1 0 0 1-2 0V2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v4a1 1 0 0 1-1 1ZM16 26a1 1 0 0 1-1-1V11a1 1 0 0 1 2 0v14a1 1 0 0 1-1 1ZM21 24a1 1 0 0 1-1-1V13a1 1 0 0 1 2 0v10a1 1 0 0 1-1 1ZM11 24a1 1 0 0 1-1-1V13a1 1 0 0 1 2 0v10a1 1 0 0 1-1 1Z" fill="#ffffff" class="fill-101820"></path></g></svg>
                                </button>
                            </div>
                            <div class="valid-feedback">Нажмите <b>Сохранить</b></div>
                        </div>
                        <div class="collapse mb-3 ml-5 mr-5 pb-3 play-panel alert alert-primary shadow " 
                             id="clipCollapse<?php echo $i; ?>" source="<?php echo $d->in; ?>"
                             dir="<?php echo $d->dir; ?>">
                            <blockquote class="blockquote blockquote-left-border">
                                Клип воспроизводится на выходы, согласно таблице коммутации.<br>
                                Для воспроизведения на другие выходы, введите их в поле ниже,<br>
                                разделяя список любым разделителем ("1,2,3", "1;2;3", "1 2 3")
                            </blockquote>
                            <div class="d-flex justify-content-between m-3">
                                <div class=" mr-3 flex-grow-1">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Выходы
                                            </span>
                                        </div>
                                        <input type="text" class='form-control clip-outs' title="Выходы" value="" placeholder="Воспроизвести на другие выходы">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <a class="btn-clear-outs">
                                                    <svg width="20" height="20" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path d="M38 12.83 35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z" fill="#7a7a7a" class="fill-000000"></path><path d="M0 0h48v48H0z" fill="none"></path></svg>
                                                </a>
                                            </div>    
                                        </div>
                                    </div>
                                </div>    

                                <div class="form-check mr-3 mt-1 autorew-check">
                                    <input class="form-check-input autorew" type="checkbox" id="autorew<?php echo $i; ?>" source="<?php echo $d->in; ?>">
                                    <label class="form-check-label" for="autorew<?php echo $i; ?>">Зациклить</label>
                                </div>
                                
                                <div>
                                    <button class="btn btn-success btn-play-outs" source="<?php echo $d->in; ?>" filename="<?php echo $d->clip; ?>" signal="<?php echo $d->signal; ?>" filedir="<?php echo "$d->dir"; ?>">
                                        <svg width="24" height="24" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M-838-2232H562v3600H-838z" fill="none"></path>
                                            <path d="M16 10v28l22-14z" fill="#ffffff" class="fill-000000"></path>
                                            <path d="M0 0h48v48H0z" fill="none"></path>
                                        </svg>
                                    </button>
                                    <button style="display: none;" class='btn btn-danger btn-stop' title="Остановить" source="<?php echo $d->in; ?>" filename="<?php echo $d->clip; ?>" filedir="<?php echo $d->dir; ?>" signal="<?php echo $d->signal; ?>">
                                        <svg width="24" height="24" baseProfile="tiny" version="1.2" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16 8v8H8V8h8m0-2H8c-1.1 0-2 .9-2 2v8c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2z" fill="#ffffff" class="fill-000000"></path>
                                        </svg>
                                    </button>
                                </div>    
                            </div>    
                        </div>
                    </div>
                    <?php    
                    
                    $i++;
                }
            }
            ?>
            
            <br>
            <h1><img src="img/audio_rec.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Аудио-клипы</h1>
            <br>

            <div id="is-play" class="alert alert-danger" role="alert" style="display: none;">
                <h5>Идет воспроизведение.</h5>
                <hr>
                <span id="play-clips"></span>
                <a id="btn-stop-all" href="#" class="alert-link">Остановить все</a>
            </div>
            
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-outline-secondary" data-toggle="collapse" href="#collapseUpload" role="button" aria-expanded="false" aria-controls="collapseUpload">Загрузить</a>
                        <!--<button id="btn-stop-all" class="btn btn-danger" style="display: none;">Остановить все</button>-->
                    </div>
                </div>
                <div class="card-body collapse" id="collapseUpload">
                    <form enctype="multipart/form-data" method="post" action="upload_clip.php">
                        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_size;?>" />
                        <input type="hidden" name="command" value="upload" />

                        <div class="">
                            <div class="input-group mt-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        Файл
                                    </span>
                                </div>
                                <input type="file" class="form-control p-1 is-invalid" name="upload-clip" id="upload-clip">
                                <!--<div class="invalid-feedback">Необходимо выбрать файл</div>-->
                            </div>
                            
                            <div class="input-group mt-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        Назначение
                                    </span>
                                </div>
                                <select class="form-control is-invalid" name="dir" id="dir-select">
                                    <option value=""></option>
                                    <option value="clips">Сервер</option>
                                    <option value="clips/pd">Пульты диспетчера</option>
                                    <option value="clips/test">Проверка звука</option>
                                </select>
                                <!--<div class="invalid-feedback">Необходимо выбрать назначение</div>-->
                            </div>
                            
                        </div>
                            
                        <div class="mt-3 d-flex justify-content-end">
                            <button class="btn btn-outline-secondary col-2" type="submit" id="submitFile" disabled>Загрузить</button>
                        </div>
                        <br>
                        <div id="fileCheck"></div>
                    </form>
                </div>
            </div>
            
            <div id="clip-list" class="border shadow" style="padding: 15px;">
                <h5>Сервер</h5>
                <?php show_clips("clips"); ?>
                
                <h5 class="pt-3">Пульты диспетчера</h5>
                <?php show_clips("clips/pd"); ?>
                
                <h5 class="pt-3">Проверка звука</h5>
                <?php show_clips("clips/test"); ?>
            </div>
        </div>
        
        <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/alertify.min.js"></script>
        <script src="js/alert.js" crossorigin="anonymous"></script>
        <script src="js/constants.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/rest.js?1"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script>
            get_settings();
            
            alertify.defaults.glossary.ok = 'OK';
            alertify.defaults.glossary.cancel = 'Отмена';    
            alertify.defaults.transition = "fade";
            alertify.defaults.theme.ok = "btn btn-outline-primary";
            alertify.defaults.theme.cancel = "btn btn-outline-danger"
            
            $("#dir-select").on("change", function(){
                if($(this).val() !== ""){
                    $(this).removeClass("is-invalid");
                    $("#submitFile").prop("disabled", false);
                }else{
                    $(this).addClass("is-invalid");
                    $("#submitFile").prop("disabled", true);
                }
            });
            
            $("#upload-clip").on("change", function(){
                $("#upload-clip-label").html(this.files[0].name);
                
                if($(this).val() === ""){
                    $(this).parent().find(".invalid-feedback").html("Необходимо выбрать файл");
                    $(this).addClass('is-invalid');
                    $("#submitFile").prop("disabled", true);
                    return;
                }else{
                    $(this).removeClass('is-invalid');
                    $("#submitFile").prop("disabled", false);
                }
                
                let max_size = <?php echo $max_size;?>;
                console.dir(this);
                if(this.files[0].size > max_size){
//                    $("#fileCheck").html('<div class="alert alert-danger" role="alert">Файл слишком большой! Для загрузки больших клипов используйте сжатые форматы (например MP3).</div>');
                    $(this).parent().find(".invalid-feedback").html("Файл слишком большой! Для загрузки больших клипов используйте сжатые форматы (например MP3)");
                    $(this).addClass('is-invalid');
                    $("#submitFile").prop("disabled", true);
                }else{
//                    $("#fileCheck").empty();
                    $(this).removeClass('is-invalid');
                    $("#submitFile").prop("disabled", false);
                }
            });
            
            
            function checkClip(clip, firstCheck=false){
                var disbl = false;
                clip.find('.file-params').each(function(){
                    var valid = $(this).attr('id') === 'filename-new' 
                            || $(this).val()!=='' 
                            || ($(this).attr('dir')!=='clips' && !$(this).hasClass('clip-in'));
                    if(valid){
                        $(this).removeClass('is-invalid');
                        if(!firstCheck){
                            $(this).addClass('is-valid');
                        }
                    }else{
                        $(this).removeClass('is-valid');
                        $(this).addClass('is-invalid');
                        disbl = true;
                    }
                });
                if(disbl || firstCheck){
                    clip.find('.btn-rename').attr('disabled','');
                }else{
                    clip.find('.btn-rename').removeAttr('disabled');
                }
            }
            $('.file-params').change(function(){
                checkClip($(this).parents('.clip-line'));
            });
            $('.file-params').keypress(function(){
                checkClip($(this).parents('.clip-line'));
            });
            
            $(".btn-rename").click(function(){
                var form = $(this).parents(".clip-line");
                var dir = form.find(".filename-dir").val();
                var name_old = form.find(".filename-old").val();
                var name_new = form.find(".filename-new").val();
                var signal = form.find(".clip-signal").val();
                if(signal == "")
                    signal = -1;
                var clip_in = form.find(".clip-in").val();
                if(clip_in=="")
                    clip_in = 0;
                clip_in--;
                console.log(dir+"/"+name_old + " -> "+dir+"/"+name_new + " sig=" + signal + " in=" + clip_in);
                
                var _this = $(this);
                
                $.post("upload_clip.php", {command: "rename", dir: dir, name_old: name_old, name_new: name_new, signal: signal, in: clip_in}, function(answer){
                    console.log(answer);
                    if(answer.includes("RENAME_ERROR")){
                        alertify.error("Ошибка переименования");
                    }else if(answer.includes("JSON_ERROR")){
                        alertify.error("Ошибка сохранения файла описания");
                    }else{
                        _this.parents('.clip-line').find('.btn-play-outs').attr('source', clip_in);
                        _this.parents('.clip-line').find('.btn-play').attr('source', clip_in);
                        _this.parents('.clip-line').find('.btn-stop').attr('source', clip_in);
                        _this.attr('disabled','disabled');
                        _this.parents('.clip-line').find('.file-params').removeClass('is-invalid');
                        _this.parents('.clip-line').find('.file-params').removeClass('is-valid');
                        alertify.success("Клип изменен");
                    }
//                    location.reload();
                });
            });
            
            $(".btn-del").click(function(){
                var form = $(this).parents(".clip-line");
                var dir = form.find(".filename-dir").val();
                var filename = form.find(".filename-old").val();
                
                console.log("Delete "+dir+"/"+filename);
                
                alertify.confirm("Удаление", "Удалить клип "+filename+"?", function(){
                    console.log("OK");
                    $.post("upload_clip.php", {command: "delete", dir: dir, filename: filename}, function(answer){
                        console.info("DELETE", answer);
                        if(answer.includes("DELETE_ERROR")){
                            alertify.error("Ошибка удаления файла");
                        }else{
                            form.remove();
                            alertify.success("Клип удален");
                        }
//                        location.reload();
                    });
                }, function(){
                    console.log("Cancel");
                });
                
            });
            
//            $(".btn-play").click(async function(){
//                var name = $(this).attr('filename');
//                var dir = $(this).attr('filedir');
//                var signal = $(this).attr('signal');
//                var autorew = $('#autorew').is(':checked');
//                console.info('PLAY', name);
//                console.info('PLAY', dir);
//                console.info('AUTOREW', autorew);
//                
//                await (new Rest()).play_clip(name, signal!="" ? parseInt(signal) : -1, autorew);
//                clipsStates();
//            });
            
            $(".btn-play-outs").click(async function(){
                var name = $(this).attr("filename");
                var dir = $(this).attr('filedir');
                var signal = $(this).attr('signal');
                var autorew = $(this).parents('.collapse').find('.autorew').is(':checked');
                console.info('PLAY', name);
                console.info('PLAY', dir);
                console.info('AUTOREW', autorew);
                
                var inInput = $(this).parents(".clip-line").find(".clip-in");
                var inStr = inInput.val();
                
                var outsInput = $(this).parents(".collapse").find(".clip-outs");
                var outsStr = outsInput.val();
                
//                if(outsStr != ''){
                if(inStr === ''){
                    alertify.error("Не задан вход");
                    return;
                }

                var outs = 0;
                if(outsStr !== undefined && outsStr !== ''){
                    var outsAr = outsStr.split(/\D+/);
//                    var outsAr = Array.from(outsStr.matchAll(/\d+/g));
                    console.info("OUTS_ARR", outsAr);
                    for(let i=0; i<outsAr.length; i++){
                        var out = parseInt(outsAr[i]);
                        if(out > 0){
                            outs |= (1<<(out-1));
                        }
                    }
                }
                console.info("OUTS", outs);
                console.info("IN", parseInt(inStr));
                await (new Rest()).play_clip_outs(`${dir}/${name}`, parseInt(inStr)-1, outs, autorew);
//                }else{
//                    await (new Rest()).play_clip(name, signal!="" ? parseInt(signal) : -1, autorew);
//                }
                clipsStates();
            });
            
            $(".btn-stop").click(async function(){
                var inInput = $(this).parents(".clip-line").find(".clip-in");
                var inStr = inInput.val();
                
                await (new Rest()).stop_clip(parseInt(inStr)-1);
                clipsStates();
            });
            
            $('#btn-stop-all').click(function(){
                (new Rest()).stop_all_clips();
                clipsStates();
            });
            
            $('.btn-clear-outs').click(function(){
                var input = $(this).parents('.input-group').find('input');
                console.info("INPUT", input);
                input.val('');
            });
            
            $('.collapse').on('show.bs.collapse', function(){
                console.log("Expand");
                $(this).parents('.clip-line').find('.btn-extra').html(`
                    <svg width="20" height="20" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g fill="#ffffff" class="fill-241f20"><path d="M24 0C10.745 0 0 10.745 0 24s10.745 24 24 24 24-10.745 24-24S37.255 0 24 0zm0 44C12.954 44 4 35.046 4 24S12.954 4 24 4s20 8.954 20 20-8.954 20-20 20z"></path><path d="M26 22H13v4h22v-4h-5.291z"></path></g></svg>
                `);
            });
            
            $('.collapse').on('hide.bs.collapse', function(){
                console.log("Collapse");
                $(this).parents('.clip-line').find('.btn-extra').html(`
                    <svg width="20" height="20" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g fill="#ffffff" class="fill-241f20"><path d="M24 0C10.745 0 0 10.745 0 24s10.745 24 24 24 24-10.745 24-24S37.255 0 24 0zm0 44C12.954 44 4 35.046 4 24S12.954 4 24 4s20 8.954 20 20-8.954 20-20 20z"></path><path d="M35 22h-9v-8.998h-4V22h-9v4h9v8.998h4V26h9z"></path></g></svg>
                `);
            });
            
            function clipState(source){
                (new Rest()).clip_state(parseInt(source)).then(function(state){
//                    console.info("STATE", state);
                    console.info("AUTOREW", $(`.autorew[source=${source}]`));
                    if(state.pid !== 0){
                        $(`.btn-play[source=${source}]`).hide();
                        $(`.btn-play-outs[source=${source}]`).hide();
                        $(`.btn-stop[source=${source}]`).show();
                        $(`.autorew[source=${source}]`).attr('disabled', 'disabled');
                    }else{
                        $(`.btn-play[source=${source}]`).show();
                        $(`.btn-play-outs[source=${source}]`).show();
                        $(`.btn-stop[source=${source}]`).hide();
                        $(`.autorew[source=${source}]`).removeAttr('disabled');
                    }
                    
                });
            }
            
            function clipsStates(){
                var srcSet = new Set();
                $('.btn-play-outs').each(function(){
                    var src = parseInt($(this).attr('source'));
                    if(src != -1){
                        srcSet.add(src);
                    }
                });
                var clip_sources = [...srcSet];
                
                console.info("CLIP SRCS", clip_sources);

                (new Rest()).clips_state(clip_sources).then(function(answer){
//                    console.info("STATES", states);
                    var play = false;
                    var clips = '';
                    if(answer.result === 'success'){
                        for(let i=0; i<answer.states.length; i++){
                            var state = answer.states[i];
                            if(state.pid !== 0){
                                $(`.btn-play[source=${state.source}]`).hide();
                                $(`.btn-play-outs[source=${state.source}]`).hide();
                                $(`.btn-stop[source=${state.source}]`).show();
                                
                                $(`.autorew[source=${state.source}]`).attr('disabled', 'disabled');
                                
                                $(`.play-panel[source=${state.source}]`).removeClass('alert-primary');
                                $(`.play-panel[source=${state.source}]`).addClass('alert-danger');
                                
                                play = true;
                                
                                var playClip = $(`.btn-play-outs[source=${state.source}]`).attr('filename')
                                clips += `${playClip}<br>`;
                            }else{
                                $(`.btn-play[source=${state.source}]`).show();
                                $(`.btn-play-outs[source=${state.source}]`).show();
                                $(`.btn-stop[source=${state.source}]`).hide();
                                
                                $(`.autorew[source=${state.source}]`).removeAttr('disabled');
                                
                                $(`.play-panel[source=${state.source}]`).removeClass('alert-danger');
                                $(`.play-panel[source=${state.source}]`).addClass('alert-primary');
                            }
                        }
                    }
                    if(play){
                        $('#play-clips').html(clips);
                        $('#is-play').show();
                    }else{
                        $('#is-play').hide();
                    }
                });
                    
//                for(let i=0; i<clip_sources.length; i++){
//                    clipState(clip_sources[i]);
//                }
            }
            
            setInterval(function(){
                clipsStates();
            }, 1000);
            
            $(document).ready(function(){
                clipsStates();
                
                $('.clip-line').each(function(){
                    checkClip($(this), true);
                });
                
                $(`.clip-line[dir!="clips/test"]`).find(`.collapse`).hide();
                $(`.clip-line[dir!="clips/test"]`).find('.btn-extra').hide();
                $(`.clip-line[dir!="clips/test"]`).find('.play-stop-btns').show();
            });
        </script>
    </body>
</html>



