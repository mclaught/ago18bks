/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPSoundSender.cpp
 * Author: user
 * 
 * Created on 16 декабря 2019 г., 15:28
 */

#include <cstring>
#include <iostream>
#include <vector>
#include "UDPSoundSender.h"
#include "MyUDPServer.h"

std::vector<PUDPSoundSender> udp_senders;

UDPSoundSender::UDPSoundSender()
    : BaseSoundSender(false)
{

}

UDPSoundSender::UDPSoundSender(const UDPSoundSender& orig) : BaseSoundSender(false) {
}

UDPSoundSender::~UDPSoundSender() {
}

PUDPSoundSender UDPSoundSender::getFreeSender(){
//    cout << "UDP senders " << udp_senders.size() << endl;
    
    for(PUDPSoundSender snd : udp_senders){
        if(!snd->isBusy())
            return snd;
    }
    
    PUDPSoundSender newSender = PUDPSoundSender(new UDPSoundSender());
    udp_senders.push_back(newSender);
    return newSender;
}

void UDPSoundSender::start(int _sock, sockaddr_in _addr, start_stop_t* start_stop){
    sock = _sock;
    addr = _addr;
    
    pd_start.channels = 1;
    pd_start.in_ch[0] = start_stop->src_ch;
    pd_start.outs = start_stop->recipients;
    
    last_active_time = time(NULL);
    
    active = true;
    
    selectMasterBoard();
    
    BaseSoundSender::start();
}

void UDPSoundSender::stop(){
    BaseSoundSender::stop();

    active = false;
    unselectMasterBoard();
}

void UDPSoundSender::sendSoundPacket(int cnt){
#if UDP_SEND_TYPE != THREAD_SEND && UDP_SEND_TYPE != TIMER_SEND
    char buf[sizeof(pack_hdr_t)+2];
    packet_command_t* pack = reinterpret_cast<packet_command_t*>(buf);
    pack->hdr.len = 2;
    pack->hdr.repeate_cnt = 0;
    pack->hdr.type = PACK_CFG;
    pack->cmd = SND_CHANK_REQ;
    pack->data[0] = cnt;
    
    if(sendto(sock, buf, sizeof(pack_hdr_t)+3, 0, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "UDPSoundSender sendto: " << strerror(errno) << endl;
    }
    
//    req_tm = GetTickCount_us();
#endif
}

void UDPSoundSender::onStart(){
    BaseSoundSender::onStart();
}

void UDPSoundSender::onStop(){
    BaseSoundSender::onStop();
}

void UDPSoundSender::touch(){
    last_active_time = time(NULL);
//    cout << "prop tm " << (GetTickCount_us() - req_tm) << endl;
}

bool UDPSoundSender::isTimedOut(){
    return (time(NULL) - last_active_time) > 5;
}
