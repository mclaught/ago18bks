#include "targetsdialog.h"
#include "ui_targetsdialog.h"
#include <QPushButton>

TargetsDialog::TargetsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TargetsDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Сохранить");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

//    setWindowState(Qt::WindowMaximized);
    showFullScreen();
}

TargetsDialog::~TargetsDialog()
{
    delete ui;
}

void TargetsDialog::setNames(QStringList names, int outs_cnt){
    ui->targetsWidget->setTargets(names, outs_cnt);
}

void TargetsDialog::setOuts(uint64_t outs){
    ui->targetsWidget->setOuts(outs);
}

uint64_t TargetsDialog::getOuts(){
    return ui->targetsWidget->getOuts();
}
