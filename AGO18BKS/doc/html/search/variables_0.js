var searchData=
[
  ['a_737',['a',['../structcodec__eq__coeff__t.html#a7abdcc7556ce25b2a3e0cfa31f586374',1,'codec_eq_coeff_t']]],
  ['a1_738',['a1',['../structbiqd__hp__coeff__t.html#af66f970c78113001657f9417c28d10d7',1,'biqd_hp_coeff_t::a1()'],['../structbiqd__lp__coeff__t.html#acb8fb08a2f0d79c9053a4b91e1eaaff2',1,'biqd_lp_coeff_t::a1()']]],
  ['a2_739',['a2',['../structbiqd__hp__coeff__t.html#ace2d586148585599dd70d7a539475890',1,'biqd_hp_coeff_t::a2()'],['../structbiqd__lp__coeff__t.html#addb6b2666a8b456c7dd0b1cd29a9d7da',1,'biqd_lp_coeff_t::a2()']]],
  ['active_740',['active',['../class_base_sound_sender.html#af42cbe7d9676bee5b272384dca9d22ad',1,'BaseSoundSender::active()'],['../class_channel.html#a1f56d8af226da204ee85121712197acf',1,'Channel::active()'],['../structshared__buf__t.html#a23718a37c50d09b6122ae0bfc4c005db',1,'shared_buf_t::active()'],['../class_my_thread.html#a9e74cd54ec2b3494b163544dcf84d4be',1,'MyThread::active()']]],
  ['addr_741',['addr',['../struct_channel_1_1send__queue__iten__t.html#adbeafac20a2f6ce8bebb9fd741b4096c',1,'Channel::send_queue_iten_t']]],
  ['adr_5ffrom_742',['adr_from',['../structpack__hdr__t.html#a6739ac34edea4c5c4f321c19d1179e39',1,'pack_hdr_t']]],
  ['adr_5fto_743',['adr_to',['../structpack__hdr__t.html#ad615678be1551f488915d2dfb7787893',1,'pack_hdr_t']]],
  ['allign_744',['allign',['../structwav__format__t.html#af879ed83bd43443178c44cdcee022fd3',1,'wav_format_t']]],
  ['audio_5fhdr_745',['audio_hdr',['../structpacket__audio__t.html#ac808552ede02833bda34ccf973553c3d',1,'packet_audio_t']]],
  ['audioformat_746',['audioFormat',['../class_my_p_c_m.html#a75dccb39d8a9616075fee188b738fa82',1,'MyPCM']]]
];
