/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyWSClient.cpp
 * Author: user
 * 
 * Created on 27 мая 2020 г., 13:27
 */

#include "MyWSClient.h"
#include "cus/log.h"
#include "ConfigServer.h"
#include <string.h>

#include <vector>
#include <iostream>
#include <algorithm>
#include <cctype>
#ifndef WRT
    #include <openssl/sha.h>
#else
    #define SHA_DIGEST_LENGTH 20
    #include "sha1-master/sha1.hpp"
#endif
#include "SoundSenderFile.h"
#include "base64.h"
#include "json.hpp"
#include <sys/types.h>
#include <sys/socket.h>
#include <sstream>
#include <sstream>
#include <ios>
#include <iomanip>
#include <netinet/in.h>
#include <unistd.h>
#include <experimental/filesystem>

const string GUIDKey = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

MyWSClient::MyWSClient(int _sock) : sock(_sock) {
}

MyWSClient::MyWSClient(const MyWSClient& orig) {
}

MyWSClient::~MyWSClient() {
    close(sock);
#ifdef WS_DEBUG
    cout << "WS client closed" << endl;
#endif
}

bool MyWSClient::process(char* data, int len){
//    cout << hex << setfill('0') << setw(2);
//    for(int i=0; i<len; i++)
//        cout << (int)(uint8_t)data[i] << " ";
//    cout << dec << endl;
    
    if(!handshaked){
        parseHeader(string(data, len));
        return false;
    }else{
        bool old_enabled = enabled;
        parsePacket(data, len);
        return (!old_enabled && enabled);
    }
}

vector<string> split(string str, string delim){
    vector<string> res;
    int delim_sz = delim.length();
    while(str.length()){
        int n = str.find(delim);
        if(n != static_cast<int>(string::npos)){
            res.push_back(str.substr(0, n));
            str.erase(0, n+delim_sz);
        }else{
            res.push_back(str);
            break;
        }
    }
    return res;
}

string trim(string str){
    while(str.length() && str[0] == ' '){
        str.erase(0,1);
    }
    while(str.length() && str[str.length()-1] == ' '){
        str.erase(str.length()-1, 1);
    }
    return str;
}

string to_lower(string str){
    transform(str.begin(), str.end(), str.begin(), [](unsigned char c){return tolower(c);});
    return str;
}

string sha1_to_string(unsigned char* hash){
    ostringstream stm;
    stm << hex << setfill('0') << setw(2);
    for(int i=0; i<SHA_DIGEST_LENGTH; i++){
        stm << (int)hash[i];
    }
    string str = stm.str();
    return str;
}

void MyWSClient::parseHeader(string header){
//    cout << header << endl;
    
    vector<string> lines = split(header, "\r\n");
    string key;
    for(string line : lines){
        vector<string> parts = split(line, ":");
        if(parts.size() >= 2){
            string name = to_lower(parts[0]);
            string value = trim(parts[1]);
            if(name == "sec-websocket-key"){
                key = value;
            }
                
        }
    }
    
    if(!key.empty()){
        key += GUIDKey;
//        cout << "Key: " << key << endl;
        
        unsigned char hash[SHA_DIGEST_LENGTH];
#ifndef WRT
        SHA1(reinterpret_cast<const unsigned char*>(key.c_str()), key.length(), hash);
//        key = sha1_to_string(hash);//string(reinterpret_cast<char*>(hash), SHA_DIGEST_LENGTH);
//        cout << "SHA1: " << key << " len=" << key.length() << endl;
        
        key = base64_encode(hash, SHA_DIGEST_LENGTH);
//        cout << "base64: " << key << " len=" << key.length() << endl;
#else
        SHA1 sha1;
        sha1.update(key);
        key = base64_encode(reinterpret_cast<const unsigned char*>(sha1.final().c_str()), SHA_DIGEST_LENGTH);
#endif
        
        string answer = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: "+key+"\r\n\r\n";
        if(send(sock, answer.c_str(), answer.length(), 0) == -1){
            cerr << red << "MyWSClient send: " << strerror(errno) << clrst << endl;
        }
        
        handshaked = true;
        
#ifdef WS_DEBUG
        cout << "WS handshaked" << endl << endl;
#endif
    }
}

void MyWSClient::parsePacket(char* data, int len){
    memcpy(packet+pack_len, data, len);
    pack_len += len;
    
    while(pack_len){
        if(pack_len >= static_cast<int>(sizeof(websocket_hdr_t))){
            websocket_hdr_t* hdr = reinterpret_cast<websocket_hdr_t*>(packet);
            char* mask = packet;
            char* body = packet;
            int body_len = 0;
            if(hdr->len < 126){
                websocket8_t* ws8 = reinterpret_cast<websocket8_t*>(packet);
                body = hdr->mask ? ws8->masked.data : ws8->unmasked.data;
                mask = hdr->mask ? ws8->masked.mask : ws8->unmasked.data;
                body_len = ws8->hdr.len;
            }else if(hdr->len == 126){
                websocket16_t* ws16 = reinterpret_cast<websocket16_t*>(packet);
                body = hdr->mask ? ws16->masked.data : ws16->unmasked.data;
                mask = hdr->mask ? ws16->masked.mask : ws16->unmasked.data;
                body_len = ntohs(ws16->len);
            }else{
                websocket64_t* ws64 = reinterpret_cast<websocket64_t*>(packet);
                body = hdr->mask ? ws64->masked.data : ws64->unmasked.data;
                mask = hdr->mask ? ws64->masked.mask : ws64->unmasked.data;
                body_len = be64toh(ws64->len);
            }
            pend_len = (body - packet) + body_len;

            if(pack_len >= pend_len){
                if(hdr->mask){
                    for(int i=0; i<body_len; i++){
                        body[i] ^= mask[i%4];
                    }
                }

                string js_str = string(body, body_len);
#ifdef WS_DEBUG
                cout << "pack: " << js_str << endl;
#endif

                if(!js_str.empty() && js_str.length()>8){
                    try{
                        json js = json::parse(js_str);
                        parseJson(js);
                    }catch(json::exception &e){
                        log_error("MyWSClient::parsePacket(): %s", e.what());
                        cerr << red << js_str << clrst << endl;
                    }
                }

                memmove(packet, packet+pend_len, pack_len-pend_len);
                pack_len -= pend_len;

                //Process packet content
            }
        }
    }
    
    
    //string text = string(body, body_len);
    //cout << "Packet: " << text << endl;
}

void MyWSClient::parseJson(json js){
    if(js.contains("type")){
        type = js["type"];
        enabled = true;
    }

    if(js.contains("command")){
        processCommand(js);
    }
}

void MyWSClient::processCommand(json js)
{
#ifdef WS_DEBUG
    printf("%s\n", js.dump(4).c_str());
#endif
    string cmd = js["command"];

    if(cmd == "play_clip"){
        if(js.contains("clip")){
            string clip = string("/var/www/html/AGO18Web/clips/")+string(js["clip"]);
            auto source = js.contains("source") ? (int)js["source"]     : BRD_ID_TO_CHAN(conf_server.server_addr, 0);
            auto outs =   js.contains("outs")   ? (ch_mask_t)js["outs"] : 0;

                conf_server.start_clip_process(source, outs, clip);
//            auto file_senders = SoundSenderFile::getFileSenders();
//            for(auto sender : file_senders){
//                if(sender->getInCh() == source && sender->isBusy())
//                    sender->stop();
//            }
//
//            auto sender = SoundSenderFile::getFreeSender();
//            sender->start(clip, source, outs);
        }
    }else if(cmd == "stop_clip"){
#ifdef WS_DEBUG
        printf("Stop clip\n");
#endif
        if(js.contains("source")){
            auto source = js.contains("source") ? (int)js["source"] : BRD_ID_TO_CHAN(conf_server.server_addr, 0);
            
            conf_server.stop_clip_process(source);
//            auto file_senders = SoundSenderFile::getFileSenders();
//            for(auto sender : file_senders){
//                if(sender->isBusy() && sender->getInCh()==source)
//                    sender->stop();
//            }
            js["result"] = "success";
        }
    }else if(cmd == "get_settings"){
        json _settings = conf_server.get_settings();

        if(!_settings["rec"].contains("store_days")){
            _settings["rec"]["store_days"] = 2;
            conf_server.set_settings(_settings);
        }

        if(!_settings["rec"].contains("enable")){
            _settings["rec"]["enable"] = true;
            conf_server.set_settings(_settings);
        }

//            _settings["time"] = json::object({
//                {"time", time(NULL)},
//                {"ntp", conf_server.getNTP()}
//            });

        js["result"] = "success";
        js["settings"] = _settings;

    }else if(cmd == "get_clips"){
        string dir = js.contains("dir") ? js["dir"] : "";
        namespace bfs = std::experimental::filesystem;
        js["result"] = "success";
        js["clips"] = json::array();
        bfs::directory_iterator itt(bfs::path("/var/www/html/AGO18Web/clips/"+dir));
        for(; itt != bfs::directory_iterator(); itt++){
            bfs::path path = itt->path();
            if(bfs::is_regular_file(path)){
                js["clips"].push_back(path);
            }
        }
    }

    string answer = js.dump();
    write(const_cast<char*>(answer.c_str()), answer.length());
}

void MyWSClient::createMask(char* mask){
    *reinterpret_cast<uint32_t*>(mask) = random();
}

void MyWSClient::mask(char* data, char* mask, int len){
    for(int i=0; i<len; i++)
        data[i] ^= mask[i%4];
}

void MyWSClient::write(char* data, int len){
    if(!enabled)
        return;
    
//    len--;
    char buf[len+sizeof(websocket64_t)+4];
    
    if(len < 126){
        websocket8_t* ws8 = reinterpret_cast<websocket8_t*>(buf);
        ws8->hdr.FIN = 1;
        ws8->hdr.RSV = 0;
        ws8->hdr.len = len;
        ws8->hdr.mask = 0;
        ws8->hdr.opcode = 1;
        
        memcpy(ws8->unmasked.data, data, len);
        
        lock_guard<mutex> lock(sock_mutex);
        send(sock, buf, len+sizeof(websocket_hdr_t), 0);
    }else if(len < 65535){
        websocket16_t* ws16 = reinterpret_cast<websocket16_t*>(buf);
        ws16->hdr.FIN = 1;
        ws16->hdr.RSV = 0;
        ws16->hdr.len = 126;
        ws16->hdr.mask = 0;
        ws16->hdr.opcode = 1;
        ws16->len = htons(len);
        
        memcpy(ws16->unmasked.data, data, len);
        
        lock_guard<mutex> lock(sock_mutex);
        send(sock, buf, len+sizeof(uint16_t)+sizeof(websocket_hdr_t), 0);
    }else{
        websocket64_t* ws64 = reinterpret_cast<websocket64_t*>(buf);
        ws64->hdr.FIN = 1;
        ws64->hdr.RSV = 0;
        ws64->hdr.len = 126;
        ws64->hdr.mask = 0;
        ws64->hdr.opcode = 1;
        ws64->len = htons(len);
        
        memcpy(ws64->unmasked.data, data, len);
        
        lock_guard<mutex> lock(sock_mutex);
        send(sock, buf, len+sizeof(uint64_t)+sizeof(websocket_hdr_t), 0);
    }
}
