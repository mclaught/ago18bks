#include <fstream>
#include <iostream>
#include <string.h>
#include "mypcm.h"

MyPCM::MyPCM(string fileName, int frameDuration, int mono) : file_name(fileName), frame_duration(frameDuration), mono(mono)
{
    error = false;
    error_str = "";
    openFile(fileName);
}

MyPCM::~MyPCM()
{
    if(file)
    {
        if(file->is_open())
            file->close();
        delete file;
    }
}

void MyPCM::openFile(string fName)
{
    try{
        file = new ifstream();
        file->open(fName);

        if(file->is_open())
        {
            isRIFF = false;
            audioFormat = channels = sample_rate = 0;

            readChunk(file, 0);

            frame_size = byte_rate*frame_duration/1000;

            //cout << "Header " << frame_size;
        }
        else
        {
            cerr << "Не возможно открыть файл " << fName << endl;
            setError("Не возможно открыть файл "+fName);
        }
    }catch(exception &e){
        cerr << "MyPCM " << e.what() << endl;
    }
}

bool MyPCM::needResample()
{
    return !isRIFF || format != 'EVAW' || audioFormat != 1 || !checkSampleRate() || (!mono && channels > 1);
}

//void MyPCM::resample(EqList eq_list)
//{
//    QFileInfo info(file_name);
//    QString tmpFile = QDir::tempPath()+"/"+info.baseName()+".wav";

//    QString sEqualizer = "";
//    bool use_eq = false;
//    if(eq_list.count())
//    {
//        sEqualizer = "-af \"";
//        foreach (EqPoint eq_pnt, eq_list) {
//            if(eq_pnt.gain)
//            {
//                sEqualizer += QString("equalizer=f=%1:g=%2:w=1.5:width_type=q,").arg(eq_pnt.freq).arg(eq_pnt.gain);
//                use_eq = true;
//            }
//        }
//        sEqualizer = use_eq ? sEqualizer.left(sEqualizer.length()-1)+"\"" : "";
//    }

//    QProcess process;
//    QString cmd = (mono ?
//                QString("ffmpeg.exe -i \"%1\" -f wav -acodec pcm_s16le -ar 48k -ac 1 %2 -y \"%3\"") :
//                QString("ffmpeg.exe -i \"%1\" -f wav -acodec pcm_s16le -ar 48k %2 -y \"%3\"")).arg(file_name).arg(sEqualizer).arg(tmpFile);
//    process.start(cmd);
//    process.waitForFinished();
//    QString out(process.readAllStandardOutput());
//    out += "\r\n"+QString(process.readAllStandardError());

//    if(file)
//        file->close();

//    openFile(tmpFile);

//    if(out.contains("Please reduce gain"))
//    {
//        setError(tr("Снизьте уровни эквалайзера"), false);
//    }
//}

//void MyPCM::deleteTemp()
//{
//    QFileInfo info(file_name);
//    QString tmpFile = QDir::tempPath()+"/"+info.fileName();
//    QFile file(tmpFile);
//    if(file.exists())
//        file.remove();
//}

//void MyPCM::copyTemp(QString newFileName)
//{
//    file->close();
//    file->copy(newFileName);
//}

bool MyPCM::readChunk(ifstream* file, int level)
{

    uint32_t chunkId;
    uint32_t chunkSize;

    file->read((char*)&chunkId, sizeof(chunkId));
    file->read((char*)&chunkSize, sizeof(chunkSize));

    switch(chunkId)
    {
    case 'FFIR':
    {
        isRIFF = true;
        file->read((char*)&format, sizeof(format));

        while(readChunk(file, level+1));
        return false;
    }

    case ' tmf':
    {
        file->read((char*)&audioFormat, sizeof(audioFormat));
//        if(audioFormat != 1)
//        {
//            setError("Формат файла не поддерживается ("+QString::number(audioFormat)+"), ожидается PCM");
//            return false;
//        }
        file->read((char*)&channels, sizeof(channels));
        file->read((char*)&sample_rate, sizeof(sample_rate));
//        if(!checkSampleRate(sample_rate))
//        {
//            setError("Не поддерживаемый samplerate "+QString::number(sample_rate));
//        }
        file->read((char*)&byte_rate, sizeof(byte_rate));
        file->read((char*)&blockAlign, sizeof(blockAlign));
        file->read((char*)&bitsPerSample, sizeof(bitsPerSample));

        for(uint32_t i=0;i<(chunkSize-16); i++)
        {
            char c;
            file->read(&c, 1);
        }
        return true;;
    }

    case 'atad':
    {
        file_size = chunkSize;
        file_pos = 0;
        return false;
    }

    default:
        if(level == 0)
        {
            setError("Формат файла не поддерживается");
            cerr << "Формат файла не поддерживается";
            return false;
        }
        else
        {
            for(uint32_t i=0;i<chunkSize; i++)
            {
                char c;
                file->read(&c, 1);
            }
            return true;
        }
    }
}

bool MyPCM::checkSampleRate()
{
    uint32_t supportSR[] = {8000, 12000, 16000, 11025, 22050, 24000, 32000, 48000};//, 44100
    for(int i=0; i<8; i++)
    {
        if(supportSR[i] == sample_rate)
            return true;
    }
    return false;
}

void MyPCM::setError(string err, bool fatal)
{
    if(fatal)
        error = true;
    if(error_str != "")
        error_str += ", ";
    error_str += err;
}

int MyPCM::readFrame(char* buf, int sz)//, int sr, int ch
{
    int cnt = (file_size-file_pos) >= sz ? sz : (file_size-file_pos);

    file->read(buf, cnt);
    if(cnt < frame_size)
    {
        memset(&buf[cnt], 0, frame_size-cnt);
    }
    file_pos += cnt;

//    if(resample)
//    {
//        double rsK = (double)sr/(double)sample_rate;

//        for(int i=0; i<sz/2; i++)
//        {
//            int j = i/rsK;
//            ((qint16*)buf)[i] = ((qint16*)tmp)[j*channels];
//        }

//        delete tmp;
//    }

    return cnt;
}

bool MyPCM::eof()
{
    return file_pos >= file_size;//
}
