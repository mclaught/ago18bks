#!/bin/sh

apt-get update
apt-get -y install gcc g++ make qt5-qmake gdb mc git libogg-dev libopus-dev net-tools apache2 php ntp ffmpeg libmpg123-dev libssl-dev p7zip gpg sudo
apt-get -y autoremove

systemctl stop ago18bks

if [ -d "ago18bks" ] 
then
	rm -r ago18bks
fi

git clone https://gitlab.com/mclaught/ago18bks.git
if [ ! -d "ago18bks" ]
then
    echo "\n\nSources download failed"
    exit 1;
fi

cp -r -f ago18bks/AGO18Web /var/www/html
if [ ! -d /var/www/html/AGO18Web/rec ]; then
    mkdir /var/www/html/AGO18Web/rec
fi
if [ ! -d /var/www/html/AGO18Web/clips/pd ]; then
    mkdir -p /var/www/html/AGO18Web/clips/pd
fi
if [ ! -d /var/www/html/AGO18Web/events ]; then
    mkdir /var/www/html/AGO18Web/events
fi
if [ ! -d /var/www/html/AGO18Web/files ]; then
    mkdir /var/www/html/AGO18Web/files
fi
chown -R www-data:www-data /var/www

if [ ! -d /var/log/journal]
then
	mkdir /var/log/journal
fi

if [ ! -e /etc/ago18bks ]
then
	mkdir /etc/ago18bks
fi
if [ ! -f /etc/ago18bks/settings.json ]
then
	cp ago18bks/AGO18BKS/settings/settings.json /etc/ago18bks
	chmod 644 /etc/ago18bks/settings.json
fi
if [ ! -f /etc/ago18bks/network.json ]
then
	cp ago18bks/AGO18BKS/settings/network.json /etc/ago18bks
	chmod 644 /etc/ago18bks/network.json
fi
if [ ! -f /etc/ago18bks/alarms.json ]
then
	cp ago18bks/AGO18BKS/settings/alarms.json /etc/ago18bks
	chmod 644 /etc/ago18bks/alarms.json
fi

cp -f ago18bks/apache/000-default.conf /etc/apache2/sites-available
for file in $(ls /etc/php/*/apache2/php.ini)
do
	dir=$(dirname ${file})
	cp -f ago18bks/apache/php.ini ${dir}

done
service apache2 restart

cp -f ago18bks/apache/www-data /etc/sudoers.d

cd ago18bks/AGO18BKS
make CONF=Release
if [ $? -ne 0 ]; then
    echo "\n\nBuild failed"
    exit 1
fi

#cp ago18bks.d /etc/init.d
#chmod 755 /etc/init.d/ago18bks.d
cp dist/Release/GNU-Linux/ago18bks /usr/sbin/ago18bks
chmod 755 /usr/sbin/ago18bks

cp ago18bks.service /etc/systemd/system
chmod 644 /etc/systemd/system/ago18bks.service
systemctl enable ago18bks
systemctl daemon-reload

cd ../ago18player
make CONF=Release
if [ $? -ne 0 ]; then
    echo "\n\nPlayer build failed"
    exit 1
fi

cp dist/Release/GNU-Linux/ago18player /usr/sbin/ago18player
chmod 755 /usr/sbin/ago18player

cd ..
cd ..

#update-rc.d ago18bks.d defaults
systemctl start ago18bks

rm -r ago18bks

echo "\n\nVersion `ago18bks -v` installed\n"

