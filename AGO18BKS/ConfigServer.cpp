/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConfigServer.cpp
 * Author: user
 * 
 * Created on 27 ноября 2019 г., 12:23
 */

#include "ConfigServer.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <iostream>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <vector>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <iomanip>
#include <sstream>
#include <stdlib.h>
#include <set>
#include <mutex>
#include <filesystem>
#include <math.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <cstdio>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <experimental/filesystem>
#include <sched.h>
#include <signal.h>
#include <sys/statvfs.h>
#include <chrono>
#include <string>

#include "proto.h"
#include "MyUDPServer.h"
#include "PacketParser.h"
#include "cus/log.h"
#include "Snd2Json.h"
#include "SoundSenderFile.h"
#include "BRU2JSON.h"
#include "TimeSync.h"
#include "AudioSaver.h"

#define SOCK_NAME   "/var/www/html/AGO18Web/files/agobks"

ConfigServer conf_server;

void config_thread_func(ConfigServer* __this);
std::string exec_res(const char* cmd);

inline uint64_t now_ms(){
    return chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now().time_since_epoch()).count();
}

ConfigServer::ConfigServer() {
    alarmOuts = 0;
    for(int i=0; i<MAX_CHANNELS; i++){
        alarmIns[i] = 0;
        activeChannels[i] = 0;//time(NULL);
    }
}

void ConfigServer::start(){
    loadFromFile();

    int ws_port = network.contains("ws_port") ? static_cast<int>(network["ws_port"]) : 8888;
    ws_server = new WSServer(ws_port);
    ws_server->onclient = [this](shared_ptr<MyWSClient> client){

        send_ws(client->type);
    };
    ws_server->start();
    
    if(unlink(SOCK_NAME) < 0){
        cerr << red << "ConfigServer unlink: " << strerror(errno) << clrst << endl;
    }
    
    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sock == -1){
        log_error("ConfigServer socket: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
//    if(fcntl(sock, F_SETFL, O_NONBLOCK) == -1){
//        cerr << "ConfigServer fcntl():" << strerror(errno) << endl;
//        return;
//    }
    
    sockaddr_un my_addr;
    memset(&my_addr, 0, sizeof(sockaddr_un));
    my_addr.sun_family = AF_UNIX;
    strcpy(my_addr.sun_path, SOCK_NAME);
    
    if(bind(sock, (sockaddr*)&my_addr, sizeof(sockaddr_un)) == -1){
        log_error("ConfigServer bind():", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    if(listen(sock, 0) == -1){
        log_error("ConfigServer listen():", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
//    string chmod = "chmod 777 ";
//    chmod += SOCK_NAME;
//    if(system(chmod.c_str()) == -1){
//        cerr << "ConfigServer system():" << strerror(errno) << endl;
//    }
    
    if(chmod(SOCK_NAME, 0666) == -1){
        log_error("ConfigServer chmod():", strerror(errno));
    }
    
    thrd = thread(config_thread_func, this);

    sched_param param;
    param.sched_priority = 0;
    int err = pthread_setschedparam(thrd.native_handle(), SCHED_OTHER, &param);//main_thread.native_handle()
    if(err != 0)
        cerr << red << "pthread_setschedparam: " << strerror(err) << clrst << endl;

}

ConfigServer::ConfigServer(const ConfigServer& ) {
}

ConfigServer::~ConfigServer() {
    //stop();
}

void ConfigServer::run(){
    memset(boardPingTime, 0, sizeof(board_ping_t)*256);

    while(active)
    try{
        fd_set rd_set, er_set;
        FD_ZERO(&rd_set);
        FD_ZERO(&er_set);
        FD_SET(sock, &rd_set);
        FD_SET(sock, &er_set);
        
        timeval to = {0, 300000};

        auto sel_socks = select(sock+1, &rd_set, NULL, &er_set, &to);
        if(sel_socks == -1){
            cerr << red << "ConfigServer select():" << strerror(errno) << clrst << endl;
        }
        if(sel_socks > 0){
            if(FD_ISSET(sock, &rd_set)){
                sockaddr_un from_addr;
                memset(&from_addr, 0, sizeof(sockaddr_un));
                socklen_t socklen = sizeof(sockaddr_un);
                int cli_sock = accept(sock, reinterpret_cast<sockaddr*>(&from_addr), &socklen);
                if(cli_sock == -1){
                    cerr << red << "ConfigServer accept():" << strerror(errno) << clrst << endl;
                    return;
                }
                
#ifdef CONFIG_DEBUG
                cout << "ConfigServer accept client" << endl;
#endif
                processSocket(cli_sock);
            }
            
            if(FD_ISSET(sock, &er_set)){
                cerr << red << "ConfigServer socket error" << clrst << endl;
            }
        }
        activeChannelsTimeout();
        checkBoardPings();
        
        if(rtc != ""){
            timeSync.sync_ok();
        }
    }catch(system_error &e){
        log_error("ConfigServer run(): %s", e.what());
    }
    
    close(sock);
    
    cout << "Config server stoped" << endl;
}

void ConfigServer::stop(){
    ws_server->terminate();
    
    if(fcntl(sock, F_SETFL, O_NONBLOCK) == -1){
        cerr << red << "ConfigServer fcntl():" << strerror(errno) << clrst << endl;
    }

    active = false;
    thrd.join();
}

void ConfigServer::processSocket(int cli_sock){
    ostringstream cmd;
    int term = 0;
    while(term < 4){
        char c;
        int rd = recv(cli_sock, &c, 1, 0);
        if(rd == -1 && errno != EAGAIN){
            log_error("ConfigServer::processSocket() recv: %s", strerror(errno));
            close(cli_sock);
            return;
        }else if(rd == 0){
            close(cli_sock);
            break;
        }else{
            cmd << c;
            if(c == '\r' || c == '\n')
                term++;
            else 
                term = 0;
        }
    }

//#ifdef CONFIG_DEBUG
//    cout << cmd.str() << endl;
//#endif
    
    processQuery(cli_sock, cmd.str());
    
    close(cli_sock);
}

void ConfigServer::processQuery(int cli_sock, string str){
    json answer = json::object();

    try{
        json js = json::parse(str);
#ifdef CONFIG_DEBUG
    cout << js.dump(4) << endl;
#endif
        if(js.type()==json::value_t::discarded || !js.is_object()){
            close(cli_sock);
            return;
        }
    
        if(!js.contains("command") || !js["command"].is_string())
            return;

        string cmd = js["command"];

        if(cmd == "get_comm_table"){

            json arr;
            if(ctToJSON(arr)){
                answer["result"] = "success";
                answer["comm_table"] = arr;
            }else{
                answer["result"] = "error";
            }
            
//            for(int i=0; i<MAX_CHANNELS; i++)
//                activeChannels[i] = 0;//time(NULL);
        }else if(cmd == "set_comm_table" && js.contains("comm_table")){
            json _comm_table = js["comm_table"];
            set_comm_table(_comm_table);
            bool result = ctFromJSON(_comm_table);
            
            addCTHystory(_comm_table);

            if(result){
                answer["result"] = "success";
                saveToFile();

                sendSWPTable();
            }else{
                answer["result"] = "error";
            }
        }else if(cmd == "get_snd_params"){
            server.sendCommand(nullptr, 0, PACK_CFG, SND_IN_CPB_REQ, 1, 0xFF);
            server.sendCommand(nullptr, 0, PACK_CFG, SND_OUT_CPB_REQ, 1, 0xFF);

            sleep(1);

            answer["result"] = "success";
            answer["snd_params"] = get_json_snd_params();
            
        }else if(cmd == "set_snd_params"){
            bool result = true;

            //sndJSON() && 
            if(js.is_object() && js.contains("channel_type") && js.contains("channel") && js.contains("snd_params")){
                
                sendSndParams(js);
                
//                set_json_snd_params(_json_snd_params);
            }else{
                result = false;
            }

            answer["result"] = result ? "success" : "error";

            saveToFile();
        }else if(cmd == "set_settings"){
            if(js.contains("settings")){
                json _settings = js["settings"];
                set_settings(_settings);
                saveToFile();
                
                string new_ntp = _settings["time"]["ntp"];
                if(new_ntp != getNTP()){
                    setNTP(new_ntp);
                }
                
                if(_settings["time"].contains("rtc")){
                    rtc = _settings["time"]["rtc"];
                }
                
                if(_settings["bks"].contains("server_addr") && _settings["bks"]["server_addr"].is_number()){
                    server_addr = _settings["bks"]["server_addr"];
                }
                
                if(_settings["bks"].contains("cus_in") && _settings["bks"]["cus_in"].is_number()){
                    cus_in = _settings["bks"]["cus_in"];
                }
                
                answer["result"] = "success";
            }else{
                answer["result"] = "error";
            }
        }else if(cmd == "get_settings"){
            json _settings = get_settings();
            
            if(!_settings["rec"].contains("store_days")){
                _settings["rec"]["store_days"] = 2;
                set_settings(_settings);
            }
            
            if(!_settings["rec"].contains("enable")){
                _settings["rec"]["enable"] = true;
                set_settings(_settings);
            }
            
            _settings["rec"]["free"] = getAvailableSpace(REC_PATH);
            
            _settings["time"] = json::object({
                {"time", time(NULL)},
                {"ntp", getNTP()},
                {"rtc", rtc}
            });
            
            answer["result"] = "success";
            answer["settings"] = _settings;

        }else if(cmd == "clear_boards"){
            cout << "Clear boards" << endl;
            unique_lock<shared_mutex> lock(boardPingMtx);
            pingBoardMap.clear();
            saveBoards();
            
            lastPingTm = chrono::steady_clock::now();
            server.sendCommand(nullptr, 0, PACK_GEN_CALL, PING);
            
            answer["result"] = "success";
        }else if(cmd == "get_boards"){
            answer["result"] = "success";
            answer["boards"] = json::array();
            for(auto b : pingBoardMap){
                board_def_t brd = b.second;
//                answer["boards"].push_back(brd.addr);
                int brdN = 1;
                string devName = devTypeToNameBrdNum((blocks_t)brd.type, brd.addr, brdN);
                answer["boards"].push_back(json::object({
                    {"addr", (int)brd.addr},
                    {"type", (int)brd.type},
                    {"name", devName},
                    {"brdN", brdN}
                }));
            }
        }else if(cmd == "get_network"){
            answer["result"] = "success";
            answer["network"] = network;

        }else if(cmd == "get_alarms"){
            answer["result"] = "success";
            answer["alarms"] = getAlarms();
        }else if(cmd == "set_alarms"){
            if(js.contains("alarms")){
                answer["result"] = "success";
                setAlarms(js["alarms"]);
            }else{
                answer["result"] = "error";
                answer["message"] = "Ошибочный формат запроса";
            }
        }else if(cmd == "get_events"){
            answer["events"] = eventsToJson();
            answer["result"] = "success";
        }else if(cmd == "set_time"){
            if(js.contains("time")){
                int ts = round(static_cast<float>(js["time"]));
                
                ostringstream stm;
                stm << "date +%s -s @" << ts;
//                cout << stm.str() << endl;
                int res = system(stm.str().c_str());
//                cout << "result=" << res << endl;
                timeSync.setRTC();
                if(res == 0 || res == -1){
                    answer["result"] = "success";
                }else{
                    answer["result"] = "error";
                    answer["message"] = string(strerror(errno));
                }

            }else{
                answer["result"] = "error";
                answer["message"] = "No time in query";
            }
        }else if(cmd == "get_backup"){
            answer["result"] = "success";
            answer["command"] = "set_restore";
            answer["backup"] = backup();
        }else if(cmd == "set_restore"){
            if(js.contains("backup")){
                restore(js["backup"]);
            }
        }else if(cmd == "get_clips"){
            string dir = js.contains("dir") ? js["dir"] : "";
            namespace bfs = std::experimental::filesystem;
            answer["result"] = "success";
            answer["clips"] = json::array();
            bfs::directory_iterator itt(bfs::path("/var/www/html/AGO18Web/clips/"+dir));
            for(; itt != bfs::directory_iterator(); itt++){
                bfs::path path = itt->path();
                auto ext = path.extension();
                if(bfs::is_regular_file(path) && ext.generic_string()!=".json"){
                    answer["clips"].push_back(path);
                }
            }
        }else if(cmd == "play_clip"){
            if(js.contains("clip")){
//                cout << js.dump() << endl;
                string clip = string("/var/www/html/AGO18Web/")+string(js["clip"]);
                auto autorew = js.contains("autorew") ? (bool)js["autorew"] : false;
                auto source = js.contains("source") ? (int)js["source"]     : BRD_ID_TO_CHAN(server_addr, 0);
                
                if(js.contains("signal") && js["signal"]!=-1){
                    int signal = js["signal"];
                    printf("Play clip %s for signal %d\n", clip.c_str(), signal);
                    start_clip_for_signal(signal, autorew);
                }else{
                    auto outs = (js.contains("outs") && js["outs"]!=0) ? (ch_mask_t)js["outs"] : getCTOutsInput(source);
                    start_clip_process(source, outs, clip, autorew);
                }
                
                
//                auto file_senders = SoundSenderFile::getFileSenders();
//                for(auto sender : file_senders){
//                    if(sender->getInCh() == source && sender->isBusy())
//                        sender->stop();
//                }
//
//                auto sender = SoundSenderFile::getFreeSender();
//                sender->start(clip, source, outs);
                
                answer["result"] = "success";
            }
        }else if(cmd == "stop_clip"){
            printf("Stop clip\n");
            if(js.contains("source")){
//                string clip = string("/var/www/html/AGO18Web/clips/")+string(js["clip"]);
                auto source = js.contains("source") ? (int)js["source"] : BRD_ID_TO_CHAN(server_addr, 0);
                
                stop_clip_process(source);
//                auto file_senders = SoundSenderFile::getFileSenders();
//                for(auto sender : file_senders){
//                    if(sender->isBusy() && sender->getInCh()==source)
//                        sender->stop();
//                }
            }else{
                stop_clip_process();
            }
            answer["result"] = "success";
        }else if(cmd == "clip_state"){
            int pid = get_player_pid(js["source"]);
            answer["result"] = "success";
            answer["pid"] = pid;
        }else if(cmd == "clips_state"){
            auto srcs =  js.contains("sources") && js["sources"].is_array() ? js["sources"] : json::array();
            answer["states"] = json::array();
            for(auto src : srcs){
                auto state = json::object({
                    {"source", src},
                    {"pid", get_player_pid(src)}
                });
                answer["states"].push_back(state);
            }
            answer["result"] = "success";
        }else if(cmd == "get_bru_cfg"){
            if(js.contains("request") && js["request"]){
                server.sendCommand(nullptr, 0, PACK_BRU_CFG, BRU_GET_CFG, 3, 0xFF);
                usleep(1000000);
            }
            
            answer["result"] = "success";
            answer["bru_cfg"] = getBRUsJson();
        }else if(cmd == "set_bru_cfg"){
            if(js.contains("bru_cfg") && js["bru_cfg"].contains("id")){
                uint8_t buf[2048];
                bru_cfg_t* bru_cfg = reinterpret_cast<bru_cfg_t*>(buf);
                json j_bru_cfg = js["bru_cfg"];
                BRU2JSON().json_to_bru_cfg(j_bru_cfg, bru_cfg, 2048);
                uint8_t id = j_bru_cfg["id"];
                setBRUCfg(id, j_bru_cfg);
                server.sendCommand(reinterpret_cast<uint8_t*>(bru_cfg), BRU_CFG_SIZE(bru_cfg), PACK_BRU_CFG, BRU_SAVE_CFG, 3, id);
                
                answer["result"] = "success";
            }else{
                answer["result"] = "error";
                answer["message"] = "Ошибка при сохранении";
            }
        }else if(cmd == "bru_measure"){
            if(js.contains("bru_id")){
                bru_measure(js["bru_id"]);
                answer["result"] = "success";
            }else{
                answer["result"] = "error";
                answer["message"] = "Ошибка запроса";
            }
        }else if(cmd == "bru_measure_cancel"){
            bru_measure_active = false;
            answer["result"] = "success";
        }else if(cmd == "bru_reset"){
            if(js.contains("bru_id")){
                auto futr = dynamic_cast<PacketParserBRU*>(PacketParser::get_instance(PACK_BRU_CFG))->switch_cfg(js["bru_id"]);
                auto status = futr.wait_for(chrono::milliseconds(3000));
                if(status == future_status::ready){
                    answer["result"] = "success";
                    uint32_t to = futr.get();
                    usleep(to*1000);
                }else if(status == future_status::deferred){
                    answer["result"] = "error";
                    answer["message"] = "Deferred";
                }else{
                    answer["result"] = "error";
                    answer["message"] = "Таймаут. Обновление конфигурации...";
                }
            }
        }else if(cmd == "get_id"){
            if(js.contains("id") && js["id"].is_number_integer()){
                PacketParserCfg* pp = dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG));
                auto ids = pp->get_id(js["id"]);
                answer["result"] = "success";
                answer["ids"] = json::array();
                for(auto id : ids){
                    answer["ids"].push_back(json::object({
                        {"id", id.second.id},
                        {"uid", id.second.uid},
                        {"descr", id.second.descr}
                    }));
                }
            }else{
                answer["result"] = "error";
                answer["message"] = "Не задан ID";
            }
        }else if(cmd == "change_id"){
            if(js.contains("old_id") && js.contains("new_id") && js.contains("offset") && js.contains("um_mode") &&
                    js["old_id"].is_number_integer() && js["new_id"].is_number_integer() && js["offset"].is_number_integer() && js["um_mode"].is_number_integer()){
                dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG))->change_id(js["old_id"], js["new_id"], js["offset"], js["um_mode"]);
                answer["result"] = "success";
            }else{
                answer["result"] = "error";
                answer["message"] = "Ошибка параметров";
            }
        }else if(cmd == "set_id"){
            if(js.contains("new_id") && js.contains("um_mode") && js["new_id"].is_number_integer() && js["um_mode"].is_number_integer()){
                brd_id_t new_brd_id;
                future_status status = dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG))->set_id(js["new_id"], js["um_mode"], &new_brd_id);
                if(status == future_status::ready){
                    answer["result"] = "success";
                    answer["new_id"] = json::object({
                                                        {"id", (int)new_brd_id.id},
                                                        {"um_mode", (int)new_brd_id.isRedundant}
                                                    });
                }else if(status == future_status::deferred){
                    answer["result"] = "error";
                    answer["message"] = "Внутренняя ошибка сервера";
                }else{
                    answer["result"] = "error";
                    answer["message"] = "Вышло время ожидания";
                }
            }else{
                answer["result"] = "error";
                answer["message"] = "Ошибка параметров";
            }
        }else if(cmd == "get_inp_mode"){
            uint32_t bits;
            auto brds = dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG))->get_inp_mode(&bits);
            if(!brds.empty()){
                answer["result"] = "success";
                answer["inp_mode"] = bits;
                answer["count"] = brds.size();
                answer["ins"] = json::array();
                for(uint8_t brd : brds){
                    answer["ins"].push_back(BRD_ID_TO_CHAN(brd, 0));
                    answer["ins"].push_back(BRD_ID_TO_CHAN(brd, 1));
                }
            }else{
                answer["result"] = "error";
                answer["message"] = "Нет ответа";
            }
        }else if(cmd == "set_inp_mode"){
            if(js.contains("inp_mode") && js["inp_mode"].is_number_integer()){
                dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG))->set_inp_mode(js["inp_mode"]);
                answer["result"] = "success";
            }else{
                answer["result"] = "error";
                answer["message"] = "Ошибка параметров";
            }
        } else{
            answer["result"] = "error";
            answer["message"] = "Unknown query";
        }
    }catch(std::exception& e){
        log_error("ConfigServer::processQuery() %s", e.what());
        close(cli_sock);
        answer = json::object();
        answer["result"] = "error";
        answer["message"] = e.what();
    }
    
    try{
#ifdef CONFIG_DEBUG
        cout << answer << endl;
#endif

        ostringstream stm;
        stm << answer;
        string strAns = stm.str();
        send(cli_sock, strAns.c_str(), strAns.length(), 0);
    }catch(json::exception& e){
        log_error("ConfigServer::processQuery() %s", e.what());
    }
}

string ConfigServer::loadNTPText(){
    ifstream f_ntp;
    f_ntp.open("/etc/ntp.conf");
    if(f_ntp.is_open()){
        ostringstream stm;
        stm << f_ntp.rdbuf();
        f_ntp.close();
        string s_ntp = stm.str();
//        cout << s_ntp << endl;
        return s_ntp;
    }else{
        return "";
    }
}

bool ConfigServer::findNTPStr(string s_ntp, int &pos, int &len){
    pos = 0; 
    bool found = false;
    while(pos != static_cast<int>(string::npos)){
        pos = s_ntp.find("server", pos+1);
        if(s_ntp[pos-1] == '\n'){
            found = true;
            break;
        }
    }
    if(found){
        int pos_end = s_ntp.find("\n", pos+1);
        if(pos_end != static_cast<int>(string::npos)){
            len = pos_end - pos;
        }else{
            found = false;
        }
    }
    return found;
}

string ConfigServer::getNTP(){
    string result;
    
    string s_ntp = loadNTPText();
    if(s_ntp.length()){
        int pos = 0;
        int len = 0;
        if(findNTPStr(s_ntp, pos, len)){
            result = s_ntp.substr(pos+7, len-7);
        }
    }
    return result;
}

void ConfigServer::setNTP(string value){
    string s_ntp = loadNTPText();
    if(s_ntp.length()){
        int pos = 0;
        int len = 0;
        if(findNTPStr(s_ntp, pos, len)){
            if(value != "")
                s_ntp.replace(pos, len, "server "+value);
            else
                s_ntp.replace(pos, len+1, "");
        }else if(value != ""){
            s_ntp = s_ntp + "server " + value + "\n";
        }
            
        ofstream f_ntp;
        f_ntp.open("/etc/ntp.conf");
        if(f_ntp.is_open()){
            f_ntp << s_ntp;
            f_ntp.close();

            system("service ntp restart");
        }
    }
}

void ConfigServer::ctInputFromJSON(int ch){
    try{
        json _comm_table_ch = get_comm_table_ch(ch);

        if(_comm_table_ch.is_object() && _comm_table_ch.contains("out")){
            unique_lock<shared_mutex> lock(swp_table_mutex);
            swp_table.sw_table[ch] = _comm_table_ch["out"];
        }
    }catch(json::exception &e){
        log_error("ConfigServer::ctInputFromJSON() %s", e.what());
    }
}

bool ConfigServer::ctFromJSON(json js){
    try{
        if(!js.is_object() || !js.contains("sw_table") || !js.contains("prio_code"))
            return false;
        
        swp_table_t _swp_table;
        memset(&_swp_table, 0, sizeof(swp_table_t));

        _swp_table.prio_code = js["prio_code"];

        int _prios[MAX_CHANNELS];
        vector<prio_t> prio_sort;

        for(json& ch : js["sw_table"]){
            //cout << "ch = " << ch << endl;

            if(!ch.contains("in") || !ch.contains("out") || !ch.contains("prio")){
                return false;;
            }

            int i = ch["in"];
            int o = ch["out"];
            int prio = ch["prio"];
            _swp_table.sw_table[i] = o;
            if(i>=0 && i<MAX_CHANNELS)
                _prios[i] = prio;
            prio_t p = {i, prio};
            prio_sort.push_back(p);
        }

        sort(prio_sort.begin(), prio_sort.end(), [_swp_table](prio_t& a, prio_t& b){
            if(a.prio == b.prio){
                return a.ch < b.ch;
            }

            if(_swp_table.prio_code == 0)
                return a.prio < b.prio;
            else
                return b.prio < a.prio;
        });
        int i=0;
        for(prio_t p : prio_sort){
            _swp_table.prio_table[i] = p.ch;
            i++;
        }

        unique_lock<shared_mutex> lock(swp_table_mutex);
        swp_table = _swp_table;
        swp_table_def = _swp_table;
        memcpy(prios, _prios, sizeof(int)*MAX_CHANNELS);

        return true;
    }catch(json::exception& e){
        log_error("ConfigServer::ctFromJSON() %s", e.what());
        return false;
    }
}

bool ConfigServer::ctToJSON(json &js){
    swp_table_t _swp_table = getSWPTable();

    js = json::object({
        {"prio_code", _swp_table.prio_code},
        {"sw_table", json::array()}
    });

    try{
        for(int i=0; i<MAX_CHANNELS; i++){
            json ch = json::object({
                {"in", i}, 
                {"out", _swp_table.sw_table[i]},
                {"prio", prios[i]},
                {"online", getChannelActive(i)},
                {"alarm", alarmOuts.load()},
                {"in_alarm", alarmIns[i].load()!=0}
            });
            js["sw_table"].push_back(ch);
        }
    }catch(json::exception& e){
        log_error("ConfigServer::ctToJSON() %s", e.what());
        return false;
    }
    
    return true;
}

void ConfigServer::addCTHystory(json ct){
    string path = string(SETTS_PATH)+"/ct_hyst";
    string cmd = string("mkdir -p ")+path;
    system(cmd.c_str());
    
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char str[100];
    sprintf(str, "%s/ct-%04d%02d%02d-%02d%02d%02d.json", path.c_str(), tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    ofstream stm;
    stm.open(str);
    if(stm.is_open()){
        try{
            stm << ct;
        }catch(json::exception &e){
            log_error("ConfigServer::addCTHystory() %s", e.what());
        }
        stm.close();
    }
}

ch_mask_t ConfigServer::getCTOutsInput(int input){
    if(input >= 0 && input < MAX_CHANNELS){
        try{
            shared_lock<shared_mutex> lock(swp_table_mutex);
            return swp_table.sw_table[input];
        }catch(json::exception &e){
            log_error("ConfigServer::getCTOutsInput() %s", e.what());
            return 0;
        }
    }else{
        return 0;
    }
}

ch_mask_t ConfigServer::getActiveOuts()
{
    ch_mask_t outs = 0;

    shared_lock<shared_mutex> lock(swp_table_mutex);
    for(int i=0; i<MAX_CHANNELS; i++){
//        if(isChannelActive(i))
        outs |= getChannelActive(i);//swp_table.sw_table[i];
    }
    return outs;
}

void ConfigServer::setCTOutsInput(int input, ch_mask_t outs){
    if(input >= 0 && input < MAX_CHANNELS){
        try{
            unique_lock<shared_mutex> lock(swp_table_mutex);
            if(swp_table.sw_table[input] != outs)
                state_changed = true;
            swp_table.sw_table[input] = outs;
        }catch(json::exception &e){
            log_error("ConfigServer::setCTOutsInput() %s", e.what());
        }
//        if(changed){
////            send_ws("state_page");
//            send_ws_input_state(input);
//            send_ws("pd");
//        }
    }
}

bool ConfigServer::sndJSON(){
    try{
        json _json_snd_params = get_json_snd_params();
        
        if(!_json_snd_params.is_object()){
            _json_snd_params = json::object();
        }

        if(!_json_snd_params.contains("ins")){
            _json_snd_params["ins"] = json::array();
        }

        if(!_json_snd_params.contains("outs")){
            _json_snd_params["outs"] = json::array();
        }

        for(int i=0; i<MAX_CHANNELS; i++){
            _json_snd_params["ins"][i] = json::object({
                {"vol", 5.0},
                {"min", 0.0},
                {"max", 10.0},
                {"step",1.0}
            });
        }
        
        for(int i=0; i<static_cast<int>(CT_BITS); i++){
            _json_snd_params["outs"][i] = json::object({
                {"vol", 5.0},
                {"min", 0.0},
                {"max", 10.0},
                {"step",1.0},
                {"treble", 5},
                {"treble_min", 0},
                {"treble_max", 10},
                {"treble_step", 1},
                {"bass", 5},
                {"bass_min", 0},
                {"bass_max", 10},
                {"bass_step", 1}
            });
        }

#ifdef CONFIG_DEBUG
        cout << _json_snd_params << endl;
#endif
        
        set_json_snd_params(_json_snd_params);
    }catch(json::exception& e){
        log_error("ConfigServer::sndJSON() %s", e.what());
        return false;
    }

    return true;
}

void ConfigServer::settsToJSON(){
    try{
        json _settings = get_settings();

        if(!_settings.is_object()){
            _settings = json::object();
        }

        if(!_settings.contains("ins_cnt")){
            _settings["ins_cnt"] = MAX_CHANNELS_USED;
        }

        if(!_settings.contains("outs_cnt")){
            _settings["outs_cnt"] = 20;
        }

        if(!_settings.contains("bks")){
            _settings["bks"] = "БКС";
        }

        if(!_settings.contains("ins")){
            _settings["ins"] = json::array();;
        }

        if(!_settings.contains("outs")){
            _settings["outs"] = json::array();;
        }

        for(int i=0; i<_settings["ins_cnt"]; i++){
            if(i >= static_cast<int>(_settings["ins"].size())){
                _settings["ins"][i] = json::object({
                    {"ch", i},
                    {"name", ""}
                });
            }
        }
        for(int i=0; i<_settings["outs_cnt"]; i++){
            if(i >= static_cast<int>(_settings["outs"].size())){
                _settings["outs"][i] = json::object({
                    {"ch", i},
                    {"name", ""}
                });
            }
        }

        set_settings(_settings);
    }catch(json::exception &e){
        log_error("ConfigServer::settsToJSON() %s", e.what());
    }
}

void ConfigServer::loadFromFile(){
    try{
        string name = string(SETTS_PATH)+"/settings.json";

        ifstream sett_file;
        sett_file.open(name);
        if(sett_file.is_open()){
            json _settings = json::parse(sett_file, nullptr, false);
            
            if(!_settings.contains("ins_cnt") || _settings["ins_cnt"] > MAX_CHANNELS){
                _settings["ins_cnt"] = MAX_CHANNELS;
            }
            
            if(!_settings.contains("outs_cnt") || _settings["outs_cnt"] > CT_BITS){
                _settings["outs_cnt"] = CT_BITS;
            }
            
            if(!_settings.contains("bks")){
                _settings["bks"] = json::object({{"name", ""}});
            }
            
            if(!_settings.contains("ins")){
                _settings["ins"] = json::array();
            }
            
            if(!_settings.contains("outs")){
                _settings["outs"] = json::array();
            }

            insRecMask = 0;
            for(int i=0; i<_settings["ins_cnt"]; i++){
                if(i >= _settings["ins"].size()){
                    _settings["ins"].push_back(json::object({
                                                                {"ch", i},
                                                                {"name", ""},
                                                                {"rec", true}
                                                            }));
                }
                if(_settings["ins"][i].contains("rec") && _settings["ins"][i]["rec"].is_boolean()){
                    bool inEnbl = _settings["ins"][i]["rec"];
                    if(inEnbl){
                        insRecMask |= (1ULL<<i);
                    }
                }else{
//                    cout << "Add ch " << i << " " << (1<<i) << endl;
                    insRecMask |= (1ULL<<i);
                }
            }
            cout << "insRecMask = " << hex << insRecMask << dec << endl;
            

            for(int i=0; i<_settings["outs_cnt"]; i++){
                if(i >= _settings["outs"].size()){
                    _settings["outs"].push_back(json::object({
                                                                {"ch", i},
                                                                {"name", ""},
                                                                {"target", ""}
                                                            }));
                }
            }

            if(!_settings.contains("rec")){
                _settings["rec"] = json::object({
                    {"bandwidth", 1104},
                    {"bitrate", 64000},
                    {"format", 1},
                    {"signal", 3002}
                });
            }
            
            if(_settings["rec"].contains("min_free") && _settings["rec"]["min_free"].is_number_integer()){
                min_disk_free = _settings["rec"]["min_free"];
            }else{
                min_disk_free = 300;
            }
            cout << "min_disk_free" << " = " << dec << min_disk_free << endl;
            
            if(_settings["bks"].contains("server_addr") && _settings["bks"]["server_addr"].is_number()){
                server_addr = _settings["bks"]["server_addr"];
            }
            
            if(_settings["bks"].contains("cus_in") && _settings["bks"]["cus_in"].is_number()){
                cus_in = _settings["bks"]["cus_in"];
            }
            
            if(_settings["bks"].contains("id_offset") && _settings["bks"]["id_offset"].is_number_integer()){
                id_offset = _settings["bks"]["id_offset"];
            }
            
            if(_settings["bks"].contains("bru_offset") && _settings["bks"]["bru_offset"].is_number_integer()){
                bru_offset = _settings["bks"]["bru_offset"];
            }
            
            if(_settings["bks"].contains("ping_period") && _settings["bks"]["ping_period"].is_number_integer()){
                ping_period = chrono::seconds((int)_settings["bks"]["ping_period"]);
            }
            
            if(_settings["bks"].contains("ping_timeout") && _settings["bks"]["ping_timeout"].is_number_integer()){
                ping_timeout = chrono::seconds((int)_settings["bks"]["ping_timeout"]);
            }

            if(_settings.contains("timeout")){
                timeout_rec = _settings["timeout"].contains("rec") ? (int)_settings["timeout"]["rec"] : 3000;
                timeout_iface = _settings["timeout"].contains("iface") ? (int)_settings["timeout"]["iface"] : 3000;
            }
            printf("Timeout for record = %d\nTimeout for activity = %d\n", timeout_rec, timeout_iface);
            
            if(!_settings.contains("player")){
                _settings["player"] = json::object({
                    {"packs_at_time", 6}
                });
            }

            if(!_settings.contains("events")){
                _settings["events"] = json::object({
                                                       {"log_period", 7}
                                                   });
            }
            
            if(_settings.contains("time") && _settings["time"].contains("rtc") && _settings["time"]["rtc"].is_string()){
                rtc = _settings["time"]["rtc"];
            }

            sett_file.close();
            
            set_settings(_settings);
        }
        
        name = string(SETTS_PATH)+"/boards.json";
        sett_file.open(name);
        if(sett_file.is_open()){
            json arr = json::parse(sett_file, nullptr, false);
            sett_file.close();
            
            unique_lock<shared_mutex> lock(boardPingMtx);
            pingBoardMap.clear();
            for(auto brd : arr){
                if(brd.is_object() && brd.contains("addr") && brd.contains("type") && brd.contains("ip")){
                    pingBoardMap[(uint8_t)brd["addr"]] = {
                        (uint8_t)brd["addr"], 
                        (uint8_t)brd["type"],
                        brd["ip"]
                    };
                }
            }
        }
    }catch(json::exception& e){
        log_error("ConfigServer::loadFromFile() settings.json %s", e.what());
    }
    
    try{
        string name = string(SETTS_PATH)+"/network.json";
        
        ifstream file;
        file.open(name);
        if(file.is_open()){
            network = json::parse(file);
            file.close();
        }
    }catch(json::exception& e){
        log_error("ConfigServer::loadFromFile() network.json %s", e.what());
    }
    
    try{
        string name = string(SETTS_PATH)+"/commtable.json";

        ifstream file;
        file.open(name);
        if(file.is_open()){
            try{
                json _comm_table = json::parse(file, nullptr, true);
                set_comm_table(_comm_table);
                ctFromJSON(_comm_table);
            }catch(json::parse_error& e){
                cerr << red << e.what() << clrst << endl;
            }

            file.close();
        }else{
            //comm_table = json::array();
            json arr;
            ctToJSON(arr);
            set_comm_table(arr);
        }
    }catch(json::exception& e){
        log_error("ConfigServer::loadFromFile() commtable.json %s", e.what());
    }

    loadAlarms();
}

void ConfigServer::saveToFile(){
    string cmd = string("mkdir -p ")+SETTS_PATH;
    system(cmd.c_str());
    
    try{
//        json array;
//        ctToJSON(array);

        string name = string(SETTS_PATH)+"/commtable.json";

        ofstream file;
        file.open(name);
        if(file.is_open()){
            file << get_comm_table();
            file.close();
        }
    }catch(json::exception& e){
        log_error("ConfigServer::saveToFile() commtable.json %s", e.what());
    }

    try{
        //Sound
        string name = string(SETTS_PATH)+"/sound.json";

        ofstream file;
        file.open(name);
        if(file.is_open()){
            file << get_json_snd_params();
            file.close();
        }
    }catch(json::exception& e){
        log_error("ConfigServer::saveToFile() sound.json %s", e.what());
    }
    
    try{
        string name = string(SETTS_PATH)+"/settings.json";
        
        ofstream sett_file;
        sett_file.open(name);
        if(sett_file.is_open()){
            sett_file << setw(2) << setfill(' ') << get_settings();
            sett_file.close();
        }
    }catch(json::exception& e){
        log_error("ConfigServer::saveToFile() settings.json %s", e.what());
    }
}

int ConfigServer::getRecParam(string name, int def){
    
    try{
        json _settings = get_settings();
        
        if(!_settings.is_object())
            return def;

        if(!_settings.contains("rec"))
            return def;

        json rec = _settings["rec"];
        if(!rec.is_object())
            return def;

        if(!rec.contains(name))
            return def;

        if(!rec[name].is_number_integer()){
            return def;
        }

        return rec[name];
    }catch(json::exception& e){
        log_error("ConfigServer::getRecParam(): %s", e.what());
        return def;
    }
}

int ConfigServer::getNetParam(string name, int def){
    try{
        auto type = network[name].type();
        bool is_int_type = (type == json::value_t::number_unsigned || type == json::value_t::number_integer);
        if(network.contains(name) && is_int_type){
            return network[name];
        }else{
            return def;
        }
    }catch(json::exception& e){
        log_error("ConfigServer::getNetParam(): %s", e.what());
        return def;
    }
}

string ConfigServer::getNetParam(string name, const char* def){
    try{
        if(network.contains(name) && network[name].type() == json::value_t::string){
            return network[name];
        }else{
            return def;
        }
    }catch(json::exception& e){
        log_error("ConfigServer::getNetParam(): %s", e.what());
        return def;
    }
}

void ConfigServer::saveNetwork(){
    try{
        //Sound
        string name = string(SETTS_PATH)+"/network.json";

        ofstream file;
        file.open(name);
        if(file.is_open()){
            file << setw(2) << setfill(' ') << network;
            file.close();
        }
    }catch(json::exception& e){
        log_error("ConfigServer::saveNetwork(): %s", e.what());
    }
}

void ConfigServer::setNetParam(string name, string val){
    network[name] = val;
    saveNetwork();
}

void ConfigServer::setNetParam(string name, int val){
    network[name] = val;
    saveNetwork();
}

void ConfigServer::replace_tag(string &str, string tag, string val){
    auto n = str.find(tag);
    if(n != string::npos){
        str = str.replace(n, tag.length(), val);
    }
}

string ConfigServer::devTypeToNameBrdNum(blocks_t dev_type, uint8_t addr, int &brdNum){
    brdNum = 1;
    switch(dev_type){
        case BKS_AUDIO:
        case BKS_CNTRL:
            return "БКС";
        case BRU:
            brdNum = addr-bru_offset;
            return "БРУ";
        case UM_TRANS:
        case UM_DPA:
            brdNum = addr-id_offset;
            return "БУ";
            break;
        case BRP:
            return "БРП";
            break;
    }
    return "";
}

string ConfigServer::alarmToStr(int alarm, int addr, int param, int ch, int brd, event_type_t &type, blocks_t dev_type){
    string str;

    if(ch < -2 || brd < -1){
//        cout << "alarmToStr: ch=" << ch << " brd=" << brd << endl;
        return str;
    }

    try{
        json j_alarm = getAlarmsStr(alarm);
        str = j_alarm.contains("message") ? j_alarm["message"] : "";
        type = j_alarm.contains("type") ? static_cast<event_type_t>(j_alarm["type"]) : EVT_DANGER;
    }catch(json::exception &e){
        log_error("alarmToStr(): %s", e.what());
    }

    if(!str.empty()){
        replace_tag(str, "<ERROR>", std::to_string(param));
        replace_tag(str, "<BRU_UM>", std::to_string(CHAN_TO_BRD_ID(ch)+1));//
        replace_tag(str, "<CHANNEL>", std::to_string(ch+1));
        replace_tag(str, "<BOARD>", std::to_string(brd+1));
        int brdN = 1;
        string sDevType = devTypeToNameBrdNum(dev_type, addr, brdN);
        replace_tag(str, "<BRD_TYPE>", sDevType);
        replace_tag(str, "<SENDER>", std::to_string(brdN));

        if(alarm == BRU_LINE_OVER_LIM){
            auto measures = reinterpret_cast<PacketParserBRU*>(PacketParser::get_instance(PACK_BRU_CFG))->get_measures();
            float val = measures[param].val;
            float ref_val = measures[param].ref_val;
            float dev = ref_val!=0 ? ((val - ref_val) / ref_val) * 100.0f : 0;

            char val_str[100];
            sprintf(val_str, "%0.1f", val);
//            cout << val_str << endl;
            replace_tag(str, "<MEASURE_VAL>", val_str);

            sprintf(val_str, "%0.1f", ref_val);
//            cout << val_str << endl;
            replace_tag(str, "<MEASURE_REF_VAL>", val_str);

            sprintf(val_str, "%0.0f", dev);
//            cout << val_str << endl;
            replace_tag(str, "<MEASURE_DEVIATION>", val_str);
        }

        return str;
    }

    switch(alarm){
        case T100:
            return "Перегрев усилителя > 100°C";
            
        case T125:
            return "Перегрев усилителя > 125°C";
            
        case SD:
            return "Перегрузка";
            
        case UM_ERROR:
            return "Ошибка инициализации усилителя";
            
        case HW_ERROR:{
            char str[50];
            sprintf(str, "Внутренняя ошибка %d", param);
            return string(str);
        }
            
        case DS1621S_OVR60:
            return "Общий перегрев > 60°C";
            
        case BRU_UM_NOT_ANSWER:
            return "БРУ: нет ответа от усилителя";
            
        case BRU_LINE_SHORT:
            return "БРУ: КЗ линии";
            
        case BRU_LINE_OPEN:
            return "БРУ: обрыв линии";
            
        case BRU_LINE_OVER_LIM:
            return "БРУ: сопротивление линии не в норме";
            
        case BRU_SWITCH_TO_BYPASS_ERROR:
            return "БРУ: ошибка аварийного ввода резерва усилителя";
            
        case BRU_SWITCH_BACK_ERROR:
            return "БРУ: ошибка ввода штатного усилителя";

        case BRU_REDUND_UM_ACTIVE:{
            char str[100];
            sprintf(str, "БРУ: резервирование канала %d", param);
            return string(str);
        }
    }
    return "";
}

uint32_t ConfigServer::alarmSource(int ch, int brd, alarm_t alarm, blocks_t dev_type){
    ch_mask_t channels = 0;
    bool is_ampl = ampl_alarms.find(alarm) != ampl_alarms.end() || dev_type==UM_TRANS || dev_type == UM_DPA;
    bool is_bks = dev_type==BKS_AUDIO || dev_type == BKS_CNTRL;
//    if(pingBoardMap.find(brd) != pingBoardMap.end()){
//        is_ampl = pingBoardMap[brd].type == UM_TRANS || pingBoardMap[brd].type == UM_DPA;
//        is_bks = pingBoardMap[brd].type == BKS_AUDIO;
//    }
    if(is_ampl){
        for(int j=0; j<MAX_INP_CHANNELS; j++){
            channels |= (1 << BRD_ID_TO_CHAN(brd, j));
        }
    }else{// if(!is_bks)
        channels |= (1 << ch);
    }
//    cout << "alarmSource" << ch << " " << dev_type << " = " << channels << endl;
    return channels;
}

void ConfigServer::setChannelAlarms(int cnt, alarm_item_t* alarms, uint8_t addr_from, blocks_t dev_type){
    for(int i=0; i<cnt; i++){
//        cout << "Alarm from 0x" << hex << (int)addr_from << " alarm_code=0x" << (int)alarms[i].alarm_code << " param=0x" << (int)alarms[i].param << endl;
        
        bool _set = (alarms[i].alarm_code & SET_ALARM_MASK) == SET_ALARM;
        alarms[i].alarm_code &= ~SET_ALARM_MASK;
        
        bool hw = alarms[i].alarm_code == HW_ERROR || alarms[i].alarm_code == GEN_CALL_NO_ANSWER;
        int ch = !hw ? alarms[i].param : 0;
        int brd = !hw ? CHAN_TO_BRD_ID(ch) : addr_from;
        if(hw && (dev_type==UM_TRANS || dev_type==UM_DPA)){
            brd -= id_offset;
        }

        ch_mask_t channels = alarmSource(ch, brd, static_cast<alarm_t>(alarms[i].alarm_code), dev_type);
        
        if(_set){
            event_type_t evt_type;
            string msg = alarmToStr(alarms[i].alarm_code, addr_from, alarms[i].param, ch-2, brd-1, evt_type, dev_type);
//            cout << "Alarm str: " << msg << endl;
            if(msg.empty())
                return;
            addEvent(channels, addr_from, &alarms[i], evt_type, msg);
        }else{
            removeEvent(addr_from, &alarms[i]);
        }
        
        ch_mask_t _alarmChannels = 0;
        for(auto e : getEvents()){
            if(e.second.type == EVT_DANGER){
                _alarmChannels |= e.second.channels;
            }
        }
        alarmOuts = _alarmChannels;
        
        if(dev_type==BKS_AUDIO || dev_type == BKS_CNTRL){
            for(int j=0; j<MAX_INP_CHANNELS; j++){
                int c = BRD_ID_TO_CHAN(brd,j);
                alarmIns[c] = _set ? 1 : 0;
            }
        }
    }
        
    send_ws("state_page");
}

bool ConfigServer::isChannelAlarmed(int ch){
    if(ch < 0 || ch >= MAX_CHANNELS)
        return false;
    
    return (alarmOuts & (1 << ch)) > 0;
}

void ConfigServer::writeEventsLog(event_t event){
    std::time_t tt = std::chrono::system_clock::to_time_t(event.time);
    std::tm tm;
    localtime_r(&tt, &tm);

    std::stringstream ssFName;
    ssFName << std::put_time( &tm, "%Y-%m-%d.log" );
    string fName = string(EVENTS_PATH)+ssFName.str();

    ofstream f;
    f.open(fName, ios_base::openmode::_S_app);
    if(f.good()){
        f << chrono::duration_cast<chrono::milliseconds>(event.time.time_since_epoch()).count() << "\t" << event.type << "\t" << event.channels << "\t" << event.message << endl; //std::put_time( &tm, "%H:%M:%S" )
        f.close();
    }

    ostringstream stm;
    stm << "find " << EVENTS_PATH << "* -type f -mtime +" << events_log_period << " -delete";//
    system(stm.str().c_str());
//    cout << stm.str() << endl;
}

json ConfigServer::eventsLogFiles()
{
    namespace bfs = std::experimental::filesystem;

    json js = json::array();

    bfs::directory_iterator itt(bfs::path(EVENTS_PATH));
    for(; itt != bfs::directory_iterator(); itt++){
        auto name = itt->path().filename();
        js.push_back(name);
    }

    return js;
}

json ConfigServer::eventsLogToJson(string fname)
{
    unique_lock<shared_mutex> lock(events_log_mutex);

    json js = json::array();

    FILE* f = fopen((string(EVENTS_PATH)+fname+string(".log")).c_str(), "rt");
    while(f && !feof(f)){
        int tm, type, ch;
        char msg[1024];
        fscanf(f, "%d\t%d\t%d\t%s\n", &tm, &type, &ch, msg);

        json j_evt = json::object({
                                      {"time", tm},
                                      {"type", type},
                                      {"channels", ch},
                                      {"message", string(msg)}
                                  });
        js.push_back(j_evt);
    }
    if(f){
        fclose(f);
    }

    return js;
}

void ConfigServer::addEvent(ch_mask_t channels, uint8_t addr, alarm_item_t* alarm, event_type_t type, const string msg){
    event_t event = {
        chrono::system_clock::now(),
        type,
        channels,
        msg};
    uint64_t key = (addr<<16) | (alarm->alarm_code<<8) | alarm->param;//(static_cast<uint64_t>(source) << 32) + alarm;
    
    cout << "Set alarm. key=0x" << hex << key << " " << msg << dec << endl;
    
    {
        unique_lock<shared_mutex> lock(events_log_mutex);
        events_map[key] = event;

        writeEventsLog(event);
    }


    state_changed = true;
}

void ConfigServer::addServerEvent(uint8_t alarm_code, event_type_t type, uint8_t param, const char* msg, ...){
    va_list args;
    va_start(args, msg);
    
    char str[256];
    sprintf(str, msg, va_arg(args, void *));
    
    alarm_item_t alarm = {alarm_code, param};
    addEvent(static_cast<ch_mask_t>(0), 0, &alarm, type, string(str));
    
    va_end(args);
}

void ConfigServer::removeEvent(uint8_t addr, alarm_item_t* alarm){
    uint64_t key = (addr<<16) | (alarm->alarm_code<<8) | alarm->param;//(static_cast<uint64_t>(source) << 32) + alarm;
    
    cout << "Reset alarm. key=0x" << hex << key << dec << endl;
    
    {
        unique_lock<shared_mutex> lock(events_log_mutex);
        events_map.erase(key);
    }
    
//    send_ws("state_page");
}

void ConfigServer::removeServerEvent(uint8_t alarm_code, event_type_t type, uint8_t param){
    alarm_item_t alarm = {alarm_code, param};
    removeEvent(0, &alarm);
}

ConfigServer::events_map_t ConfigServer::getEvents(){
    shared_lock<shared_mutex> lock(events_log_mutex);
    return events_map;
}

json ConfigServer::eventsToJson(){
    json js_events = json::array();
    int i=0;
    for(auto event : getEvents()){
        ch_mask_t src = (event.first & 0xFFFFFFFF00000000) >> 32;
        js_events[i++] = json::object({
            {"type", event.second.type},
            {"message", event.second.message},
            {"source", src}
        });
    }
    return js_events;
}

swp_table_t ConfigServer::getSWPTable(){
    shared_lock<shared_mutex> lock(swp_table_mutex);
    return swp_table;
}

void ConfigServer::sendSWPTable()
{
    swp_table_t _swp_table = getSWPTable();

    int ins_cnt = 0;
    try{
        json setts = get_settings();
        ins_cnt = setts["ins_cnt"];
        int buf_sz = sizeof(ch_mask_t)*ins_cnt + sizeof(uint8_t)*ins_cnt + sizeof(uint8_t);

        uint8_t buf[buf_sz];
        uint8_t* p = buf;
        for(int i=0; i<ins_cnt; i++){
            *reinterpret_cast<ch_mask_t*>(p) = _swp_table.sw_table[i];
            p += sizeof(ch_mask_t);
        }
        *p = _swp_table.prio_code;
        p += sizeof(uint8_t);
        for(int i=0; i<ins_cnt; i++){
            *p = _swp_table.prio_table[i];
            p += sizeof(uint8_t);
        }

        printf("Send sw_table buf_sz=%d\n", buf_sz);
        server.sendCommand(buf, buf_sz, PACK_CFG, EXT_SWT);
    }catch(json::exception &e){
        log_error("ConfigServer::sendSWPTable^ %s", e.what());
    }

}

void ConfigServer::sendSndParams(json js){
    try{
        string type = js["channel_type"];
        int ch = js["channel"];
        json snd_params = js["snd_params"];
        int brd = CHAN_TO_BRD_ID(ch);
        int idx = CHAN_TO_INDX(ch);

        uint8_t buf[1000];

        json _json_snd_params = get_json_snd_params();
        if(!_json_snd_params.is_object())
            _json_snd_params = json::object();
        if(!_json_snd_params.contains(type))
            _json_snd_params[type] = json::array();

        if(!snd_params.is_object())
            return;

        if(snd_params["lvl"] != _json_snd_params[type][ch]["cfg"]["lvl"]){
            int sz = sizeof(in_out_cfg_t);//sizeof(filter_t)+sizeof(snd_lvl_t)+2)*MAX_INP_CHANNELS;
            memset(buf, 0, sz);
            in_out_cfg_t* p = reinterpret_cast<in_out_cfg_t*>(buf);

            p->ch_index = idx;
            p->lvl = snd_params["lvl"]; //snd_params["lvl"].is_number() ?
//                pow(10, (float)snd_params["lvl"]/20.0) :
//                0;//

            server.sendCommand(buf, sz, PACK_CFG, type=="ins" ? AUDIO_CFG_IN : AUDIO_CFG_OUT, 1, brd);

            _json_snd_params[type][ch]["cfg"]["lvl"] = p->lvl;
        }

        if(snd_params["amplf_volt"] != _json_snd_params[type][ch]["cfg"]["amplf_volt"]){
            int sz = sizeof(pa_cfg_t);//sizeof(filter_t)+sizeof(snd_lvl_t)+2)*MAX_INP_CHANNELS;
            memset(buf, 0, sz);
            pa_cfg_t* p = reinterpret_cast<pa_cfg_t*>(buf);

            p->ch_indx = idx;
            p->volt_indx = snd_params["amplf_volt"];

            server.sendCommand(buf, sz, PACK_CFG, PA_CFG, 1, brd);

            _json_snd_params[type][ch]["cfg"]["amplf_volt"] = p->volt_indx;
        }

        //fcut_indx
        if(snd_params["fcut_indx"] != _json_snd_params[type][ch]["cfg"]["fcut_indx"]){
            int sz = sizeof(hpf_cfg_t);//sizeof(filter_t)+sizeof(snd_lvl_t)+2)*MAX_INP_CHANNELS;
            memset(buf, 0, sz);
            hpf_cfg_t* p = reinterpret_cast<hpf_cfg_t*>(buf);

            p->ch_index = idx;
            p->fcut_indx = snd_params["fcut_indx"];

            server.sendCommand(buf, sz, PACK_CFG, HPF_CFG, 1, brd);

            _json_snd_params[type][ch]["cfg"]["fcut_indx"] = p->fcut_indx;
        }

        //eq
        if(snd_params["eq"] != _json_snd_params[type][ch]["cfg"]["eq"]){// !param_compare(snd_params["eq"], _json_snd_params[type][ch]["cfg"]["eq"]) snd_params.contains("eq") && snd_params["eq"].contains("outGain") && snd_params["eq"].contains("ec")
            int sz = sizeof(eq_cfg_t);//sizeof(filter_t)+sizeof(snd_lvl_t)+2)*MAX_INP_CHANNELS;
            memset(buf, 0, sz);
            eq_cfg_t* p = reinterpret_cast<eq_cfg_t*>(buf);

            p->ch_index = idx;
            p->out_gain = snd_params["eq"]["outGain"].is_number() ? pow(10, (float)snd_params["eq"]["outGain"]/20.0) : 0;

            bool changed = false;

            if(snd_params["eq"]["ec"].is_array()){
                if(snd_params["eq"]["outGain"] != _json_snd_params[type][ch]["cfg"]["eq"]["outGain"]){//_json_snd_params[type][ch]["cfg"]["eq"]["outGain"] != snd_params["eq"]["outGain"]
                    p->eq_index = 0;
                    p->band = snd_params["eq"]["ec"][0]["band"];
                    p->fcentr_hz = snd_params["eq"]["ec"][0]["freq"];
                    p->gain = snd_params["eq"]["ec"][0]["gain"].is_number() ?
                        pow(10, (float)snd_params["eq"]["ec"][0]["gain"]/20.0) :
                        0;
                    printf("out_gain=%f\n", p->out_gain);

                    server.sendCommand(buf, sz, PACK_CFG, SW_EQ, 1, brd);

                    changed = true;
                }

                for(size_t i=0; i<snd_params["eq"]["ec"].size(); i++){
                    if(snd_params["eq"]["ec"][i] != _json_snd_params[type][ch]["cfg"]["eq"]["ec"][i]){//changed || snd_params["eq"]["ec"][i] != _json_snd_params[type][ch]["cfg"]["eq"]["ec"][i]
                        p->eq_index = i;
                        p->band = snd_params["eq"]["ec"][i]["band"];
                        p->fcentr_hz = snd_params["eq"]["ec"][i]["freq"];
                        p->gain = snd_params["eq"]["ec"][i]["gain"].is_number() ?
                            pow(10, (float)snd_params["eq"]["ec"][i]["gain"]/20.0) :
                            0;
                        printf("gain=%f\n", p->gain);

                        server.sendCommand(buf, sz, PACK_CFG, SW_EQ, 1, brd);

                        changed = true;
                    }
                }
            }

            if(snd_params["eq"]["bypass"] != _json_snd_params[type][ch]["cfg"]["eq"]["bypass"]){
                uint8_t data[2];
                data[0] = idx;
                data[1] = snd_params["eq"]["bypass"];

                server.sendCommand(data, 2, PACK_CFG, SW_EQ_BYPASS, 1, brd);

                changed = true;
            }

            if(changed)
                _json_snd_params[type][ch]["cfg"]["eq"] = snd_params["eq"];
        }

        set_json_snd_params(_json_snd_params);
    }catch(json::exception &e){
        log_error("ConfigServer::sendSndParams(): %s", e.what());
    }
}

void ConfigServer::setSndInCapabs(int ch, uint8_t* &capabs){
    if(ch >= MAX_CHANNELS)
        return;

    json _json_snd_params = get_json_snd_params();
    if(capabs){
        try{
            Snd2Json snd2json(capabs, "snd_in_capab_t");
            _json_snd_params["ins"][ch]["capabs"] = snd2json.snd_in_capab_to_json(capabs);
        }catch(json::exception &e){
            log_error("ConfigServer::setSndInCapabs(): %s", e.what());
        }
    }

    set_json_snd_params(_json_snd_params);

//    cout << _json_snd_params.dump(4) << endl;
}

void ConfigServer::setSndInParams(int ch, uint8_t* &cfg, int index){
    if(ch >= MAX_CHANNELS)
        return;

    json _json_snd_params = get_json_snd_params();
    if(cfg){
        try{
            Snd2Json snd2json(cfg, "snd_in_cfg_t");
            _json_snd_params["ins"][ch]["cfg"] = snd2json.snd_in_cfg_to_json(cfg, index);
        }catch(json::exception &e){
            log_error("ConfigServer::setSndInParams(): %s", e.what());
        }
    }

    set_json_snd_params(_json_snd_params);

//    cout << _json_snd_params.dump(4) << endl;
}

void ConfigServer::setSndOutCapabs(int ch, uint8_t* &capabs){
    json _json_snd_params = get_json_snd_params();
    if(capabs){
        try{
            Snd2Json snd2json(capabs, "snd_out_capab_t");
            json new_capabs = snd2json.snd_out_capab_to_json(capabs);
            _json_snd_params["outs"][ch]["capabs"] = new_capabs;
        }catch(json::exception &e){
            log_error("ConfigServer::setSndOutCapabs(): %s", e.what());
        }
    }

    set_json_snd_params(_json_snd_params);

//    cout << _json_snd_params.dump(4) << endl;
}

void ConfigServer::setSndOutParams(int ch, uint8_t* &cfg, int index){
    json _json_snd_params = get_json_snd_params();
    if(cfg){
        try{
            Snd2Json snd2json(cfg, "snd_out_cfg_t");
            json new_cfg = snd2json.snd_out_cfg_to_json(cfg, index);
            new_cfg["eq"]["bypass"] = _json_snd_params["outs"][ch]["cfg"]["eq"]["bypass"];
            _json_snd_params["outs"][ch]["cfg"] = new_cfg;
        }catch(json::exception &e){
            log_error("ConfigServer::setSndOutParams(): %s", e.what());
        }
    }
    
    set_json_snd_params(_json_snd_params);
    
//    cout << _json_snd_params.dump(4) << endl;
}

void ConfigServer::setChannelActivity(int ch, ch_mask_t active){
    if(getChannelActive(ch) != active)
        state_changed = true;
    activeChannels[ch] = active;
    channelsTime[ch] = active ? now_ms() : 0;

    if(!active){
        setCTOutsInput(ch, swp_table_def.sw_table[ch]);
    }

//    if(changed){
////        send_ws("state_page");
//        send_ws_input_state(ch);
//        send_ws("pd");
//    }
}

ch_mask_t ConfigServer::getChannelActive(int ch){
//    lock_guard<mutex> lock(active_ch_mutex);

    return activeChannels[ch];
}

bool ConfigServer::isChannelTimedOut(int ch)
{
    return getChannelActive(ch) && (now_ms() - channelsTime[ch]) > timeout_iface;
}

void ConfigServer::activeChannelsTimeout()
{
    bool changed = false;

    for(int i=0; i<MAX_CHANNELS; i++){
        if(isChannelTimedOut(i)){
            changed = true;

            setChannelActivity(i, 0);
        }
    }
    if(state_changed){
        send_ws("state_page");
        send_ws("pd");
        state_changed = false;
    }
}

void ConfigServer::saveBoards(){
    string name = string(SETTS_PATH)+"/boards.json";
    
    json arr = json::array();
    for(auto b : pingBoardMap){
        board_def_t brd = b.second;
        arr.push_back(json::object({
            {"addr", (int)brd.addr},
            {"type", (int)brd.type},
            {"ip", brd.ip_addr}
        }));
    }
    
    ofstream f;
    f.open(name);
    if(f.good()){
        f << arr.dump(4);
        f.close();
    }
}

void ConfigServer::setBoardPing(uint8_t addr, uint8_t dev_type, string ip){
    
    unique_lock<shared_mutex> lock(boardPingMtx);
    
    if(pingBoardMap.find(addr) == pingBoardMap.end()){
        cout << "Board found: id=" << dec << (int)addr << ", type=" << (int)dev_type << endl;
        pingBoardMap[addr] = {addr, dev_type, ip};
        saveBoards();
    }
    
    if(boardPingTime[addr].active){
        //Убрать сигнал
        alarm_item_t alarm;
        alarm.alarm_code = GEN_CALL_NO_ANSWER | CLEAR_ALARM;
        alarm.param = 0;
        setChannelAlarms(1, &alarm, addr, (blocks_t)dev_type);
    }
    
    boardPingTime[addr] = {
        false,
        chrono::steady_clock::now()
    };
}

void ConfigServer::systemPing(string ip, int secs){
//            cout << "Sys ping " << brd.ip_addr << endl;
    
    char cmd[256];
    sprintf(cmd, "/usr/bin/ping %s -w%d -q &", ip.c_str(), secs);// > null
    system(cmd);
    
//            if(system(cmd) < 0){
//                cerr << "Run ping error: " << strerror(errno);
//            }
            
//            pid_t pid = fork();
//            if(pid == 0){
//                if(execl("/usr/bin/ping", "ping", brd.ip_addr.c_str(), "-c10", 0) < 0){//
//                    cerr << "execl error: " << strerror(errno) << endl;
//                }
//                exit(0);
//            }else if(pid < 0){
//                cerr << "Fork error: " << strerror(errno) << endl;
//            }
}

void ConfigServer::checkBoardPings(){
    auto tm = chrono::steady_clock::now();
    
    int sysPingCnt = chrono::duration_cast<chrono::seconds>(ping_period).count()-1;
    if(sysPingCnt < 1)
        sysPingCnt = 1;
    if((tm - lastPingTm) > ping_period){
//        cout<<"PING"<<endl;
        lastPingTm = tm;
        server.sendCommand(nullptr, 0, PACK_GEN_CALL, PING);
    }
    
    if(!startPing){//(tm - lastSysPingTm) > 60s
        lastSysPingTm = tm;
        startPing = true;
        
        for(auto b : pingBoardMap){
            board_def_t brd = b.second;
            if(brd.ip_addr.empty())
                continue;
            
            systemPing(brd.ip_addr, 5);
        }
    }
    
    unique_lock<shared_mutex> lock(boardPingMtx);
    
    for(auto &b : pingBoardMap){
        board_def_t brd = b.second;
        auto dtm = chrono::duration_cast<chrono::seconds>(tm - boardPingTime[brd.addr].time);
//        cout<<"CHECK PING: "<<dec<<dtm.count()<<"-"<<ping_timeout.count()<<endl;
        
        if(dtm >= ping_timeout && !boardPingTime[brd.addr].active){
            boardPingTime[brd.addr].active = true;
            
//            cout << "Set ping alarm for " << hex << (int)addr << dec << endl;
            
            //Установить сигнал
            alarm_item_t alarm;
            alarm.alarm_code = GEN_CALL_NO_ANSWER | SET_ALARM;
            alarm.param = 0;
            setChannelAlarms(1, &alarm, brd.addr, (blocks_t)brd.type);
        }
        
        if(boardPingTime[brd.addr].active && (tm - boardPingTime[brd.addr].lastSysPingTm)>10s && !brd.ip_addr.empty()){
//            cout << "PING " << brd.ip_addr << endl;
            boardPingTime[brd.addr].lastSysPingTm = tm;
            systemPing(brd.ip_addr, 9);
        }
    }
}

int ConfigServer::getStoreDays(){
    json _settings = get_settings();
    if(_settings["rec"].contains("store_days") && _settings["rec"]["store_days"].is_number()){
        return _settings["rec"]["store_days"];
    }else{
        return 2;
    }
}

vector<string> ConfigServer::getCUSTargets(){
    vector<string> targets;
    
    json _settings = get_settings();
    
    try{
        if(_settings.contains("outs")){
            for(json out : _settings["outs"]){
                if(out.contains("target")){
                    targets.push_back(out["target"]);
                }else{
                    targets.push_back("");
                }
            }
        }
    }catch(json::exception &e){
        log_error("ConfigServer::getCUSTargets(): %s", e.what());
    }
    
//    string name = string(SETTS_PATH)+"/cus_targets.json";
//    ifstream file;
//    file.open(name);
//    if(file.is_open()){
//        json jTgts = json::parse(file, nullptr, false);
//        
//        if(jTgts.is_array()){
//            for(int i=0; i<jTgts.size(); i++){
//                targets.push_back(jTgts[i]);
//            }
//        }
//        
//        file.close();
//    }
    
    return targets;
}

json ConfigServer::get_json_snd_params(){
    shared_lock<shared_mutex> lock(json_snd_params_mutex);
    return json_snd_params;
}

void ConfigServer::set_json_snd_params(json value){
    unique_lock<shared_mutex> lock(json_snd_params_mutex);
    json_snd_params = value;
}

json ConfigServer::get_settings(){
    shared_lock<shared_mutex> lock(settings_mutex);
    return settings;
}

void ConfigServer::set_settings(json value){
    unique_lock<shared_mutex> lock(settings_mutex);
    settings = value;
    
//    cout << settings.dump(4) << endl;
    
    if(settings["rec"]["enable"].is_boolean())
        rec_enable = settings["rec"]["enable"];
    
    if(settings["rec"].contains("min_free") && settings["rec"]["min_free"].is_number_integer()){
        min_disk_free = settings["rec"]["min_free"];
    }else{
        min_disk_free = 300;
    }
    cout << "min_disk_free" << " = " << dec << min_disk_free << endl;
    
    insRecMask = 0;
    for(int i=0; i<settings["ins_cnt"]; i++){
        if(settings["ins"][i].contains("rec") && settings["ins"][i]["rec"].is_boolean()){
            bool inEnbl = settings["ins"][i]["rec"];
            if(inEnbl){
                insRecMask |= (1ULL<<i);
            }
        }else{
            insRecMask |= (1ULL<<i);
        }
    }
    cout << "insRecMask = " << hex << insRecMask << dec << endl;
    
    if(settings["events"]["log_period"].is_number_integer())
        events_log_period = settings["events"]["log_period"];
            
    if(settings["bks"].contains("ping_period") && settings["bks"]["ping_period"].is_number_integer()){
        ping_period = chrono::seconds((int)settings["bks"]["ping_period"]);
    }

    if(settings["bks"].contains("ping_timeout") && settings["bks"]["ping_timeout"].is_number_integer()){
        ping_timeout = chrono::seconds((int)settings["bks"]["ping_timeout"]);
    }
}

json ConfigServer::get_comm_table(){
    shared_lock<shared_mutex> lock(comm_table_mutex);
//    cout << "COMMTBL: get_comm_table" << endl;
//    cout << comm_table["sw_table"][0].dump(4) << endl;
    return comm_table;
}

void ConfigServer::set_comm_table(json value){
    unique_lock<shared_mutex> lock(comm_table_mutex);
    comm_table = value;
//    cout << "COMMTBL: set_comm_table" << endl;
//    cout << comm_table["sw_table"][0].dump(4) << endl;
}

json ConfigServer::get_comm_table_ch(int ch){
    try{
        shared_lock<shared_mutex> lock(comm_table_mutex);
        return comm_table["sw_table"][ch];
    }catch(json::exception &e){
        log_error("ConfigServer::get_comm_table_ch: %s", e.what());
        return json();
    }
}

void ConfigServer::error(string mes, bool _exit){
    log_error("Network settings: %s", mes.c_str());
    if(_exit)
        exit(EXIT_FAILURE);
}

json ConfigServer::backup(){
    json j_comm_table;
    ctToJSON(j_comm_table);
    
    json j_snd_params = get_json_snd_params();
    
    json brus_array = json::array();
    for(auto p : brus){
        brus_array.push_back(json::object({
            {"id", p.first},
            {"bru", p.second}
        }));
    }
    
    json backup =  json::object({
        {"settings", get_settings()},
        {"alarms", getAlarms()},
        {"comm_table", j_comm_table},
        {"snd_params", j_snd_params},
        {"brus", brus_array}
    });
    
#ifdef CONFIG_DEBUG
    cout << "Backup: " << backup.dump(4) << endl;
#endif
    
    return backup;
}

void ConfigServer::restore(json js){
    if(js.contains("settings")){
        set_settings(js["settings"]);
        saveToFile();
    }
    
    if(js.contains("alarms")){
        setAlarms(js["alarms"]);
    }
    
    if(js.contains("comm_table")){
        json _comm_table = js["comm_table"];
        set_comm_table(_comm_table);
        bool result = ctFromJSON(_comm_table);

        addCTHystory(_comm_table);

        if(result){
            saveToFile();

            swp_table_t _swp_table = getSWPTable();
            server.sendCommand(reinterpret_cast<uint8_t*>(&_swp_table), sizeof(swp_table_t), PACK_CFG, EXT_SWT);
        }
    }
    
    if(js.contains("snd_params")){
        restore_snd_params(js, "ins");
        restore_snd_params(js, "outs");
    }
    
    if(js.contains("brus")){
        json j_brus = js["brus"];
        if(j_brus.is_array()){
            brus.clear();
            for(json j : j_brus){
                brus[j["id"]] = j["bru"];
            }
        }
    }
}

void ConfigServer::restore_snd_params(json js, string type){
    try{
        if(js["snd_params"].contains(type)){
            json ins = js["snd_params"][type];
            if(!ins.is_array())
                return;

            for(size_t i=0; i<ins.size(); i++){
                if(ins[i].contains("cfg")){
                    json j_send = json::object({
                        {"channel_type", type},
                        {"channel", i},
                        {"snd_params", ins[i]["cfg"]},
                    });
                    sendSndParams(j_send);
                }
            }
        }
    }catch(json::exception &e){
        log_error("ConfigServer::restore_snd_params(): %s", e.what());
    }
}

void ConfigServer::loadAlarms()
{
    ifstream f;
    f.open(string(SETTS_PATH)+"/alarms.json");
    if(f.good()){
        unique_lock<shared_mutex> lock(alarms_mutex);

        alarms_defs = json::parse(f, nullptr, false);
//        alarms_strs.clear();
//        while(!f.eof()){
//            string str;
//            std::getline(f, str, '\n');
//            alarms_strs.push_back(str);
//        }

        f.close();
    }
    
    if(alarms_defs.size() < 14){
        alarms_defs.push_back(json::object(
            {
                {"message", "<b>Блок <BRD_TYPE> (<SENDER>,<BOARD>):</b> Нет ответа на периодический опрос"},
                {"type", 2}
            }
        ));
        setAlarms(alarms_defs);
    }
}

void ConfigServer::setAlarms(json js)
{
    ofstream f;
    f.open(string(SETTS_PATH)+"/alarms.json");

    {
        unique_lock<shared_mutex> lock(alarms_mutex);
        alarms_defs = js;

        if(f.good()){
            f << alarms_defs.dump(4);
            f.close();
        }
    }
    
    dynamic_cast<PacketParserCfg*>(PacketParser::get_instance(PACK_CFG))->set_critical_alarms(getAlarmsBits());
}

json ConfigServer::getAlarms(){
    shared_lock<shared_mutex> lock(alarms_mutex);
    return alarms_defs;
}

json ConfigServer::getAlarmsStr(int n)
{
    shared_lock<shared_mutex> lock(alarms_mutex);

    if(!alarms_defs.is_array() || n >= static_cast<int>(alarms_defs.size()))
        return string("Ошибка ")+to_string(n);
    else
        return alarms_defs[n];
}

uint32_t ConfigServer::getAlarmsBits(){
    uint32_t bits = 0;
    auto j_alarms = getAlarms();
    
    if(j_alarms.is_array()){
        for(int i=0; i<j_alarms.size(); i++){
            auto alrm = j_alarms[i];
            if(alrm.is_object() && alrm.contains("type") && alrm["type"].is_number_integer() && alrm["type"] == EVT_DANGER){
                bits |= (1<<i);
            }
        }
    }
    
//    cout << "Alarm bits " << hex << bits << dec <<endl;
    
    return bits;
}

bool ConfigServer::inRecEnabled(int i){
    bool recEnbl = rec_enable && (insRecMask & (1ULL << i));
//    cout << "Rec: " << i << "\t" << insRecMask << "\t" << recEnbl << endl;
    return recEnbl;
}

void ConfigServer::send_ws(string type){
//    if(ws_server)
//        ws_server->send(cmd);
    
    try{
        json js = json::object();
        if(type == "state_page"){
            json arr;
            if(ctToJSON(arr)){
                js["comm_table"] = arr;
            }
            js["events"] = eventsToJson();
        }else if(type == "pd"){
            js["outs"] = getActiveOuts();
        }
        
        string str = js.dump(4);
        send_ws(type, str);
        
    }catch(json::exception &e){
        log_error("ConfigServer::send_ws(): %s", e.what());
    }
}

void ConfigServer::send_ws(string type, string data){
    if(ws_server)
        ws_server->send(type, data);
}

void ConfigServer::send_ws_input_state(int in){
    try{
        json js = json::object();
        js["in_state"] = json::object({
                                          {"in", in},
                                          {"outs", getCTOutsInput(in)},
                                          {"active", getChannelActive(in)}
                                      });
        send_ws("state_page", js.dump());
    }catch(json::exception &e){
        log_error("ConfigServer::send_ws_input_state(): %s", e.what());
    }
}

void ConfigServer::setBRUCfg(uint8_t id, bru_cfg_t* cfg, int sz){
    json js = BRU2JSON().bru_cfg_to_json(cfg, sz);
    js["id"] = id;
    setBRUCfg(id, js);
}

void ConfigServer::setBRUCfg(uint8_t id, json cfg){
    unique_lock<shared_mutex> lock(brus_mutex);
    brus[id] = cfg;
}

map<uint8_t, json> ConfigServer::get_brus(){
    shared_lock<shared_mutex> lock(brus_mutex);
    return brus;
}

json ConfigServer::getBRUsJson(){
    json array = json::array();
    
    for(auto p : get_brus()){
        array.push_back(p.second);
    }
    
    return array;
}

string ConfigServer::select_if(string type, string def, string &bc_addr){
    printf("Выберите интерфейс для %s:\n", type.c_str());

    vector<iface_t> ifs;
    struct ifaddrs *addrs,*tmp;

    getifaddrs(&addrs);
    tmp = addrs;

    while (tmp)
    {
        if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_INET){
            iface_t iface = {
                string(tmp->ifa_name),
                string(inet_ntoa(reinterpret_cast<sockaddr_in*>(tmp->ifa_addr)->sin_addr)),
                string(inet_ntoa(reinterpret_cast<sockaddr_in*>(tmp->ifa_ifu.ifu_broadaddr)->sin_addr))

            };
            ifs.push_back(iface);
        }

        tmp = tmp->ifa_next;
    }

    freeifaddrs(addrs);

    for(int i=0; i<static_cast<int>(ifs.size()); i++){
        printf("%d: %s\tip:%s\tbroadcast:%s\n", (i+1), ifs[i].name.c_str(), ifs[i].addr.c_str(), ifs[i].bc_addr.c_str());//
    }
    printf("[%s]> ", def.c_str());

    string ans;
    getline(cin, ans);
    if(ans.empty())
        return "";

    int n = stoi(ans);
    if(n > 0 && n <= static_cast<int>(ifs.size())){
        bc_addr = ifs[n-1].bc_addr;
        return ifs[n-1].name;
    }else{
        bc_addr = "";
        return "";
    }
}

void ConfigServer::enter_str_param(string descr, string& value){
    printf("\n%s [%s]>", descr.c_str(), value.c_str());
    string ans;
    getline(cin, ans);
    if(!ans.empty()){
        value = ans;
    }
}

void ConfigServer::select_ifaces()
{
    loadFromFile();

    string bc;
    bool chg = false;
    string _if = select_if("внутренней сети", network["in_iface"], bc);
    if(!_if.empty()){
        printf("Выбран интерфейс - %s, broadcast = %s\n\n", _if.c_str(), bc.c_str());
        network["in_iface"] = _if;
        network["in_broadcast"] = bc;
        chg = true;
    }

    _if = select_if("внешней сети", network["out_iface"], bc);
    if(!_if.empty()){
        printf("Выбран интерфейс - %s\n\n", _if.c_str());
        network["out_iface"] = _if;
        chg = true;
    }
    
    string mcast = network["udp-multicast"];
    enter_str_param("Мультикаст группа управляющего интерфейса СЦО", mcast);
    network["udp-multicast"] = mcast;

    printf("Настройка сетевых интерфейсов завершена.\n");

    printf("%s\n", network.dump(4).c_str());

    if(chg){
        saveNetwork();
        printf("Настройки сохранены.\n");
    }
}

json ConfigServer::getBRUCfg(uint8_t id){
    shared_lock<shared_mutex> lock(brus_mutex);
    return brus[id];
}

void ConfigServer::getBRUCfg(uint8_t id, bru_cfg_t* cfg){
    BRU2JSON().json_to_bru_cfg(getBRUCfg(id), cfg, 2048);
}

void ConfigServer::bru_measure(int bru_id){
    json bru = getBRUCfg(bru_id);
    if(bru.contains("line_min") && bru.contains("line_max")){
        measure_async = std::async(std::launch::async, [](ConfigServer* _this, uint8_t bru_id){
            uint8_t buf[2048];
            bru_cfg_t* bru_cfg = reinterpret_cast<bru_cfg_t*>(buf);
            _this->getBRUCfg(bru_id, bru_cfg);
            
            json j_measure_state = json::object({
                {"bru", bru_id},
                {"line", 0},
                {"state", "process"},
                {"result", "failed"}
            });
            
//            server.sendCommand(nullptr, 0, PACK_BRU_CFG, BRU_TEST_MODE_START, 1, bru_id);
            auto measure_timeout_future = dynamic_cast<PacketParserBRU*>(PacketParser::get_instance(PACK_BRU_CFG))->start_test_mode(bru_id);
            auto status = measure_timeout_future.wait_for(chrono::milliseconds(300));
            if(status == future_status::timeout){
//                cout << "NOT RECEIVED BRU_TEST_MODE_START" << endl;
                for(uint8_t i=bru_cfg->line_min; i<=bru_cfg->line_max; i++){
                    j_measure_state["line"] = i;
                    j_measure_state["state"] = "timeout";
                    _this->send_ws("measure", j_measure_state.dump());
                }
                j_measure_state["line"] = -1;
                _this->send_ws("measure", j_measure_state.dump());
                return;
            }
//            cout << "RECEIVED BRU_TEST_MODE_START" << endl;
            
            uint32_t measure_timeout = measure_timeout_future.get();
            
            _this->bru_measure_active = true;
            
            int complete = 0;
            for(uint8_t line_id=bru_cfg->line_min; line_id<=bru_cfg->line_max; line_id++){
                if(!_this->bru_measure_active)
                    break;
                
                uint8_t line_i = line_id - bru_cfg->line_min;
                j_measure_state["line"] = line_id;
                j_measure_state["state"] = "process";
                
                for(int c=0; c<3; c++){
                    if(!_this->bru_measure_active)
                        break;

//                    cout << "BRU_LINE_MEASURE " << (int)line_i << " - " << c << endl;
                    
                    auto line_measure_future = dynamic_cast<PacketParserBRU*>(PacketParser::get_instance(PACK_BRU_CFG))->line_measure(bru_id, line_id);
                    auto status = line_measure_future.wait_for(chrono::milliseconds(measure_timeout));

                    if(status == future_status::timeout){
                        j_measure_state["state"] = "timeout";
                    }else{
                        bru_measure_t _bru_meas = line_measure_future.get();
                    
                        if(_bru_meas.lstate == LINE_ERR_MEAS){
                            j_measure_state["state"] = "error";
                        }else{
                            j_measure_state["state"] = "ok";

//                            cout << "RECEIVED BRU_LINE_MEASURE" << endl;

                            bru_cfg->line[line_i].ref_value_ohm = _bru_meas.val;
                            bru_cfg->line[line_i].low_trshld_ohm =  (_bru_meas.val * (float)(100 - bru_cfg->max_meas_tolerance_proc)) / 100.f;
                            bru_cfg->line[line_i].high_trshld_ohm = (_bru_meas.val * (float)(100 + bru_cfg->max_meas_tolerance_proc)) / 100.f;
                            bru_cfg->line[line_i].shortOhm =  (_bru_meas.val * (float)(100 - bru_cfg->max_devi_tolerance_proc)) / 100.f;
                            bru_cfg->line[line_i].openOhm = (_bru_meas.val * (float)(100 + bru_cfg->max_devi_tolerance_proc)) / 100.f;
                            bru_cfg->line[line_i].state = _bru_meas.lstate;
                            bru_cfg->line[line_i].range = _bru_meas.mode;

                            complete++;

                            break;
                        }
                    } 
                }
                
                _this->send_ws("measure", j_measure_state.dump());
            }
            
            server.sendCommand(nullptr, 0, PACK_BRU_CFG, BRU_TEST_MODE_STOP, 1, bru_id);
            
            _this->setBRUCfg(bru_id, bru_cfg, 2048);
//            server.sendCommand(reinterpret_cast<uint8_t*>(bru_cfg), BRU_CFG_SIZE(bru_cfg), PACK_BRU_CFG, BRU_SAVE_CFG, 3, bru_id);
            
            j_measure_state["line"] = -1;
            if(complete == (bru_cfg->line_max - bru_cfg->line_min + 1))
                j_measure_state["result"] = "complete";
            else if(complete == 0)
                j_measure_state["result"] = "failed";
            else
                j_measure_state["result"] = "partial";
            _this->send_ws("measure", j_measure_state.dump());
        }, this, bru_id);
        
//        as.get();
        
//        cout << "bru_measure END" << endl;
        
    }
}

int ConfigServer::start_clip_process(uint8_t in, ch_mask_t outs, string fileName, bool autorew){
    auto pid = get_player_pid(in);
    if(pid){
        printf("Already played for %d with PID=%d\n", (int)in, pid);
        return -1;
    }

//    ifstream pidfile("/run/ago18player"+to_string(in));
//    if(pidfile.good())
//        return -1;

//    if(system("killall -s9 ago18player") == -1){//execl("kill", "`pidof ago18player`")
//        cerr << red << strerror(errno) << clrst << endl;
//    }
    
//    printf("Start clip %02X %X %s %s\n", (uint32_t)in, outs, autorew?"cycle":"once", fileName.c_str());
    
    json _setts = get_settings();
    int packs = _setts.contains("player") && _setts["player"].contains("packs_at_time") ? (int)_setts["player"]["packs_at_time"] : 6;

    string ip = getNetParam("in_broadcast", "");
    string cmd = "ago18player " + to_string(server_addr) + " " + to_string(in) + " " + to_string(outs) + " " + ip + " \""+fileName+"\" "+to_string(packs);
    if(autorew){
        cmd += " 1";
    }
    cmd += " &";
#ifdef CONFIG_DEBUG
    printf("%s\n", cmd.c_str());
#endif
    printf("%s\n", cmd.c_str());
    if(system(cmd.c_str()) == -1){//execl("/usr/sbin/ago18player", to_string(server_addr).c_str(), to_string(in).c_str(), to_string(outs).c_str(), ip.c_str(), ("\""+fileName+"\"").c_str(), NULL)
//        cerr << "Run player: " << red << strerror(errno) << clrst << endl;
    }
    return 0;
}

int ConfigServer::get_player_pid(int in){
    string cmd = "ps -xo pid,args | grep 'ago18player "+to_string(server_addr)+" "+to_string(in)+" '";
//    printf("%s\n",cmd.c_str());
    
    FILE* pr = popen(cmd.c_str(), "r");
    if(pr){
        while(!feof(pr)){
            char str[1000];
            if(fgets(str, 1000, pr) && !strstr(str, "grep")){
//                printf("%s\n", str);
//                char* p = strstr(str, " ");
//                *p = 0;
                int pid = atoi(str);
//                printf("PID=%d\n", pid);
                pclose(pr);
                return pid;
            }
        }
        pclose(pr);
    }

    return 0;
}

void ConfigServer::stop_clip_process(int in){
    int pid = get_player_pid(in);
    if(pid){
        string cmd = "kill "+to_string(pid);
//        printf("%s\n", cmd.c_str());
        if(system(cmd.c_str()) == -1){//execl("kill", "`pidof ago18player`")
            cerr << red << strerror(errno) << clrst << endl;
        }
    }

//    ifstream pidfile("/run/ago18player"+to_string(in));
//    if(pidfile.good()){
//        int pid = 0;
//        pidfile >> pid;
//        pidfile.close();

//        if(pid){
//            string cmd = "kill "+to_string(pid);
//            printf("%s\n", cmd.c_str());
//            if(system(cmd.c_str()) == -1){//execl("kill", "`pidof ago18player`")
//                cerr << red << strerror(errno) << clrst << endl;
//            }
//        }
//    }

}

void ConfigServer::stop_clip_process(){
    string cmd = "killall ago18player";
    if(system(cmd.c_str()) == -1){
        cerr << red << strerror(errno) << clrst << endl;
    }
}

void ConfigServer::start_clip_for_signal(int signal, bool autorew){
    namespace bfs = std::experimental::filesystem;

    int i=0;
    bfs::directory_iterator itt(bfs::path("/var/www/html/AGO18Web/clips"));
    for(; itt != bfs::directory_iterator(); itt++){
        bfs::path path = itt->path();
        if(bfs::is_regular_file(path) && path.extension().generic_string()!=".json"){
            json def = get_clip_def(path.generic_string());
            try{
                cout << def.dump() << endl;
                if(def.contains("signal") && def.contains("in") && def["signal"] == signal){
                    uint8_t in = def["in"];
                    ch_mask_t outs = getCTOutsInput(in);
                    start_clip_process(in, outs, path.generic_string(), autorew);
                }
            }catch(json::exception &e){
                cerr << red << e.what() << clrst << endl;
            }
        }
    }
}

json ConfigServer::get_clip_def(string name){
    try{
        ifstream file;
        file.open(name+".json");
        if(file.is_open()){
            auto js = json::parse(file, nullptr, false);
            file.close();
            return js;
        }
        return json::object({
                                {"signal", -1},
                                {"in", 0}
                            });
    }catch(json::exception &e){
        cerr << red << "ConfigServer::get_clip_def: " << e.what() << clrst << endl;
        return json::object({
                                {"signal", -1},
                                {"in", 0}
                            });
    }
}

long ConfigServer::getAvailableSpace(const char* path)
{
  struct statvfs stat;

  if (statvfs(path, &stat) != 0) {
    // error happens, just quits here
    return -1;
  }

  // the available size is f_bsize * f_bavail
  return stat.f_bsize * stat.f_bavail;
}

void config_thread_func(ConfigServer* __this){
    __this->run();
}

std::string exec_res(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

