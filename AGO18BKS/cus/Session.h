/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Session.h
 * Author: user
 *
 * Created on 20 сентября 2018 г., 11:41
 */

#ifndef SESSION_H
#define SESSION_H

#include <string>
#include <set>
#include "MyThread.h"
#include <mpg123.h>
#include "MyPlayer.h"

#define PCM_SZ 10240
#define SESSION_TIMEOUT 10

using namespace std;

struct shared_buf_t{
    //targets_t targets;
    bool active;
};

typedef uint32_t targets_t;

class Session : public MyThread {
public:
    Session();
    Session(uint32_t id, string url, string iface);
    Session(const Session&);
    virtual ~Session();
    
    virtual void exec();
    
    void log(string str, bool error=false);
    void init();
    void close_me();
//    bool start();
//    void stop();
    void addTarget(int tgt);
    void removeTarget(int tgt);
    void addTargets(targets_t tgt_mask);
    void removeTargets(targets_t tgt_mask);
    targets_t getTargets();
private:
    uint32_t id;
    string url;
    string iface;
    string host; 
    uint16_t port;
    mpg123_handle* mh;
    long rate;
    int channels;
    MyPlayer player;
    uint32_t start_tm = 0;
    //vector<string> targetsList;
    uint16_t targetsMask;
//    shared_buf_t *shared_buf;
    timeval recv_tm;
//    pid_t pid;
    int sock = 0;
    string name;
    uint64_t tm_mp3_first = 0;
    uint64_t tm_mp3_play = 0;
    uint32_t last_seq_n = 0;
    
    virtual void sendData(string data);
    virtual void sendDataTo(string data, sockaddr_in addr);
    virtual void onTime();
    virtual void onRead();
    virtual void run();
    bool mpgError(string pref, int error);
    bool parseUrl(string url, string &host, uint16_t &port);
//    int findTargetId(string tgt);
};

#endif /* SESSION_H */

