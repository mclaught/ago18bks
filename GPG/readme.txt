Скопировать каталог private/.gnupg в /root на компе разработчика или импортировать private.asc (gpg --import private.asc).
Файл public/.gnupg предназначен для сервера БКС.

Подписать файл install.sh.
Linux: 
	tar -c * | gpg -s > upgrade.tar.sig
	Пароль: 921elcom9103599
Windows:
	gpg4win.Kleopatra
	Выбрать несколько файлов (при выборе одного файла создается только файл подписи). Включить только опцию "Сертификат подписи".
	
Проверка подписи:
	gpg --verify install.sh.sig
	
Распаковка:
	gpg --decrypt | tar -xv  -C /tmp
