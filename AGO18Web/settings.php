<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?2" />
        
        <script src="js/jquery-3.4.1.min.js"></script>
        <!--<script src="js/popper.min.js"></script>-->
        <!--<script src="js/bootstrap.bundle.js"></script>-->
        
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        $nb_add = "<button class='btn btn-outline-light my-2 my-sm-0' id='btn-save'>Сохранить</button>";
        include "navbar.php";
        include "alert.html";
        include 'version.php';
        
        function get_timezones(){
            $res = "";
            $files = scandir('/usr/share/zoneinfo/Europe');
            foreach($files as $file){
                if($file[0] != '.'){
                    $res .= "<option>Europe/$file</option>";
                }
            }
            $files = scandir('/usr/share/zoneinfo/Asia');
            foreach($files as $file){
                if($file[0] != '.'){
                    $res .= "<option>Asia/$file</option>";
                }
            }
            return $res;
        }

        //Load network interface setts
        $netText = file_get_contents("/etc/network/interfaces.d/eth0");
        $lines = explode("\n", $netText);
        $netDHCP = 'dhcp';
        $netIP = '';
        $netMask = '';
        $netGate = '';
        $netLimit = '';
        foreach ($lines as $line) {
            if(strpos($line, 'static')){
                $netDHCP = 'static';
            }
            if(strpos($line, 'address')){
                $netIP = substr($line, strpos($line, 'address')+8);
            }
            if(strpos($line, 'netmask')){
                $netMask = substr($line, strpos($line, 'netmask')+8);
            }
            if(strpos($line, 'gateway')){
                $netGate = substr($line, strpos($line, 'gateway')+8);
            }
            if(strpos($line, '100baseTx')){
                $netLimit = 'checked';
            }
        }
        
        //Load network.json setts
        $netJsText = file_get_contents('/etc/ago18bks/network.json');
        $netJs = json_decode($netJsText);
        $cus_enable = $netJs->cus_enable==1 ? 'checked' : '';
        $udp_multicast = get_object_vars($netJs)['udp-multicast'];
        ?>
        
        <div class="container">
            <br>
            <h1><img src="img/settings.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Общие настройки</h1>
            <br>
            
            <div class="accordion" id="accordionSetts">
                
                <div class="card shadow">
                    <div class="card-header" id="headingDaemon">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseDaemon" aria-expanded="true" aria-controls="collapseDaemon">
                                Служба
                            </button>
                        </h5>
                    </div>
                    <div id="collapseDaemon" class="collapse show" aria-labelledby="headingDaemon" data-parent="#accordionSetts">
                        <div class="card-body m-3">
                            <div class="d-flex flex-row justify-content-between">
                                <h1><span id="daemon-state" class="flex-grow-1 alert alert-secondary px-5" style="display: none;">Нет связи</span></h1>
                                <!--<div class="col-1"></div>-->
                                <!--<div class="btn-group col-4" role="group" aria-label="Basic example">-->
                                    <button type="button" class="btn btn-success btn-daemon" id="btn-daemon-start" style="display: none; width: 250px;">Старт</button>
                                    <button type="button" class="btn btn-danger btn-daemon" id="btn-daemon-stop" style="display: none; width: 250px;">Стоп</button>
                                    <!--<button type="button" class="btn btn-warning">Перезапуск</button>-->
                                <!--</div>-->                        
                                <!--<div class="col-1"></div>-->
                                <button type="button" class="btn btn-dark btn-daemon" id="btn-reboot" style="display: none; width: 200px;">Перезагрузка<br>сервера</button>
                            </div>
                            <!--<div id="server-alert" class="alert alert-danger hide mt-3" role="alert">Нет связи с сервером</div>-->
<!--                            <div class="row mb-3">
                                <div class="col"></div>
                                <button type="button" class="btn btn-primary col-4" id="btn-reboot">Перезагрузка</button>
                            </div>-->
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingCommon">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseCommon" aria-expanded="true" aria-controls="collapseCommon">
                                Объект
                            </button>
                        </h5>
                    </div>
                    <div id="collapseCommon" class="collapse " aria-labelledby="headingCommon" data-parent="#accordionSetts">
                        <div class="card-body">
                            <div class="m-3">
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Название объекта</div>
                                    </div>
                                    <input type="text" class="form-control" id="objectName" placeholder="Название объекта">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Логический адрес сервера</div>
                                    </div>
                                    <input type="text" class="form-control" id="serverAddr" placeholder="Логический адрес сервера">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Вход для трансляций ЦУС</div>
                                    </div>
                                    <input type="text" class="form-control" id="cusIn" placeholder="Вход для трансляций ЦУС">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Смещение ID усилителя</div>
                                    </div>
                                    <input type="text" class="form-control" id="id-offset" placeholder="Смещение ID усилителя">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Смещение ID БРУ</div>
                                    </div>
                                    <input type="text" class="form-control" id="bru-offset" placeholder="Смещение ID БРУ">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Кол-во отображаемых входов</div>
                                    </div>
                                    <input type="text" class="form-control" id="ins-cnt" placeholder="Количество входов">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Кол-во отображаемых выходов</div>
                                    </div>
                                    <input type="text" class="form-control" id="outs-cnt" placeholder="Количество выходов">
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Период опроса плат</div>
                                    </div>
                                    <input type="text" class="form-control" id="ping-period" placeholder="Период опроса плат">
                                    <div class="input-group-append"><span class="input-group-text">сек</span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Таймаут опроса плат</div>
                                    </div>
                                    <input type="text" class="form-control" id="ping-timeout" placeholder="Таймаут опроса плат">
                                    <div class="input-group-append"><span class="input-group-text">сек</span></div>
                                </div>
                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Список оборудования</div>
                                    </div>
                                    <textarea type="text" class="form-control" id="boardsList" rows="1" readonly></textarea>
                                    <div class="input-group-append">
                                        <!--<span class="input-group-text"></span>-->
                                        <button class="btn btn-outline-dark" id="clearBoards">Обновить</button>
                                    </div>
                                </div>
                                <!--<button class="btn btn-outline-danger" id="clearBoards">Очистить список оборудования</button>-->
<!--                                <div class="input-group mb-2 w-75">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="width:250px;">Список оборудования</div>
                                    </div>
                                    <button class="btn btn-outline-danger" id="clearBoards">Очистить</button>
                                    <div class="input-group-append"><span class="input-group-text"></span></div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingChannels">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseChannels" aria-expanded="true" aria-controls="collapseChannels">
                                Каналы
                            </button>
                        </h5>
                    </div>
                    <div id="collapseChannels" class="collapse" aria-labelledby="headingChannels" data-parent="#accordionSetts">
                        <div class="card-body">
                            <div class="row m-3">
                                <div class="col-4"><div class="mr-3" id="inChannels"></div></div>
                                <div class="col-4"><div class="mr-3" id="outChannels"></div></div>
                                <div class="col-4"><div id="targets"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingRec">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseRec" aria-expanded="true" aria-controls="collapseRec">
                                Параметры записи
                            </button>
                        </h5>
                    </div>
                    <div id="collapseRec" class="collapse" aria-labelledby="headingRec" data-parent="#accordionSetts">
                        <div class="card-body row">
                            <div class="col-5">
                                <form>
                                    <div class="form-group">
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" id="recEnable">
                                            <label class="form-check-label" for="recEnable">Включить запись</label>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Формат записи</div>
                                            </div>
                                            <select class="form-control" id="audioFormat">
                                                <option value="0">PCM (.wav)</option>
                                                <option value="1">OPUS (.opus)</option>
                                            </select>
                                            <div class="input-group-append"><span class="input-group-text"></span></div>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Битрейт</div>
                                            </div>
                                            <select class="form-control" id="opusBitrate">
                                                <option value="16000">16 кб/с</option>
                                                <option value="24000">24 кб/с</option>
                                                <option value="32000">32 кб/с</option>
                                                <option value="64000">64 кб/с</option>
                                                <option value="96000">96 кб/с</option>
                                                <option value="128000">128 кб/с</option>
                                                <option value="256000">256 кб/с</option>
                                            </select>
                                            <div class="input-group-append"><span class="input-group-text"></span></div>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Режим</div>
                                            </div>
                                            <select class="form-control" id="opusSignal">
                                                <option value="3001">Голос</option>
                                                <option value="3002">Музыка</option>
                                            </select>
                                            <div class="input-group-append"><span class="input-group-text"></span></div>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Диапазон</div>
                                            </div>
                                            <select class="form-control" id="opusBandwidth">
                                                <option value="1101">до 4 kHz</option>
                                                <option value="1102">до 6 kHz</option>
                                                <option value="1103">до 8 kHz</option>
                                                <option value="1104">до 12 kHz</option>
                                                <option value="1105">до 20 kHz</option>
                                            </select>
                                            <div class="input-group-append"><span class="input-group-text"></span></div>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Хранить</div>
                                            </div>
                                            <input class="form-control" type="number" id="storeDays">
                                            <div class="input-group-append">
                                                <span class="input-group-text">дней</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Мин.своб.место</div>
                                            </div>
                                            <input class="form-control" type="number" id="minDiskFreeMb">
                                            <div class="input-group-append">
                                                <span class="input-group-text">Мб</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" style="min-width: 150px;">Свободно</div>
                                            </div>
                                            <input class="form-control" type="number" id="diskFreeMb" readonly>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Мб</span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>    
                            <div class="col-7 card">
                                <div class="card-header">
                                    Каналы
                                </div>
                                <div class="card-body">
                                    <div class="row" id="recChannels"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingAlarms">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseAlarms" aria-expanded="true" aria-controls="collapseAlarms">
                                Сообщения
                            </button>
                            
                        </h5>
                    </div>
                    <div id="collapseAlarms" class="collapse" aria-labelledby="headingAlarms" data-parent="#accordionSetts">
                        <div class="card-body" id="alarms-view">
                            
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingNet">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseNet" aria-expanded="true" aria-controls="collapseNet">
                                Сеть
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNet" class="collapse" aria-labelledby="headingNet" data-parent="#accordionSetts">
                        <div class="card-body">
                            <div class="input-group mx-3 mt-3 mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">Вариант настройки</span>
                                </div>
                                <select class="form-control" id="net-dhcp" value="<?php echo $netDHCP; ?>">
                                    <option value="dhcp" <?php echo $netDHCP=='dhcp' ? 'selected' : ''?>>DHCP</option>
                                    <option value="static" <?php echo $netDHCP=='static' ? 'selected' : ''?>>Статическая</option>
                                </select>
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mx-3 mt-3 mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">IP-адрес</span>
                                </div>
                                <input class="form-control" id="net-ip-addr" type="text" value="<?php echo $netIP; ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mx-3 mt-3 mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">Маска подсети</span>
                                </div>
                                <input class="form-control" id="net-mask" type="text" value="<?php echo $netMask; ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mx-3 mt-3 mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">Шлюз</span>
                                </div>
                                <input class="form-control" id="net-gateway" type="text" value="<?php echo $netGate; ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="form-check mx-3 mt-3">
                                <input class="form-check-input" type="checkbox" value="" id="net-speed-limit" <?php echo $netLimit; ?>>
                                <label class="form-check-label" for="net-speed-limit">
                                  Ограничить скорость интерфейса до 100Мбит/с
                                </label>
                            </div>
                            <div class="mt-3 ml-5">
                                <button id="btn-save-net" class="btn btn-outline-success">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingCUS">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseCUS" aria-expanded="true" aria-controls="collapseCUS">
                                ЦУС
                            </button>
                        </h5>
                    </div>
                    <div id="collapseCUS" class="collapse" aria-labelledby="headingCUS" data-parent="#accordionSetts">
                        <div class="card-body">
                            <div class="form-check mx-3">
                                <input class="form-check-input" type="checkbox" value="" id="cus-enable" <?php echo $cus_enable; ?>>
                                <label class="form-check-label" for="net-speed-limit">
                                  Поддержка ЦУС
                                </label>
                            </div>
                            <div class="input-group mx-3 mt-3 mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">Мультикаст адрес</span>
                                </div>
                                <input class="form-control" id="udp-multicast" type="text" value="<?php echo $udp_multicast; ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="mt-3 ml-5">
                                <button id="btn-save-netjs" class="btn btn-outline-success">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="card shadow">
                    <div class="card-header" id="headingTime">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseTime" aria-expanded="true" aria-controls="collapseTime">
                                Время
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTime" class="collapse" aria-labelledby="headingTime" data-parent="#accordionSetts">
                        <div class="card-body row">
                            <div class="input-group mx-3 mt-3 mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">Текущее время на сервере</span>
                                </div>
                                <input class="form-control" id="cur-datetime" type="datetime" value="0">
                                <div class="input-group-append">
                                    <button  class="btn btn-outline-dark" type='button' id="btn-sync-time">Синхронизировать с этим компьютером</button >
                                </div>
                            </div>
                            <div class="input-group mx-3 mb-1 mt-0">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">NTP-сервер</span>
                                </div>
                                <input class="form-control" id="ntp-server" placeholder="NTP-сервер">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mx-3 mb-1 mt-0">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">Часовой пояс</span>
                                </div>
                                <input class="form-control" id="timezone" placeholder="Часовой пояс" value="<?php echo exec("timedatectl | grep zone"); ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mx-3 mb-1 mt-0">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">RTC</span>
                                </div>
                                <select class="form-control" id="rtc-dev">
                                    <option value="/dev/rtc0">RTC0</option>
                                    <option value="/dev/rtc1">RTC1</option>
                                </select>
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
<!--                            <div class="input-group mx-3 mb-3 mt-0">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="width: 250px;">... установить</span>
                                </div>
                                <select class="form-control" id="timezone-set">
                                    <?php
                                    echo get_timezones();
                                    ?>
                                </select>
                            </div>-->
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingBackup">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseBackup" aria-expanded="true" aria-controls="collapseBackup">
                                Резервная копия
                            </button>
                        </h5>
                    </div>
                    <div id="collapseBackup" class="collapse" aria-labelledby="headingBackup" data-parent="#accordionSetts">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg col-md-12 pl-3 pr-2">
                                    <div class="card shadow">
                                        <div class="card-body">
                                            <p>Нажмите "Загрузить" для создания и сохранения файла резервной копии. Файл будет скачан на Ваш компьютер.</p>
                                            <p><a class="btn btn-outline-primary" href='command.php?query={"command": "get_backup"}'>Загрузить</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg col-md-12 pl-2 pr-3">
                                    <div class="card shadow">
                                        <div class="card-body">
                                            <p>Выберите сохраненный файл резервной копии и нажмите "Отправить". Произойдет восстановление всех настроек из резервной копии.</p>
                                            <form action="restore.php" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                                                <input name="file" type="file" />
                                                <button class="btn btn-outline-success">Отправить</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingUser">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseUser" aria-expanded="true" aria-controls="collapseUser">
                                Пользователь
                            </button>
                        </h5>
                    </div>
                    
                    <div id="collapseUser" class="collapse" aria-labelledby="headingUser" data-parent="#accordionSetts">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="labelUser" style="width:200px;">Имя пользователя</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="Имя пользователя" aria-describedby="labelUser" id="user" value="<?php echo $user; ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="labelPass" style="width:200px;">Пароль</span>
                                </div>
                                <input type="password" class="form-control password-edit" placeholder="Пароль" aria-label="Пароль" aria-describedby="labelPass" id="password" value="<?php echo $pass; ?>">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="labelPass2" style="width:200px;">Повтор пароля</span>
                                </div>
                                <input type="password" class="form-control password-edit" placeholder="Повтор пароля" aria-label="Повтор пароля" aria-describedby="labelPass2" id="password2">
                                <div class="invalid-feedback">Пароли не совпадают</div>
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <button class="btn btn-outline-success" id="btn-user">Изменить</button>
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingEvts">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseEvts" aria-expanded="true" aria-controls="collapseEvts">
                                Лог событий
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEvts" class="collapse" aria-labelledby="headingEvts" data-parent="#accordionSetts">
                        <div class="card-body row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="labelEvtsPeriod" style="width:300px;">Период хранения событий (дней)</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Период хранения событий (дней)" aria-label="Период хранения событий (дней)" aria-describedby="labelEvtsPeriod" id="events-period">
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card shadow">
                    <div class="card-header" id="headingVer">
                        <h5 class="mb-0">
                            <button class="btn" type="button" data-toggle="collapse" data-target="#collapseVer" aria-expanded="true" aria-controls="collapseVer">
                                Версия ПО
                            </button>
                        </h5>
                    </div>
                    <div id="collapseVer" class="collapse" aria-labelledby="headingVer" data-parent="#accordionSetts">
                        <div class="card-body ">
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1" style="width: 150px;">Служба</span>
                                </div>
                                <label type="text" class="form-control"><?php echo exec("ago18bks -v"); ?></label>
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1" style="width: 150px;">Плеер</span>
                                </div>
                                <label type="text" class="form-control"><?php echo exec("ago18player -v"); ?></label>
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1" style="width: 150px;">Web-интерфейс</span>
                                </div>
                                <label type="text" class="form-control"><?php echo $web_ver; ?></label>
                                <div class="input-group-append"><span class="input-group-text"></span></div>
                            </div>
                            <form class="mt-3" action="upgrade.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Обновление ПО</span>
                                    </div>
                                    <input class="form-control" name="file" type="file" required />
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-success">Обновить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
        
        <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/alertify.min.js"></script>
        <script type="text/javascript" src="js/alert.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/constants.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/rest.js?1" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/get_setts.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/MyView.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/AlarmsView.js?1" crossorigin="anonymous"></script>
        <script>
            alertify.defaults.glossary.ok = 'OK';
            alertify.defaults.glossary.cancel = 'Отмена';    
            alertify.defaults.transition = "fade";
            alertify.defaults.theme.ok = "btn btn-outline-primary";
            alertify.defaults.theme.cancel = "btn btn-outline-danger"
            
            var alarmsView = new AlarmsView($('#alarms-view'));
            var rest = new Rest();
            
            function createChannelSetts(type, ch, placeholder){
                var form = $('<form>' ,{
                    class: 'row'
                });
                
                var input_group = $('<div>',{
                    class: "input-group mb-2"
                });
                form.append(input_group);
                
                var inp_gr_prep = $('<div>',{
                    class: "input-group-prepend"
                });
                input_group.append(inp_gr_prep);
                
                var name = "";//type == "in" ? "Вход " : "Выход ";
                switch(type){
                    case "in":
                        name = "Вход ";
                        break;
                    case "out":
                        name = "Выход ";
                        break;
                    case "tgt":
                        name = "Target ";
                        break;
                }
                name += (ch+1);
                var input_group_text = $('<div>',{
                    class: "input-group-text",
                    text: name
                });
                inp_gr_prep.append(input_group_text);
                
                var input = $('<input>',{
                    type:"text", 
                    class: "form-control", 
                    id: type+"-name-"+ch, 
                    placeholder: placeholder
                });
                input_group.append(input);
                
                input_group.append($(`<div class="input-group-append"><span class="input-group-text"></span></div>`));
                
//                if(type == "out"){
//                    var targ_group = $('<div>',{
//                        class: "input-group mb-2 col-6"
//                    });
//                    form.append(targ_group);
//
//                    var targ_gr_prep = $('<div>',{
//                        class: "input-group-prepend"
//                    });
//                    targ_group.append(targ_gr_prep);
//
//                    targ_gr_prep.append($('<div>',{
//                        class: "input-group-text",
//                        text: "Target"
//                    }));
//
//                    var targ_input = $('<input>',{
//                        type:"text", 
//                        class: "form-control", 
//                        id: "target-"+ch, 
//                        placeholder: "Target"
//                    });
//                    targ_group.append(targ_input);
//                }
//                console.dir(form);
                
                return form;
            }
            
            function setDaemonState(answer){
                let lastState = $("#daemon-state").html();
                
                $("#daemon-state").html(answer);
                $("#daemon-state").removeClass("alert-secondary");
                $("#daemon-state").removeClass("alert-success");
                $("#daemon-state").removeClass("alert-danger");
                $("#daemon-state").removeClass("alert-warning");
                if(answer === "Работает"){
                    if(lastState !== "Остановка..."){
                        $("#btn-daemon-start").hide();
                        $("#btn-daemon-stop").show();
                        $("#daemon-state").addClass("alert-success");
                    }
                }else if(answer === "Не работает"){
                    if(lastState !== "Запуск..."){
                        $("#btn-daemon-start").show();
                        $("#btn-daemon-stop").hide();
                        $("#daemon-state").addClass("alert-danger");
                    }
                }else if(answer === "Запуск..."){
                    $(".btn-daemon").hide();
                    $("#daemon-state").addClass("alert-warning");
                }else if(answer === "Остановка..."){
                    $(".btn-daemon").hide();
                    $("#daemon-state").addClass("alert-warning");
                }else{
                    $(".btn-daemon").hide();
                    $("#daemon-state").addClass("alert-secondary");
                }
                
                if($("#daemon-state").hasClass("alert-secondary")){
                    $("#btn-reboot").hide();
                }else{
                    $("#btn-reboot").show();
                }
                
                $("#daemon-state").show();
            }
            
            function update_daemon(){
//                console.log("Update daemon");
                $.ajax({
                    url: "daemon.php",
                    type: "GET",
                    data: {command: "status"},
                    success: function(answer){
                        setDaemonState(answer);
                        $("#server-alert").hide();
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.info("jqXHR", jqXHR);
                        console.info("status", textStatus);
                        console.info("error", errorThrown);
//                        $("#server-alert").show();

                        setDaemonState("Нет связи");
                    }
                });
//                $.get("daemon.php", {command: "status"}, function(answer){
//                    setDaemonState(answer);
//                });
            }
            
            function loadSettings(){
                get_settings(function(setts){
                    console.dir(setts);
                    console.log(setts.time.time);

                    var date = new Date(setts.time.time*1000);
                    setInterval(function(){
                        settings.time.time++;
                        date = new Date(settings.time.time*1000);
                        $('#cur-datetime').val(date.toLocaleString());
                        update_daemon();
                    },1000);

                    $('#objectName').val(setts.bks.name);
                    $('#serverAddr').val(setts.bks.server_addr);
                    $('#cusIn').val(setts.bks.cus_in);
                    if('id_offset' in setts.bks){
                        $('#id-offset').val(setts.bks.id_offset);
                    }else{
                        $('#id-offset').val(8);
                    }
                    if('bru_offset' in setts.bks){
                        $('#bru-offset').val(setts.bks.bru_offset);
                    }else{
                        $('#bru-offset').val(25);
                    }
                    
                    if('ping_period' in setts.bks){
                        $('#ping-period').val(setts.bks.ping_period);
                    }else{
                        $('#ping-period').val(1);
                    }
                    
                    if('ping_timeout' in setts.bks){
                        $('#ping-timeout').val(setts.bks.ping_timeout);
                    }else{
                        $('#ping-timeout').val(10);
                    }

                    $('#ins-cnt').val(setts.ins_cnt);
                    $('#outs-cnt').val(setts.outs_cnt);

                    $('#recEnable').prop("checked", setts.rec.enable);
                    $('#audioFormat').val(setts.rec.format);
                    $('#opusBitrate').val(setts.rec.bitrate);
                    $('#opusSignal').val(setts.rec.signal);
                    if(setts.rec.store_days != null)
                        $('#storeDays').val(setts.rec.store_days);
                    if(setts.rec.min_free != null)
                        $('#minDiskFreeMb').val(setts.rec.min_free);
                    else
                        $('#minDiskFreeMb').val(300);
                    if(setts.rec.free != null)
                        $('#diskFreeMb').val(Math.round(setts.rec.free/1024.0/1024.0));
                    $('#opusBandwidth').val(setts.rec.bandwidth);
                    $('#cur-datetime').val(date.toLocaleString());
                    $('#ntp-server').val(setts.time.ntp);
                    console.info('TIME', setts.time);
                    $('#rtc-dev').val(setts.time.rtc);
                    $('#events-period').val(setts.events.log_period);

                    $('#inChannels').html('');
                    $('#outChannels').html('');
                    $('#targets').html('');
                    $('#recChannels').html('');
                    for(var i=0; i<insCnt; i++){
                        $('#inChannels').append(createChannelSetts("in", i, "Название"));
                        
                        var recDiv = $('<div>', {
                            class: 'col-3 text-truncate',
                            title: setts.ins[i].name,
                            'data-toggle': 'tooltip'
                        });
                        var recCheck = $('<input>', {
                            type: 'checkbox',
                            class: 'recCheck',
                            inCh: i
                        });
                        recCheck.prop('checked', setts.ins[i].rec ?? true);//
                        recDiv.append(recCheck);
                        
                        var recChLabel = $('<small>', {
                            class: 'ml-1' 
                        });
                        if(setts.ins[i].name != '')
                            recChLabel.html(setts.ins[i].name);
                        else
                            recChLabel.html(`${i+1}`);
                        recDiv.append(recChLabel);
                        //recDiv.append($(`<small>${setts.ins[i].name}</small>`));

                        $('#recChannels').append(recDiv);
                        
                        if(i < setts.ins.length){
                            $('#in-name-'+i).val(setts.ins[i].name);
                        }else{
                            $('#in-name-'+i).val("");
                        }
                    }
                    for(var i=0; i<outsCnt; i++){
                        $('#outChannels').append(createChannelSetts("out", i, "Название"));
                        $('#targets').append(createChannelSetts("tgt", i, "target"));
                        if(i < setts.outs.length){
                            $('#out-name-'+i).val(setts.outs[i].name);
                            if(setts.outs[i].target != null){
                                $('#tgt-name-'+i).val(setts.outs[i].target);
                            }
                        }else{
                            $('#out-name-'+i).val("");
                            $('#tgt-name-'+i).val("");
                        }
                    }

                    alarmsView.init();
                });
            }
            
            function checkInsOuts(){
            
            }
            
            $('#btn-save').click(async function(){
                settings.bks.name = $('#objectName').val();
                settings.bks.server_addr = parseInt($('#serverAddr').val());
                settings.bks.cus_in = parseInt($('#cusIn').val());
                settings.bks.id_offset = parseInt($('#id-offset').val());
                settings.bks.bru_offset = parseInt($('#bru-offset').val());
                settings.bks.ping_period = parseInt($('#ping-period').val());
                settings.bks.ping_timeout = parseInt($('#ping-timeout').val());
                settings.rec.enable = $("#recEnable").is(":checked");
                settings.rec.format = parseInt($('#audioFormat').val());
                settings.rec.bitrate = parseInt($('#opusBitrate').val());
                settings.rec.signal = parseInt($('#opusSignal').val());
                settings.rec.bandwidth = parseInt($('#opusBandwidth').val());
                settings.rec.store_days = parseInt($('#storeDays').val());
                settings.rec.min_free = parseInt($('#minDiskFreeMb').val());
                settings.events.log_period = parseInt($('#events-period').val());
                
                insCnt = parseInt($('#ins-cnt').val());
                outsCnt = parseInt($('#outs-cnt').val())<=32 ? parseInt($('#outs-cnt').val()) : 32;
                settings.ins_cnt = insCnt;
                settings.outs_cnt = outsCnt;
                
                settings.ins = [];
                for(var i=0; i<insCnt; i++){
                    var rec = $(`.recCheck[inCh=${i}]`).is(':checked');
                    settings.ins[i] = {
                        ch: i,
                        name: $('#in-name-'+i).val() ?? '',
                        rec: rec
                    };
                }
                settings.outs = [];
                for(var i=0; i<outsCnt; i++){
                    settings.outs[i] = {
                        ch: i,
                        name: $('#out-name-'+i).val() ?? '',
                        target: $('#tgt-name-'+i).val() ?? ''
                    };
                }
                
                settings.time = {
                    ntp: $("#ntp-server").val(),
                    rtc: $('#rtc-dev').val()
                };
                console.info("SETTINGS", settings);
                
                var strObj = JSON.stringify(settings, null, 2);
                console.log(strObj);
                
                var res = await (rest.set_settings(settings));
                if(res.result == "success"){
                    alertify.success("Настройки сохранены");
                    loadSettings();
//                    get_settings();
                    //showAlert("Сохранение", "Настройки сохранены");
                }else{
                    alertify.error("Ошибка при сохранении: "+res.message);
                }
            });

            loadSettings();

            update_daemon();
            
            $("#btn-daemon-start").click(function(){
                setDaemonState("Запуск...");
                $.get("daemon.php", {command: "start"}, function(answer){
                    console.log("Start");
                    console.log(answer);
                    update_daemon();
                    location.reload();
                });
            });
            
            $("#btn-daemon-stop").click(function(){
                setDaemonState("Остановка...");
                $.get("daemon.php", {command: "stop"}, function(answer){
                    console.log("Stop");
                    console.log(answer);
                    update_daemon();
                });
            });
            
            $("#btn-reboot").click(function(){
                alertify.defaults.glossary.ok = 'Далее';
                alertify.defaults.glossary.cancel = 'Отмена';    
                alertify.confirm("Перезагрузка", "Будет перезагружена ОС сервера. Нажмите 'Далее' для продолжения.", function(){
                    $.get("daemon.php", {command: "reboot"}, function(answer){
                        console.log("Reboot");
                        console.log(answer);
                        setDaemonState("Перезагрузка...");
                    });
                }, function(){});
            });
            
            $("#btn-sync-time").click(async function(){
                var new_date = Math.round(Date.now()/1000);
                console.log(new_date);
                
                var obj = await (rest.set_time(new_date));
                if(obj.result == "success"){
                    alertify.success("Время синхронизировано");
                    
                    get_settings(function(setts){
                        console.log(settings.time.time);
                        let date = new Date(settings.time.time*1000);
                        $('#cur-datetime').val(date.toLocaleString());
                    });
                }
                else
                    alertify.error(obj.message);
            });
            
            $(".password-edit").on("input", function(){
                if($("#password").val() !== $("#password2").val()){
                    $("#password2").addClass("is-invalid");
                }else{
                    $("#password2").removeClass("is-invalid");
                }
            });
            
            $('#btn-user').click(function(){
                let user = $('#user').val();
                let pass = $('#password').val();
                let pass2 = $('#password2').val();
                if(pass === pass2){
                    console.log(`${user}:${pass}`);
                    
                    $.post("user.php", {user: user, pass: pass}, function(answer){
                        console.log(answer);
                    });
                }
            });
            
            $('#btn-save-net').click(function(){
                var dhcp = $("#net-dhcp").val();
                var ip = $("#net-ip-addr").val();
                var mask = $("#net-mask").val();
                var gate = $("#net-gateway").val();
                var speedLimit = $("#net-speed-limit").is(':checked');
                
                
                console.log(`${dhcp} ${ip} ${gate} ${speedLimit}`);
                $.post("net_setts.php", {
                    dhcp: dhcp, 
                    ip: ip, 
                    mask: mask, 
                    gate: gate,
                    limit: speedLimit
                }, function(answer){
                    console.log(answer);
                    alertify.success("Настройки сети сохранены.\r\nНеобходимо перезагрузить сервер.");
                });
            });
            
            $('#cus-enable').change(function(){
                var enbl = this.checked;
            });
            
            $('#btn-save-netjs').click(function(){
                var enbl = $("#cus-enable").is(':checked');
                var udp_multicast = $('#udp-multicast').val();
                console.info('cus_enable', enbl);
                console.info('udp_multicast', udp_multicast);
                
                $.post('network_json_set.php', {
                    cus_enable: enbl ? 1 : 0,
                    udp_multicast: udp_multicast
                }, function(answer){
                    console.log(answer);
                    alertify.success("Настройки сохранены.\r\nНеобходимо перезагрузить сервер.");
                });
            });
            
            async function get_boards(){
                var boardsList = await (new Rest()).get_boards();
//                console.info("get_boards()", boardsList);
                
                var boards = '';
                for(var i=0; i<boardsList.boards.length; i++){
                    if(boards != ''){
                        boards += ', ';
                    }
                    
                    boards += `${boardsList.boards[i].name}(${boardsList.boards[i].brdN},${boardsList.boards[i].addr})`;
                }
                
                $('#boardsList').val(boards);
            }
            
            $('#clearBoards').click(function(){
                alertify.confirm('Обновить','Список оборудования будет очищен и выполнен новый опрос состава оборудования. Продолжить?',
                    async function(){
                        $('#boardsList').val('Обновление...');
                        var res = await (rest.clear_boards());
                        if(res.result == "success"){
                            alertify.success("Список оборудования очищен. Список обновится через 3 сек.");
                            setTimeout(function(){
//                                location.reload();
                                get_boards();
                            }, 3000);
                        }else{
                            alertify.error("Ошибка: "+res.message);
                        }
                    },
                    function(){}
                );
                
            });
            
//            $( document ).ready(function() {
//                $('[data-toggle="tooltip"]').tooltip();
//                console.log("READY");
//            });

            $( window ).on('load', function() {
                $('[data-toggle="tooltip"]').tooltip();
                
                get_boards();
                
                console.log("READY");
            });
            
        </script>
    </body>
</html>
