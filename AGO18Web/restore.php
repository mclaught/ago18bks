<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="themes/theme.css" />
    </head>
    <body class="bg-light">
        <div class="container">
            <?php
//            print_r($_FILES);
            
                $json = file_get_contents($_FILES["file"]["tmp_name"]);
                if($json == FALSE){
                    ?>
                        <div class="alert alert-danger" role="alert">
                          Ошибка загрузки файла!
                        </div>                    
                    <?php
                    exit();
                }

                $sock = socket_create(AF_UNIX, SOCK_STREAM, 0);
                if($sock == FALSE){
                    ?>
                        <div class="alert alert-danger" role="alert">
                          Нет связи с сервером!
                        </div>                    
                    <?php
                    exit();
                }
                
                if(!socket_connect($sock, "/var/www/html/AGO18Web/files/agobks")){
                    ?>
                        <div class="alert alert-danger" role="alert">
                          Нет связи с сервером!
                        </div>                    
                    <?php
                    exit();
                }

                if(socket_send($sock, $json, strlen($json), 0) == FALSE){
                    ?>
                        <div class="alert alert-danger" role="alert">
                          Нет связи с сервером!
                        </div>                    
                    <?php
                    exit();
                }
            ?>
            
            <div class="alert alert-success" role="alert">
              Настройки загружены
            </div>                    
        </div> 
    </body>
</html>

