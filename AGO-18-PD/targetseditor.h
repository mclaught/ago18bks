#ifndef TARGETSEDITOR_H
#define TARGETSEDITOR_H

#include <QDialog>
#include <QListWidgetItem>

namespace Ui {
class TargetsEditor;
}

class TargetsEditor : public QDialog
{
    Q_OBJECT

public:
    QString path;

    explicit TargetsEditor(QString path, int targets_cnt, QWidget *parent = nullptr);
    ~TargetsEditor();

    void load();
    void save();
private slots:
    void on_targets_itemClicked(QListWidgetItem *item);

private:
    Ui::TargetsEditor *ui;
    int targets_cnt;
};

#endif // TARGETSEDITOR_H
