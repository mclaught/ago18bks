/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WSServer.cpp
 * Author: user
 * 
 * Created on 24 ноября 2020 г., 15:02
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <exception>
#include <netinet/in.h>
#include <fcntl.h>
#include <cstring>
#include <map>
#include <iostream>
#include <memory>

#include "WSServer.h"
#include "cus/log.h"


WSServer::WSServer(int port) : MyThread() {
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1)
        throw runtime_error(strerror(errno));
    
    int arg = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &arg, sizeof(int)) == -1){
        log_error("WSServer socket: %s", strerror(errno));
        exit(1);
    }
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &arg, sizeof(int)) == -1){
        log_error("WSServer setsockopt: %s", strerror(errno));
        exit(1);
    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        log_error("WSServer bind: %s", strerror(errno));
        exit(1);
    }
    
    if(listen(sock, 10) == -1){
        log_error("WSServer listen: %s", strerror(errno));
        exit(1);
    }
}

WSServer::WSServer(const WSServer& orig) : MyThread() {
}

WSServer::~WSServer() {
}

void WSServer::exec(){
    while(!terminated){
        fd_set rd_set;
        FD_ZERO(&rd_set);
        FD_SET(sock, &rd_set);
        int max_sock = sock;
        
        for(auto cli : get_clients()){
            FD_SET(cli.first, &rd_set);
            max_sock = max(cli.first, max_sock);
        }
        
        timeval tv = {1,0};
        
        int cnt = select(max_sock+1, &rd_set, nullptr, nullptr, &tv);
        if(cnt == -1)
            cerr << red << "WS select: " << strerror(errno) << clrst << endl;
        else if(cnt>0){
            if(FD_ISSET(sock, &rd_set)){
                sockaddr_in cli_addr;
                memset(&cli_addr, 0, sizeof(sockaddr_in));
                cli_addr.sin_family = AF_INET;
                socklen_t len = sizeof(sockaddr_in);
                int cli_sock = accept(sock, reinterpret_cast<sockaddr*>(&cli_addr), &len);
                if(cli_sock == -1)
                    cerr << red << "WS accept: " << strerror(errno) << clrst << endl;
                else{
                    shared_ptr<MyWSClient> client = make_shared<MyWSClient>(cli_sock);

                    {
                        unique_lock<shared_mutex> lock(clients_mutex);
                        clients[cli_sock] = client;
                    }
                } 
            }
            
            for(auto cli : get_clients()){
                if(FD_ISSET(cli.first, &rd_set)){
                    char buf[2048];
                    int rd = recv(cli.first, buf, 2048, 0);
                    if(rd == -1){
                        cerr << red << "WS recv: " << strerror(errno) << clrst << endl;
                    }else if(rd == 0){
                        cli.second->enabled = false;
                        remove_client(cli.first);
                    }else{
                        if(cli.second->process(buf, rd)){
                            onclient(cli.second);
                        }
                    }
                }
            }
        }
    }
    
    cout << "WS server stoped" << endl;
}

map<int, shared_ptr<MyWSClient>> WSServer::get_clients(){
    shared_lock<shared_mutex> lock(clients_mutex);
    return clients;
}

void WSServer::remove_client(int s){
    unique_lock<shared_mutex> lock(clients_mutex);
    clients.erase(s);
}

void WSServer::send(string type, string str){
    for(auto p : get_clients()){
        if(p.second->type == type){
#ifdef WS_DEBUG
            cout << "ws send: " << str << endl;
#endif
            p.second->write(const_cast<char*>(str.c_str()), str.length());
        }
    }
}
