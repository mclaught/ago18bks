#include "filesoundsource.h"

#include <QtConcurrent>

FileSoundSource::FileSoundSource(QObject *parent) : BaseSoundSource(parent)
{

}

bool FileSoundSource::start(QString fileName, int pos)
{
    BaseSoundSource::start();

    pcm = QSharedPointer<MyPCM>(new MyPCM(fileName, AUDIO_FRAME_MS, true));
    if(!pcm->error){
        if(pos){
            pcm->file_pos = pos;
        }

        int prebuf = FIFO_SIZE - AUDIO_SAMPLES;
        int16_t buf[FIFO_SIZE];
        int rd = pcm->read(reinterpret_cast<char*>(buf),
                           static_cast<int>(sizeof(int16_t)*static_cast<size_t>(prebuf)));
        rd /= sizeof(int16_t);
        fifo_add(buf, rd);


        active = true;
        readThread = QtConcurrent::run([this](){
            while(active && !pcm->eof()){
                int _fifo_cnt = fifo_cnt();
                if((FIFO_SIZE - _fifo_cnt) >= AUDIO_SAMPLES*10){//
                    int16_t buf[AUDIO_SAMPLES*10];
                    int rd = pcm->read(reinterpret_cast<char*>(buf), sizeof(int16_t)*AUDIO_SAMPLES*9);
                    rd /= sizeof(int16_t);
                    fifo_add(buf, rd);

                    emit progress(pcm->file_size, pcm->file_pos);
                }else{
                    QThread::usleep(1000);
                }
            }
            emit complete();
        });
    }else{
        errorStr = pcm->error_str;
        qDebug() << pcm->error_str;
        return false;
    }

    return true;
}

void FileSoundSource::stop()
{
    active = false;
    readThread.waitForFinished();
}

int FileSoundSource::getPos()
{
    return pcm->file_pos;
}

void FileSoundSource::setPos(int pos)
{
    if(pos < pcm->file_size)
        pcm->file_pos = pos;
}
