#!/bin/bash

apt update && apt install gpg

gpg --import private.asc
gpg --list-keys
