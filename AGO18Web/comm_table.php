<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/common.css?2">
        
<!--        <style>
            input[type=checkbox]:checked{
                color: #007bff
            }
        </style>-->
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        $nb_add = "<button class='btn btn-outline-light my-2 my-sm-0' id='btn-save'>Сохранить</button>";
        include "navbar.php";
        include "alert.html";
        ?>
        
        <div class="container">
            <br>
            <h1><img src="img/cross.svg" width="32" height="32"><span>&nbsp;&nbsp;&nbsp;</span>Таблица коммутации</h1>
            <br>

            <form>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">
                            Направление сортировки
                        </span>
                    </div>
                    <select class="form-control" id="prio_code">
                        <option value="0"> По возрастанию</option>
                        <option value="1"> По убыванию</option>
                    </select>
                    <div class="input-group-append"><span class="input-group-text"></span></div>
                </div>
            </form>
            
            <table class="table">
                <thead class="thead-dark">
                    <tr id="commcols">
                    </tr>
                </thead>
                <tbody id="commtbl">
                </tbody>
            </table>
        </div>
        
        <script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/alert.js" crossorigin="anonymous"></script>
        <script src="js/constants.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/rest.js"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script>
            async function update(){

                var objAnswer = await (new Rest()).get_comm_table();
                console.dir(objAnswer);
                var prio_code = objAnswer.comm_table.prio_code;
                var array = objAnswer.comm_table.sw_table;
                
                $('#prio_code option').attr('selected', false);
                $('#prio_code option[value='+prio_code+']').attr('selected', true);
                
                console.dir(settings);
                
                for(var i=0; i<insCnt; i++){
                    var ch = i<array.length ? 
                        array[i] :
                        {
                            in: i,
                            out: 0,
                            prio: 0
                        };
//                    console.dir(ch);

                    var inp_n = ch.in;
                    var outs = ch.out;
                    var prio = ch.prio;
                    let in_name = settings.ins[i]!==null && settings.ins[i].name !== null && settings.ins[i].name !== "" ? settings.ins[i].name : (i+1);
                    //console.log(inp_n);

                    var row = $('<tr>');
                    $("#commtbl").append(row);

                    var row_h = settings.ins==null || settings.ins[inp_n]==null || settings.ins[inp_n].name=="" ? $('<th>',{
                        scope: 'row',
                        'class': 'table-dark',
                        style: 'text-align: center; vertical-align: middle; font-size:12pt;',
                        text: inp_n+1

                    }) : $('<th>',{
                        scope: 'row',
                        'class': 'table-dark',
                        style: 'text-align: center; vertical-align: middle; font-size:8pt;',
                        text: settings.ins[inp_n].name

                    });
                    row.append(row_h);

                    var prio_col = $('<td>',{
                        class: 'table-light'
                    });
                    row.append(prio_col);

                    var prio_spin = $('<input>',{
                        id: "prio-"+inp_n,
                        type: 'number',
                        style: 'max-width: 40px; font-size: 10pt;',
                        min: 0,
                        max: insCnt-1,
                        step: 1,
                        value: prio
                    });
                    prio_col.append(prio_spin);

                    for(var j=0; j<outsCnt; j++){
                        let out_name = settings.outs[j] && settings.outs[j].name && settings.outs[j].name !== "" ? settings.outs[j].name : (j+1);
                        var active = (outs & (1<<j)) > 0;

                        var cell = $('<td>',{
                            class: 'table-light',
                            style: 'text-align: center; vertical-align: middle;'//max-width:60px; min-width:60px; 
                        });
                        row.append(cell);
                        
                        let title = in_name + ' - ' + out_name;

                        var check = $('<input>',{
                            class: 'comm-check', 
                            type: 'checkbox', 
                            //style: 'width:10pt; height:10pt;',
                            id: "check-"+inp_n+"-"+j, 
                            checked: active, 
                            inp: inp_n, 
                            out: j,
                            title: title
                        });
                        cell.append(check);
                    }
                }

                $("#commcols").append($('<th>',{
                    scope: 'col',
                    text: 'Вх/Вых'
                }));
                $("#commcols").append($('<th>',{
                    scope: 'col',
                    text: 'Прио'
                }));

                for(var i=0; i<outsCnt; i++){
                    $("#commcols").append(
                        !settings.outs || !settings.outs[i] || settings.outs[i].name==="" ?    
                            $('<th>',{
                                scope: 'col',
                                style: 'max-width:40px; min-width:40px; text-align: center; vertical-align: middle; font-size:12pt;',
                                text: i+1
                            }) : 
                            $('<th>',{
                                scope: 'col',
                                style: 'max-width:60px; min-width:60px; text-align: center; vertical-align: middle; font-size:8pt;',
                                text: settings.outs[i].name
                            })
                        );
                }
            }
            
            $('#btn-save').click(async function(){
                console.log('Save');
                
                let prio_code = parseInt($('#prio_code').val());
                
                var comm_table = {
                    prio_code: prio_code,
                    sw_table: []
                };
                
                for(var i=0; i<insCnt; i++){
                    var o = 0;
                    for(var j=0; j<outsCnt; j++){
                        var check_name = "#check-"+i+"-"+j;
                        if($(check_name).is(':checked'))
                            o |= (1<<j);
                    }
                    var prio = parseInt($("#prio-"+i).val(), 10);
                    
                    comm_table.sw_table[i] = {
                        in: i,
                        out: o,
                        prio: prio
                    };
                }
                
                var obj = await (new Rest()).set_comm_table(comm_table);
                console.dir(obj);
                console.log(obj.result);
                if(obj.result == "success"){
                    showAlert("Сохранение", "Сохранено успешно");
                }else{
                    showAlert("Сохранение", "Ошибка сохранения");
                }
            });
            
            get_settings(function(){
                update();
            });
            
        </script>
    </body>
</html>

