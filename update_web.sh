#!/bin/sh

git clone https://gitlab.com/mclaught/ago18bks.git
if [ ! -d "ago18bks" ]
then
    echo "\n\nSources download failed"
    exit 1;
fi

cp -r -f ago18bks/AGO18Web /var/www/html
chown -R www-data:www-data /var/www/html/AGO18Web

rm -r ago18bks
