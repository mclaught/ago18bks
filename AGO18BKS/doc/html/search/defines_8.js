var searchData=
[
  ['magenta_975',['magenta',['../log_8h.html#aec378d18c614bc816f159b45a6e11f14',1,'log.h']]],
  ['max_5fchannels_976',['MAX_CHANNELS',['../proto_8h.html#ac69ee46f4a51ed14f0d68628c2dec71d',1,'proto.h']]],
  ['max_5fchannels_5fused_977',['MAX_CHANNELS_USED',['../proto_8h.html#aeef82747f434493b1243381912228c87',1,'proto.h']]],
  ['max_5fevents_978',['MAX_EVENTS',['../_config_server_8h.html#ae42954bb8545d24e3e9dcde5920c9a0b',1,'ConfigServer.h']]],
  ['max_5ffile_5fsize_979',['MAX_FILE_SIZE',['../_audio_saver_8h.html#a649ad5274d988d17a275a3cead056746',1,'AudioSaver.h']]],
  ['max_5finp_5fchannels_980',['MAX_INP_CHANNELS',['../proto_8h.html#ade356c3e94a0ac6bc51930d9899bb797',1,'proto.h']]],
  ['max_5fpacket_5fsize_981',['MAX_PACKET_SIZE',['../_my_u_d_p_server_8h.html#a879456c3b8e2853f7044d764e9c180d4',1,'MyUDPServer.h']]]
];
