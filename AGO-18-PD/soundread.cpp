#include "soundread.h"

#include <QAudioDeviceInfo>
#include <QAudioFormat>
#include <QDebug>
#include <math.h>

SoundReader *reader;
QList<QAudioDeviceInfo> gDevList;

SoundReader::SoundReader(QString devName, QObject *parent) : BaseSoundSource(parent)
{
    deviceName = devName;
    auto m_Inputdevice = defDevice();
    if(!devName.isEmpty()){
        for(auto dev : devList()){
            if(dev.deviceName()==deviceName){
                m_Inputdevice = dev;
                break;
            }
        }
    }
    qDebug() << m_Inputdevice.deviceName();

    QAudioFormat m_format = usedFormat();
    qDebug() << m_format;

    m_audioInput = new QAudioInput(m_Inputdevice, m_format, this);
    m_audioInput->setNotifyInterval(1000);
    connect(m_audioInput, &QAudioInput::notify, this, &SoundReader::onNotify);
}

SoundReader::~SoundReader()
{
//    m_audioInput->deleteLater();
}

void SoundReader::onNotify(){
    emit onDebug(QString("B:%1/%2").arg(fifo_cnt()).arg(m_audioInput->bufferSize()));
}

QList<QAudioDeviceInfo> SoundReader::devList(){
    if(gDevList.isEmpty()){
        gDevList = QAudioDeviceInfo::availableDevices(QAudio::Mode::AudioInput);
    }
    return gDevList;
}

QAudioDeviceInfo SoundReader::defDevice(){
    auto inputDev = QAudioDeviceInfo::defaultInputDevice();
    if(inputDev.deviceName().isEmpty()){
        for(auto dev : QAudioDeviceInfo::availableDevices(QAudio::Mode::AudioInput)){
            if(dev.deviceName()=="default"){
                inputDev = dev;
                break;
            }
        }
    }
    qDebug() << "Def input device: " << inputDev.deviceName();
    return inputDev;
}

QAudioFormat SoundReader::usedFormat(){
    auto m_Inputdevice = defDevice();

    QAudioFormat m_format;
    m_format.setSampleRate(48000);
    m_format.setChannelCount(1); //set channels to mono
    m_format.setSampleSize(16); //set sample sze to 16 bit
    m_format.setSampleType(QAudioFormat::SignedInt ); //Sample type as usigned integer sample
    m_format.setByteOrder(QAudioFormat::LittleEndian); //Byte order
    m_format.setCodec("audio/pcm"); //set codec as simple audio/pcm

    QAudioDeviceInfo infoIn(m_Inputdevice);
    if (!infoIn.isFormatSupported(m_format))
    {
        //Default format not supported - trying to use nearest
        m_format = infoIn.nearestFormat(m_format);
    }

    return m_format;
}

void SoundReader::start(bool resmpl)
{
    resample_enable = resmpl;
    m_input = m_audioInput->start();
    qDebug() << "Period=" << m_audioInput->periodSize();
    connect(m_input, &QIODevice::readyRead, this, &SoundReader::readData);
    timer.start();
    monitorTm.start();
    rd_sum = 0;

    BaseSoundSource::start();
}

void SoundReader::stop()
{
    m_audioInput->stop();
}

bool SoundReader::isVolumeSupported()
{
    if(m_audioInput){
        double old = m_audioInput->volume();
        m_audioInput->setVolume(0.5);
        bool supp = m_audioInput->volume()==0.5;
        m_audioInput->setVolume(old);
        return supp;
    }else{
        return false;
    }
}

int SoundReader::volume()
{
    if(m_audioInput){
        return static_cast<int>(round(m_audioInput->volume()*100));
    }else{
        return 0.0;
    }
}

void SoundReader::setVolume(int vol)
{
    if(m_audioInput){
        m_audioInput->setVolume(static_cast<double>(vol)/100.0);
    }
}

QString SoundReader::getDeviceName()
{
    return deviceName;
}

void SoundReader::readData(){
    int16_t buf[10240];

    auto rd = m_input->read(reinterpret_cast<char*>(buf), sizeof(int16_t)*10240);
    if(rd){
        int dst_cnt = resampleSize(rd/sizeof(int16_t));
        int16_t dst_data[10240];
        resample(buf, rd/sizeof(int16_t), dst_data, dst_cnt);
        fifo_add(dst_data, dst_cnt);
        rd_sum += rd;

        if(monitorTm.elapsed() >= 100){
            int16_t min = 32767;
            int16_t max = -32767;
            for(int i=0; i<static_cast<int>(rd/sizeof(int16_t)); i++){
                if(buf[i] < min)
                    min = buf[i];
                if(buf[i] > max)
                    max = buf[i];
            }
            uint16_t lvl = max - min;
            emit on_level(lvl);

            monitorTm.restart();
        }
    }
}

int SoundReader::resampleSize(int cnt){
    return cnt;

//    if(resample_enable && timer.elapsed() > 1000 && in_sum){
//        double f_new_cnt = cnt*static_cast<double>(out_sum)/static_cast<double>(in_sum);
//        int i_new_cnt = static_cast<int>(f_new_cnt);
//        sum_new_cnt += f_new_cnt;
//        sum_new_cnt -= i_new_cnt;
//        if(sum_new_cnt >= 1.0){
//            i_new_cnt++;
//            sum_new_cnt -= 1.0;
//        }
//        return i_new_cnt;
//    }else
//        return cnt;
}

void SoundReader::resample(int16_t* src_data, int src_cnt, int16_t* dst_data, int16_t dst_cnt){
    if(src_cnt == dst_cnt){
        memcpy(dst_data, src_data, src_cnt*sizeof(int16_t));
        return;
    }

    for(int d=0; d<dst_cnt; d++){
        int s = d*src_cnt/dst_cnt;
        if(s >= src_cnt)
            s = src_cnt-1;
        dst_data[d] = src_data[s];
    }
}
