/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PDUDPServer.h
 * Author: user
 *
 * Created on 16 декабря 2019 г., 14:53
 */

#ifndef PDUDPSERVER_H
#define PDUDPSERVER_H

#include "UDPSoundSender.h"
#include <thread>
#include <map>

using namespace std;

class PDUDPServer {
public:
    PDUDPServer();
    PDUDPServer(const PDUDPServer& orig);
    virtual ~PDUDPServer();
    
    void start();
    void stop();
private:
    typedef map<string, PUDPSoundSender> senders_t;
    
    thread thrd;
    bool active;
    int sock;
    string iface;
    uint16_t port;
    senders_t senders;
    chrono::time_point<chrono::steady_clock> last_sent_tm;
    
    void run();
    
    void error(string mes, bool _exit);
    
    friend void pd_udp_thread_func(PDUDPServer *__this);
};

extern PDUDPServer pd_udp_server;

#endif /* PDUDPSERVER_H */

