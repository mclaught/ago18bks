#ifndef TARGETSCHECKWIDGET_H
#define TARGETSCHECKWIDGET_H

#include "wordwrapcheck.h"

#include <QWidget>

namespace Ui {
class TargetsCheckWidget;
}

class TargetsCheckWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TargetsCheckWidget(QWidget *parent = nullptr);
    ~TargetsCheckWidget();

    void setTargets(QStringList targets, int outs_cnt);
    uint32_t getOuts();
    void setOuts(uint32_t outs);
    void setChecksEnabled(uint32_t outs);
    void setPlayState(bool play, uint32_t outs);
private:
    Ui::TargetsCheckWidget *ui;
    QStringList names;
    QVector<WordWrapCheck*> dstCheck;

signals:
    void onChecked(uint64_t outs);
};

#endif // TARGETSCHECKWIDGET_H
