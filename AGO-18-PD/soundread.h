#ifndef SOUNDREAD_H
#define SOUNDREAD_H

#include "basesoundsource.h"

#include <QAudioInput>
#include <QElapsedTimer>
#include <QObject>
#include <mutex>

class SoundReader : public BaseSoundSource
{
    Q_OBJECT
public:
    explicit SoundReader( QString devName, QObject *parent = nullptr);
    ~SoundReader();

    void start(bool resmpl);
    virtual void stop()override;

    bool isVolumeSupported();
    int volume();
    void setVolume(int vol);

    QString getDeviceName();

    static QAudioDeviceInfo defDevice();
    static QAudioFormat usedFormat();
    static QList<QAudioDeviceInfo> devList();
private:
    QString deviceName;
    QAudioInput *m_audioInput = nullptr;
    QIODevice *m_input;
    QElapsedTimer timer;
    QElapsedTimer monitorTm;
    int rd_sum = 0;
    double sum_new_cnt = 0.0;
    bool resample_enable = true;

    void resample(int16_t *src_data, int src_cnt, int16_t *dst_data, int16_t dst_cnt);
    int resampleSize(int cnt);

signals:
    void on_level(uint16_t lvl);
    void onDebug(QString str);
private slots:
    void readData();
    void onNotify();
};

extern SoundReader *reader;

#endif // SOUNDREAD_H
