<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>БКС. Панель управления</title>
        
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="themes/theme.css" />
        <link rel="stylesheet" href="css/alertify.css" />
        <link rel="stylesheet" href="css/themes/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css?1">
    </head>
    <body class="bg-light">
        <?php
        $cur_page=basename(__FILE__);
        $nb_add = "<button class='btn btn-outline-light my-2 my-sm-0' id='btn-update'>Опрос БРУ</button>";
        include "navbar.php";
        ?>
        
        <div class="modal fade" id="lineModal" tabindex="-1" role="dialog" aria-labelledby="lineModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="lineModalLabel">Линия</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="lines-modal-body">
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="btn-dlg-save">Сохранить</button>
              </div>
            </div>
          </div>
        </div>        
        
        <div class="modal fade" id="measureModal" tabindex="-1" role="dialog" aria-labelledby="measureModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="measureModalLabel">Измерение завершено</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="measure-modal-body">
                  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-dlg-measure">OK</button>
              </div>
            </div>
          </div>
        </div>        
        
        <div class="container">
            <br>
            <h1>
<!--                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bug" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4.355.522a.5.5 0 0 1 .623.333l.291.956A4.979 4.979 0 0 1 8 1c1.007 0 1.946.298 2.731.811l.29-.956a.5.5 0 1 1 .957.29l-.41 1.352A4.985 4.985 0 0 1 13 6h.5a.5.5 0 0 0 .5-.5V5a.5.5 0 0 1 1 0v.5A1.5 1.5 0 0 1 13.5 7H13v1h1.5a.5.5 0 0 1 0 1H13v1h.5a1.5 1.5 0 0 1 1.5 1.5v.5a.5.5 0 1 1-1 0v-.5a.5.5 0 0 0-.5-.5H13a5 5 0 0 1-10 0h-.5a.5.5 0 0 0-.5.5v.5a.5.5 0 1 1-1 0v-.5A1.5 1.5 0 0 1 2.5 10H3V9H1.5a.5.5 0 0 1 0-1H3V7h-.5A1.5 1.5 0 0 1 1 5.5V5a.5.5 0 0 1 1 0v.5a.5.5 0 0 0 .5.5H3c0-1.364.547-2.601 1.432-3.503l-.41-1.352a.5.5 0 0 1 .333-.623zM4 7v4a4 4 0 0 0 3.5 3.97V7H4zm4.5 0v7.97A4 4 0 0 0 12 11V7H8.5zM12 6H4a3.99 3.99 0 0 1 1.333-2.982A3.983 3.983 0 0 1 8 2c1.025 0 1.959.385 2.666 1.018A3.989 3.989 0 0 1 12 6z"/>
                </svg>-->
                <img src="img/bru.svg" width="32" height="32">
                <span>&nbsp;&nbsp;&nbsp;</span>БРУ
            </h1>
            <div class="alert alert-danger" role="alert" id="connection-alert">
                Отсутствует соединение с сервером!
            </div>
            <div class="alert alert-danger" role="alert" id="no-bru">
                Данные БРУ отсутствуют!
            </div>
            <br>
            <div id="bru"></div>
        </div>
        
        <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/alert.js"></script>
        <script type="text/javascript" src="js/alertify.js"></script>
        <script type="text/javascript" src="js/constants.js"></script>
        <script type="text/javascript" src="js/rest.js"></script>
        <script type="text/javascript" src="js/get_setts.js"></script>
        <script type="text/javascript" src="js/MyView.js"></script>
        <script type="text/javascript" src="js/LineDialog.js"></script>
        <script type="text/javascript" src="js/BRUCfgView.js"></script>
        <script>
            let bruCfgView = new BRUCfgView($('#bru'));
            get_settings(function(setts){
                bruCfgView.init();
            });
            
        </script>
    </body>
</html>
