#ifndef SCHEDITEMDIALOG_H
#define SCHEDITEMDIALOG_H

#include <QDialog>
#include "scheduller.h"

namespace Ui {
class SchedItemDialog;
}

class SchedItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SchedItemDialog(PFileInfo fileInfo, QWidget *parent = nullptr);
    ~SchedItemDialog();

    void save();
private:
    Ui::SchedItemDialog *ui;
    PFileInfo fileInfo;
};

#endif // SCHEDITEMDIALOG_H
