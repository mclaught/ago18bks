/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AudioSaver.h
 * Author: user
 *
 * Created on 26 ноября 2019 г., 19:03
 */

#ifndef AUDIOSAVER_H
#define AUDIOSAVER_H

#include <mutex>
#include <thread>
#include <fstream>
#include <atomic>
#include <queue>
#include <condition_variable>

#include "proto.h"
#include "wavsaver.h"
#include "OGGWriter.h"

#define PCM_FIFO_SIZE       1000
#define PCM_FIFO_PACKS      20
#define MAX_FILE_SIZE       (1024*1024)
#define REC_STEP_SEC        (60*60)
#define FORMAT              opus
#define REC_PATH            "/var/www/html/AGO18Web/rec"

using namespace std;

#pragma pack(push, 1)
typedef struct{
    uint32_t pattern;
    uint8_t  ver;
    uint8_t hdr_type;
    uint64_t gran_pos;
    uint32_t bitstream_sn;
    uint32_t pg_sn;
    uint32_t checksum;
    uint8_t segm_cnt;
    uint8_t segments[];
}ogg_hdr_t;
#pragma pack(pop)

typedef struct{
    int16_t buf[PCM_FIFO_PACKS*AUDIO_SAMPLES];
    int s;
}fifo_item_t;

class AudioSaver {
public:
    AudioSaver(int _channel);
    AudioSaver(const AudioSaver& orig);
    virtual ~AudioSaver();

    void add_frame(int16_t* pcm);
private:
    int channel;
    thread* svr_thread;
//    mutex fifo_mutex;
    atomic_bool active = true;
    int cur_file_size = 0;
    string file_name = "";
    time_t last_time_step = 0;
    BaseWriter* file = nullptr;
    atomic_uint64_t last_write_tm = 0;
    unsigned long last_log_tm = 0;
    char descr[100];
    
    fifo_item_t pcm_fifo[PCM_FIFO_SIZE];
    int pcm_fifo_in = 0;
    int pcm_fifo_out = 0;
    atomic_int pcm_fifo_cnt = 0;
    mutex fifo_mutex;

    condition_variable sync;
    mutex sync_mutex;
    
    void save_pcm(int16_t* pcm, int cnt);
    string new_file();
    void flush();
    void clearRecStorage();
    
    friend void saver_func(AudioSaver* __this);
};

#endif /* AUDIOSAVER_H */

