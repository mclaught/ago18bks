class AlarmsView extends MyView{
    
    init(){
        this.alarms = null;
        this.update();
        super.init();
    }
    
    async update(){
        let obj = await (new Rest()).get_alarms();
        console.log(obj);
//        let obj = JSON.parse();
        if(obj.result === 'success'){
            this.alarms = obj.alarms;
        }
        this.setState();
    }
    
    create_alarm_view(i, alrm){
        console.log(alrm);
        
        return $('<div>', {class: 'input-group'}).append(// row
                $('<div>', {class: 'input-group-prepend'}).append(// col-1
                    $('<span>', {class: 'input-group-text', style: "width: 40px;"}).html(i+1)
                ),
                $('<textarea>', {type: "text", cols: '100', rows: '1', name: 'Сообщение', 
                    class: "form-control msg-edit", alarm_id: i
                }).html(alrm.message),// col
                $('<div>', {class: "input-group-append"}).append(
                    $('<select>', {class: "form-control input-group-text type-select", alarm_id: i}).append(// col-2
                        $('<option>', {value: 0}).prop('selected', alrm.type===0).html('Сообщение'),
                        $('<option>', {value: 1}).prop('selected', alrm.type===1).html('Предупреждение'),
                        $('<option>', {value: 2}).prop('selected', alrm.type===2).html('Авария')
                    )
                ),
            );
//        return $('<div>', {class: 'row mb-3 pr-3'}).append(
//            $('<div>', {class: 'input-group col'}).append(// row
//                $('<div>', {class: 'input-group-prepend'}).append(// col-1
//                    $('<span>', {class: 'input-group-text'}).html(i+1)
//                ),
//                $('<textarea>', {type: "text", cols: '100', rows: '1', name: 'Сообщение', class: "form-control msg-edit", alarm_id: i}).html(alrm.message),// col
//            ),
//            $('<select>', {class: "form-control type-select col-2", alarm_id: i}).append(// col-2
//                $('<option>', {value: 0}).prop('selected', alrm.type===0).html('Сообщение'),
//                $('<option>', {value: 1}).prop('selected', alrm.type===1).html('Предупреждение'),
//                $('<option>', {value: 2}).prop('selected', alrm.type===2).html('Авария')
//            )
//        );
    }
    
    build(){
        let items = [];
        console.log(this.alarms);
        if(this.alarms !== null){
            for(let i=0; i<this.alarms.length; i++){
                items.push(this.create_alarm_view(i, this.alarms[i]));
            }
        }
        
        let div = $('<div>').append(
            $('<div>', {class: "alert alert-primary", role: "alert"}).append(
                $('<h5>', {class: ""}).html('Используются специальные теги:'),    
                $('<dl>', { class: 'row small'}).append(
                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;ERROR&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('исчисление параметра начинается с 0, используется при выводе номера внутренней ошибки'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;BRU_UM&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('исчисление параметра начинается с 1, используется при выводе номеров усилителей в сообщениях БРУ'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;CHANNEL&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('исчисление параметра начинается с 1, используется при выводе номеров линий, усилителей'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;BOARD&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('исчисление параметра начинается с 1, используется при выводе номеров усилителей'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;SENDER&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('исчисление параметра начинается с 1, используется при выводе номеров плат-отправителей'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;MEASURE_VAL&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('используются для подстановки измеренного значения сопротивления линии'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;MEASURE_REF_VAL&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('используются для подстановки опорного значения сопротивления линии'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;MEASURE_DEVIATION&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('используется для подстановки величины отклонения измеренного значения от опорного'),

                    $('<dt>', {class: "mb-0 col-3"}).html('&lt;BRD_TYPE&gt;'),
                    $('<dd>', {class: "mb-1 col-9"}).html('используется для подстановки типа платы в сообщениях периодического опроса')
                )
            ),    
            items,
            $('<button>', {class: 'btn btn-outline-success btn-save-alarms mt-3'}).html('Сохранить')
        );

        let _this = this;
        div.find('.btn-save-alarms').click(function(){
            for(let i=0; i<_this.alarms.length; i++){
                let msg = div.find('.msg-edit[alarm_id='+i+']').val();
                let tp = div.find('.type-select[alarm_id='+i+']').val();
                console.log(msg + ' - '+tp);
                
                _this.alarms[i].message = msg;
                _this.alarms[i].type = parseInt(tp);
            }
            
            console.log(_this.alarms);
            
            (new Rest()).set_alarms(_this.alarms);
        });
        
        return div;
    }
}